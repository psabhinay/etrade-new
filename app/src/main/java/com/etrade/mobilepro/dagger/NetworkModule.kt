package com.etrade.mobilepro.dagger

import android.content.Context
import com.etrade.eo.rest.RetrofitBuilderFacade
import com.etrade.eo.rest.authentication.AuthenticationInterceptor
import com.etrade.eo.rest.authentication.Authenticator
import com.etrade.eo.rest.retrofit.SingleKeyRequestConverter
import com.etrade.eo.webviewcookiejar.SharedCookieManager
import com.etrade.mobile.power.json.gson.serializers.BigDecimalDeserializer
import com.etrade.mobilepro.BuildConfig
import com.etrade.mobilepro.apiguard.ApiGuardInterceptor
import com.etrade.mobilepro.appconfig.api.repo.AppConfigRepo
import com.etrade.mobilepro.applicationinfo.ApplicationInfoInterceptor
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.backends.mgs.adapters.getBaseMetaAdapterFactory
import com.etrade.mobilepro.backends.mgs.adapters.getBaseReferenceAdapterFactory
import com.etrade.mobilepro.backends.mgs.adapters.getBaseScreenViewAdapterFactory
import com.etrade.mobilepro.backends.neo.MessageDto
import com.etrade.mobilepro.balance.data.rest.BalancesService
import com.etrade.mobilepro.bloombergtv.data.BloombergTvService
import com.etrade.mobilepro.caching.okhttp.OkHttpCacheInterceptor
import com.etrade.mobilepro.chartiq.api.rest.ChartIqApiClient
import com.etrade.mobilepro.chat.data.messages.service.ChatMessagesApiService
import com.etrade.mobilepro.chat.data.setup.service.ChatSetupApiService
import com.etrade.mobilepro.common.di.Api
import com.etrade.mobilepro.common.di.AppsFlyer
import com.etrade.mobilepro.common.di.MobileEtrade
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.common.di.Mod
import com.etrade.mobilepro.common.di.MoshiConverter
import com.etrade.mobilepro.common.di.SimpleXmlConverter
import com.etrade.mobilepro.common.di.Unauthenticated
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dagger.qualifiers.GuardedApi
import com.etrade.mobilepro.dynamicui.DynamicMetaType
import com.etrade.mobilepro.dynamicui.DynamicReferenceType
import com.etrade.mobilepro.dynamicui.DynamicScreenRpcService
import com.etrade.mobilepro.dynamicui.DynamicViewType
import com.etrade.mobilepro.dynamicui.addSubtypes
import com.etrade.mobilepro.earnings.repo.DefaultEarningsRepoImpl
import com.etrade.mobilepro.earnings.repo.EarningsRepo
import com.etrade.mobilepro.feedback.data.FeedbackService
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.portfoliosnapshot.PortfolioSnapshotService
import com.etrade.mobilepro.quote.createQuoteResponseAdapter
import com.etrade.mobilepro.session.ConverterWithSessionValidationFactory
import com.etrade.mobilepro.session.SessionTerminationStateProvider
import com.etrade.mobilepro.session.SessionTerminationStateProviderImpl
import com.etrade.mobilepro.session.SessionValidationInterceptor
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.session.api.UserConsumerKeyInterface
import com.etrade.mobilepro.tier.CustomerTierService
import com.etrade.mobilepro.trade.data.advanced.AdvancedAccountDto
import com.etrade.mobilepro.trade.data.advanced.AdvancedExecInstructionDto
import com.etrade.mobilepro.trade.data.advanced.AdvancedLegInfoDto
import com.etrade.mobilepro.trade.data.advanced.AdvancedOrderDetailDto
import com.etrade.mobilepro.trade.data.advanced.AdvancedOrderDto
import com.etrade.mobilepro.trade.data.advanced.AdvancedOrderServerMessageDto
import com.etrade.mobilepro.trade.data.advanced.AdvancedOrderSvcResponseDto
import com.etrade.mobilepro.trade.data.advanced.AdvancedProductIdDto
import com.etrade.mobilepro.util.CookieSweepInterceptor
import com.etrade.mobilepro.util.FilteredCookieHandler
import com.etrade.mobilepro.util.ObjectArrayMismatchAdapter
import com.etrade.mobilepro.util.json.AnnotatedTypeConverter
import com.etrade.mobilepro.util.json.BigDecimalAdapter
import com.etrade.mobilepro.util.json.BigIntegerJsonAdapter
import com.etrade.mobilepro.util.json.FallbackEnum
import com.etrade.mobilepro.util.json.SingleToArrayAdapter
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.CookieJar
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.File
import java.math.BigDecimal
import java.net.CookieHandler
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val CACHE_SIZE = 10L * 1024L * 1024L
const val CACHE_SUB_DIR_NAME = "offlineCache"

@Suppress("TooManyFunctions")
@Module
class NetworkModule {

    @Provides
    internal fun provideCookieHandler(sharedCookieManager: SharedCookieManager): CookieHandler {
        return FilteredCookieHandler(sharedCookieManager)
    }

    @Provides
    @Singleton
    internal fun providesAuthenticator(user: User): Authenticator = user

    @Provides
    @Singleton
    internal fun provideCookieJar(cookieHandler: CookieHandler): CookieJar = JavaNetCookieJar(cookieHandler)

    @Provides
    @Singleton
    @Unauthenticated
    internal fun provideUnauthenticatedOkHttpClient(
        appConfigRepo: AppConfigRepo,
        applicationContext: Context,
        applicationInfo: ApplicationInfo,
    ): OkHttpClient {
        val timeouts = appConfigRepo.getAppConfig().timeouts
        return OkHttpClient.Builder()
            .cache(applicationContext.createCache())
            .connectTimeout(timeouts.connection.seconds, TimeUnit.SECONDS)
            .readTimeout(timeouts.read.seconds, TimeUnit.SECONDS)
            .writeTimeout(timeouts.write.seconds, TimeUnit.SECONDS)
            .addInterceptor(ApplicationInfoInterceptor(applicationInfo))
            .addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = if (BuildConfig.DEBUG) { HttpLoggingInterceptor.Level.BODY } else { HttpLoggingInterceptor.Level.NONE }
                }
            )
            .addNetworkInterceptor(OkHttpCacheInterceptor())
            .build()
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient(
        @Unauthenticated okHttpClient: OkHttpClient,
        cookieJar: CookieJar,
        authenticator: Authenticator,
        moshi: Moshi,
        sessionTerminationStateProvider: SessionTerminationStateProvider,
    ): OkHttpClient =
        okHttpClient.newBuilder()
            .cookieJar(cookieJar)
            .addInterceptor(AuthenticationInterceptor(authenticator, null))
            .addNetworkInterceptor(SessionValidationInterceptor(sessionTerminationStateProvider, moshi))
            .addNetworkInterceptor(
                CookieSweepInterceptor(
                    cookiesToSweep = setOf("FORMCRED"),
                    excludePaths = setOf("LoginUserSSO", "MobileLogin")
                )
            )
            .build()

    @Provides
    @Singleton
    @GuardedApi
    internal fun provideGuardedOkHttp(okHttpClient: OkHttpClient): OkHttpClient {
        return okHttpClient.newBuilder().apply {
            interceptors().add(0, ApiGuardInterceptor())
        }.build()
    }

    @Provides
    @MoshiConverter
    internal fun provideMoshiConverterFactory(
        moshi: Moshi,
        sessionTerminationStateProvider: SessionTerminationStateProvider
    ): Converter.Factory {
        return ConverterWithSessionValidationFactory(
            MoshiConverterFactory.create(moshi),
            sessionTerminationStateProvider
        )
    }

    @Suppress("DEPRECATION") // SimpleXML is deprecated
    @Provides
    @SimpleXmlConverter
    internal fun provideSimpleXmlConverterFactory(sessionTerminationStateProvider: SessionTerminationStateProvider): Converter.Factory {
        return ConverterWithSessionValidationFactory(
            retrofit2.converter.simplexml.SimpleXmlConverterFactory.create(),
            sessionTerminationStateProvider
        )
    }

    @Provides
    @Singleton
    internal fun providesRetrofitBuilder(
        okHttpClient: OkHttpClient,
        gson: Gson,
        @MoshiConverter moshiConverterFactory: Converter.Factory,
        @SimpleXmlConverter simpleXmlConverterFactory: Converter.Factory
    ): Retrofit.Builder {
        val annotatedTypeConverter = AnnotatedTypeConverter(
            GsonConverterFactory.create(gson),
            simpleXmlConverterFactory,
            ScalarsConverterFactory.create(),
            moshiConverterFactory
        )
        val singleKeyRequestConverter = SingleKeyRequestConverter(annotatedTypeConverter, gson)

        return Retrofit.Builder()
            .addConverterFactory(singleKeyRequestConverter)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(okHttpClient)
            .validateEagerly(true)
    }

    @Provides
    @Singleton
    @Unauthenticated
    internal fun provideUnauthenticatedRetrofitBuilder(
        retrofitBuilder: Retrofit.Builder,
        @Unauthenticated okHttpClient: OkHttpClient,
    ): Retrofit.Builder = retrofitBuilder.client(okHttpClient)

    @Provides
    @Singleton
    @MobileTrade
    internal fun providesMobileTradeRetrofit(retrofitBuilder: Retrofit.Builder, @MobileTrade baseUrl: String): Retrofit {
        return retrofitBuilder
            .baseUrl(baseUrl)
            .build()
    }

    @Provides
    @Singleton
    @Mod
    internal fun provideMarketOnDemandRetrofit(retrofitBuilder: Retrofit.Builder, @Mod baseUrl: String): Retrofit {
        return retrofitBuilder
            .baseUrl(baseUrl)
            .build()
    }

    @Provides
    @Singleton
    @Api
    internal fun provideApiRetrofit(retrofitBuilder: Retrofit.Builder, @Api baseUrl: String): Retrofit {
        return retrofitBuilder
            .baseUrl(baseUrl)
            .build()
    }

    @Provides
    @Singleton
    @AppsFlyer
    internal fun provideAppsFlyerRetrofit(retrofitBuilder: Retrofit.Builder, @AppsFlyer baseUrl: String): Retrofit {
        return retrofitBuilder
            .baseUrl(baseUrl)
            .build()
    }

    @Provides
    @Singleton
    @Web
    internal fun provideWebRetrofit(retrofitBuilder: Retrofit.Builder, @Web baseUrl: String): Retrofit {
        return retrofitBuilder
            .baseUrl(baseUrl)
            .build()
    }

    @Provides
    @Singleton
    @MobileEtrade
    internal fun provideMobileEtradeRetrofit(retrofitBuilder: Retrofit.Builder, @MobileEtrade baseUrl: String): Retrofit {
        return retrofitBuilder
            .baseUrl(baseUrl)
            .build()
    }

    /** Used for OH built components mainly **/
    @Provides
    @Singleton
    internal fun providesRetrofitBuilderFacade(
        gson: Gson,
        moshi: Moshi,
        okHttpClient: OkHttpClient
    ): RetrofitBuilderFacade = RetrofitBuilderFacade(okHttpClient, okHttpClient, gson, moshi)

    @Provides
    @Singleton
    @GuardedApi
    internal fun provideGuardedRetrofitBuilderFacade(
        gson: Gson,
        moshi: Moshi,
        @GuardedApi okHttpClient: OkHttpClient
    ): RetrofitBuilderFacade = RetrofitBuilderFacade(okHttpClient, okHttpClient, gson, moshi)

    @Provides
    @Singleton
    internal fun providesBalancesService(@MobileTrade retrofit: Retrofit): BalancesService = retrofit.create(BalancesService::class.java)

    @Provides
    @Singleton
    internal fun providesDynamicScreenRpcService(@MobileTrade retrofit: Retrofit): DynamicScreenRpcService =
        retrofit.create(DynamicScreenRpcService::class.java)

    @Provides
    @Singleton
    internal fun providesChatSetupApiService(@MobileTrade retrofit: Retrofit): ChatSetupApiService =
        retrofit.create(ChatSetupApiService::class.java)

    @Provides
    @Singleton
    internal fun providesChatMessagesService(@MobileTrade retrofit: Retrofit): ChatMessagesApiService =
        retrofit.create(ChatMessagesApiService::class.java)

    @Provides
    @Singleton
    internal fun providesMoversService(@MobileTrade retrofit: Retrofit): MoverService = retrofit.create(MoverService::class.java)

    @Provides
    @Singleton
    internal fun providesPortfolioSnapshotService(@MobileTrade retrofit: Retrofit): PortfolioSnapshotService =
        retrofit.create(PortfolioSnapshotService::class.java)

    @Provides
    @Singleton
    internal fun providesChartIqService(@MobileTrade retrofit: Retrofit): ChartIqApiClient = retrofit.create(ChartIqApiClient::class.java)

    @Provides
    @Singleton
    internal fun providesFeedbackService(@MobileTrade retrofit: Retrofit): FeedbackService = retrofit.create(FeedbackService::class.java)

    @Provides
    @Singleton
    internal fun providesBloombergtvService(@MobileTrade retrofit: Retrofit): BloombergTvService = retrofit.create(BloombergTvService::class.java)

    @Provides
    @Singleton
    internal fun providesCustomerTierService(@MobileTrade retrofit: Retrofit): CustomerTierService = retrofit.create(CustomerTierService::class.java)

    @Provides
    @Singleton
    internal fun provideEarningsRepo(@Api apiRetrofit: Retrofit, @MobileTrade mobileTradeRetrofit: Retrofit, user: UserConsumerKeyInterface): EarningsRepo =
        DefaultEarningsRepoImpl(apiRetrofit, mobileTradeRetrofit, user::getConsumerKey)

    @Provides
    @Singleton
    internal fun providesGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.registerTypeAdapter(BigDecimal::class.java, BigDecimalDeserializer())
        gsonBuilder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    @SuppressWarnings("LongMethod")
    internal fun providesMoshi(): Moshi {
        return Moshi.Builder()
            .add(ObjectArrayMismatchAdapter.newFactory(AdvancedAccountDto::class.java))
            .add(ObjectArrayMismatchAdapter.newFactory(AdvancedExecInstructionDto::class.java))
            .add(ObjectArrayMismatchAdapter.newFactory(AdvancedLegInfoDto::class.java))
            .add(ObjectArrayMismatchAdapter.newFactory(AdvancedOrderDetailDto::class.java))
            .add(ObjectArrayMismatchAdapter.newFactory(AdvancedOrderDto::class.java))
            .add(ObjectArrayMismatchAdapter.newFactory(AdvancedAccountDto::class.java))
            .add(ObjectArrayMismatchAdapter.newFactory(AdvancedOrderServerMessageDto::class.java))
            .add(ObjectArrayMismatchAdapter.newFactory(AdvancedOrderSvcResponseDto::class.java))
            .add(ObjectArrayMismatchAdapter.newFactory(AdvancedProductIdDto::class.java))
            .add(ObjectArrayMismatchAdapter.newFactory(MessageDto::class.java))
            .add(FallbackEnum.ADAPTER_FACTORY)
            .add(
                addSubtypes(
                    getBaseScreenViewAdapterFactory(),
                    DynamicViewType.values().toList()
                )
            )
            .add(
                addSubtypes(
                    getBaseReferenceAdapterFactory(),
                    DynamicReferenceType.values().toList()
                )
            )
            .add(
                addSubtypes(
                    getBaseMetaAdapterFactory(),
                    DynamicMetaType.values().toList()
                )
            )
            .add(createQuoteResponseAdapter())
            .add(BigDecimalAdapter)
            .add(BigIntegerJsonAdapter())
            .add(SingleToArrayAdapter.SingleToArrayAdapterFactory())
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    @Provides
    @Singleton
    internal fun provideSessionTerminationStateProvider(): SessionTerminationStateProvider = SessionTerminationStateProviderImpl()
}

/**
 * Creates 10 MB cache.
 */
private fun Context.createCache(): Cache = Cache(File(cacheDir, CACHE_SUB_DIR_NAME), CACHE_SIZE)
