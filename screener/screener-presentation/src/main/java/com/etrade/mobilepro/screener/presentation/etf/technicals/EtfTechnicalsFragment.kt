package com.etrade.mobilepro.screener.presentation.etf.technicals

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.screener.api.MovingAverage
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.etf.EtfTechnicalsFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.FragmentEtfTechnicalsBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

private const val AVERAGE_PERCENT_TAG = "average percent tag"

class EtfTechnicalsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<EtfTechnicalsFilter, EtfTechnicalsViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {
    override val layoutRes: Int = R.layout.fragment_etf_technicals
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.ETF
    private val binding by viewBinding(FragmentEtfTechnicalsBinding::bind)
    override val viewModel by viewModels<EtfTechnicalsViewModel> { viewModelFactory }
    private lateinit var averageDropDownSelector: DropDownMultiSelector<MovingAverage>
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<EtfTechnicalsFilter, EtfTechnicalsViewModel.ViewState, Boolean>>
        get() = mapOf(
            binding.bottomTwentyPercent to EtfTechnicalsVolume.BOTTOM_20,
            binding.belowMidTwentyPercent to EtfTechnicalsVolume.BELOW_MID_20,
            binding.midTwentyPercent to EtfTechnicalsVolume.MID_20,
            binding.aboveMidTwentyPercent to EtfTechnicalsVolume.ABOVE_MID_20,
            binding.topTwentyPercent to EtfTechnicalsVolume.TOP_20
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
    }

    private fun setupFragment() {
        averageDropDownSelector = DefaultDropDownMultiSelector(
            parentFragmentManager,
            DropDownMultiSelector.Settings(
                tag = AVERAGE_PERCENT_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.average),
                itemsData = viewModel.averagePercentData,
                selectListener = { _, item ->
                    viewModel.averageSelected(item.value)
                }
            )
        )
        setupMovingAveragesClickListeners()
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                binding.apply {
                    average.text = if (it.filter.average.isAllItemsElement) {
                        getString(R.string.average)
                    } else {
                        it.filter.average.title
                    }
                    average.isEnabled = it.isAverageEnabled()
                    priceMovingAbove.isChecked = it.filter.priceMovingAbove
                    priceMovingBelow.isChecked = it.filter.priceMovingBelow
                    bottomTwentyPercent.isChecked = it.filter.bottom20
                    belowMidTwentyPercent.isChecked = it.filter.belowMid20
                    midTwentyPercent.isChecked = it.filter.mid20
                    aboveMidTwentyPercent.isChecked = it.filter.aboveMid20
                    topTwentyPercent.isChecked = it.filter.top20
                }
            }
        )
    }

    private fun setupMovingAveragesClickListeners() {
        binding.apply {
            average.setOnClickListener {
                averageDropDownSelector.open()
            }
            priceMovingBelow.checkedStateListener = { isChecked, byUser ->
                if (byUser) {
                    viewModel.priceMovingToggled(PriceMoving.BELOW, isChecked)
                }
            }
            priceMovingAbove.checkedStateListener = { isChecked, byUser ->
                if (byUser) {
                    viewModel.priceMovingToggled(PriceMoving.ABOVE, isChecked)
                }
            }
        }
    }
}
