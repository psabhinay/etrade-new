package com.etrade.mobilepro.dagger

import android.os.Handler
import android.os.Looper
import com.etrade.mobilepro.common.di.ApplicationScope
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import java.util.concurrent.Executor
import javax.inject.Singleton

@Module
class ConcurrencyModule {

    @Singleton
    @Provides
    @ApplicationScope
    fun provideApplicationCoroutineScope(): CoroutineScope =
        CoroutineScope(SupervisorJob() + Dispatchers.Main)

    @Singleton
    @Provides
    fun provideMainThreadExecutor(): Executor {
        return object : Executor {
            private val handler: Handler = Handler(Looper.getMainLooper())
            override fun execute(command: Runnable) {
                handler.post(command)
            }
        }
    }
}
