package com.etrade.mobilepro.checkdeposit.presentation.deposit

import android.content.Intent
import com.miteksystems.misnap.params.MiSnapApi

internal fun Intent?.miSnapResultCode() = this?.getStringExtra(MiSnapApi.RESULT_CODE)
internal fun Intent?.mibiData() = this?.getStringExtra(MiSnapApi.RESULT_MIBI_DATA)
