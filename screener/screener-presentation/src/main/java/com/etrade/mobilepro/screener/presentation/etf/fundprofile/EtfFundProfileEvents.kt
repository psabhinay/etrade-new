package com.etrade.mobilepro.screener.presentation.etf.fundprofile

import com.etrade.mobilepro.screener.api.filter.etf.EtfFundProfileFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class FundSwitch : ScreenerFilterEvent<EtfFundProfileFilter, EtfFundProfileViewModel.ViewState, Boolean> {
    ALL_STAR {
        override fun update(viewState: EtfFundProfileViewModel.ViewState, eventValue: Boolean): EtfFundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(allStarsFunds = eventValue))
    },
    OPTIONS_AVAILABLE {
        override fun update(viewState: EtfFundProfileViewModel.ViewState, eventValue: Boolean): EtfFundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(optionsAvailable = eventValue))
    }
}

internal enum class ExpenseRatio : ScreenerFilterEvent<EtfFundProfileFilter, EtfFundProfileViewModel.ViewState, Boolean> {
    BOTTOM_20 {
        override fun update(viewState: EtfFundProfileViewModel.ViewState, eventValue: Boolean): EtfFundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(bottom20 = eventValue))
    },
    BELOW_MID_20 {
        override fun update(viewState: EtfFundProfileViewModel.ViewState, eventValue: Boolean): EtfFundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(belowMid20 = eventValue))
    },
    MID_20 {
        override fun update(viewState: EtfFundProfileViewModel.ViewState, eventValue: Boolean): EtfFundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(mid20 = eventValue))
    },
    ABOVE_MID_20 {
        override fun update(viewState: EtfFundProfileViewModel.ViewState, eventValue: Boolean): EtfFundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(aboveMid20 = eventValue))
    },
    TOP_20 {
        override fun update(viewState: EtfFundProfileViewModel.ViewState, eventValue: Boolean): EtfFundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(top20 = eventValue))
    }
}

internal enum class LeveragedETfs : ScreenerFilterEvent<EtfFundProfileFilter, EtfFundProfileViewModel.ViewState, Boolean> {
    EXCLUDE {
        override fun update(viewState: EtfFundProfileViewModel.ViewState, eventValue: Boolean): EtfFundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(leveragedEtfsExclude = eventValue, leveragedEtfsShowOnly = false))
    },
    SHOW_ONLY {
        override fun update(viewState: EtfFundProfileViewModel.ViewState, eventValue: Boolean): EtfFundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(leveragedEtfsShowOnly = eventValue, leveragedEtfsExclude = false))
    }
}

internal enum class ExchangeTradedNotes : ScreenerFilterEvent<EtfFundProfileFilter, EtfFundProfileViewModel.ViewState, Boolean> {
    EXCLUDE {
        override fun update(viewState: EtfFundProfileViewModel.ViewState, eventValue: Boolean): EtfFundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(exchangeTradedNotesExclude = eventValue, exchangeTradedNotesShowOnly = false))
    },
    SHOW_ONLY {
        override fun update(viewState: EtfFundProfileViewModel.ViewState, eventValue: Boolean): EtfFundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(exchangeTradedNotesShowOnly = eventValue, exchangeTradedNotesExclude = false))
    }
}
