package com.etrade.mobilepro.inboxmessages

import android.net.Uri
import android.os.Build
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.P])
class DeepLinkGenericLinkHelperTest {

    private val validGenericDeepLink = "etrade://show/genericlink?urlToLoad=https://refer.etrade.net/ios&title=Refer%20A%20Friend"
    private val validGenericDeepLinkExtraParams = "etrade://show/genericlink?urlToLoad=https://refer.etrade.net/ios&leadSource=ET%20Mobile&title=Refer%20A%20Friend&campaignCode=ET%20Campaign"
    private val invalidGenericDeepLink = "etrade://show/genericlinks?urlToLoad=https://refer.etrade.net/ios&title=Refer%20A%20Friend"
    private val baseUrl = "https://us.uat.etrade.com"

    @Test
    fun testIsGenericLink() {
        val validGenericDeepLinkUri = Uri.parse(validGenericDeepLink)
        val invalidGenericDeepLinkUri = Uri.parse(invalidGenericDeepLink)
        assertTrue(validGenericDeepLinkUri.isValidGenericLinkFormat())
        assertFalse(invalidGenericDeepLinkUri.isValidGenericLinkFormat())
    }

    @Test
    fun testHasFullUrl() {
        val fullUrlDeepLink = validGenericDeepLink
        val relativeUrlDeepLink = "etrade://show/genericlink?urlToLoad=/knowledge/investing-basics&title=Knowledge%20Base"
        val fullUrlDeepLinkUri = Uri.parse(fullUrlDeepLink)
        val relativeUrlDeepLinkUri = Uri.parse(relativeUrlDeepLink)
        assertTrue(fullUrlDeepLinkUri.hasFullUrl())
        assertFalse(relativeUrlDeepLinkUri.hasFullUrl())
    }

    @Test
    fun testGetTitle() {
        val uri = Uri.parse(validGenericDeepLink)
        val expectedTitle = "Refer A Friend"
        assertTrue(uri.getTitle() == expectedTitle)
    }

    @Test
    fun testGetEmbeddedUrl() {
        val uri = Uri.parse(validGenericDeepLink)
        val expectedUrl = "https://refer.etrade.net/ios"
        val embeddedUrl = uri.getEmbeddedUrl(baseUrl)
        assertTrue(embeddedUrl == expectedUrl)
    }

    @Test
    fun testGetExtraUriParamsAsQueryString() {
        val uri = Uri.parse(validGenericDeepLinkExtraParams)
        val expectedQueryString = "leadSource=ET%20Mobile&campaignCode=ET%20Campaign"
        val embeddedQueryString = uri.getExtraUriParamsAsQueryString()
        assertTrue(expectedQueryString == embeddedQueryString)
    }

    @Test
    fun testGetExtraUriParamsAsQueryStringNull() {
        val uri = Uri.parse(validGenericDeepLink)
        val embeddedQueryString = uri.getExtraUriParamsAsQueryString()
        assertNull(embeddedQueryString)
    }

    @Test
    fun testGetEmbeddedUrlWithExtraParams() {
        val uri = Uri.parse(validGenericDeepLinkExtraParams)
        val expectedUrl = "https://refer.etrade.net/ios?leadSource=ET%20Mobile&campaignCode=ET%20Campaign"
        val embeddedUrl = uri.getEmbeddedUrl(baseUrl)
        assertTrue(embeddedUrl == expectedUrl)
    }

    @Test
    fun testResolvedDeepLinkForFullUrl() {
        val uri = Uri.parse(validGenericDeepLinkExtraParams)
        val expectedDeepLink = "etrade://webview?title=Refer A Friend&url=https://refer.etrade.net/ios?leadSource=ET%20Mobile&campaignCode=ET%20Campaign"
        val actualDeepLink = uri.getResolvedGenericDeepLink(baseUrl)
        assertTrue(actualDeepLink == expectedDeepLink)
    }

    @Test
    fun testResolvedDeepLinkForRelativeUrl() {
        val relativeUrlDeepLink = "etrade://show/genericlink?urlToLoad=/knowledge/investing-basics&title=Knowledge%20Base"
        val uri = Uri.parse(relativeUrlDeepLink)
        val expectedDeepLink = "etrade://webview?title=Knowledge Base&url=https://us.uat.etrade.com/knowledge/investing-basics"
        val actualDeepLink = uri.getResolvedGenericDeepLink(baseUrl)
        assertTrue(actualDeepLink == expectedDeepLink)
    }
}
