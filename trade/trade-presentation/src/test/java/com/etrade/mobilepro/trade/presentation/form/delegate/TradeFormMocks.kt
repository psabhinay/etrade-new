package com.etrade.mobilepro.trade.presentation.form.delegate

import android.content.Context
import android.content.res.Resources
import androidx.lifecycle.LiveData
import com.etrade.mobilepro.balance.viewmodel.AccountBalanceViewModel
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quoteapi.QuoteBidAskItem
import com.etrade.mobilepro.quoteapi.QuotePriceWidgetItem
import com.etrade.mobilepro.trade.api.AccountMode
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.data.form.input.DefaultConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultConditionalTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultContingentOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultContingentStockTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultMutualFundTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultStockTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultStopLossOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultStopLossStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.SelectedAccountDelegate
import com.etrade.mobilepro.trade.optionlegselect.TradeLegBidAskViewModel
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter
import com.etrade.mobilepro.util.ValueCoder
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock

internal const val PREVIEW_BUTTON = "preview"
internal const val SAVE_BUTTON = "save"

internal val mockResources: Resources = mock {
    on { getString(R.string.trade_hint_limit_price) } doReturn ("--")
    on { getString(R.string.trade_hint_stop_price) } doReturn ("--")
    on { getString(R.string.trade_hint_symbol) } doReturn ("--")
    on { getString(R.string.trade_hint_account) } doReturn ("--")
    on { getString(R.string.trade_hint_security_type) } doReturn ("--")
    on { getString(R.string.trade_security_type_stock) } doReturn ("--")
    on { getString(R.string.trade_security_type_options) } doReturn ("--")
    on { getString(R.string.trade_security_type_mutual_fund) } doReturn ("--")
    on { getString(R.string.trade_security_type_conditional) } doReturn ("--")
    on { getString(R.string.trade_security_type_stock_and_options) } doReturn ("--")
    on { getString(R.string.trade_hint_order_type) } doReturn ("--")
    on { getString(R.string.trade_order_type_buy) } doReturn ("--")
    on { getString(R.string.trade_order_type_sell) } doReturn ("--")
    on { getString(R.string.trade_order_type_buy_to_cover) } doReturn ("--")
    on { getString(R.string.trade_order_type_sell_short) } doReturn ("--")
    on { getString(R.string.trade_hint_quantity) } doReturn ("--")
    on { getString(R.string.trade_hint_price_type) } doReturn ("--")
    on { getString(any()) } doReturn ("--")

    on { getString(R.string.trade_action_preview) } doReturn PREVIEW_BUTTON
    on { getString(R.string.trade_action_save_order) } doReturn SAVE_BUTTON
}

internal val mockContext: Context = mock {
    on { resources } doReturn mockResources
}

internal val encodedTradeLegs = """
                        [{"expiryDay":"31","expiryMonth":"12","expiryYear":"2020","optionType":"2","quantity":1,"strikePrice":"267.5","symbol":"FB----201231P00267500","transaction":"1"}]
""".trimIndent()

private val decodedTradeLegs = listOf(
    TestTradeLeg(
        "FB----201231P00267500",
        TransactionType.SELL_CLOSE_OPT,
        10,
        "FB Dec 31 '20 \$257.50 Put",
        false
    )
)

internal val mockOptionsTradeLegsCoder: ValueCoder<List<TradeLeg>> = mock {
    on { decode(encodedTradeLegs) } doReturn decodedTradeLegs
    on { encode(decodedTradeLegs) } doReturn encodedTradeLegs
}

internal val mockDelegates: TradeFormDelegates = TradeFormDelegates(
    context = mockContext,
    searchIntentFactory = mock(),
    conditionalOrderTradeFormInputId = DefaultConditionalOrderTradeFormInputId,
    conditionalTradeFormInputId = DefaultConditionalTradeFormInputId,
    contingentStockTradeFormInputId = DefaultContingentStockTradeFormInputId,
    mutualFundTradeFormInputId = DefaultMutualFundTradeFormInputId,
    optionTradeFormInputId = DefaultOptionTradeFormInputId,
    stockTradeFormInputId = DefaultStockTradeFormInputId,
    contingentOptionTradeFormInputId = DefaultContingentOptionTradeFormInputId,
    stopLossStockTradeFormInputId = DefaultStopLossStockTradeFormInputId,
    stopLossOptionTradeFormInputId = DefaultStopLossOptionTradeFormInputId,
    tradeFormParametersBuilderProvider = mock(),
    optionsTradeLegsCoder = mockOptionsTradeLegsCoder,
    tradingDefaultsPreferences = mock()
)

private val mockBalanceViewModel: AccountBalanceViewModel = mock {
    on { accountBalance } doReturn mock()
}

private val mockQuoteWidgetViewModel: QuotesWidgetViewModel = mock {
    on { isQuoteLoaded } doReturn mock()
}

private val mockWalkthroughViewModel: WalkthroughStatesViewModel = mock {
    on { state } doReturn mock()
}

internal val mockParameter: CreateTradeFormParameter = mock {
    on { balanceViewModel } doReturn mockBalanceViewModel
    on { conditionalOrderDisclosureViewModel } doReturn mock()
    on { quoteSearchHistoryViewModel } doReturn mock()
    on { quoteWidgetViewModel } doReturn mockQuoteWidgetViewModel
    on { onActionClickListener } doReturn mock()
    on { walkthroughViewModel } doReturn mockWalkthroughViewModel
}

private val mockTradeLegBidAskViewModel: TradeLegBidAskViewModel = mock {
    on { quoteBidAskItem } doReturn object : QuoteBidAskItem {
        override val bidPrice: LiveData<QuotePriceWidgetItem>
            get() = mock()
        override val askPrice: LiveData<QuotePriceWidgetItem>
            get() = mock()
    }
}

internal val mockOptionsTradeParameter: CreateTradeFormParameter = mock {
    on { balanceViewModel } doReturn mockBalanceViewModel
    on { quoteWidgetViewModel } doReturn mock()
    on { onActionClickListener } doReturn mock()
    on { conditionalOrderDisclosureViewModel } doReturn mock()
    on { quoteSearchHistoryViewModel } doReturn mock()
    on { tradeLegBidAskViewModel } doReturn mockTradeLegBidAskViewModel
    on { walkthroughViewModel } doReturn mockWalkthroughViewModel
}

internal data class TestTradeLeg(
    override val symbol: String,
    override val transactionType: TransactionType,
    override val quantity: Int,
    override val displaySymbol: String,
    override val isAMOption: Boolean
) : TradeLeg

internal val mockTradeAccounts: List<TradeAccount> = listOf(
    object : TradeAccount {
        override val id: String = "31459185"
        override val name: String = "Investment-account"
        override val accountMode: AccountMode = AccountMode.CASH
        override val approvalLevel: Int = 4
    }
)

internal fun mockSelectedAccountDelegate(accountId: String) = object : SelectedAccountDelegate {
    private var accountId = accountId
    override fun getCurrentAccountId(securityType: SecurityType) = this.accountId
    override fun setUserSelectedAccountId(securityType: SecurityType, selectedAccountId: String) {
        this.accountId = selectedAccountId
    }
    override fun onSecurityType(securityType: SecurityType) {}
}
