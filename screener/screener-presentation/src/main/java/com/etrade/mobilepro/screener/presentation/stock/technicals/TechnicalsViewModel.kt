package com.etrade.mobilepro.screener.presentation.stock.technicals

import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.filter.stock.TechnicalsFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Stock
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

private const val DEFAULT_PERCENT = "0"
class TechnicalsViewModel @Inject constructor(
    seekerInputDependenciesFactory: SeekerInputDependenciesFactory,
    aggregator: ScreenerFilterAggregator,
    @Stock screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<TechnicalsFilter, TechnicalsViewModel.ViewState>(aggregator, ScreenerType.Stock.Technicals, screenerInteractor) {
    internal val lowPercentViewDependencies = seekerInputDependenciesFactory.getPercentInputDependencies()
    internal val highPercentViewDependencies = seekerInputDependenciesFactory.getPercentInputDependencies()
    internal val lowPercentMutableData = MutableLiveData<String>().apply { value = DEFAULT_PERCENT }
    internal val highPercentMutableData = MutableLiveData<String>().apply { value = DEFAULT_PERCENT }

    override fun provideInitialState(initialFilter: TechnicalsFilter): ViewState {
        lowPercentMutableData.value = initialFilter.stock52WeekLow
        highPercentMutableData.value = initialFilter.stock52WeekHigh
        return ViewState(filter = initialFilter)
    }

    internal fun lowPercentChanged(value: String) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    stock52WeekLow = value
                )
            )
        }
    }

    internal fun highPercentChanged(value: String) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    stock52WeekHigh = value
                )
            )
        }
    }

    data class ViewState(
        override val filter: TechnicalsFilter
    ) : BaseFilterScreenViewModel.ViewState<TechnicalsFilter>
}
