package com.etrade.mobilepro.settings.viewmodel

class SettingTextDynamicViewModel(
    override val title: String,
    override val value: String? = null,
    val alterValue: String? = null,
    var hasExpandedText: Boolean = false,
    override val layoutId: Int
) : SettingDynamicViewModel(title, value, layoutId)
