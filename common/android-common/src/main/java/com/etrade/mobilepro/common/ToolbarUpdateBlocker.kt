package com.etrade.mobilepro.common

import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver

/**
 * This interface supposed to block toolbar updates and fragment doesn't now about activity responsible for them.
 * This can be useful when fragments belong to different modules and should be self responsible for updating toolbar,
 * because otherwise activity can keep updating according to its defaults.
 * E.g. activity may trigger updates on orientation changes.
 */
interface ToolbarUpdateBlocker : OnNavigateUpListener {

    /**
     * Used to check if updates are currently blocked
     */
    val isToolbarUpdateBlocked: Boolean

    /**
     * Used to block toolbar updates when given [Fragment] is visible to the user.
     * Its implementation should cover all user navigation usecases including back or popup navigation.
     */
    fun Fragment.blockTitleUpdates()
}

/**
 * Default implementation of [ToolbarUpdateBlocker].
 */
class ToolbarUpdateBlockerImpl : ToolbarUpdateBlocker {

    override val isToolbarUpdateBlocked get() = !isNavigatingBack && isBlocked
    private val observer: LifecycleEventObserver = LifecycleEventObserver { _, event ->
        when (event) {
            Lifecycle.Event.ON_START -> isBlocked = true
            Lifecycle.Event.ON_STOP -> isBlocked = false
            Lifecycle.Event.ON_DESTROY -> isNavigatingBack = false
            else -> {
            }
        }
    }
    private var isNavigatingBack = false
    private var isBlocked = false

    override fun onNavigateUp(): Boolean {
        isNavigatingBack = true
        return false
    }

    override fun Fragment.blockTitleUpdates() {
        viewLifecycleOwner.lifecycle.apply {
            removeObserver(observer)
            addObserver(observer)
        }
        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner) {
            isNavigatingBack = true
            if (isEnabled) {
                isEnabled = false
                activity?.onBackPressed()
            }
        }
    }
}
