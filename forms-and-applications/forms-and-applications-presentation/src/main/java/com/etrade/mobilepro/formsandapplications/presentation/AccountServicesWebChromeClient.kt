package com.etrade.mobilepro.formsandapplications.presentation

import android.Manifest
import android.app.Activity
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.media.MediaScannerConnection
import android.net.Uri
import android.provider.MediaStore
import android.webkit.PermissionRequest
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebView
import androidx.activity.result.ActivityResultRegistry
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.util.android.extension.selfPermissionGranted
import com.etrade.mobilepro.util.formatters.parseCustomDateFromMillis
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File

private const val CAMERA_PERMISSION = Manifest.permission.CAMERA
private const val FILE_SUFFIX = ".jpg"
private const val MEDIA_PATH_DIR = "captured_media"
private const val ACCEPT_TYPE_ANY = "*/*"
private const val REQUEST_CODE_CAMERA_PERMISSIONS = 2
private const val FILE_NAME_DATE_FORMAT = "yyyyMMdd_HHmmss"
private const val FILE_NAME_PREFIX = "IMG_"

class AccountServicesWebChromeClient(
    private val context: Context,
    private val registry: ActivityResultRegistry,
    private val lifecycleOwner: LifecycleOwner,
    private val requestPermissionCallback: (String, Int) -> Unit,
    private val errorCallback: () -> Unit
) : WebChromeClient() {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    private val authorityProvider: String = "${context.packageName}.provider"

    private var uploadMessageCallback: ValueCallback<Uri?>? = null
    private var tempMediaFile: File? = null

    private val fileChooserRequest = registry.register(
        "chooserLauncher",
        lifecycleOwner,
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        onFileChosen(result.resultCode, result.data)
    }

    override fun onPermissionRequest(request: PermissionRequest) = request.grant(request.resources)

    override fun onShowFileChooser(
        webView: WebView,
        filePathCallback: ValueCallback<Array<Uri>>,
        fileChooserParams: FileChooserParams
    ): Boolean {
        val acceptTypes = fileChooserParams.acceptTypes
        var acceptType = acceptTypes
            .filter { it != null && it.isNotEmpty() }
            .fold("") { acc, s -> "$acc$s;" }
        if (acceptType.isEmpty()) {
            acceptType = ACCEPT_TYPE_ANY
        }
        uploadMessageCallback = ValueCallback { value ->
            val result: Array<Uri>? = value?.let { arrayOf(it) }
            filePathCallback.onReceiveValue(result)
        }
        openFileChooser(acceptType)
        return true
    }

    private fun openFileChooser(acceptType: String) {
        val readContentIntent = Intent(Intent.ACTION_GET_CONTENT)
        readContentIntent.addCategory(Intent.CATEGORY_OPENABLE)
        readContentIntent.type = acceptType
        val contentUri = createTempFileContentUri()
        val chooser = createChooserIntent(createCameraIntent(contentUri))
        chooser.putExtra(Intent.EXTRA_INTENT, readContentIntent)
        fileChooserRequest.launch(chooser)
    }

    private fun createChooserIntent(vararg intents: Intent): Intent {
        return Intent(Intent.ACTION_CHOOSER).apply {
            putExtra(Intent.EXTRA_INITIAL_INTENTS, intents)
            putExtra(Intent.EXTRA_TITLE, context.getString(R.string.forms_and_applications_chooser_title))
        }
    }

    private fun createCameraIntent(contentUri: Uri?): Intent {
        return Intent(MediaStore.ACTION_IMAGE_CAPTURE).apply {
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
            putExtra(MediaStore.EXTRA_OUTPUT, contentUri)
            clipData = ClipData.newUri(context.contentResolver, authorityProvider, contentUri)
        }
    }

    private fun createTempFileContentUri(): Uri {
        val mediaPath = File(context.filesDir, MEDIA_PATH_DIR)
        if (!mediaPath.exists() && !mediaPath.mkdir()) {
            logger.debug("Unable to create folder $mediaPath")
            errorCallback()
        }
        val fileName = FILE_NAME_PREFIX + parseCustomDateFromMillis(FILE_NAME_DATE_FORMAT) + FILE_SUFFIX
        val file = File(mediaPath, fileName)
        tempMediaFile = file
        return FileProvider.getUriForFile(context, authorityProvider, file)
    }

    fun onFileChosen(resultCode: Int, intent: Intent?) {
        var result = if (intent == null || resultCode != Activity.RESULT_OK) {
            null
        } else {
            intent.data
        }
        if (result == null) {
            tempMediaFile?.apply {
                if (exists() && length() > 0) {
                    // Broadcast to the media scanner that we have a new photo
                    // so it will be added into the gallery for the user.
                    MediaScannerConnection.scanFile(context, arrayOf(toString()), arrayOf(name), null)
                    result = Uri.fromFile(this)
                } else {
                    checkAndRequestCameraPermission()
                    result = null
                }
            }
        }
        uploadMessageCallback?.onReceiveValue(result)
        uploadMessageCallback = null
    }

    private fun checkAndRequestCameraPermission(): Boolean {
        val permissionNeeded = !context.selfPermissionGranted(CAMERA_PERMISSION)
        if (permissionNeeded) {
            requestPermissionCallback(CAMERA_PERMISSION, REQUEST_CODE_CAMERA_PERMISSIONS)
        }
        return permissionNeeded
    }
}
