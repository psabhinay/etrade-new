package com.etrade.mobilepro.localytics.data

import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.localytics.api.AppStartRepo
import com.etrade.mobilepro.localytics.data.rest.AppStartService
import com.localytics.androidx.Localytics
import retrofit2.Retrofit
import javax.inject.Inject

class DefaultAppStartRepo @Inject constructor(@MobileTrade val retrofit: Retrofit) : AppStartRepo {
    private val apiClient = retrofit.create(AppStartService::class.java)

    override suspend fun getGDPRStatus() {
        runCatching {
            apiClient.getGDPRStatus()
        }.onSuccess {
            Localytics.setOptedOut(it.screen.references.firstOrNull()?.data?.firstOrNull()?.isSubjectToGDPR ?: true)
        }.onFailure {
            Localytics.setOptedOut(true)
        }
    }
}
