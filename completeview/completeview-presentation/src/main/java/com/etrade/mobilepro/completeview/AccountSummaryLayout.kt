package com.etrade.mobilepro.completeview

import com.etrade.mobilepro.dynamicui.api.GenericLayout

abstract class AccountSummaryLayout(accountUuid: String?) : GenericLayout {

    val uuid: String? = accountUuid

    open val accountSourceLabel: String? = null

    override val uniqueIdentifier: String = "${javaClass.name}_$uuid"
}
