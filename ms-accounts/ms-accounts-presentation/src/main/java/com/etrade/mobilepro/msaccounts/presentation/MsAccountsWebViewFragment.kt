package com.etrade.mobilepro.msaccounts.presentation

import android.os.Bundle
import android.view.MenuItem
import android.webkit.JavascriptInterface
import android.webkit.WebView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import org.chromium.customtabsclient.shared.WebViewFragment
import org.chromium.customtabsclient.shared.WebviewActivity
import javax.inject.Inject

@RequireLogin
class MsAccountsWebViewFragment @Inject constructor(
    @DeviceType deviceType: String,
    private val mainNavigation: MainNavigation
) : WebViewFragment(deviceType) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    finishFragment()
                }
            }
        )
    }

    override fun setupWebView(webView: WebView) {
        super.setupWebView(webView)
        webView.addJavascriptInterface(
            EtMobileInterface(),
            ET_MOBILE_INTERFACE_NAME
        )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finishFragment()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    // Uncomment to test closeWebView functionality
    // override fun getUrl(): String = "file:///android_asset/ms-accounts/closeWebView.html"

    fun finishFragment() {
        setResults()
        activity?.let {
            if (it is WebviewActivity) {
                it.finish()
            } else {
                it.runOnUiThread { findNavController().popBackStack() }
            }
        }
    }

    fun navigateToCompleteView() {
        activity?.apply {
            runOnUiThread {
                setResults()
                mainNavigation.navigateToCompleteView(this)
            }
        }
    }

    private fun setResults() {
        setFragmentResult(RESULT_MS_ACCOUNTS_WEB_VIEW_CLOSED, Bundle())
        activity?.let {
            if (it is WebviewActivity) {
                it.setResult(RESULT_CODE_MS_ACCOUNTS_WEB_VIEW_CLOSED)
            }
        }
    }

    inner class EtMobileInterface {

        @JavascriptInterface
        fun closeWebView() {
            logger.debug("JS Output - closeWebView")
            navigateToCompleteView()
        }

        @Deprecated(
            message = "Keep the misspelled version available for web view pages that use it",
            replaceWith = ReplaceWith("closeWebView()")
        )
        @JavascriptInterface
        fun closeWebview() {
            closeWebView()
        }
    }

    companion object {
        const val RESULT_MS_ACCOUNTS_WEB_VIEW_CLOSED = "RESULT_MS_ACCOUNTS_WEB_VIEW_CLOSED"
        const val RESULT_CODE_MS_ACCOUNTS_WEB_VIEW_CLOSED = 101

        private const val ET_MOBILE_INTERFACE_NAME = "ETMobileInterface"
    }
}
