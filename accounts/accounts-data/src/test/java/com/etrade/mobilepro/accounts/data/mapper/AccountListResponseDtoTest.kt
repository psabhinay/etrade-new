package com.etrade.mobilepro.accounts.data.mapper

import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.accounts.data.rest.AccountListResponseDto
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.util.json.BigDecimalAdapter
import org.junit.Assert.assertEquals
import org.junit.Assert.assertSame
import org.junit.Assert.assertTrue
import org.junit.Test
import java.math.BigDecimal

class AccountListResponseDtoTest {

    @Test
    fun `test the mapping of account list dto`() {
        val responseJson = getAccountsJsonResponse()
        val responseModel = responseJson.mapToApiModel()

        responseModel.accounts.assert()
        assertSame(responseModel.highRiskFlag, true)
        assertEquals(BigDecimal("0.0"), responseModel.liabilityValue)
        assertEquals(BigDecimal("1301835283.3891"), responseModel.netAssetValue)
    }

    private fun getAccountsJsonResponse(): AccountListResponseDto {
        return this::class.getObjectFromJson(
            resourcePath = "accounts.json",
            AccountListResponseDto::class.java,
            adapters = listOf(BigDecimalAdapter)
        )
    }
}

private fun List<AccountDetails>.assert() {
    assertEquals(25, this.size)
    this.first().let {
        assertEquals("84228561", it.accountId)
        assertEquals("g0dCHoUPgjJT-O8s11_upg", it.accountIdKey)
        assertEquals("CASH", it.accountMode)
        assertEquals(false, it.isExternalAccount)
        assertEquals("8561 OTHER", it.displayString())
        assertEquals("BROKERAGE", it.institutionType)
        assertEquals("Corporation", it.accountName)
    }

    assertEquals("IRA", this[1].accountMode)
    assertTrue(this[2].isExternalAccount)
}
