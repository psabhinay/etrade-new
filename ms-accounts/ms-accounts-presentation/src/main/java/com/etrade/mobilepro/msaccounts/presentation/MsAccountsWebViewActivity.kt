package com.etrade.mobilepro.msaccounts.presentation

import android.content.Context
import android.content.Intent
import android.view.MenuItem
import com.etrade.mobilepro.util.android.extension.instantiateFragment
import org.chromium.customtabsclient.shared.WebviewActivity
import org.chromium.customtabsclient.shared.createWebViewActivityIntent

class MsAccountsWebViewActivity : WebviewActivity() {

    override fun instantiateFragment() {
        fragmentInjectionFactory.instantiateFragment(
            MsAccountsWebViewFragment::class.java,
            createFragmentArgs()
        ).apply {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, this)
                .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // it's overridden to propagate option click event into [MsAccountsWebViewFragment]
        return false
    }

    companion object {

        /**
         * Creates an intent to start [MsAccountsWebViewActivity].
         *
         * @param context application context
         * @param url an url to open
         * @param titleResId resource ID of a titleResId to display
         *
         * @return the intent to start the activity
         */
        fun intent(
            context: Context,
            url: String,
            title: String,
            upIconResId: Int = R.drawable.ic_close
        ): Intent {
            return context.createWebViewActivityIntent(
                activityClass = MsAccountsWebViewActivity::class.java,
                url = url,
                title = title,
                upIconResId = upIconResId
            )
        }
    }
}
