package com.etrade.mobilepro.searching.rest

import com.etrade.eo.rest.retrofit.Xml
import com.etrade.mobilepro.searching.dto.SymbolLookupResponseDto
import com.etrade.mobilepro.searching.dto.SymbolSearchDto
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface SymbolSearchApiClient {
    @JsonMoshi
    @GET("/app/product/lookup/{resource}.json")
    fun searchSymbol(@Path("resource") resource: String): Single<SymbolSearchDto>

    @Xml
    @FormUrlEncoded
    @POST("/e/t/mobile/SymbolLookup")
    fun symbolLookup(
        @Field("CompanyName") companyName: String,
        @Field("ProductType") productType: String,
        @Field("ExpiryYear") expiryYear: String,
        @Field("ExpiryMonth") expiryMonth: String,
        @Field("ExpiryDay") expiryDay: String,
        @Field("Include_sdo_flag") includeSdoFlag: Int,
        @Field("ScalabilityRange") scalabilityRange: Int
    ): Single<SymbolLookupResponseDto>
}
