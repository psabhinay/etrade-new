package com.etrade.mobilepro.accounts

import com.etrade.mobilepro.editlist.EditListItem
import com.etrade.mobilepro.util.formatDash

data class AccountOrderItem(
    override val displayText: CharSequence,
    val uuid: String,
    override val isChecked: Boolean = true
) : EditListItem {
    override val contentDescription: String = displayText.toString().formatDash() ?: displayText.toString()

    override val dragHandleContentDescription = "$contentDescription. Double Tap and hold to change portfolio detail order"
}
