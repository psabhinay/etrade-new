package com.etrade.mobilepro.usersubscriptiondata

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.streaming.lightstreamer.level2.Level2BookType
import com.etrade.mobilepro.usersubscriptionapi.BookFilter
import com.etrade.mobilepro.usersubscriptionapi.MarketDataAgreementResponse
import com.etrade.mobilepro.usersubscriptionapi.UserSubscriptionRepo
import java.util.ArrayList

private const val TOOL_SET_ARCA = "ARCA"
private const val TOOL_SET_TOTALVIEW = "TOTALVIEW"
private const val TOOL_SET_NYSE = "NYSE OPEN BOOK"

class DefaultUserSubscriptionRepo(
    private val userSubscriptionApiClient: UserSubscriptionApiClient
) : UserSubscriptionRepo {

    override suspend fun loadUserMda(): ETResult<MarketDataAgreementResponse> {
        return userSubscriptionApiClient.runCatchingET {
            getMda()
        }
    }

    override suspend fun getBookFilters(isOption: Boolean): ETResult<List<BookFilter>> {
        return userSubscriptionApiClient.runCatchingET {
            getSubscriptions()
        }.map {
            it.data?.let { data ->
                data.userResponse?.let { userSubscriptionResponse ->
                    getCurrentBookFilters(userSubscriptionResponse.toolList ?: emptyList(), isOption)
                }
            }.orEmpty()
        }
    }

    private fun getCurrentBookFilters(toolList: List<UserSubscriptionToolListDto>, isOption: Boolean): List<BookFilter> {
        return if (isOption) {
            getOptionBookFilters(toolList)
        } else {
            getBookFilters(toolList)
        }
    }

    @SuppressWarnings("MagicNumber") // The size of the array will be consistent
    private fun getBookFilters(toolList: List<UserSubscriptionToolListDto>): List<BookFilter> {
        val bookFilters = ArrayList<BookFilter>(5)
        var arcaEligible = false
        var nsdqEligible = false
        toolList.forEach { tool ->
            when (tool.toolSetName) {
                TOOL_SET_ARCA -> {
                    bookFilters.add(BookFilter(Level2BookType.ARCA, tool.eligible == true))
                    arcaEligible = tool.eligible == true
                }
                TOOL_SET_TOTALVIEW -> {
                    bookFilters.add(BookFilter(Level2BookType.NSDQ, tool.eligible == true))
                    bookFilters.add(BookFilter(Level2BookType.MONTAGE, tool.eligible == true))
                    nsdqEligible = tool.eligible == true
                }
                TOOL_SET_NYSE -> {
                    bookFilters.add(BookFilter(Level2BookType.NYSE_OPEN, tool.eligible == true))
                }
            }
        }
        bookFilters.add(BookFilter(Level2BookType.ARCA_NSDQ, arcaEligible || nsdqEligible))
        return bookFilters
    }

    private fun getOptionBookFilters(toolList: List<UserSubscriptionToolListDto>): List<BookFilter> {
        return toolList.filter { it.toolSetName == TOOL_SET_TOTALVIEW }
            .map { BookFilter(Level2BookType.MONTAGE, it.eligible == true) }
    }
}
