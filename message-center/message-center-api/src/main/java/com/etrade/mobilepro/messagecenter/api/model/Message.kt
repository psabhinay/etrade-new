package com.etrade.mobilepro.messagecenter.api.model

import org.threeten.bp.Instant

data class Message(
    val id: String,
    val accountId: String,
    val accountShortName: String,
    val descId: String,
    val subjectId: String,
    val caseTypeId: String,
    val caseTypeDesc: String,
    val caseNo: String,
    val isUnread: Boolean,
    val status: String,
    val createdAt: Instant?,
    val createdAtFormatted: String,
    val readAt: Instant?
)
