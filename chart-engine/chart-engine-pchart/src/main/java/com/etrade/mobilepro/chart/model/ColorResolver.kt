package com.etrade.mobilepro.chart.model

import android.content.Context
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat

interface ColorResolver {
    @ColorInt
    fun getColorInt(@ColorRes colorRes: Int): Int
}

class ColorResolverImpl(val context: Context) : ColorResolver {
    override fun getColorInt(colorRes: Int) = ContextCompat.getColor(context, colorRes)
}
