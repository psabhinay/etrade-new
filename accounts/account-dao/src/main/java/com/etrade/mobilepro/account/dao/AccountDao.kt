package com.etrade.mobilepro.account.dao

import androidx.lifecycle.LiveData
import com.etrade.mobile.accounts.dto.AccountType
import io.reactivex.Observable

interface AccountDao {

    fun getAllAccounts(async: Boolean = false): LiveData<List<Account>>

    fun getAccountWithAccountId(accountId: String): Account?

    fun getAccountWithAccountIdAndType(accountId: String, accountType: AccountType = AccountType.Brokerage, async: Boolean = false): Account?

    fun getAccountWithUuid(uuid: String): Account?

    fun getAllAccountsObservable(): Observable<List<Account>>

    fun getAllAccountsNonAsync(): List<Account>
}
