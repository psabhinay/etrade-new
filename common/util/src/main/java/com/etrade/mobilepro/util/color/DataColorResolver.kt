package com.etrade.mobilepro.util.color

interface DataColorResolver {
    fun resolveDataColor(value: Any?): DataColor

    fun resolveFilledDataColor(value: Any?): DataColor
}
