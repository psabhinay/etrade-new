package com.etrade.mobilepro.trade.form.api.input

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.util.ValueCoder

interface WithSymbolTradeFormInputId : TradeFormInputId {
    val symbol: String
    val symbolCoder: ValueCoder<WithSymbolInfo>

    fun extractSymbol(map: InputValuesSnapshot): String? = extractSymbolInfo(map)?.symbol

    fun extractSymbolInfo(snapshot: InputValuesSnapshot): WithSymbolInfo? = snapshot[symbol]?.let { symbolCoder.decode(it) }
}
