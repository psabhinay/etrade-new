package com.etrade.mobilepro.quote.presentation

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.quote.mapper.MobileQuoteWidgetItemMapper
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.MutualFundsTradeQuotesRepo
import com.etrade.mobilepro.quoteapi.MutualFundsTradeQuotesResult
import com.etrade.mobilepro.quoteapi.QuoteWidgetItem
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.quoteapi.usecase.TradeIsExtendedHoursOnUseCase
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.executeBlocking
import com.etrade.mobilepro.util.invoke
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class QuotesWidgetViewModel @Inject constructor(
    private val quoteService: MobileQuoteRepo,
    private val quoteMapper: MobileQuoteWidgetItemMapper,
    private val mutualFundsTradeQuotesRepo: MutualFundsTradeQuotesRepo,
    private val isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase,
    private val marketTradeStatusDelegate: MarketTradeStatusDelegate,
    private val streamingStatusController: StreamingStatusController,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    private val tradeIsExtendedHoursOnUseCase: TradeIsExtendedHoursOnUseCase
) : DynamicViewModel(R.layout.quote_widget_primary), OrderDetailItemInterface, LifecycleObserver {

    override val variableId: Int by lazy { BR.obj }

    private val compositeDisposable = CompositeDisposable()
    private val streamingCompositeDisposable = CompositeDisposable()

    private val fields: Set<Level1Field> = setOf(
        Level1Field.ASK,
        Level1Field.ASK_SIZE,
        Level1Field.BID,
        Level1Field.BID_SIZE,
        Level1Field.CHANGE,
        Level1Field.CHANGE_PERCENT,
        Level1Field.LAST_PRICE,
        Level1Field.PREVIOUS_CLOSE_PRICE,
        Level1Field.TICK_INDICATOR,
        Level1Field.TIMESTAMP_SINCE_EPOCH,
        Level1Field.VOLUME,
        Level1Field.TRADE_STATUS
    )

    private val exchangeCodeFields: Set<Level1Field> = setOf(
        Level1Field.ASK_EXCHANGE,
        Level1Field.ASK_SIZE,
        Level1Field.BID_EXCHANGE,
        Level1Field.BID_SIZE,
        Level1Field.LAST_PRICE_EXCHANGE,
        Level1Field.LAST_VOLUME
    )

    val loadingIndicator: MutableLiveData<Boolean> = MutableLiveData(true)
    val loadingFailedIndicator: LiveData<Boolean>
        get() = _loadingFailedIndicator

    val errorMessage = MutableLiveData<ConsumableLiveEvent<String>>()

    private val mutableQuote: MutableLiveData<QuoteWidgetItem?> = MutableLiveData()
    val quote: LiveData<QuoteWidgetItem?> get() = mutableQuote

    val isQuoteLoaded: LiveData<Boolean>
        get() = _isQuoteLoaded
    private val _isQuoteLoaded: MutableLiveData<Boolean> = MutableLiveData(false)

    private val _primaryFundsQuote: MutableLiveData<ConsumableLiveEvent<MutualFundsTradeQuotesResult>> =
        MutableLiveData()
    val primaryFundsQuote: LiveData<ConsumableLiveEvent<MutualFundsTradeQuotesResult>> get() = _primaryFundsQuote

    private val _secondaryFundsQuote: MutableLiveData<ConsumableLiveEvent<MutualFundsTradeQuotesResult>> =
        MutableLiveData()
    val secondaryFundsQuote: LiveData<ConsumableLiveEvent<MutualFundsTradeQuotesResult>> get() = _secondaryFundsQuote

    private val _loadingFailedIndicator: MutableLiveData<Boolean> = MutableLiveData(false)

    val isInErrorState
        get() = _loadingFailedIndicator.value ?: false

    private var previousAccountId: String? = null
    private var previousSymbol: String? = null
    private var previousRequireExchangeCode: Boolean = false
    private var previousInstrumentType: InstrumentType? = null
    private var previousTradeType: TradeQuoteType? = null
    private var isRefreshing: Boolean = false

    private var subscriberCounter = 0

    override fun onCleared() {
        super.onCleared()
        clearDisposables()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onLifeCyclePause() {
        /** We want to unsubscribe from streaming */
        if (--subscriberCounter == 0) {
            streamingCompositeDisposable.clear()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onLifeCycleResume() {
        /** We need to subscribe streaming here because gets unsubscribe onPause lifecycle */
        subscriberCounter++
        if (streamingStatusController.isStreamingToggleEnabled && !isRefreshing) {
            quote.value?.let {
                streamQuotes(it.symbol, it.instrumentType)
            }
        } else if (!streamingStatusController.isStreamingToggleEnabled) {
            streamingCompositeDisposable.clear()
        }
    }

    fun refreshTradeFundQuotes() {
        if (previousSymbol != null && previousAccountId != null && previousTradeType != null) {
            getTradeQuoteByType(previousSymbol!!, previousAccountId!!, previousTradeType!!, previousInstrumentType)
        } else {
            logger.error("Invalid symbol and accountId")
        }
    }

    fun getTradeQuoteByType(
        symbol: String,
        accountId: String,
        tradeType: TradeQuoteType,
        instrumentType: InstrumentType? = null
    ) {
        previousSymbol = symbol
        previousAccountId = accountId
        previousTradeType = tradeType
        previousInstrumentType = instrumentType

        loadingIndicator.value = true
        _loadingFailedIndicator.value = false

        viewModelScope.launch {
            getQuote(symbol, accountId, tradeType, instrumentType).fold(
                onSuccess = { onSuccess(it, tradeType) },
                onFailure = ::onFailure
            )
        }
    }

    fun updateQuotes(quote: QuoteWidgetItem) {
        clearDisposables()
        mutableQuote.postValue(quote)
        loadingIndicator.postValue(false)
        if (quote.showUnderlyingInfo) {
            streamCombinedStockOptionQuotes(quote)
        } else {
            streamQuotes(quote.symbol, quote.instrumentType)
        }
    }

    fun refresh() = getQuotes(forceRefresh = true)

    fun getQuotes(
        symbol: String? = previousSymbol,
        requireExchangeCode: Boolean = previousRequireExchangeCode,
        forceRefresh: Boolean = false
    ) {
        if (symbol == null) {
            _isQuoteLoaded.postValue(false)
            return
        }

        if (forceRefresh) {
            fetchQuote(symbol, requireExchangeCode)
        } else {
            if (symbol == previousSymbol && requireExchangeCode == previousRequireExchangeCode) {
                _isQuoteLoaded.postValue(true)
                return
            }
            if (previousSymbol != null && symbol != previousSymbol) {
                mutableQuote.postValue(null)
            }
            fetchQuote(symbol, requireExchangeCode)
        }
    }

    private fun fetchQuote(symbol: String, requireExchangeCode: Boolean) {
        // Save previous values to support refreshes with the same set of parameters.
        previousSymbol = symbol
        previousRequireExchangeCode = requireExchangeCode
        isRefreshing = true
        clearDisposables()

        loadingIndicator.postValue(true)
        _loadingFailedIndicator.postValue(false)
        _isQuoteLoaded.postValue(false)

        runBlocking {
            val isExtendedHours = if (requireExchangeCode) {
                tradeIsExtendedHoursOnUseCase()
            } else {
                isExtendedHoursOnUseCase()
            }
            compositeDisposable += quoteService.loadMobileQuote(
                symbol,
                requireEarningsDate = false,
                extendedHours = isExtendedHours,
                requireExchangeCode = requireExchangeCode
            ).subscribeBy(
                onError = {
                    onQuoteError(it)
                },
                onNext = {
                    onQuoteSuccess(it, isExtendedHours, symbol)
                }
            )
        }
    }

    private fun onQuoteSuccess(it: MobileQuote, isExtendedHours: Boolean, symbol: String) {
        isRefreshing = false
        val quote = quoteMapper.map(it, isExtendedHours)
        mutableQuote.postValue(quote)
        streamQuotes(symbol, it.typeCode)
        loadingIndicator.postValue(false)
        _loadingFailedIndicator.postValue(false)
        _isQuoteLoaded.postValue(true)
    }

    private fun onQuoteError(t: Throwable) {
        isRefreshing = false
        t.localizedMessage?.let { errorMessage.postValue(ConsumableLiveEvent(it)) }
        loadingIndicator.postValue(false)
        _loadingFailedIndicator.postValue(true)
        _isQuoteLoaded.postValue(false)
    }

    private fun streamQuotes(symbol: String, instrumentType: InstrumentType) {
        streamQuote(symbol, instrumentType) {
            updateItem(it, isExtendedHoursOnUseCase.executeBlocking())
            marketTradeStatusDelegate.onMarketStatusUpdate(it)
        }
    }

    private fun streamCombinedStockOptionQuotes(quote: QuoteWidgetItem) {
        streamQuote(quote.symbol, quote.instrumentType) {
            updateBidAsk(it)
        }

        streamQuote(quote.underlyingSymbol, quote.underlyingInstrumentType) {
            updateLastChangeVolume(it, isExtendedHoursOnUseCase.executeBlocking())
            updateTimeStamp(it)
        }
    }

    private fun streamQuote(
        symbol: String,
        instrumentType: InstrumentType,
        updateWithLevel1: QuoteWidgetItem.(Level1Data) -> Unit
    ) {
        if (streamingStatusController.isStreamingToggleEnabled) {
            streamingCompositeDisposable.add(
                streamingQuoteProvider.getQuote(
                    symbol,
                    instrumentType,
                    if (previousRequireExchangeCode) {
                        fields + exchangeCodeFields
                    } else {
                        fields
                    }
                ).subscribeBy { data ->
                    mutableQuote.value?.apply {
                        hasNeutralColorForLast = false
                        isSingleItem = true
                        updateWithLevel1(data)
                    }
                }
            )
        }
    }

    private fun clearDisposables() {
        compositeDisposable.clear()
        streamingCompositeDisposable.clear()
    }

    private suspend fun getQuote(
        symbol: String,
        accountId: String,
        tradeType: TradeQuoteType,
        instrumentType: InstrumentType? = null
    ): ETResult<MutualFundsTradeQuotesResult> {
        return when (tradeType) {
            TradeQuoteType.BUY -> mutualFundsTradeQuotesRepo.loadFundsQuote(
                symbol = symbol,
                accountId = accountId
            )
            TradeQuoteType.SELL -> {
                requireNotNull(instrumentType)
                mutualFundsTradeQuotesRepo.loadOwnedFundsQuote(
                    symbol = symbol,
                    accountId = accountId,
                    instrumentType = instrumentType
                )
            }
            TradeQuoteType.EXCHANGE_BUY -> mutualFundsTradeQuotesRepo.loadFundsQuote(
                symbol = symbol,
                accountId = accountId
            )
        }
    }

    private fun onSuccess(
        mutualFundsTradeQuotesResult: MutualFundsTradeQuotesResult,
        tradeType: TradeQuoteType
    ) {
        when (tradeType) {
            TradeQuoteType.EXCHANGE_BUY -> _secondaryFundsQuote.postValue(
                ConsumableLiveEvent(
                    mutualFundsTradeQuotesResult
                )
            )
            else -> _primaryFundsQuote.value = ConsumableLiveEvent(mutualFundsTradeQuotesResult)
        }

        loadingIndicator.value = false
        _loadingFailedIndicator.value = false
    }

    private fun onFailure(exception: Throwable) {
        logger.error(exception.localizedMessage)
        exception.localizedMessage?.let { errorMessage.postValue(ConsumableLiveEvent(it)) }
        loadingIndicator.value = false
        _loadingFailedIndicator.value = true
    }
}
