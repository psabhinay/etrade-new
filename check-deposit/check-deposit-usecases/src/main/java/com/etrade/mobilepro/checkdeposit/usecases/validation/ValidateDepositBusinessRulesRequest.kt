package com.etrade.mobilepro.checkdeposit.usecases.validation

data class ValidateDepositBusinessRulesRequest(
    val instNo: String,
    val accountNo: String,
    val depositAmount: String,
    val iraDepositType: String? = null
)
