package com.etrade.mobilepro.holdings.repo

import com.etrade.mobilepro.holdings.dto.HoldingsRequestDto
import com.etrade.mobilepro.holdings.dto.HoldingsResponseDto
import com.etrade.mobilepro.holdings.dto.RequestValue
import com.etrade.mobilepro.holdingsapi.PositionHoldingsItem
import com.etrade.mobilepro.holdingsapi.PositionsHoldingsRepo
import com.etrade.mobilepro.positionsholdingsapi.restapi.HoldingsApiClient
import io.reactivex.Observable

class DefaultPositionsHoldingsRepo(
    private val apiClient: HoldingsApiClient,
    private val mapper: (HoldingsResponseDto) -> PositionHoldingsItem
) : PositionsHoldingsRepo {
    override fun loadHoldings(
        brokerageAccounts: String,
        stockPlanAccounts: String,
        symbol: String
    ): Observable<PositionHoldingsItem> {
        val request = HoldingsRequestDto(RequestValue(brokerageAccounts, stockPlanAccounts, symbol))
        return apiClient
            .getHoldings(request)
            .toObservable()
            .map {
                mapper.invoke(it)
            }
    }
}
