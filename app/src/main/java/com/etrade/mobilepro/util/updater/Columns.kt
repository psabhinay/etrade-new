package com.etrade.mobilepro.util.updater

import android.database.Cursor
import java.io.ByteArrayInputStream
import java.io.File
import java.io.ObjectInputStream

fun Migration.readColumns(databaseFile: File, tableName: String): List<Int> {
    return readTableRows(databaseFile, tableName, listOf("Objectdata")) {
        readColumns(it)
    }
}

private fun readColumns(cursor: Cursor): List<Int> {
    return if (cursor.moveToNext()) {
        deserialize(cursor.getBlob(0))
    } else {
        emptyList()
    }
}

private fun deserialize(array: ByteArray): List<Int> {
    return ObjectInputStream(ByteArrayInputStream(array)).use {
        it.readObject() as List<*>
    }.map { it as Int }
}
