package com.etrade.mobilepro.chart.performance.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0011\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J,\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\r\u001a\u00020\u000bH\u0002J8\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00110\u00100\u000f2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\r\u001a\u00020\u0012H\u0016J\f\u0010\u0013\u001a\u00020\u0011*\u00020\u0014H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/etrade/mobilepro/chart/performance/data/DefaultPerformanceChartRepo;", "Lcom/etrade/mobilepro/chart/performance/data/PerformanceChartRepo;", "retrofit", "Lretrofit2/Retrofit;", "(Lretrofit2/Retrofit;)V", "chartApiClient", "Lcom/etrade/mobilepro/chart/performance/data/PerformanceChartApiClient;", "createRequest", "Lcom/etrade/mobilepro/chart/data/ChartRequestDto;", "accounts", "", "", "indexes", "period", "getPerformanceChart", "Lio/reactivex/Observable;", "Lcom/etrade/mobilepro/util/Resource;", "Lcom/etrade/mobilepro/chart/performance/model/PerformanceChartData;", "Lcom/etrade/mobilepro/chartengine/TimeFrame;", "toPerformanceChartData", "Lcom/etrade/mobilepro/chart/performance/data/PerformanceChartResponseDataDto;", "performance_debug"})
public final class DefaultPerformanceChartRepo implements com.etrade.mobilepro.chart.performance.data.PerformanceChartRepo {
    private final com.etrade.mobilepro.chart.performance.data.PerformanceChartApiClient chartApiClient = null;
    
    @javax.inject.Inject()
    public DefaultPerformanceChartRepo(@org.jetbrains.annotations.NotNull()
    @com.etrade.mobilepro.common.di.MobileTrade()
    retrofit2.Retrofit retrofit) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.reactivex.Observable<com.etrade.mobilepro.util.Resource<com.etrade.mobilepro.chart.performance.model.PerformanceChartData>> getPerformanceChart(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> indexes, @org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.chartengine.TimeFrame period) {
        return null;
    }
    
    private final com.etrade.mobilepro.chart.data.ChartRequestDto createRequest(java.util.List<java.lang.String> accounts, java.util.List<java.lang.String> indexes, java.lang.String period) {
        return null;
    }
    
    private final com.etrade.mobilepro.chart.performance.model.PerformanceChartData toPerformanceChartData(com.etrade.mobilepro.chart.performance.data.PerformanceChartResponseDataDto $this$toPerformanceChartData) {
        return null;
    }
}