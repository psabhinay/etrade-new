package com.etrade.mobilepro.messagecenter.presentation.details.support.adapter

import com.etrade.mobilepro.messagecenter.api.model.SupportMessage

interface SupportMessageItem

internal data class Header(
    val referenceNumber: String = "",
    val status: String = "",
    val accountName: String = "",
    val subject: String = "",
    val topic: String = "",
    val replyEnabled: Boolean = false
) : SupportMessageItem

internal class Reply(
    var content: String = ""
) : SupportMessageItem

internal class Separator : SupportMessageItem

internal class Letter(
    val content: SupportMessage.Communication,
    val hasDivider: Boolean = false
) : SupportMessageItem
