package com.etrade.msaccounts.dto

import com.etrade.completeview.dto.AccountSummarySectionDto
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AggregateValuesViewDto(
    @Json(name = "data")
    override val data: AggregateValuesDto = AggregateValuesDto(),
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>? = null
) : BaseScreenView()

@JsonClass(generateAdapter = true)
data class AggregateValuesDto(
    @Json(name = "net_assets")
    val netAssets: String = "",
    @Json(name = "account_id")
    val accountId: String? = null,
    @Json(name = "ms_accounts_total_assets")
    val msAccountsTotalAssets: AccountSummarySectionDto? = null,
    @Json(name = "ms_accounts_total_liabilities")
    val msAccountsTotalLiabilities: AccountSummarySectionDto? = null
)
