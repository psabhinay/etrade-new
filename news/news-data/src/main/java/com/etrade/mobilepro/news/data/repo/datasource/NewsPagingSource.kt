package com.etrade.mobilepro.news.data.repo.datasource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.data.dto.DocDto
import com.etrade.mobilepro.news.data.dto.NewsDto
import com.etrade.mobilepro.news.data.mapper.TextNewsMapper
import io.reactivex.Observable
import io.reactivex.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.rx2.await
import kotlinx.coroutines.rx2.awaitLast
import org.slf4j.Logger
import org.slf4j.LoggerFactory

abstract class NewsPagingSource(
    private val requestParams: NewsRequest? = null,
    private val headerItemsSource: Observable<List<NewsItem>>? = null
) : PagingSource<Int, NewsItem>() {
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)
    private val textNewsMapper = TextNewsMapper()

    protected abstract fun getNewsRange(
        startIndex: Int,
        size: Int,
        requestParams: NewsRequest? = this.requestParams
    ): Single<NewsDto>

    override fun getRefreshKey(state: PagingState<Int, NewsItem>): Int? {
        return state.anchorPosition?.let {
            state.closestPageToPosition(it)?.nextKey
        }
    }

    @ExperimentalCoroutinesApi
    @Suppress("TooGenericExceptionCaught")
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, NewsItem> {
        return try {
            val newsItems = getNewsRange(params.key ?: 0, params.loadSize).await()
            val isInitialCall = params.key == null
            val docsList = newsItems.data?.newsPayload?.docsList?.docs ?: emptyList()
            val newsItemList = mapPairedNewsList(
                docsList,
                isInitialCall
            )

            val combinedItems = if (isInitialCall) {
                attachFirstItemToHeader(getHeaderItems().awaitLast(), newsItemList.firstOrNull()) + newsItemList
            } else {
                newsItemList
            }

            val nextKey = docsList.size + (params.key ?: 0)
            LoadResult.Page(combinedItems, null, nextKey.takeIf { it > (params.key ?: 0) })
        } catch (ex: Exception) {
            logger.error(ex.message)
            LoadResult.Error(ex)
        }
    }

    private fun mapPairedNewsList(pairedList: List<DocDto>, isInitial: Boolean): List<NewsItem> {
        if (pairedList.isEmpty()) {
            return if (isInitial) {
                listOf(NewsItem.Empty)
            } else {
                emptyList()
            }
        }
        val result = ArrayList<NewsItem>()
        pairedList.forEach { docDto ->
            docDto.textDoc?.let {
                result.add(textNewsMapper.map(it))
            }
        }
        return result
    }

    private fun getHeaderItems(): Observable<List<NewsItem>> =
        headerItemsSource ?: Observable.just(emptyList())

    private fun attachFirstItemToHeader(
        headerItems: List<NewsItem>,
        firstItem: NewsItem?
    ): List<NewsItem> {
        return headerItems.map {
            return@map if (it is NewsItem.CompanyHeader) {
                it.copy(subsequentFirstItem = firstItem)
            } else {
                it
            }
        }
    }
}
