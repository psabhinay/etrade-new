package com.etrade.mobilepro.orders.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AuditInfoDto(
    @Json(name = "createBy")
    val createBy: String,
    @Json(name = "createUtc")
    val createUtc: String,
    @Json(name = "machineName")
    val machineName: String,
    @Json(name = "updateBy")
    val updateBy: String,
    @Json(name = "updateUtc")
    val updateUtc: String
)
