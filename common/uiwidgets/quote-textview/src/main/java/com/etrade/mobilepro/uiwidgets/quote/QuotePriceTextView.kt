package com.etrade.mobilepro.uiwidgets.quote

import android.content.Context
import android.text.SpannableString
import android.text.Spanned
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.util.android.textutil.CustomAlignSuperscriptSpan
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.formatters.formatVolumeCustom
import com.etrade.mobilepro.util.isQuoteSizeValid
import com.etrade.mobilepro.util.safeParseBigDecimal

private const val PRICE_BID_ASK_FORMAT = "%s X"
private const val SIZE_FORMAT = " %s%s"

class QuotePriceTextView : AppCompatTextView {

    constructor(ctx: Context) : this(ctx, null)
    constructor(ctx: Context, attributeSet: AttributeSet?) : this(ctx, attributeSet, 0)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    fun setDetailsViewFormattedPrice(price: String, size: String, format: String) {
        this.text = String.format(format, formatPriceVal(price), size)
    }

    fun setTradeViewFullPriceText(priceVal: String?, sizeVal: String, exchangeCode: String? = null) {
        this.text = getBidAskSpannableText(formatPriceVal(priceVal), sizeVal, exchangeCode)
    }

    fun setTradeViewShortPriceText(priceVal: String?) {
        this.text = formatPriceVal(priceVal)
    }

    private fun formatPriceVal(priceVal: String?): String = MarketDataFormatter.formatMarketDataCustom(priceVal?.safeParseBigDecimal())

    fun formatQuotePrice(quotePrice: String?, quoteSize: String?, quoteExchangeCode: String?) {
        if (quoteSize?.isQuoteSizeValid() == true) {
            val code = quoteExchangeCode ?: ""
            val size = quoteSize.safeParseBigDecimal()?.stripTrailingZeros()
            setTradeViewFullPriceText(
                quotePrice,
                MarketDataFormatter.formatVolumeCustom(size),
                code
            )
        } else {
            setTradeViewShortPriceText(quotePrice)
        }
    }

    private fun getBidAskSpannableText(priceVal: String, sizeVal: String, exchangeCode: String?): SpannableString {
        val price = String.format(PRICE_BID_ASK_FORMAT, priceVal)
        val size = String.format(SIZE_FORMAT, sizeVal, exchangeCode.orEmpty())
        val spannablePrice = SpannableString(price + size)

        // bid ask separator - X font size reduced by factor 1.3f
        spannablePrice.setSpan(
            CustomAlignSuperscriptSpan(0.99F, 1.3f),
            spannablePrice.length - size.length - 1,
            spannablePrice.length - size.length,
            Spanned.SPAN_MARK_MARK
        )

        // ExchangeCode shift top left and font size reduced by factor 1.25f
        if (!exchangeCode.isNullOrBlank()) {
            spannablePrice.setSpan(
                CustomAlignSuperscriptSpan(0.01F, 1.5f),
                spannablePrice.length - 1,
                spannablePrice.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        return spannablePrice
    }
}
