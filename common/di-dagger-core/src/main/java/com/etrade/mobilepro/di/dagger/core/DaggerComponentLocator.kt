package com.etrade.mobilepro.di.dagger.core

interface DaggerComponentLocator : DaggerComponentProvider {

    fun <T : Any> put(componentClass: Class<T>, component: T)

    fun <T : Any> putFactory(componentClass: Class<T>, componentFactory: () -> T)

    fun <T : Any> remove(componentClass: Class<T>)
}
