package com.example.lightstreamer.mergemode

import com.etrade.eo.streaming.UpdateInfo
import com.etrade.mobilepro.streaming.api.Level2DataEvent
import com.etrade.proto.ProtoContainer
import com.etrade.proto.ProtoLevel2BookPriceEntry
import java.util.Base64
import java.util.Objects

class L2BookResponseMapper {
    private val mergeContainers = mutableMapOf<String, ProtoContainer.Builder>()

    fun mapMerge(updateInfo: UpdateInfo): List<Level2DataEvent> {
        val raw = updateInfo.getNewValue("L2_BOOK")
        if (raw.isNullOrEmpty()) {
            return emptyList()
        }
        val decodedBytes = Base64.getDecoder().decode(raw)
        val container = mergeContainers.getOrDefault(updateInfo.getItemName(), ProtoContainer.parseFrom(decodedBytes).toBuilder())
        container.mergeFrom(decodedBytes)
        return if (container.hasLevel2MarketResponse()) {
            container.level2MarketResponse.level2BookList.map { DefaultLevel2DataEvent(it) }
        } else {
            emptyList()
        }
    }

    private class DefaultLevel2DataEvent : Level2DataEvent {
        override val key: String
        override val side: String
        override val price: Double
        override val size: Long
        override val consolidatedSize: Long
        override val marketMakerId: String?
        override val time: Long
        override val consolidateCount: Long

        constructor(entry: ProtoLevel2BookPriceEntry) {
            key = entry.key
            side = entry.side
            price = entry.price
            size = entry.size
            consolidatedSize = entry.consolidatedSize
            marketMakerId = entry.mmid
            time = entry.lastUpdateTime
            consolidateCount = entry.consolidateCount
        }

        constructor(source: Level2DataEvent) {
            key = source.key
            side = source.side
            price = source.price
            size = source.size
            consolidatedSize = source.consolidatedSize
            marketMakerId = source.marketMakerId
            time = source.time
            consolidateCount = source.consolidateCount
        }

        override fun copy(): Level2DataEvent = DefaultLevel2DataEvent(this)

        override fun hashCode(): Int {
            return Objects.hash(key, side, size, price)
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) {
                return true
            }
            if (javaClass != other?.javaClass) {
                return false
            }

            other as Level2DataEvent

            if (key != other.key) {
                return false
            }
            if (side != other.side) {
                return false
            }
            if (size != other.size) {
                return false
            }
            if (price != other.price) {
                return false
            }
            return true
        }
    }
}
