package com.etrade.mobilepro.screener.data.rest

import com.etrade.eo.rest.retrofit.Scalar
import com.etrade.mobilepro.screener.data.rest.dto.FundCategoryResponse
import com.etrade.mobilepro.screener.data.rest.dto.PrescreenResultResponse
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import retrofit2.http.Url

interface ScreenerApiClient {
    @JsonMoshi
    @GET("/Research/Screener/GetDistinctValues")
    fun getFundCategory(
        @Query("resultSafename") resultSafeName: String = "FundCategory",
        @Query("filterSafename") filterSafeName: String = "SuperCategory",
        @Query("screenType") screenType: String,
        @Query("filterValue") filterValue: String
    ): Single<FundCategoryResponse>

    @JsonMoshi
    @GET
    fun getStockPrescreen(@Url complexRequest: String): Single<PrescreenResultResponse>

    @JsonMoshi
    @POST
    fun getMFPrescreen(@Url url: String, @Body body: RequestBody): Single<PrescreenResultResponse>

    @JsonMoshi
    @POST
    fun getETFPrescreen(@Url complexRequest: String): Single<PrescreenResultResponse>

    @Scalar
    @POST
    fun getStockResults(@Url complexRequest: String): Single<String>

    @Scalar
    @POST
    fun getMfResults(@Url url: String, @Body body: RequestBody): Single<String>

    @Scalar
    @POST
    fun getEtfResults(@Url complexRequest: String): Single<String>
}
