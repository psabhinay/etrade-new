package com.etrade.mobilepro.dagger.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class AmericanDateOnly
