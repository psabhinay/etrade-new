package com.etrade.mobilepro.pdf.presentation

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

interface PdfViewModelFactoryFactory {
    fun create(url: String?): ViewModelProvider.Factory
}

class PdfViewModelFactoryFactoryImp @Inject constructor(
    private val factory: PdfViewModel.Factory
) : PdfViewModelFactoryFactory {
    override fun create(url: String?): ViewModelProvider.Factory =
        viewModelFactory { factory.create(url) }
}
