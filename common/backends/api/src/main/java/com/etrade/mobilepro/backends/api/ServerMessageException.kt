package com.etrade.mobilepro.backends.api

class ServerMessageException(val messages: Collection<ServerMessage>) : Exception() {

    constructor(message: ServerMessage) : this(listOf(message))

    init {
        require(messages.isNotEmpty()) {
            "Exception must have at least one message."
        }
    }
}
