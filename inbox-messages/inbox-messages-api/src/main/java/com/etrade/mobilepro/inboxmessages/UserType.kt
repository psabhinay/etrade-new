package com.etrade.mobilepro.inboxmessages

import com.etrade.mobilepro.account.dao.Account

enum class UserType(val type: String) {

    /**
     Given User has not yet logged in
     */
    TYPE_1("1"),

    // Single Account Scenarios

    /**
     Given User has has logged in
     And User holds only 1 account
     And User holds a self-directed brokerage account
     */
    TYPE_2("2"),

    /**
     Given User has has logged in
     And User holds only 1 account
     And User holds a self-directed retirement account
     */
    TYPE_3("3"),

    /**
     Given User has has logged in
     And User holds only 1 account
     And User holds a bank account
     */
    TYPE_4("4"),

    /**
     Given User has has logged in
     And User holds only 1 account
     And User holds a core (managed) account
     */
    TYPE_5("5"),

    // Multi-Account Scenarios

    /**
     Given User has has logged in
     And User does not hold a self-directed brokerage account, considering linked accounts
     */
    TYPE_6("6"),

    /**
     Given User has has logged in
     And User does hold multiple (2 or more) self-directed brokerage accounts, considering linked accounts
     And User does not hold a bank account, considering linked accounts
     */
    TYPE_7("7"),

    /**
     Given User has has logged in
     And User does hold a +self-directed brokerage account, considering linked accounts+
     And User does hold a bank account, considering linked accounts
     And User does not hold a core (managed) account, considering linked accounts
     */
    TYPE_8("8"),

    /**
     Given User has has logged in
     And User does hold a +self-directed brokerage account, considering linked accounts+
     And User does hold a bank account, considering linked accounts
     And User does hold a core (managed) account considering linked accounts
     And User does not hold a self-directed retirement account, considering linked accounts
     */
    TYPE_9("9"),

    /**
     Given User has has logged in
     And User does hold a brokerage account, considering linked accounts
     And User does hold a bank account, considering linked accounts
     And User does hold a managed account, considering linked accounts
     And User does hold a retirement account, considering linked accounts
     */
    TYPE_10("10");

    companion object {
        @Suppress("ComplexMethod")
        fun convert(type: String?): List<UserType>? {
            return type?.split(",")?.map {
                when (it) {
                    "1" -> TYPE_1
                    "2" -> TYPE_2
                    "3" -> TYPE_3
                    "4" -> TYPE_4
                    "5" -> TYPE_5
                    "6" -> TYPE_6
                    "7" -> TYPE_7
                    "8" -> TYPE_8
                    "9" -> TYPE_9
                    "10" -> TYPE_10
                    else -> return null
                }
            }
        }
    }
}

@Suppress("ComplexMethod")
fun List<Account>.userType(): UserType {
    val hasManagedAccount = any { it.isManagedAccount() }
    val hasBankAccount = any { it.isBankAccount() }
    val brokerageAccounts = filter { it.isBrokerageAccount() }
    val hasBrokerageAccount = brokerageAccounts.isNotEmpty()
    val retirementAccounts = filter { it.isRetirementAccount() }
    val hasRetirementAccount = retirementAccounts.isNotEmpty()

    return when {
        hasBrokerageAccount && hasBankAccount && hasManagedAccount && hasRetirementAccount -> UserType.TYPE_10
        hasBrokerageAccount && hasBankAccount && hasManagedAccount -> UserType.TYPE_9
        hasBrokerageAccount && hasBankAccount -> UserType.TYPE_8
        brokerageAccounts.size > 1 -> UserType.TYPE_7
        !hasBrokerageAccount && size > 1 -> UserType.TYPE_6
        hasManagedAccount && size == 1 -> UserType.TYPE_5
        hasBankAccount && size == 1 -> UserType.TYPE_4
        hasRetirementAccount && size == 1 -> UserType.TYPE_3
        hasBrokerageAccount && size == 1 -> UserType.TYPE_2
        else -> UserType.TYPE_1
    }
}
