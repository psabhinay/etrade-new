package com.etrade.mobilepro.streaming.lightstreamer.level1

import com.etrade.eo.streaming.UpdateInfo
import com.etrade.mobilepro.util.toNullIfBlank
import org.threeten.bp.LocalDate

// Reference
// https://confluence.corp.etradegrp.com/display/INITIATIVES/Vela+-+Streaming+Level1
enum class Level1Column(val column: String) {
    /** Tot. Volume, long  */
    ACCUM_VOLUME("ACCUM_VOLUME"),
    /** Ask, double	 */
    ASK("ASK"),
    /** Ask Exchange, String  */
    ASK_EXCHANGE("ASK_EXCHANGE"),
    /** Ask Size, double  */
    ASK_SIZE("ASK_SIZE"),
    /** Risk measure of a stock's volatility relative to the market as a whole from Reuters, double  */
    BETA("BETA"),
    /** Bid, double  */
    BID("BID"),
    /** Bid Exchange, String  */
    BID_EXCHANGE("BID_EXCHANGE"),
    /** Bid Size, double  */
    BID_SIZE("BID_SIZE"),
    /** Closing Mark  */
    CLOSING_MARK("CLOSING_MARK"),
    /** Delta, double */
    DELTA("DELTA"),
    /** Dividend rate, double */
    DIVIDEND_RATE("DIVIDEND_RATE"),
    /** Dividend Yield %, double */
    DIVIDEND_YIELD("YIELD"),
    /** Earnings per share from reference data */
    EPS("EARNINGS_PER_SHARE"),
    /** Exchange code of Last Trade, String  */
    EXCHANGE_CODE("EXCHANGE_CODE"),
    /** Exchange code of last extended hours trade, String */
    EXT_HOURS_EXCHANGE_CODE("EXT_HOURS_EXCHANGE_CODE"),
    /** Ext. Hours Last Change, long  */
    EXT_HOURS_LAST_CHANGE("EXT_HOURS_LAST_CHANGE"),
    /** Ext. Hours Last Change Percentage */
    EXT_HOURS_LAST_CHANGE_PCT("EXT_HOURS_LAST_CHANGE_PCT"),
    /** Ext. Hours Last, double  */
    EXT_HOURS_LAST("EXT_HOURS_LAST"),
    /** Ext. Hours Last Traded Volume, Long **/
    EXT_HOURS_VOL("EXT_HOURS_VOL"),
    /** Gamma, double */
    GAMMA("GAMMA"),
    /** Today's highest transaction value from Reuters, double */
    HIGH("HIGH"),
    /** Implied volatility, double */
    IMPLIED_VOLATILITY("IMP_VOLATILITY"),
    /** Intrinsic Value, double */
    INTRINSIC_VALUE("INTR_VAL"),
    /** Last Trade, double  */
    LAST("LAST"),
    /** Last Trade size, long */
    LAST_VOLUME("LAST_VOLUME"),
    /** ? */
    LAST_CHANGE("LAST_CHANGE"),
    /** ? */
    LAST_CHANGE_PCT("LAST_CHANGE_PCT"),
    /** Trade Time, java time, long  */
    LAST_TIME_MS("LAST_TIME_MS"),
    /** Today's lowest transaction value from Reuters, double */
    LOW("LOW"),
    /** Mark, double  */
    MARK("MARK"),
    /** Market Cap, long */
    MARKET_CAP("MARKET_CAP"),
    /** State, String  */
    MARKET_STATUS("MARKET_STATUS"),
    /** Open price, double */
    OPEN("OPEN"),
    /** Open interest, double */
    OPEN_INTEREST("OPEN_INTEREST"),
    /** Previous close, double */
    PREVIOUS_CLOSE("PREV_CLOSE"),
    /** Primary exchange code, String */
    PRIMARY_EXCHANGE("PRIMARY_EXCHANGE"),
    /** Rho, double */
    RHO("RHO"),
    /** Sale Time used during extended hours, java time, long  */
    SALE_TIME_MS("SALE_TIME_MS"),
    /** Tick indicator, String  */
    TICK_INDICATOR("TICK_INDICATOR"),
    /** Trade status, String  */
    TRADE_STATUS("TRADE_STATUS"),
    /** Trade halt reason code, String  */
    // https://confluence.corp.etradegrp.com/display/INITIATIVES/Trading+Halt+Reason
    TRADE_HALT_REASON("TRADE_HALT_REASON"),
    /** Theta, double */
    THETA("THETA"),
    /** Vega, double */
    VEGA("VEGA"),
    /** Ten day average volume from reference data, double */
    VOLUME_TEN_DAY_AVERAGE("TENDAY_AVERAGE_VOLUME"),
    /** 52 week High, double  */
    YEAR_HIGH("YEAR_HIGH"),
    /** Date of year high, Date - Equity/Option -DD MON YYYY, Futures YYYY-MM-DD */
    YEAR_HIGH_DATE("YEAR_HIGH_DATE"),
    /** 52 Week Low, double  */
    YEAR_LOW("YEAR_LOW"),
    /** Date of year low, Date - Equity/Option -DD MON YYYY, Futures YYYY-MM-DD */
    YEAR_LOW_DATE("YEAR_LOW_DATE")
}

internal fun UpdateInfo.getValue(column: Level1Column): String? = getNewValue(column).toNullIfBlank()

internal fun UpdateInfo.getTickIndicator(dependency: Level1Column): Short? =
    runCatching {
        if (isValueChanged(Level1Column.ACCUM_VOLUME.column)) {
            if (!isValueChanged(dependency.column)) {
                return 0
            }
            when (getNewValue(Level1Column.TICK_INDICATOR)) {
                "+" -> 1
                "-" -> -1
                else -> 0
            }
        } else {
            null
        }?.toShort()
    }.getOrNull()

internal fun UpdateInfo.getYearDate(dependency: Level1Column): LocalDate? = getValue(dependency)?.let { YearDateParser.parse(it) }

// Use this one all times, don't use getNewValue(String), will lead to crash if called on a column you didn't subscribe to.
private fun UpdateInfo.getNewValue(column: Level1Column): String? = runCatching { getNewValue(column.column) }.getOrNull()

internal fun UpdateInfo.getMarketHrsTimeValue(): Long? {
    var time = getValue(Level1Column.SALE_TIME_MS)

    if (time.isNullOrEmpty()) {
        time = getValue(Level1Column.LAST_TIME_MS)
    }

    return time?.toLongOrNull()
}

internal fun UpdateInfo.getTimeValue() = getValue(Level1Column.SALE_TIME_MS)?.toLongOrNull()
