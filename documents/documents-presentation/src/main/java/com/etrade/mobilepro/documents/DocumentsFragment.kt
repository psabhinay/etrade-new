package com.etrade.mobilepro.documents

import android.os.Bundle
import android.util.Range
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.eo.corelibandroid.createViewModel
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.ScrollOnTopListener
import com.etrade.mobilepro.documents.DocumentsViewModel.PageType
import com.etrade.mobilepro.documents.adapter.DocumentsRecyclerViewAdapter
import com.etrade.mobilepro.documents.databinding.DocumentsFragmentBinding
import com.etrade.mobilepro.documents.router.DocumentArgumentsHelper
import com.etrade.mobilepro.documents.router.DocumentsRouter
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.addDividerItemDecoration
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.formatters.americanSimpleDateFormatter
import com.google.android.material.snackbar.Snackbar
import org.threeten.bp.LocalDate
import javax.inject.Inject

private const val DOCUMENTS_TYPE_TAG = "documents_type_tag"

const val DOCUMENTS_TAX_YEAR = "DocumentsFragment:documents_tax_year"
private const val DOCUMENTS_START_DATE = "DocumentsFragment:documents_start_date"
private const val DOCUMENTS_END_DATE = "DocumentsFragment:documents_end_date"

@RequireLogin
@Suppress("LargeClass")
class DocumentsFragment @Inject constructor(
    private val viewModelFactory: DocumentsViewModel.Factory,
    private val router: DocumentsRouter,
    private val argumentsHelper: DocumentArgumentsHelper,
    private val snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.documents_fragment), ScrollOnTopListener {

    private val binding by viewBinding(DocumentsFragmentBinding::bind) { onDestroyBinding() }

    private fun onDestroyBinding() {
        binding.swipeRefresh.setOnRefreshListener(null)
        binding.itemsRv.adapter = null
    }

    private val viewModel: DocumentsViewModel by createViewModel {
        val accountUuid = argumentsHelper.getAccountUuid(arguments)
        var pageType = PageType.ACCOUNT_DETAIL_DOCUMENTS_PAGE
        var documentType = argumentsHelper.getDocumentType(arguments)
        if (accountUuid.isNullOrEmpty()) {
            pageType = PageType.MENU_TAX_PAGE
            documentType = DocumentType.TAX_DOCS
        }
        viewModelFactory.create(
            accountUuid = accountUuid.orEmpty(),
            pageType = pageType,
            defaultDocumentType = documentType
        )
    }
    private lateinit var accountsSelector: DropDownMultiSelector<Account>
    private lateinit var documentsTypeSelector: DropDownMultiSelector<DocumentType>
    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private var snackBar: Snackbar? = null
    private val itemsAdapter: DocumentsRecyclerViewAdapter
        get() = binding.itemsRv.adapter as DocumentsRecyclerViewAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val accountsContainer = view.findViewById<ConstraintLayout>(R.id.accounts_container)
        val typeContainer = view.findViewById<ConstraintLayout>(R.id.type_container)
        val selectorTypeValue = typeContainer.findViewById<TextView>(R.id.selector_value)
        val selectorAccountValue = accountsContainer.findViewById<TextView>(R.id.selector_value)

        binding.retryButton.setOnClickListener { viewModel.load() }
        if (viewModel.pageType == PageType.ACCOUNT_DETAIL_DOCUMENTS_PAGE) {
            accountsContainer.visibility = View.INVISIBLE
            typeContainer.visibility = View.VISIBLE
            typeContainer.findViewById<TextView>(R.id.selector_label).text =
                getString(R.string.documents_type)
            setupDocumentsTypeDropDownLists()
            setupDocumentTypeListeners(selectorTypeValue)
        } else {
            accountsContainer.visibility = View.VISIBLE
            typeContainer.visibility = View.INVISIBLE
            accountsContainer.findViewById<TextView>(R.id.selector_label).text =
                getString(R.string.documents_account)
            setupAccountsDropDownLists()
            setupAccountsListeners(selectorAccountValue)
        }
        bindViewModel(selectorTypeValue, selectorAccountValue)
        setupRecyclerView()
        setupDocumentsFilterValuesObserver()
    }

    override val isOnTop: Boolean get() = binding.itemsRv.computeVerticalScrollOffset() == 0

    override fun scrollUp(): Unit = with(binding.itemsRv) {
        stopScroll()
        smoothScrollToPosition(0)
    }

    @Suppress("LongMethod")
    private fun setupDocumentsFilterValuesObserver() {
        val navController = findNavController()
        // https://developer.android.com/guide/navigation/navigation-programmatic#returning_a_result
        val navBackStackEntry = if (viewModel.pageType == PageType.ACCOUNT_DETAIL_DOCUMENTS_PAGE) {
            navController.getBackStackEntry(R.id.accountTabHostFragment)
        } else {
            navController.getBackStackEntry(R.id.documents_fragment)
        }
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_RESUME) {
                handleDocumentsFilterValue(
                    navBackStackEntry.savedStateHandle,
                    DOCUMENTS_TAX_YEAR,
                    viewModel::updateTaxYear
                )
                handleDocumentsFilterValue(
                    navBackStackEntry.savedStateHandle,
                    DOCUMENTS_START_DATE,
                    viewModel::updateStartDate
                )
                handleDocumentsFilterValue(
                    navBackStackEntry.savedStateHandle,
                    DOCUMENTS_END_DATE,
                    viewModel::updateEndDate
                )
            }
        }
        navBackStackEntry.lifecycle.addObserver(observer)

        viewLifecycleOwner.lifecycle.addObserver(
            LifecycleEventObserver { _, event ->
                if (event == Lifecycle.Event.ON_DESTROY) {
                    navBackStackEntry.lifecycle.removeObserver(observer)
                }
            }
        )
    }

    private fun <T> handleDocumentsFilterValue(
        savedStateHandle: SavedStateHandle,
        key: String,
        action: (T) -> Unit
    ) {
        savedStateHandle.remove<T>(key)?.let(action)
    }

    private fun setupDocumentTypeListeners(selectorValue: TextView) {
        selectorValue.setOnClickListener {
            viewModel.cancelPendingNavigation()
            documentsTypeSelector.open()
        }
        binding.swipeRefresh.setOnRefreshListener { viewModel.load() }
    }

    private fun setupAccountsListeners(selectorValue: TextView) {
        selectorValue.setOnClickListener {
            viewModel.cancelPendingNavigation()
            accountsSelector.open()
        }
        binding.swipeRefresh.setOnRefreshListener { viewModel.load() }
    }

    @Suppress("LongMethod")
    private fun bindViewModel(selectorTypeValue: TextView, selectorAccountValue: TextView) {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is DocumentsViewModel.ViewState.Loading -> handleLoadingState()
                    is DocumentsViewModel.ViewState.Success -> handleSuccessState(
                        it.documents,
                        it.emptyText
                    )
                    is DocumentsViewModel.ViewState.Failed -> handleFailedState()
                }
            }
        )
        viewModel.documentsFilterState.observe(
            viewLifecycleOwner,
            Observer {
                selectorTypeValue.text = getString(it.type.getLabel())
                if (viewModel.pageType == PageType.ACCOUNT_DETAIL_DOCUMENTS_PAGE) {
                    handleFilterState(it.type, it.taxYear, it.dateRange)
                }
            }
        )

        viewModel.accountsFilterState.observe(
            viewLifecycleOwner,
            Observer {
                selectorAccountValue.text = it.accountDisplayName
                if (viewModel.pageType == PageType.MENU_TAX_PAGE) {
                    handleFilterState(it.type, it.taxYear)
                }
            }
        )

        viewModel.navSignal.observe(
            viewLifecycleOwner,
            Observer { event ->
                event.consume {
                    router.navigateToDetails(
                        findNavController(),
                        it.document
                    )
                }
            }
        )
        viewModel.errorSignal.observe(
            viewLifecycleOwner,
            Observer {
                it.consume {
                    snackBar?.dismiss()
                    snackBar = snackBarUtil.errorSnackbar()
                }
            }
        )
    }

    private fun setupRecyclerView() {
        binding.itemsRv.apply {
            layoutManager = LinearLayoutManager(requireContext())
            addDividerItemDecoration(R.drawable.thin_divider)
            adapter = DocumentsRecyclerViewAdapter {
                when (it.status) {
                    StatefulDocument.Status.Loading -> {
                        /* nothing */
                    }
                    StatefulDocument.Status.Available -> router.navigateToDetails(
                        findNavController(),
                        it.document
                    )
                    StatefulDocument.Status.Unavailable -> viewModel.loadDocument(it)
                    is StatefulDocument.Status.Failed -> it.status.retry()
                }
            }
        }
    }

    @Suppress("LongMethod")
    private fun handleFilterState(
        type: DocumentType,
        taxYear: Int,
        dateRange: Range<LocalDate>? = null
    ) {
        when (type) {
            DocumentType.STATEMENTS -> {
                binding.dateRangeSelector.apply {
                    dateRangeLabel.text = getString(R.string.documents_filter_date_range)

                    dateRangeEndDate.visibility = View.VISIBLE
                    dateRangeDivider.visibility = View.VISIBLE

                    dateRangeStartDate.text =
                        americanSimpleDateFormatter.format(dateRange?.lower)
                    dateRangeEndDate.text = americanSimpleDateFormatter.format(dateRange?.upper)

                    setupDateRangeSelector(
                        dateRangeStartDate,
                        viewModel.startDate,
                        DOCUMENTS_START_DATE,
                        viewModel.minYear,
                        viewModel.endDate
                    )
                    setupDateRangeSelector(
                        dateRangeEndDate,
                        viewModel.endDate,
                        DOCUMENTS_END_DATE,
                        viewModel.startDate,
                        null
                    )
                }
            }
            DocumentType.TAX_DOCS -> {
                binding.dateRangeSelector.apply {
                    dateRangeLabel.text = getString(R.string.documents_filter_tax_year)

                    dateRangeEndDate.visibility = View.GONE
                    dateRangeDivider.visibility = View.GONE

                    dateRangeStartDate.text = taxYear.toString()
                }
                setupTaxYearSelector()
            }
        }
    }

    private fun handleLoadingState() {
        binding.emptyListText.visibility = View.GONE
        binding.errorLayout.visibility = View.GONE

        snackBar?.dismiss()

        if (!binding.swipeRefresh.isRefreshing) {
            binding.contentProgress.show()
        }
    }

    private fun handleSuccessState(items: List<StatefulDocument>, emptyText: String) {
        if (items.find { it.status is StatefulDocument.Status.Failed } == null) {
            snackBar?.dismiss()
        }
        binding.apply {
            contentProgress.hide()
            errorLayout.visibility = View.GONE
            swipeRefresh.isRefreshing = false

            if (items.isEmpty()) {
                emptyListText.text = emptyText
                emptyListText.visibility = View.VISIBLE
                itemsRv.visibility = View.GONE
            } else {
                emptyListText.visibility = View.GONE
                itemsRv.visibility = View.VISIBLE
            }
        }
        itemsAdapter.updateItems(items)
    }

    private fun handleFailedState() {
        binding.apply {
            contentProgress.hide()
            swipeRefresh.isRefreshing = false
            errorLayout.visibility = View.VISIBLE
        }
    }

    private fun setupDocumentsTypeDropDownLists() {
        documentsTypeSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = DOCUMENTS_TYPE_TAG,
                title = getString(R.string.documents_type),
                multiSelectEnabled = false,
                itemsData = viewModel.documentsTypeSelectorData,
                selectListener = { _, item ->
                    viewModel.updateDocumentsType(item.value)
                }
            )
        )
    }

    private fun setupAccountsDropDownLists() {
        accountsSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = DOCUMENTS_TYPE_TAG,
                title = getString(R.string.documents_account),
                multiSelectEnabled = false,
                itemsData = viewModel.accountSelectorData,
                selectListener = { _, item: DropDownMultiSelector.Selectable<Account> ->
                    viewModel.updateAccountUuid(item.value.accountUuid)
                }
            )
        )
    }

    private fun setupDateRangeSelector(
        view: View,
        date: LocalDate,
        valueKey: String,
        minDate: LocalDate?,
        maxDate: LocalDate?
    ) {
        view.setOnClickListener {
            viewModel.cancelPendingNavigation()
            router.openDateRangeSelector(findNavController(), date, valueKey, minDate, maxDate)
        }
    }

    private fun setupTaxYearSelector() {
        binding.dateRangeSelector.dateRangeStartDate.setOnClickListener {
            viewModel.cancelPendingNavigation()
            router.openTaxYearSelector(findNavController(), viewModel.minYear.year, LocalDate.now().year, viewModel.taxYear)
        }
    }
}
