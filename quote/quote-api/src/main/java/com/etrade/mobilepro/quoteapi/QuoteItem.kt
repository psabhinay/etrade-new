package com.etrade.mobilepro.quoteapi

import androidx.annotation.ColorRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.livedata.DistinctLiveData
import com.etrade.mobilepro.livedata.postUpdateValue
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.OptionData
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.safeParseBigDecimal
import org.threeten.bp.LocalDate
import java.math.BigDecimal

private const val MAX_MOMENTUM_SIZE = 500

sealed class QuoteItem {
    class QuoteRangeBarItem(
        initialRangeBarData: QuoteRangeBarData,
        private val week52DateFormatter: (DateStruct) -> String
    ) : QuoteItem(), QuoteItemInterface {

        val rangeBarData: LiveData<QuoteRangeBarData>
            get() = _rangeBarData

        private val _rangeBarData: MutableLiveData<QuoteRangeBarData> = DistinctLiveData(initialRangeBarData)

        override fun updateItem(data: Level1Data, isExtendedHours: Boolean) {
            _rangeBarData.updateWith {
                it.copy(
                    week52High = data.yearHigh ?: it.week52High,
                    week52Low = data.yearLow ?: it.week52Low,
                    dayHigh = data.dayHigh ?: it.dayHigh,
                    dayLow = data.dayLow ?: it.dayLow,
                    week52HighDate = data.yearHighDate?.let { date ->
                        formatWeek52Date(date)
                    } ?: it.week52HighDate,
                    week52LowDate = data.yearLowDate?.let { date ->
                        formatWeek52Date(date)
                    } ?: it.week52LowDate
                )
            }
        }

        private fun formatWeek52Date(date: LocalDate) = week52DateFormatter(
            DateStruct(
                day = date.dayOfMonth.toString(),
                month = date.monthValue.toString(),
                year = date.year.toString()
            )
        )
    }

    class QuoteDetailItem(
        val symbol: String,
        initialOpenPrice: String,
        initialPrevClosePrice: String,
        initialAverageVolume: String,
        initialPriceToEarnings: String,
        initialEarningsPerShare: String,
        val nextEarningsDate: String,
        initialMarketCap: String,
        val sharesOutstanding: String,
        initialBeta: String,
        val thirtyDayYield: String?,
        val isThirtyDayYieldVisible: Boolean,
        initialDividendYield: String,
        initialDividend: String,
        val exDividendDate: String,
        val dividendPayableDate: String,
        val exchangeCode: String?,
        val sector: LiveData<String?>,
        val nextEarningsDateRaw: Long?
    ) : QuoteItem(), QuoteItemInterface {

        val openPrice: LiveData<String>
            get() = _openPrice
        val prevClosePrice: LiveData<String>
            get() = _prevClosePrice
        val averageVolume: LiveData<String>
            get() = _averageVolume
        val priceToEarnings: LiveData<String>
            get() = _priceToEarnings
        val earningsPerShare: LiveData<String>
            get() = _earningsPerShare
        val marketCap: LiveData<String>
            get() = _marketCap
        val beta: LiveData<String>
            get() = _beta
        val dividendYield: LiveData<String>
            get() = _dividendYield
        val dividend: LiveData<String>
            get() = _dividend

        private val _openPrice: MutableLiveData<String> = DistinctLiveData(initialOpenPrice)
        private val _prevClosePrice: MutableLiveData<String> = DistinctLiveData(initialPrevClosePrice)
        private val _averageVolume: MutableLiveData<String> = DistinctLiveData(initialAverageVolume)
        private val _priceToEarnings: MutableLiveData<String> = DistinctLiveData(initialPriceToEarnings)
        private val _earningsPerShare: MutableLiveData<String> = DistinctLiveData(initialEarningsPerShare)
        private val _marketCap: MutableLiveData<String> = DistinctLiveData(initialMarketCap)
        private val _beta: MutableLiveData<String> = DistinctLiveData(initialBeta)
        private val _dividendYield: MutableLiveData<String> = DistinctLiveData(initialDividendYield)
        private val _dividend: MutableLiveData<String> = DistinctLiveData(initialDividend)

        override fun updateItem(data: Level1Data, isExtendedHours: Boolean) {
            if (data is StockData) {
                _openPrice.postValue(data.openPrice)
                _prevClosePrice.postValue(data.previousClosePrice)
                _averageVolume.postValue(data.volumeTenDayAverage)
                _priceToEarnings.postValue(data.priceToEarnings)
                _earningsPerShare.postValue(data.earningsPerShare)
                _marketCap.postValue(data.marketCap)
                _beta.postValue(data.beta)
                _dividendYield.postValue(data.dividendYield)
                _dividend.postValue(data.dividend)
            }
        }
    }

    class QuoteOptionDetailItem(
        initialDayHigh: String,
        initialDayLow: String,
        initialOpenPrice: String,
        initialPrevClosePrice: String,
        val daysToTrade: String,
        initialOpenInterest: String,
        initialIntrinsicValue: String,
        initialTimePremium: String,
        val deliverables: String,
        initialDelta: String,
        initialGamma: String,
        initialTheta: String,
        initialVega: String,
        initialRho: String,
        initialImpliedVolatility: String
    ) : QuoteItem(), QuoteItemInterface {

        val dayHigh: LiveData<String>
            get() = _dayHigh
        val dayLow: LiveData<String>
            get() = _dayLow
        val openPrice: LiveData<String>
            get() = _openPrice
        val prevClosePrice: LiveData<String>
            get() = _prevClosePrice
        val openInterest: LiveData<String>
            get() = _openInterest
        val intrinsicValue: LiveData<String>
            get() = _intrinsicValue
        val delta: LiveData<String>
            get() = _delta
        val gamma: LiveData<String>
            get() = _gamma
        val theta: LiveData<String>
            get() = _theta
        val vega: LiveData<String>
            get() = _vega
        val rho: LiveData<String>
            get() = _rho
        val impliedVolatility: LiveData<String>
            get() = _impliedVolatility

        private val _dayHigh: MutableLiveData<String> = DistinctLiveData(initialDayHigh)
        private val _dayLow: MutableLiveData<String> = DistinctLiveData(initialDayLow)
        private val _openPrice: MutableLiveData<String> = DistinctLiveData(initialOpenPrice)
        private val _prevClosePrice: MutableLiveData<String> = DistinctLiveData(initialPrevClosePrice)
        private val _openInterest: MutableLiveData<String> = DistinctLiveData(initialOpenInterest)
        private val _intrinsicValue: MutableLiveData<String> = DistinctLiveData(initialIntrinsicValue)
        private val _delta: MutableLiveData<String> = DistinctLiveData(initialDelta)
        private val _gamma: MutableLiveData<String> = DistinctLiveData(initialGamma)
        private val _theta: MutableLiveData<String> = DistinctLiveData(initialTheta)
        private val _vega: MutableLiveData<String> = DistinctLiveData(initialVega)
        private val _rho: MutableLiveData<String> = DistinctLiveData(initialRho)
        private val _impliedVolatility: MutableLiveData<String> = DistinctLiveData(initialImpliedVolatility)
        private val _bid: MutableLiveData<String> = DistinctLiveData()
        private val _ask: MutableLiveData<String> = DistinctLiveData()

        val timePremium: LiveData<String> = MediatorLiveData<String>().apply {
            var bid: String? = null
            var ask: String? = null
            var intrinsicValue: String? = null

            val calculate = {
                value = if (bid == null || ask == null || intrinsicValue == null) {
                    MarketDataFormatter.formatMarketDataCustom(initialTimePremium.safeParseBigDecimal())
                } else {
                    try {
                        MarketDataFormatter.formatMarketData((BigDecimal(bid) + BigDecimal(ask)) / BigDecimal.valueOf(2L) - BigDecimal(intrinsicValue))
                    } catch (e: NumberFormatException) {
                        initialImpliedVolatility
                    }
                }
            }

            addSource(_bid) {
                bid = it
                calculate()
            }

            addSource(_ask) {
                ask = it
                calculate()
            }

            addSource(_intrinsicValue) {
                intrinsicValue = it
                calculate()
            }
        }

        override fun updateItem(data: Level1Data, isExtendedHours: Boolean) {
            _dayHigh.postValue(data.dayHigh)
            _dayLow.postValue(data.dayLow)
            _openPrice.postValue(data.openPrice)
            _prevClosePrice.postValue(data.previousClosePrice)

            if (data is OptionData) {
                _openInterest.postValue(data.openInterest)
                _intrinsicValue.postValue(data.intrinsicValue)
                _delta.postValue(data.delta)
                _gamma.postValue(data.gamma)
                _theta.postValue(data.theta)
                _vega.postValue(data.vega)
                _rho.postValue(data.rho)
                _impliedVolatility.postValue(data.impliedVolatility)
                data.bid?.let { _bid.postValue(it) }
                data.ask?.let { _ask.postValue(it) }
            }
        }
    }

    class ClosingPriceItem(
        val title: String,
        val underlyingSymbol: String,
        initialLastPrice: QuotePriceWidgetItem,
        initialChange: QuoteChangeWidgetItem,
        @ColorRes initialChangeColor: Int,
        initialVolume: String,
        initialLastTradeTime: String?,
        private val resources: QuoteTemplateResources,
        val isOption: Boolean
    ) : QuoteItem(), QuoteItemInterface {

        val lastPrice: LiveData<QuotePriceWidgetItem>
            get() = _lastPrice
        val change: LiveData<QuoteChangeWidgetItem>
            get() = _change
        val changeColor: LiveData<Int>
            get() = _changeColor
        val volume: LiveData<String>
            get() = _volume
        val lastTradeTime: LiveData<String>
            get() = _lastTradeTime

        private val _lastPrice: MutableLiveData<QuotePriceWidgetItem> = DistinctLiveData(initialLastPrice)
        private val _change: MutableLiveData<QuoteChangeWidgetItem> = DistinctLiveData(initialChange)
        private val _changeColor: MutableLiveData<Int> = DistinctLiveData(initialChangeColor)
        private val _volume: MutableLiveData<String> = DistinctLiveData(initialVolume)
        private val _lastTradeTime: MutableLiveData<String> = DistinctLiveData(initialLastTradeTime)

        override fun updateItem(data: Level1Data, isExtendedHours: Boolean) {
            if (isExtendedHours.not()) {
                _lastPrice.updateWith { data.toLastPrice(it, resources) }
                _change.updateWith { data.toChange(it) }
                _changeColor.updateWith { data.toChangeColor(it, resources) }

                data.volume?.let { volume -> _volume.postValue(volume) }

                data.timestampSinceEpoch?.let { timestampSinceEpoch ->
                    _lastTradeTime.postValue(DateFormattingUtils.formatFullShortTimeDateWithSlashes(timestampSinceEpoch))
                }
            }
        }
    }

    data class NewsRetry(val errorMessage: String) : QuoteItem(), QuoteItemInterface
    data class QuoteChainItem(val symbol: SearchResultItem.Symbol) : QuoteItem(), QuoteItemInterface
    data class HeaderButtons(val type: InstrumentType) : QuoteItem(), QuoteItemInterface
    data class QuoteChartItem(val symbol: SearchResultItem.Symbol) : QuoteItem(), QuoteItemInterface
    object Empty : QuoteItem(), QuoteItemInterface
    object QuoteFooterItem : QuoteItem(), QuoteItemInterface
    object QuoteEtfLegalFooterItem : QuoteItem(), QuoteItemInterface
    object QuoteDisclosureItem : QuoteItem(), QuoteItemInterface
    object QuoteEtfDisclosureItem : QuoteItem(), QuoteItemInterface
    object QuoteNewsFooterItem : QuoteItem(), QuoteItemInterface
    object NewsLoading : QuoteItem(), QuoteItemInterface
    object ThinGutter : QuoteItemInterface
    object ThickGutter : QuoteItemInterface
    object QuoteHoldings : QuoteItemInterface

    class QuoteMomentumBar : QuoteItemInterface {
        var bufferSize = MAX_MOMENTUM_SIZE

        val momentum: LiveData<List<Short>>
            get() = _momentum

        private val _momentum: MutableLiveData<List<Short>> = DistinctLiveData(emptyList())

        override fun updateItem(data: Level1Data, isExtendedHours: Boolean) {
            val tickIndicator = data.tickIndicator ?: return
            _momentum.postUpdateValue {
                val values = momentum.value.orEmpty().toMutableList()
                if (values.size == bufferSize) {
                    values.removeAt(0)
                }
                values += tickIndicator
                values
            }
        }
    }
}
