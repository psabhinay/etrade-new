package com.etrade.mobilepro.instrument

private const val OPTION_UNDERLIER_INDEX = 0
private const val EXPIRY_MONTH_INDEX = 1
private const val EXPIRY_DAY_INDEX = 2
private const val EXPIRY_YEAR_INDEX = 3
private const val OPTIONS_STRIKE_PRICE_INDEX = 4
private const val OPTIONS_TYPE_INDEX = 5
private const val OPTIONS_SYMBOL_DESCRIPTIONS_VALID_SIZE = 6

fun String.toBondSymbolWithSubTitle(): CharSequence {
    val displayText = split(" ")
    if (displayText.isEmpty() || displayText.size < 2) return this

    val maturity = displayText[displayText.size - 1]
    val bondRateWithNewLine = "\n" + displayText[displayText.size - 2]
    return maturity + bondRateWithNewLine
}

fun String.toOptionsSymbolWithSubtitle(): Pair<CharSequence, CharSequence> {

    // options description SPX Dec 17 '21 $4000 Put, we want to shrink everything after the underlyer ticker
    // watch list entries returns option descriptions with trailing characters w or q that we need to omit
    val trimmedDesc = trimEnd('w', 'q', ' ')
    val descriptions = trimmedDesc.split(" ")
    if (descriptions.isEmpty() || descriptions.size < OPTIONS_SYMBOL_DESCRIPTIONS_VALID_SIZE) return Pair(this, this)

    val expiryDate = descriptions[EXPIRY_MONTH_INDEX] + descriptions[EXPIRY_DAY_INDEX] + descriptions[EXPIRY_YEAR_INDEX]
    val optionsTypeStrikePriceWithNewLine = "\n" + descriptions[OPTIONS_STRIKE_PRICE_INDEX] + descriptions[OPTIONS_TYPE_INDEX]
    return Pair(descriptions[OPTION_UNDERLIER_INDEX], expiryDate + optionsTypeStrikePriceWithNewLine)
}
