package com.etrade.mobilepro.login.data

import android.content.Context
import android.webkit.WebView
import androidx.annotation.MainThread
import com.etrade.mobilepro.cache.CacheClearHandler
import com.etrade.mobilepro.common.CommonRealmModule
import com.etrade.mobilepro.dagger.CACHE_SUB_DIR_NAME
import io.realm.Realm
import io.realm.RealmConfiguration
import java.io.File
import javax.inject.Inject

@MainThread
class CacheClearHandlerImpl @Inject constructor(private val context: Context) : CacheClearHandler {
    override fun clear() {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction {
                it.deleteAll()
            }
        }

        val commonRealmConfig = RealmConfiguration.Builder()
            .name("common.history")
            .modules(CommonRealmModule())
            .build()

        Realm.getInstance(commonRealmConfig).use { realm ->
            realm.executeTransaction {
                it.deleteAll()
            }
        }

        File(context.cacheDir, CACHE_SUB_DIR_NAME).deleteRecursively()
        WebView(context).clearCache(true)
    }
}
