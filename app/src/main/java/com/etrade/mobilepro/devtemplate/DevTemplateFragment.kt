package com.etrade.mobilepro.devtemplate

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.R
import com.etrade.mobilepro.databinding.FragmentOverviewBinding
import com.etrade.mobilepro.dynamicui.GenericRecyclerAdapter
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

internal class DevTemplateFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.fragment_overview), ViewInterface<DevTemplateViewModel> {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    private val binding by viewBinding(FragmentOverviewBinding::bind)

    private val devTemplateViewModel: DevTemplateViewModel by viewModels { viewModelFactory }

    private lateinit var snackbarUtil: SnackBarUtil
    private var viewAdapter: GenericRecyclerAdapter? = null
    private var retrySnackbar: Snackbar? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        snackbarUtil = snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view })

        setUpViewModels()
        bindViewModels()
        binding.swipeRefresh.setOnRefreshListener {
            fetchValues()
        }
    }

    private fun fetchValues() {
        devTemplateViewModel.fetchDataFromServer()
    }

    private fun setUpViewModels() {
        viewAdapter = GenericRecyclerAdapter(this, emptyList())
        with(binding.overviewRecyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = viewAdapter
        }
    }

    private fun bindViewModels() {
        devTemplateViewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is ViewModelInterface.ViewState.Loading -> {
                        onViewStateLoading(it)
                    }
                    is ViewModelInterface.ViewState.Error -> {
                        onViewStateError(it)
                    }
                    is ViewModelInterface.ViewState.Success -> {
                        onViewStateSuccess(it)
                    }
                }
            }
        )
    }

    override fun onViewStateLoading(loadingViewState: ViewModelInterface.ViewState.Loading) {
        if (!binding.swipeRefresh.isRefreshing) {
            binding.loadingIndicator.show()
        }
        retrySnackbar?.dismiss()
    }

    override fun onViewStateError(errorViewState: ViewModelInterface.ViewState.Error) {
        viewAdapter?.values = emptyList()
        binding.loadingIndicator.hide()
        binding.swipeRefresh.isRefreshing = false
        retrySnackbar = snackbarUtil.retrySnackbar { fetchValues() }
    }

    override fun onViewStateSuccess(successViewState: ViewModelInterface.ViewState.Success) {
        val resultList = mutableListOf<DynamicViewModel>()

        successViewState.results?.let {
            resultList.addAll(it)
        }

        viewAdapter?.values = resultList

        binding.loadingIndicator.hide()
        retrySnackbar?.dismiss()
        binding.swipeRefresh.isRefreshing = false
    }
}
