package com.etrade.mobilepro.chart.performance.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J8\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\n\u001a\u00020\u000bH&\u00a8\u0006\f"}, d2 = {"Lcom/etrade/mobilepro/chart/performance/data/PerformanceChartRepo;", "", "getPerformanceChart", "Lio/reactivex/Observable;", "Lcom/etrade/mobilepro/util/Resource;", "Lcom/etrade/mobilepro/chart/performance/model/PerformanceChartData;", "accounts", "", "", "indexes", "period", "Lcom/etrade/mobilepro/chartengine/TimeFrame;", "performance_debug"})
public abstract interface PerformanceChartRepo {
    
    @org.jetbrains.annotations.NotNull()
    public abstract io.reactivex.Observable<com.etrade.mobilepro.util.Resource<com.etrade.mobilepro.chart.performance.model.PerformanceChartData>> getPerformanceChart(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> indexes, @org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.chartengine.TimeFrame period);
}