package com.etrade.mobilepro.trade.form.api

import com.etrade.mobilepro.trade.api.AdvancedTradeType

data class TradeFormType(val securityType: SecurityType, val advancedOrderType: AdvancedTradeType = AdvancedTradeType.None)
