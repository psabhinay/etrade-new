package com.etrade.mobilepro.util.android.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Provider
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

/**
 * Allows to scope entity to a set of Nav destinations.
 * The entity will be GCed if current nav destination leaves the set it was provided with.
 * It does not require the nav destinations to be from the same nav graph
 */
inline fun <reified T> Fragment.navScoped(scopedTo: Set<Int>, initializer: Provider<T>): Lazy<T> = lazy {
    val className = T::class.java.canonicalName ?: throw IllegalArgumentException("Nav scoped property type ${T::class} has null name (is it anonymous?)")
    val storage = ((this.activity as? NavScopedStorageRegistererHolder)?.navScopedStorage as? NavScopedStorage)
        ?: throw IllegalStateException("NavScoped storage holder not found for the fragment $this")
    storage.getOrStoreScoped(className, scopedTo, initializer)
}
/**
 * Purges instances with of type T with the passed scope (@param scopedTo) from the nav scoped storage
 */
inline fun <reified T> Fragment.purgeNavScoped(instance: T, scopedTo: Set<Int>) {
    val clazz = instance!!::class.java
    purgeNavScoped(clazz, scopedTo)
}
/**
 * Purges instances with of type T with the passed scope (@param scopedTo) from the nav scoped storage
 */
fun <T> Fragment.purgeNavScoped(clazz: Class<T>, scopedTo: Set<Int>) {
    val storage = ((this.activity as? NavScopedStorageRegistererHolder)?.navScopedStorage as? NavScopedStorage)
        ?: throw IllegalStateException("NavScoped storage holder not found for the fragment $this")
    storage.purge(clazz, scopedTo)
}

/**
 * Allows to scope property to a set of Nav destinations.
 * The entity will be GCed if current nav destination leaves the set it was provided with.
 * It does not require the nav destinations to be from the same nav graph
 */
inline fun <reified T> Fragment.navScopedProperty(scopedTo: Set<Int>, initializer: Provider<T>): ReadOnlyProperty<Fragment?, T> = NavScopedProperty(
    className = T::class.java.canonicalName ?: throw IllegalArgumentException("Nav scoped property type ${T::class} has null name (is it anonymous?)"),
    scopedTo, initializer
)

class NavScopedProperty<T>(
    private val className: String,
    private val scopedTo: Set<Int>,
    private val initializer: Provider<T>
) : ReadOnlyProperty<Fragment?, T> {
    override fun getValue(thisRef: Fragment?, property: KProperty<*>): T {
        val storage = ((thisRef?.activity as? NavScopedStorageRegistererHolder)?.navScopedStorage as? NavScopedStorage)
            ?: throw IllegalStateException("NavScoped storage holder not found for the fragment $thisRef")
        return storage.getOrStoreScoped(className, scopedTo, initializer)
    }
}

interface NavScopedStorage {
    fun <T> getOrStoreScoped(className: String, scopedTo: Set<Int>, initializer: Provider<T>): T
    fun <T> purge(clazz: Class<T>, scopedTo: Set<Int>)
}

interface NavScopedStorageRegisterer : NavScopedStorage, NavController.OnDestinationChangedListener

interface NavScopedStorageRegistererHolder {
    val navScopedStorage: NavScopedStorageRegisterer
}

class DefaultNavScopedStorage @Inject constructor() : NavScopedStorageRegisterer {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val store = mutableMapOf<ScopedInstanceKey, Any>()
    private var currentDestination: Int = -1

    @Suppress("UNCHECKED_CAST")
    override fun <T> getOrStoreScoped(className: String, scopedTo: Set<Int>, initializer: Provider<T>): T {
        val key = ScopedInstanceKey(className, scopedTo)
        return if (scopedTo.contains(currentDestination)) {
            store.getOrPut(key, { initializer.get() as Any }) as T
        } else {
            // we are outside of intended scope, just return instance without retaining
            initializer.get()
        }
    }

    override fun <T> purge(clazz: Class<T>, scopedTo: Set<Int>) {
        val className = clazz.canonicalName ?: throw IllegalArgumentException("Nav scoped property type $clazz has null name (is it anonymous?)")
        val key = ScopedInstanceKey(className, scopedTo)
        val purged = store.remove(key)
        logger.debug("navScoped storage purged $className, was stored = ${purged != null}")
    }

    override fun onDestinationChanged(controller: NavController, destination: NavDestination, arguments: Bundle?) {
        logger.debug("onDestChanged start: ${store.size}")
        currentDestination = destination.id
        val iterator = store.iterator()
        while (iterator.hasNext()) {
            val entry = iterator.next()
            if (!entry.key.scopedTo.contains(destination.id)) {
                iterator.remove()
            }
        }
        logger.debug("onDestChanged end: ${store.size}")
    }

    private data class ScopedInstanceKey(
        val className: String,
        val scopedTo: Set<Int>,
    )
}
