package com.etrade.mobilepro.trade.presentation.util.adapters

import android.content.res.Resources
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.action
import com.etrade.mobilepro.trade.api.quantity
import com.etrade.mobilepro.trade.api.stock.StockOrder
import com.etrade.mobilepro.trade.api.ticker
import com.etrade.mobilepro.trade.presentation.util.OrderDisplayItem
import com.etrade.mobilepro.trade.presentation.util.R
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

internal class StockOrderAdapter(
    private val resources: Resources,
    private val confirmedOrderAdapterDelegate: OrderAdapterDelegate,
    private val withCostOrderAdapterDelegate: OrderAdapterDelegate,
    private val withPriceOrderAdapterDelegate: OrderAdapterDelegate,
    private val withOtcTierAdapterDelegate: OrderAdapterDelegate
) : OrderAdapter {

    private val withHardToBorrowOrderAdapterDelegate: OrderAdapterDelegate = WithHardToBorrowOrderAdapterDelegate(resources)

    override fun createOrderDisplayItem(order: Order): OrderDisplayItem {
        return OrderDisplayItem(
            createLabel(resources, order.legs),
            confirmedOrderAdapterDelegate.createOrderSectionItems(order) + createStockOrderSectionItems(order as StockOrder)
        )
    }

    private fun createStockOrderSectionItems(response: StockOrder): List<TableSectionItemLayout> {
        return with(response) {
            listOfNotNull(
                accountItem(resources, this),
                sectionItem(resources, R.string.trade_label_order_type, action.getLabel()),
                sectionItem(resources, R.string.trade_label_symbol, ticker),
                sectionItem(resources, R.string.trade_label_shares, quantity?.toPlainString())
            ) + withPriceOrderAdapterDelegate.createOrderSectionItems(this) +
                withOtcTierAdapterDelegate.createOrderSectionItems(this) +
                withCostOrderAdapterDelegate.createOrderSectionItems(this) +
                withHardToBorrowOrderAdapterDelegate.createOrderSectionItems(this)
        }
    }
}
