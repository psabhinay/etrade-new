package com.etrade.mobilepro.landing.presentation.adapter

import com.etrade.mobilepro.landing.presentation.LandingPageSelectionViewModel
import com.etrade.mobilepro.landing.presentation.adapter.delegate.LandingPageHeaderDelegate
import com.etrade.mobilepro.landing.presentation.adapter.delegate.LandingPageItemDelegate
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

internal class LandingPageSelectionAdapter(listener: (LandingPageItem) -> Unit) : ListDelegationAdapter<MutableList<LandingPageSelectionItem>>() {
    init {
        delegatesManager.apply {
            addDelegate(LandingPageItemDelegate(listener))
            addDelegate(LandingPageHeaderDelegate())
        }

        items = mutableListOf()
    }

    fun submitState(state: LandingPageSelectionViewModel.ViewState) {
        val updatedItems = mutableListOf<LandingPageSelectionItem>(LandingPageHeader(status = state.status))
        updatedItems.addAll(state.items.map { LandingPageItem(value = it) })
        dispatchUpdates(items, updatedItems)
        items = updatedItems
    }
}
