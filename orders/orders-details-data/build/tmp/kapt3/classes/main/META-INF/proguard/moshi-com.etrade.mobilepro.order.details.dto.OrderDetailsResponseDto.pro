-if class com.etrade.mobilepro.order.details.dto.OrderDetailsResponseDto
-keepnames class com.etrade.mobilepro.order.details.dto.OrderDetailsResponseDto
-if class com.etrade.mobilepro.order.details.dto.OrderDetailsResponseDto
-keep class com.etrade.mobilepro.order.details.dto.OrderDetailsResponseDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.etrade.mobilepro.order.details.dto.OrderDetailsResponseDto
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.etrade.mobilepro.order.details.dto.OrderDetailsResponseDto
-keepclassmembers class com.etrade.mobilepro.order.details.dto.OrderDetailsResponseDto {
    public synthetic <init>(com.etrade.mobilepro.order.details.dto.OrderDetailsDto,com.etrade.mobilepro.order.details.dto.OrderDetailsDto,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
