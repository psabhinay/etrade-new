package com.etrade.mobilepro.searchingapi

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import io.reactivex.Observable

interface SearchInteractor {
    fun searchResultItemSelected(searchType: SearchType, item: SearchResultItem)
    fun searchResultItemAsLiveData(searchType: SearchType): LiveData<SearchResultItem>
    fun searchResultItemAsObservable(searchType: SearchType): Observable<SearchResultItem>
    fun searchResultItemUntilChangedAsObservable(searchType: SearchType): Observable<SearchResultItem>
    fun searchResultItemUntilChangedAsLiveData(searchType: SearchType): LiveData<SearchResultItem>
}
