package com.etrade.mobilepro.watchlist

import com.etrade.mobilepro.watchlist.presentation.PATTERN_WATCHLIST_NAME_FILTER
import org.junit.Assert.assertEquals
import org.junit.Test
import kotlin.random.Random

class WatchlistNamePatternSuite {

    private val regex: Regex = PATTERN_WATCHLIST_NAME_FILTER.toRegex()

    private val allowedSymbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 _$%./()-"
    private val illegalSymbols = "\n\t`~!@#^&*{}[]:;\"\'?<>абвгдеёжзиклмнопрстуфхцчшщъыьэюяàáâäæãåā"

    @Test
    fun `test watchlist name with allowed symbols only`() {
        val expected = allowedSymbols
        val actual = regex.replace(expected, "")
        assertEquals(expected, actual)
    }

    @Test
    fun `test empty watchlist name`() {
        val expected = ""
        val actual = regex.replace(expected, "")
        assertEquals(expected, actual)
    }

    @Test
    fun `test illegal symbols`() {
        val actual = regex.replace(illegalSymbols, "")
        assertEquals("", actual)
    }

    @Test
    fun `test filtering`() {
        val seed = System.currentTimeMillis()
        val random = Random(seed)

        for (i in 1..100) {
            val expectedBuilder = StringBuilder()
            val watchlistNameCandidate = StringBuilder()

            val nameLength = random.nextInt(1, 32)
            for (j in 1..nameLength) {
                if (random.nextBoolean()) {
                    val char = allowedSymbols[random.nextInt(0, allowedSymbols.length)]
                    expectedBuilder.append(char)
                    watchlistNameCandidate.append(char)
                } else {
                    val char = illegalSymbols[random.nextInt(0, illegalSymbols.length)]
                    watchlistNameCandidate.append(char)
                }
            }

            val expected = expectedBuilder.toString()
            val actual = regex.replace(watchlistNameCandidate.toString(), "")
            assertEquals("Failed on iteration $i (seed: $seed), name candidate = $watchlistNameCandidate", expected, actual)
        }
    }
}
