package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.util.MutualConverter

// Used in Stop Loss and Conditional Order Trade
object CommonAdvancedPriceTypeValueId : PriceTypeValueId {
    override val even: String = ""
    override val limit: String = "2"
    override val market: String = "1"
    override val netCredit: String = ""
    override val netDebit: String = ""
    override val stop: String = "3"
    override val stopLimit: String = "4"
    override val trailingStopDollar: String = "6"
    override val trailingStopPercent: String = "7"
    override val converter: MutualConverter<String, PriceType> by lazy { createConverter() }
}
