package com.etrade.mobilepro.login.router

import android.content.Context
import android.content.Intent
import com.etrade.mobilepro.common.EULA_TITLE
import com.etrade.mobilepro.common.EULA_URL
import com.etrade.mobilepro.common.getOverlayIntentForUrlAction
import com.etrade.mobilepro.login.presentation.router.LoginAuxRouter
import javax.inject.Inject

class DefaultLoginAuxRouter @Inject constructor() : LoginAuxRouter {
    override fun navigateToEula(context: Context) {
        context.startActivity(getOverlayIntentForUrlAction(EULA_URL, EULA_TITLE, context).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
    }
}
