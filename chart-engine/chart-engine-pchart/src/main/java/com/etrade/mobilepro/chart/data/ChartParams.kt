package com.etrade.mobilepro.chart.data

import androidx.annotation.ColorRes

class ChartParams constructor(
    val chartId: String,
    val displayName: CharSequence,
    @ColorRes val lineColor: Int
)
