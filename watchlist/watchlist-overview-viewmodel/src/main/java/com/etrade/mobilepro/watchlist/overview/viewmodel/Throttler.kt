package com.etrade.mobilepro.watchlist.overview.viewmodel

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean

class Throttler<K, V>(
    private val skipMs: Long = 500L,
    private val coroutineScope: CoroutineScope,
    private val destinationFunction: (Map<K, V>) -> Unit
) {
    private val updatesToPost = ConcurrentHashMap<K, V>()
    private val delayDone = AtomicBoolean(true)

    fun throttle(key: K, value: V) {
        updatesToPost[key] = value
        if (delayDone.get()) {
            delayDone.set(false)
            coroutineScope.launch {
                delay(skipMs)
                val copyOfUpdates = updatesToPost.toMap()
                updatesToPost.clear()
                destinationFunction(copyOfUpdates)
                delayDone.set(true)
            }
        }
    }
}
