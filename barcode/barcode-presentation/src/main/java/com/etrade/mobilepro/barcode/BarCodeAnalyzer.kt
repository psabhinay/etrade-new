package com.etrade.mobilepro.barcode

import android.media.Image
import androidx.camera.core.ExperimentalGetImage
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.android.gms.tasks.OnCompleteListener
import com.google.mlkit.vision.barcode.Barcode
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.common.InputImage.IMAGE_FORMAT_YV12
import java.nio.ByteBuffer

class BarCodeAnalyzer(
    private val onBarCodeDetected: (barCode: Barcode) -> Unit
) : ImageAnalysis.Analyzer {

    @ExperimentalGetImage
    override fun analyze(image: ImageProxy) {
        val options = BarcodeScannerOptions.Builder()
            .setBarcodeFormats(
                Barcode.FORMAT_UPC_A,
                Barcode.FORMAT_UPC_E
            )
            .build()

        val detector = BarcodeScanning.getClient(options)
        val buffer = image.planes[0].buffer
        val rotationDegrees = image.imageInfo.rotationDegrees
        val width = image.width
        val height = image.height
        val currentImage = image.image
        val visionImage = getFirebaseVisionImage(currentImage, buffer, width, height, rotationDegrees)

        detector.process(visionImage)
            .addOnCompleteListener(
                recognitionOnCompleteListener
            )
        image.close()
    }

    private fun getFirebaseVisionImage(
        currentImage: Image?,
        buffer: ByteBuffer,
        width: Int,
        height: Int,
        rotationDegrees: Int
    ): InputImage {
        return if (currentImage != null) {
            InputImage.fromMediaImage(currentImage, rotationDegrees)
        } else {
            InputImage.fromByteBuffer(buffer, width, height, rotationDegrees, IMAGE_FORMAT_YV12)
        }
    }

    private val recognitionOnCompleteListener: OnCompleteListener<List<Barcode>> = OnCompleteListener { task ->
        if (task.isSuccessful) {
            task
                .result
                .firstOrNull { it.valueType == Barcode.TYPE_PRODUCT }
                ?.also {
                    onBarCodeDetected(it)
                }
        }
    }
}
