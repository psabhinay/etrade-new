package com.etrade.mobilepro.usersubscriptionapi

import com.etrade.mobilepro.common.result.ETResult

interface UserSubscriptionRepo {

    suspend fun loadUserMda(): ETResult<MarketDataAgreementResponse>

    suspend fun getBookFilters(isOption: Boolean): ETResult<List<BookFilter>>
}
