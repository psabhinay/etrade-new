package com.etrade.mobilepro.positions.data.mapper

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.positions.api.PortfolioPosition
import com.etrade.mobilepro.positions.data.dto.PortfolioPositionDto
import com.etrade.mobilepro.util.safeParseBigDecimal

const val BOOLEAN_STRING_VALUE_TRUE = "1"

internal class PortfolioPositionMapper {
    fun map(dto: PortfolioPositionDto): PortfolioPosition = with(dto) {
        PortfolioPosition(
            recordId = positionId ?: throw IllegalArgumentException("Portfolio position must have an ID."),
            symbol = symbol ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE,
            symbolDescription = displaySymbol ?: symbol ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE,
            displaySymbol = displaySymbol ?: symbol ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE,
            daysGain = daysGainValue?.safeParseBigDecimal(),
            totalGain = totalGainValue?.safeParseBigDecimal(),
            pricePaid = pricePaid?.safeParseBigDecimal(),
            commission = commission?.safeParseBigDecimal(),
            fees = fees?.safeParseBigDecimal(),
            quantity = quantity?.safeParseBigDecimal(),
            displayQuantity = displayQuantity?.safeParseBigDecimal(),
            instrumentType = InstrumentType.from(typeCode),
            ask = ask?.safeParseBigDecimal(),
            bid = bid?.safeParseBigDecimal(),
            daysGainPercentage = daysGainPercentage?.safeParseBigDecimal(),
            priceEarningRation = priceEarningRation?.safeParseBigDecimal(),
            totalGainPercentage = totalGainPercentage?.safeParseBigDecimal(),
            earningsPerShare = earningsPerShare?.safeParseBigDecimal(),
            fiftyTwoWeekHigh = fiftyTwoWeekHigh?.safeParseBigDecimal(),
            fiftyTwoWeekLow = fiftyTwoWeekLow?.safeParseBigDecimal(),
            lastPrice = lastPrice?.safeParseBigDecimal(),
            marketCap = marketCap?.safeParseBigDecimal(),
            marketValue = marketValue?.safeParseBigDecimal(),
            todayClose = todayClose?.safeParseBigDecimal(),
            volume = volume?.safeParseBigDecimal(),
            daysChange = daysChange?.safeParseBigDecimal(),
            daysChangePercentage = daysChangePercentage?.safeParseBigDecimal(),
            mark = markToMarket?.safeParseBigDecimal(),
            delta = delta?.safeParseBigDecimal(),
            gamma = gamma?.safeParseBigDecimal(),
            theta = theta?.safeParseBigDecimal(),
            vega = vega?.safeParseBigDecimal(),
            impliedVolatilityPercentage = impliedVolatilityPercentage?.safeParseBigDecimal(),
            baseSymbolAndPrice = baseSymbolPrice,
            daysToExpire = daysExpiration?.toIntOrNull(),
            underlyingSymbolInfo = underlyingSymbol?.let { Symbol(it, instrumentType = InstrumentType.from(underlyingTypeCode)) },
            inTheMoneyFlag = inTheMoneyFlag == BOOLEAN_STRING_VALUE_TRUE
        )
    }
}
