package com.etrade.mobilepro.trade.accountupgrade.util

private const val ACCOUNT_UPGRADE_URL = "/e/t/mou/application?upgrade=%s&ext=mobile"

private fun getApprovalLevelUpgradeUrlParam(isIra: Boolean): String {
    return if (isIra) {
        "3"
    } else {
        "2"
    }
}

fun getApprovalLevelUpgradeUrl(isIra: Boolean) = String.format(ACCOUNT_UPGRADE_URL, getApprovalLevelUpgradeUrlParam(isIra))

fun getMarginAccountUpgradeUrl() = String.format(ACCOUNT_UPGRADE_URL, "1")
