package com.etrade.mobilepro.bank.transactions

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
data class BankTransactionDataDto(
    @Json(name = "transaction_type")
    val transactionType: String?,
    @Json(name = "transaction_amount")
    val transactionAmount: BigDecimal?,
    @Json(name = "transaction_description")
    val transactionDescription: String?,
    @Json(name = "transaction_date")
    val transactionDate: String?
)
