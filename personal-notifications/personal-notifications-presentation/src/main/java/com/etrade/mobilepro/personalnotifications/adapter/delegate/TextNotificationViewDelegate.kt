package com.etrade.mobilepro.personalnotifications.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.personalnotifications.adapter.holder.TextNotificationViewHolder
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.ExternalAccountVerification
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.Localytics
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.PendingTransaction
import com.etrade.mobilepro.personalnotifications.databinding.ViewTextNotificationItemBinding
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class TextNotificationViewDelegate(
    val readStatusChecker: ((PersonalNotification) -> Boolean),
    val clickListener: ((PersonalNotification) -> Unit)
) : AbsListItemAdapterDelegate<PersonalNotification, PersonalNotification, TextNotificationViewHolder>() {
    override fun isForViewType(
        item: PersonalNotification,
        items: MutableList<PersonalNotification>,
        position: Int
    ): Boolean {
        return item is ExternalAccountVerification ||
            item is PendingTransaction ||
            item is Localytics.Inline
    }

    override fun onCreateViewHolder(parent: ViewGroup): TextNotificationViewHolder {
        return TextNotificationViewHolder(parent.viewBinding(ViewTextNotificationItemBinding::inflate), clickListener)
    }

    override fun onBindViewHolder(
        item: PersonalNotification,
        holder: TextNotificationViewHolder,
        payloads: MutableList<Any>
    ) {
        holder.bind(item, readStatusChecker.invoke(item))
    }
}
