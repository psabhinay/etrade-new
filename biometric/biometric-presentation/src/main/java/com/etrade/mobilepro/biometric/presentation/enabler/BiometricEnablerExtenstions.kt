package com.etrade.mobilepro.biometric.presentation.enabler

import android.content.res.Resources
import com.etrade.mobilepro.biometric.api.BiometricProviderError
import com.etrade.mobilepro.biometric.presentation.R

fun BiometricProviderError.getLabelFrom(resources: Resources) = resources.getString(
    when (this) {
        BiometricProviderError.ERROR_NO_BIOMETRICS -> R.string.biometric_missing_enrollment
        BiometricProviderError.ERROR_HW_NOT_PRESENT -> R.string.biometric_hw_not_present
        BiometricProviderError.ERROR_HW_UNAVAILABLE -> R.string.biometric_hw_unavailable
        BiometricProviderError.ERROR_BIOMETRIC_NO_HW_BACKED_KEYSTORE -> R.string.error_biometric_no_hw_backed_keystore
        BiometricProviderError.ERROR_BIOMETRIC_GENERIC -> R.string.biometric_generic_error
    }
)

fun BiometricProviderError.errorCode() =
    when (this) {
        BiometricProviderError.ERROR_NO_BIOMETRICS -> BiometricEnablerError.ERROR_NO_BIOMETRICS
        BiometricProviderError.ERROR_HW_NOT_PRESENT -> BiometricEnablerError.ERROR_HW_NOT_PRESENT
        BiometricProviderError.ERROR_HW_UNAVAILABLE -> BiometricEnablerError.ERROR_HW_UNAVAILABLE
        else -> BiometricEnablerError.BIOMETRIC_GENERAL_ERROR
    }.biometricConstant
