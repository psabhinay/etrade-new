package com.etrade.mobilepro.trade.presentation.form.delegate.shared

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.etrade.mobile.libs.futures.EMPTY_FORMATTED_VALUE
import com.etrade.mobilepro.dynamic.form.api.ExternalAction
import com.etrade.mobilepro.dynamic.form.api.ExternalInput
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.api.inputParameterValue
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.instrument.underlier
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quoteapi.QuoteBidAskItem
import com.etrade.mobilepro.quoteapi.QuotePriceWidgetItem
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.searchingapi.StartSearchParams
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.form.api.input.WithTradeLegsTradeFormInputId
import com.etrade.mobilepro.trade.presentation.BR
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.TradeFormAction

internal fun GroupBuilder.createOptionSymbolExternalAction(
    context: Context,
    params: SymbolInputParams,
    inputId: WithTradeLegsTradeFormInputId,
    searchIntentFactory: SearchIntentFactory,
    onActionClickListener: OnActionClickListener
): ExternalAction {

    return {
        val intent = searchIntentFactory.createIntent(
            context = context,
            params = StartSearchParams(
                query = SearchQuery(SearchType.SYMBOL),
                stockQuoteSearchOnly = params.isStockQuoteSearchOnly,
                finishOnSearch = true,
                showOwnedSymbols = params.showOwnedSymbols &&
                    inputValue<List<TradeLeg>?>(inputId.tradeLegs).value?.firstOrNull()?.transactionType == TransactionType.SELL_CLOSE_OPT,
                selectedAccountId = inputParameterValue(inputId.accountId).value as? String
            ),
            reorderToFront = false
        )
        onActionClickListener.onActionClick(TradeFormAction.SymbolLookup(intent, params.symbolInputId))
    }
}

internal fun GroupBuilder.optionsBidAskWidget(symbolInputId: String, viewModel: QuotesWidgetViewModel) {
    layout(R.layout.trade_view_live_bid_ask) {
        bindings {
            binding(BR.liveTradeBidAskItem) {
                variable = viewModel.bidAskItem
            }
        }
        rule {
            val symbolInput = requireInput<ExternalInput<WithSymbolInfo>>(symbolInputId)
            symbolInput.value.observeForever {
                viewModel.getQuotes(symbol = it?.symbol, requireExchangeCode = true)
            }
            visibleIf {
                symbolInput.hasValue
            }
        }
    }
}

internal fun GroupBuilder.underlierQuoteWidget(symbolInputId: String, viewModel: QuotesWidgetViewModel) {
    layout(R.layout.quote_view_base_with_retry) {
        val symbol = requireInput<ExternalInput<WithSymbolInfo>>(symbolInputId).value
        bindings {
            binding(BR.viewModel) {
                variable = viewModel
            }
        }
        visibleIf {
            symbol.map { it != null }
        }
        rule {
            symbol.observeForever {
                viewModel.getQuotes(symbol = it?.underlier?.symbol, requireExchangeCode = true)
            }
        }
    }
}

private val emptyPriceItem = MutableLiveData(QuotePriceWidgetItem(EMPTY_FORMATTED_VALUE))
val QuotesWidgetViewModel.bidAskItem: LiveData<QuoteBidAskItem>
    get() = quote.map {
        object : QuoteBidAskItem {
            override val bidPrice: LiveData<QuotePriceWidgetItem> = it?.bidPrice ?: emptyPriceItem
            override val askPrice: LiveData<QuotePriceWidgetItem> = it?.askPrice ?: emptyPriceItem
        }
    }
