package com.etrade.mobilepro.trade.presentation.form.delegate.shared

import android.content.res.Resources
import androidx.annotation.LayoutRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.balance.viewmodel.AccountBalanceViewModel
import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.dynamic.form.api.DropDownOptionsBuilder
import com.etrade.mobilepro.dynamic.form.api.ExternalAction
import com.etrade.mobilepro.dynamic.form.api.ExternalInput
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.InputHolder
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.api.TextInputIcon
import com.etrade.mobilepro.dynamic.form.api.TextInputType
import com.etrade.mobilepro.dynamic.form.api.inputParameterValue
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.instrument.displaySymbol
import com.etrade.mobilepro.instrument.underlier
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.api.TradeOptionInfoType
import com.etrade.mobilepro.trade.dropdown.form.tradeSelect
import com.etrade.mobilepro.trade.dropdown.help.TradeHelpParams
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.form.api.input.ContingentStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithQuantity
import com.etrade.mobilepro.trade.form.api.input.shared.StopLossInputId
import com.etrade.mobilepro.trade.form.api.value.ConditionalPriceTypeValueId
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.trade.presentation.BR
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.extensions.selectedAccount
import com.etrade.mobilepro.trade.presentation.form.TradeFormAction
import com.etrade.mobilepro.trade.presentation.form.component.AccountDataPointsComponent
import com.etrade.mobilepro.trade.presentation.form.component.FooterLink
import com.etrade.mobilepro.trade.presentation.form.component.builder.FooterComponentBuilder
import com.etrade.mobilepro.trade.presentation.form.component.builder.FooterLinkBuilder
import com.etrade.mobilepro.trade.presentation.form.component.builder.footer
import com.etrade.mobilepro.util.ValueCoder
import com.etrade.mobilepro.util.android.accessibility.formatAccountNameContentDescription
import com.etrade.mobilepro.util.formatSymbolDollar
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.etrade.mobilepro.walkthrough.api.IdleState
import com.etrade.mobilepro.walkthrough.api.State
import com.etrade.mobilepro.walkthrough.api.TradePlacingState

private const val PRICE_MAX_FRACTION_DIGITS = 5
internal const val QUANTITY_MAX_FRACTION_DIGITS = 5

typealias PriceInputVisibilityProvider = (selectedPriceType: Any?) -> Boolean

internal fun GroupBuilder.account(resources: Resources, inputId: TradeFormInputId, accounts: List<TradeAccount>, defaultAccountId: String?) {
    select(inputId.accountId) {
        title = resources.getString(R.string.trade_hint_account)
        contentDescription = resources.getString(R.string.trade_content_description_account)
        contentDescriptionFormatter = { t, dv ->
            formatInputContentDescription(t, resources.formatAccountNameContentDescription(dv?.toString()))
        }
        defaultOptionId = defaultAccountId

        // Account selection needs to be preserved within a Trade session
        doNotReset = true

        options {
            accounts.forEach {
                option(it.id) {
                    title = it.name
                    contentDescription = resources.formatAccountNameContentDescription(title)
                }
            }
        }
    }
}

internal fun GroupBuilder.accountDataPoints(
    resources: Resources,
    inputId: TradeFormInputId,
    accounts: List<TradeAccount>,
    balanceViewModel: AccountBalanceViewModel
) {
    custom {
        AccountDataPointsComponent(
            resources = resources,
            selectedAccount = selectedAccount(inputId, accounts),
            balanceViewModel = balanceViewModel
        )
    }
}

internal fun GroupBuilder.stopLimitPrices(
    inputId: WithPriceTradeFormInputId,
    resources: Resources,
    valueId: PriceTypeValueId
) {
    horizontalGroup {
        price(inputId.limitPriceForStopLimit, inputId, null, resources.getString(R.string.trade_hint_limit_price)) {
            it == valueId.stopLimit
        }
        price(inputId.stopPriceForStopLimit, inputId, null, resources.getString(R.string.trade_hint_stop_price)) {
            it == valueId.stopLimit
        }
    }
}

internal fun GroupBuilder.price(
    id: String,
    inputId: WithPriceTradeFormInputId,
    priceTypePriceLabelMap: Map<String, String>? = null,
    priceInputLabel: CharSequence? = null,
    isVisible: PriceInputVisibilityProvider
) {
    text(id) {
        dynamicTitle {
            priceTypePriceLabelMap?.let { labelMap ->
                inputParameterValue(inputId.priceType).map {
                    @Suppress("USELESS_CAST") // as? CharSequence is necessary
                    it?.let { labelMap[it] } as? CharSequence
                }
            } ?: MutableLiveData(priceInputLabel)
        }
        contentDescriptionFormatter = { t, dv ->
            formatInputContentDescription(t.toString().formatSymbolDollar(), dv)
        }
        textInputType = TextInputType.Decimal(PRICE_MAX_FRACTION_DIGITS)

        visibleIf {
            inputParameterValue(inputId.priceType).map {
                isVisible(it)
            }
        }
    }
}

@Suppress("LongParameterList", "LongMethod")
internal fun GroupBuilder.priceType(
    resources: Resources,
    inputId: WithPriceTradeFormInputId,
    walkthroughState: LiveData<State>,
    editModePriceType: PriceType?,
    defaultPriceType: PriceType? = null,
    securityType: SecurityType = SecurityType.STOCK
) {
    tradeSelect(inputId.priceType) {
        title = resources.getString(R.string.trade_hint_price_type)
        defaultPriceType?.let {
            defaultOptionId = inputId.priceTypeValueId.converter.toKey(it)
        }
        contentDescriptionFormatter = { t, dv ->
            formatInputContentDescription(t, dv?.toString().formatSymbolDollar())
        }
        helpParams = TradeHelpParams(TradeOptionInfoType.PRICE_TYPE)
        options {
            when {
                inputId is StopLossInputId -> getStopLossPriceTypes(resources, inputId, securityType)
                inputId is ContingentStockTradeFormInputId -> addContingentPriceTypes(resources, inputId.priceTypeValueId)
                editModePriceType != null -> addEditPriceTypes(resources, inputId.priceTypeValueId, editModePriceType)
                else -> addDefaultPriceTypes(resources, inputId.priceTypeValueId)
            }
        }
        interceptClickIf {
            walkthroughState.map { it !is IdleState }
        }
        enabledIfReachesState(walkthroughState, TradePlacingState.HighlightPriceType)
    }
}

private fun DropDownOptionsBuilder.getStopLossPriceTypes(
    resources: Resources,
    inputId: WithPriceTradeFormInputId,
    securityType: SecurityType
) {
    addStopLossPriceTypes(resources, inputId.priceTypeValueId)
    if (securityType == SecurityType.STOCK) {
        addStopLossTrailingStopPercent(resources, inputId.priceTypeValueId)
    }
}

private fun DropDownOptionsBuilder.addContingentPriceTypes(
    resources: Resources,
    priceTypeValueId: PriceTypeValueId
) {
    option(priceTypeValueId.market) { title = resources.getString(R.string.trade_price_type_market) }
    option(priceTypeValueId.limit) { title = resources.getString(R.string.trade_price_type_limit) }
    option(priceTypeValueId.stop) { title = resources.getString(R.string.trade_price_type_stop) }
    option(priceTypeValueId.stopLimit) { title = resources.getString(R.string.trade_price_type_stop_limit) }
}

private fun DropDownOptionsBuilder.addStopLossPriceTypes(
    resources: Resources,
    inputId: PriceTypeValueId
) {
    option(inputId.stop) { title = resources.getString(R.string.trade_price_type_stop) }
    option(inputId.stopLimit) { title = resources.getString(R.string.trade_price_type_stop_limit) }
    option(inputId.trailingStopDollar) {
        title = resources.getString(R.string.trade_price_type_trailing_stop)
        contentDescription = title.formatSymbolDollar().orEmpty()
    }
}

private fun DropDownOptionsBuilder.addStopLossTrailingStopPercent(
    resources: Resources,
    inputId: PriceTypeValueId
) {
    option(inputId.trailingStopPercent) { title = resources.getString(R.string.trade_price_type_trailing_stop_percent) }
}

private fun DropDownOptionsBuilder.addDefaultPriceTypes(
    resources: Resources,
    priceTypeValueId: PriceTypeValueId
) {
    option(priceTypeValueId.market) { title = resources.getString(R.string.trade_price_type_market) }
    option(priceTypeValueId.limit) { title = resources.getString(R.string.trade_price_type_limit) }
    option(priceTypeValueId.stop) { title = resources.getString(R.string.trade_price_type_stop) }
    option(priceTypeValueId.stopLimit) { title = resources.getString(R.string.trade_price_type_stop_limit) }
    if (priceTypeValueId is ConditionalPriceTypeValueId) {
        option(priceTypeValueId.hiddenStop) { title = resources.getString(R.string.trade_price_type_hidden_stop) }
    }
    option(priceTypeValueId.trailingStopDollar) {
        title = resources.getString(R.string.trade_price_type_trailing_stop)
        contentDescription = title.formatSymbolDollar().orEmpty()
    }
    option(priceTypeValueId.trailingStopPercent) { title = resources.getString(R.string.trade_price_type_trailing_stop_percent) }
}

private fun DropDownOptionsBuilder.addEditPriceTypes(
    resources: Resources,
    priceTypeValueId: PriceTypeValueId,
    editModePriceType: PriceType?
) {
    if (editModePriceType == PriceType.TRAILING_STOP_DOLLAR ||
        editModePriceType == PriceType.TRAILING_STOP_PERCENT
    ) {
        option(priceTypeValueId.trailingStopDollar) {
            title = resources.getString(R.string.trade_price_type_trailing_stop)
            contentDescription = title.formatSymbolDollar().orEmpty()
        }
        option(priceTypeValueId.trailingStopPercent) {
            title = resources.getString(R.string.trade_price_type_trailing_stop_percent)
        }
    } else {
        option(priceTypeValueId.market) { title = resources.getString(R.string.trade_price_type_market) }
        option(priceTypeValueId.limit) { title = resources.getString(R.string.trade_price_type_limit) }
        option(priceTypeValueId.stop) { title = resources.getString(R.string.trade_price_type_stop) }
        option(priceTypeValueId.stopLimit) { title = resources.getString(R.string.trade_price_type_stop_limit) }
    }
}

internal fun GroupBuilder.quoteWidget(symbolInputId: String, viewModel: QuotesWidgetViewModel, @LayoutRes layoutRes: Int) {
    layout(layoutRes) {
        val symbol = requireInput<ExternalInput<WithSymbolInfo>>(symbolInputId).value
        bindings {
            binding(BR.viewModel) {
                variable = viewModel
            }
        }
        visibleIf {
            symbol.map { it != null }
        }
        rule {
            symbol.observeForever {
                viewModel.getQuotes(symbol = it?.underlier?.symbol, requireExchangeCode = true)
            }
        }
    }
}

@Suppress("LongMethod")
internal fun GroupBuilder.securityType(
    resources: Resources,
    inputId: TradeFormInputId,
    securityType: SecurityType,
    walkthroughState: LiveData<State>
) {
    tradeSelect(inputId.securityType) {
        title = resources.getString(R.string.trade_hint_security_type)
        defaultOptionId = inputId.securityTypeValueId.converter.toKey(securityType)
        options {
            val securityTypeValueId = inputId.securityTypeValueId
            option(securityTypeValueId.stock) {
                title = resources.getString(R.string.trade_security_type_stock)
            }
            option(securityTypeValueId.option) {
                title = resources.getString(R.string.trade_security_type_options)
            }
            option(securityTypeValueId.mutualFund) {
                title = resources.getString(R.string.trade_security_type_mutual_fund)
            }
            option(securityTypeValueId.stockAndOptions) {
                title = resources.getString(R.string.trade_security_type_stock_and_options)
            }
            option(securityTypeValueId.conditional) {
                title = resources.getString(R.string.trade_security_type_conditional)
            }
        }
        helpParams = TradeHelpParams(TradeOptionInfoType.SECURITY_TYPE)
        interceptClickIf {
            walkthroughState.map { it !is IdleState }
        }
    }
}

internal fun GroupBuilder.stockAndOptionsFooter(
    resources: Resources,
    onActionClickListener: OnActionClickListener,
    init: FooterComponentBuilder.() -> Unit = { /* intentionally blank */ }
) {
    footer {
        links {
            goodFor60DaysDisclaimerLink(resources, onActionClickListener)
            exchangeKeyLink(resources, onActionClickListener)
        }
        init()
    }
}

internal fun GroupBuilder.symbol(
    resources: Resources,
    params: SymbolInputParams,
    externalAction: ExternalAction,
    initialSymbol: WithSymbolInfo? = null
) {
    externalSelect(params.symbolInputId, params.symbolCoder) {
        action = externalAction
        title = resources.getString(R.string.trade_hint_symbol)
        isValid = { params.allowedInstruments.contains(it.instrumentType) }
        valueFormatter = { it.displaySymbol }
        onInvalidValue = params.onInvalidSymbol
        if (initialSymbol != null && value == null) {
            value = initialSymbol
        }
    }
}

internal data class SymbolInputParams(
    val allowedInstruments: Set<InstrumentType>,
    val symbolInputId: String,
    val symbolCoder: ValueCoder<WithSymbolInfo>,
    val onActionClickListener: OnActionClickListener? = null,
    val isStockQuoteSearchOnly: Boolean = false,
    val showOwnedSymbols: Boolean = true,
    val onInvalidSymbol: () -> Unit = { /* intentionally blank */ }
)

@Suppress("LongMethod")
internal fun createTermOptions(
    resources: Resources,
    inputId: WithPriceTradeFormInputId,
    limitOrderTermLabelMap: Map<String, String>,
    priceTypeOption: DropDownOption?
): List<DropDownOption> {

    val termList = createCommonTermList(resources, inputId)

    val termValueId = inputId.termValueId
    val priceTypeValueId = inputId.priceTypeValueId

    when (priceTypeOption?.id) {
        priceTypeValueId.limit -> {
            for ((key, value) in limitOrderTermLabelMap) {
                termList.add(DropDownOption(key, value))
            }
        }
        priceTypeValueId.stop,
        priceTypeValueId.stopLimit,
        priceTypeValueId.trailingStopDollar,
        priceTypeValueId.trailingStopPercent -> {
            termList.add(resources.goodUntilCancelOption(termValueId))
        }
        priceTypeValueId.netCredit,
        priceTypeValueId.netDebit,
        priceTypeValueId.even -> {
            termList.apply {
                add(resources.goodUntilCancelOption(termValueId))
                add(DropDownOption(termValueId.immediateOrCancel, resources.getString(R.string.trade_term_ioc)))
            }
        }
    }

    if (priceTypeValueId is ConditionalPriceTypeValueId) {
        when (priceTypeOption?.id) {
            priceTypeValueId.hiddenStop -> {
                termList += resources.goodUntilCancelOption(termValueId)
            }
        }
    }

    return termList
}

internal fun createSellShortTermOptions(resources: Resources, inputId: WithPriceTradeFormInputId, priceTypeOption: DropDownOption?): List<DropDownOption> {
    val termList = createCommonTermList(resources, inputId)
    if (priceTypeOption?.id == inputId.priceTypeValueId.limit) {
        termList += DropDownOption(inputId.termValueId.extendedHoursDay, resources.getString(R.string.trade_term_ehday))
    }
    return termList
}

private fun createCommonTermList(resources: Resources, inputId: WithPriceTradeFormInputId): MutableList<DropDownOption> {
    return mutableListOf(DropDownOption(inputId.termValueId.goodForDay, resources.getString(R.string.trade_term_day)))
}

internal fun TradeFormParametersBuilder.collectStopOrLimitOrStopLimit(
    inputHolder: InputHolder,
    priceTypeId: String,
    stopLimitPriceTypeId: String,
    isLimit: Boolean = false
) {
    var price = inputHolder.inputValue<String>(priceTypeId).value?.safeParseBigDecimal()

    if (price == null) {
        price = inputHolder.inputValue<String>(stopLimitPriceTypeId).value?.safeParseBigDecimal()
    }

    if (isLimit) {
        limitPrice = price
    } else {
        stopPrice = price
    }
}

internal fun FooterLinkBuilder.exchangeKeyLink(resources: Resources, onActionClickListener: OnActionClickListener) {
    link {
        FooterLink(
            title = resources.getString(R.string.trade_link_exchange_keys),
            action = {
                onActionClickListener.onActionClick(TradeFormAction.StockExchangeKeys(resources.getString(R.string.trade_exchange_keys_title)))
            }
        )
    }
}

private fun FooterLinkBuilder.goodFor60DaysDisclaimerLink(resources: Resources, onActionClickListener: OnActionClickListener) {
    link {
        FooterLink(
            title = resources.getString(R.string.trade_link_disclaimer),
            action = {
                onActionClickListener.showDialog {
                    title = resources.getString(R.string.trade_disclaimer_title)
                    message = resources.getString(R.string.trade_disclaimer_message)
                    positiveAction {
                        label = resources.getString(R.string.ok)
                    }
                }
            }
        )
    }
}

private fun formatInputContentDescription(title: CharSequence?, displayValue: CharSequence?): String {
    return "${title ?: ""} ${displayValue ?: ""}"
}

private fun Resources.goodUntilCancelOption(valueId: OrderTermValueId): DropDownOption {
    return DropDownOption(valueId.goodUntilCancel, getString(R.string.trade_term_gtc))
}

internal fun <T> GroupBuilder.quantity(
    inputId: T,
    titleParam: String,
    textInputIconParam: TextInputIcon? = null,
    defaultQuantity: String? = null,
    walkthroughState: LiveData<State>? = null
)
        where T : WithActionTradeFormInputId, T : WithQuantity {
    text(inputId.quantity) {
        title = titleParam
        if (defaultQuantity.isNullOrEmpty().not()) {
            value = defaultQuantity
        }
        textInputIcon = textInputIconParam
        dynamicTextInputType {
            requireInput<SelectInput>(inputId.action).value.map {
                TextInputType.Decimal(QUANTITY_MAX_FRACTION_DIGITS)
            }
        }
        enabledIfReachesState(walkthroughState, TradePlacingState.HighlightQuantity)
    }
}
