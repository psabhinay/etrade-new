package com.etrade.mobilepro.dropdown.expandableselect

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.common.Navigation
import com.etrade.mobilepro.common.getToolbarTitleContentDescription
import com.etrade.mobilepro.common.overlayToolbarTitleView
import com.etrade.mobilepro.common.setupToolbar
import com.etrade.mobilepro.common.toolbarActionEndView
import com.etrade.mobilepro.dropdown.R
import com.etrade.mobilepro.dropdown.databinding.OverlayBottomsheetBinding
import com.etrade.mobilepro.dropdown.expandableselect.adapter.DropDownExpandableAdapter
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.accessibility.executeWithoutAccessibilityFocusChange
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

private const val LAST_EXPANDED_ITEM_KEY = "Last expanded item key"

class OverlayDropDownExpandableFragment : BottomSheetDialogFragment() {

    private val binding by viewBinding(OverlayBottomsheetBinding::bind)

    private var snackbarUtilFactory: SnackbarUtilFactory? = null
    private var isExpandableSelect = false
    private var pendingAction: (() -> Unit)? = null
    private var onCancelAction: (() -> Unit)? = null
    private var lastExpandedItem: ExpandableItemState? = null
    private val handler = Handler(Looper.getMainLooper())

    override fun getTheme() = R.style.BottomSheetDialogTheme

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.overlay_bottomsheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lastExpandedItem = savedInstanceState?.getParcelable(LAST_EXPANDED_ITEM_KEY)
        setupToolbar(
            navigation = Navigation(
                icon = R.drawable.ic_close,
                onClick = {
                    onCancelAction?.invoke()
                    dismiss()
                },
                contentDescription = getString(R.string.close)
            )
        )

        pendingAction?.invoke()
        pendingAction = null
        dialog?.window?.attributes?.windowAnimations = R.style.DialogAnimation
    }

    override fun onSaveInstanceState(outState: Bundle) {
        lastExpandedItem?.let { outState.putParcelable(LAST_EXPANDED_ITEM_KEY, it) }
        super.onSaveInstanceState(outState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setOnShowListener {
            val bottomSheetDialog = dialog as BottomSheetDialog
            val bottomSheet = bottomSheetDialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)!!
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            val layoutParams: CoordinatorLayout.LayoutParams = CoordinatorLayout.LayoutParams(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.MATCH_PARENT
            )
            layoutParams.behavior = bottomSheetBehavior
            bottomSheet.layoutParams = layoutParams
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        return dialog
    }

    override fun onDestroyView() {
        handler.removeCallbacksAndMessages(null)
        super.onDestroyView()
    }

    override fun onCancel(dialog: DialogInterface) {
        onCancelAction?.invoke()
        super.onCancel(dialog)
    }

    internal fun <T> applySettings(settings: DropDownExpandableSelector.Settings<T>, fallBackValues: Set<T>?) {
        val pendingAction: () -> Unit = createPendingAction(settings, fallBackValues)
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
            pendingAction.invoke()
        } else {
            this.pendingAction = pendingAction
        }
    }

    @Suppress("LongMethod")
    private fun <T> createPendingAction(
        settings: DropDownExpandableSelector.Settings<T>,
        fallBackValues: Set<T>?
    ): () -> Unit = {
        this.onCancelAction = {
            settings.onCancelListener?.invoke(fallBackValues)
        }
        overlayToolbarTitleView?.apply {
            text = settings.title
            contentDescription = getToolbarTitleContentDescription(settings.title, requireContext())
        }
        this.snackbarUtilFactory = settings.snackbarUtilFactory
        handleProvidedSelectionFlow(settings)
        val adapter = DropDownExpandableAdapter(
            lastExpandedItem,
            provideInnerListener(settings),
            updateListener = { index, length, newIndex ->
                view?.executeWithoutAccessibilityFocusChange {
                    when {
                        index == 0 -> binding.overlayRecyclerView.scrollToPosition(0)
                        length > 0 && newIndex > -1 -> handler.post {
                            binding.overlayRecyclerView.smoothScrollToPosition(newIndex)
                        }
                    }
                }
            }
        )
        binding.overlayRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.overlayRecyclerView.adapter = adapter
        settings.itemsData.removeObservers(viewLifecycleOwner)
        settings.itemsData.observe(viewLifecycleOwner) { list ->
            adapter.submitItems(
                if (settings.hideSingleSubItems) {
                    list.removeSingleElementSubItems()
                } else {
                    list
                }
            )
        }
        handleActionButton(settings)
        handleLoadingState(settings)
    }

    @Suppress("ComplexMethod", "LongMethod") // Nothing special
    private fun toggleState(isProcessing: Boolean, includeActionButton: Boolean) {
        toolbarActionEndView?.visibility = when {
            isProcessing -> {
                View.GONE
            }
            isExpandableSelect -> {
                View.VISIBLE
            }
            else -> {
                View.GONE
            }
        }
        binding.overlayRecyclerView.visibility = if (isProcessing) {
            View.GONE
        } else {
            View.VISIBLE
        }
        binding.loadingIndicator.visibility = if (isProcessing) {
            View.VISIBLE
        } else {
            View.GONE
        }
        if (includeActionButton) {
            binding.actionButton.visibility = if (isProcessing) {
                View.GONE
            } else {
                View.VISIBLE
            }
        }
    }

    private fun <T> provideInnerListener(settings: DropDownExpandableSelector.Settings<T>): (Int, DropDownExpandableSelector.Selectable<T>, Boolean) -> Unit =
        if (settings.selectionFlow == null) {
            { pos, item, shouldDismiss ->
                if (item.subItems == null) {
                    settings.selectListener.invoke(pos, item)
                } else {
                    // negation is needed as item altering logic was no yet triggered
                    lastExpandedItem = ExpandableItemState(isExpanded = !item.isExpanded, hashCodeValue = item.hashCodeWithoutExpandedState)
                }
                if (item.enabled && shouldDismiss) {
                    this.dismiss()
                }
            }
        } else {
            { pos, item, _ ->
                settings.selectionFlow.willSelectListener.invoke(pos, item)
            }
        }

    @Suppress("LongMethod") // nothing to split / extract, short enough
    private fun <T> handleProvidedSelectionFlow(settings: DropDownExpandableSelector.Settings<T>) {
        settings.selectionFlow?.let { selectionFlow ->
            selectionFlow.stageData.removeObservers(viewLifecycleOwner)
            selectionFlow.stageData.observe(viewLifecycleOwner,) {
                val pos = it.data?.first
                val item = it.data?.second
                val hasActionButton = settings.actionButtonSettings != null
                toggleState(isProcessing = it is Resource.Loading, includeActionButton = hasActionButton)
                if (pos != null && item != null) {
                    when (it) {
                        is Resource.Loading -> {
                            // nop
                        }
                        is Resource.Success -> {
                            settings.selectListener.invoke(pos, item)
                            if (item.enabled) {
                                this.dismiss()
                            }
                            selectionFlow.didSelectListener.invoke(pos, item)
                        }
                        is Resource.Failed -> {
                            selectionFlow.onError?.invoke(pos, item)
                            selectionFlow.onRetry?.let { onRetryClosure ->
                                snackbarUtilFactory?.createSnackbarUtil({ viewLifecycleOwner }, { view })?.retrySnackbar {
                                    onRetryClosure.invoke(pos, item)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun <T> handleActionButton(settings: DropDownExpandableSelector.Settings<T>) {
        settings.actionButtonSettings?.let { buttonSettings ->
            with(binding.actionButton) {
                visibility = View.VISIBLE
                text = buttonSettings.buttonTitle
                setOnClickListener {
                    buttonSettings.actionButtonListener.invoke()
                }
            }
        }
    }

    private fun <T> handleLoadingState(settings: DropDownExpandableSelector.Settings<T>) {
        settings.loadingState?.let { state ->
            state.observe(viewLifecycleOwner) {
                val hasActionButton = settings.actionButtonSettings != null
                toggleState(isProcessing = it is Resource.Loading, includeActionButton = hasActionButton)
                if (it is Resource.Failed) {
                    if (it.retry == null) {
                        snackbarUtilFactory?.createSnackbarUtil({ viewLifecycleOwner }, { view })?.errorSnackbar()
                    } else {
                        snackbarUtilFactory?.createSnackbarUtil({ viewLifecycleOwner }, { view })?.retrySnackbar { it.retry?.invoke() }
                    }
                }
            }
        }
    }
}
