package com.etrade.mobilepro.livedata

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class DistinctLiveDataTest {

    @Test
    fun `check initial value`() {
        assertNull(DistinctLiveData<String>(null).value)
        assertEquals("expected", DistinctLiveData("expected").value)
    }

    @Test
    fun `check resetting of initial value`() {
        var callbacks = 0
        DistinctLiveData("initial").apply {
            observeForever { callbacks++ }
            setValue("initial")
        }
        assertEquals(1, callbacks)
    }

    @Test
    fun `check reposting of initial value`() {
        var callbacks = 0
        DistinctLiveData("initial").apply {
            observeForever { callbacks++ }
            postValue("initial")
        }
        assertEquals(1, callbacks)
    }

    @Test
    fun `check setting and posting of the same value`() {
        var callbacks = 0
        DistinctLiveData("initial").apply {
            observeForever { callbacks++ }
            postValue("new")
            setValue("new")
        }
        assertEquals(2, callbacks)
    }
}
