package com.etrade.mobilepro.testutil

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.lang.reflect.Type
import kotlin.reflect.KClass

/**
 * Gets an object from a JSON document with a type of [cls1] parametrized with a type of [cls2].
 *
 * @param resourcePath path to a resource file
 * @param cls1 parameterized type
 * @param cls2 parameter type
 * @param adapters additional JsonAdapter.Factory's or Objects annotated appropriately to be added to moshi instance
 *
 * @return an instance of specified type
 */
inline fun <reified T> KClass<*>.getObjectFromJson(
    resourcePath: String,
    cls1: Class<*>,
    cls2: Class<*>,
    adapters: List<Any> = emptyList()
): T {
    return getObjectFromJson(resourcePath, Types.newParameterizedType(cls1, cls2), adapters)
}

inline fun <reified T> KClass<*>.getObjectFromJson(
    resourcePath: String,
    type: Type = T::class.java,
    adapters: List<Any> = emptyList()
): T {
    val resourceText = FileReader.getStringFromFile(resourcePath, java)
    val moshi = Moshi.Builder()
    for (adapter in adapters) {
        if (adapter is JsonAdapter.Factory) {
            moshi.add(adapter)
        } else {
            moshi.add(adapter)
        }
    }
    val moshiAdapter = moshi.build().adapter<T>(type)
    return moshiAdapter.fromJson(resourceText)!!
}
