package com.etrade.mobilepro.screener.presentation.tableview

import android.content.Context
import android.text.TextUtils
import android.view.ViewGroup
import android.widget.TextView
import com.etrade.mobilepro.screener.api.result.MorningstarRatingResult
import com.etrade.mobilepro.screener.api.result.ResultDataCell
import com.etrade.mobilepro.screener.api.result.ResultSymbolCell
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.tableviewutils.presentation.adapter.TableViewAdapter
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.DataCell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.tableviewutils.presentation.holder.DataCellViewHolder
import com.etrade.mobilepro.tableviewutils.presentation.holder.SymbolRowHeaderViewHolder
import com.etrade.mobilepro.util.formatLetterRepresentationToNumber
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

private const val SIMPLE_ROW_TYPE = 1000
private const val ROW_WITH_LABELS_TYPE = 1100
private const val STARS_CELL = 1200

class SymbolWithExtraHeightTableViewAdapter(
    context: Context
) : TableViewAdapter(context) {

    private lateinit var rowItems: List<Cell>
    private lateinit var dataItems: List<List<DataCell>>

    private val customColumnWidths = mapOf(
        context.resources.getString(R.string.fund_name) to context.resources.getDimension(R.dimen.fund_name_width).toInt(),
        context.resources.getString(R.string.fund_profile_title_fund_category) to context.resources.getDimension(R.dimen.fund_category_width).toInt()
    )
    private val onSingleLineDataCellTextViewUpdated: (TextView, DataCell) -> Unit = { textView, dataCell ->
        if (dataCell is ResultDataCell) {
            textView.ellipsize = if (dataCell.hasEllipsize) {
                TextUtils.TruncateAt.END
            } else {
                null
            }
        }
    }

    private val onDoubleLineDataCellTextViewUpdated: (TextView, DataCell) -> Unit = { textView, dataCell ->
        if (dataCell is ResultDataCell) {
            textView.ellipsize = if (dataCell.hasEllipsize) {
                TextUtils.TruncateAt.END
            } else {
                null
            }
            if (dataCell.hasEllipsize) {
                textView.maxLines = 2
            } else {
                textView.maxLines = 1
            }
        }
    }

    override fun setAllItems(
        columnHeaderItems: List<Cell>,
        rowHeaderItems: List<Cell>,
        cellItems: List<List<DataCell>>
    ) {
        rowItems = rowHeaderItems
        dataItems = cellItems
        super.setAllItems(columnHeaderItems, rowHeaderItems, cellItems)
    }

    override fun onBindCellViewHolder(holder: AbstractViewHolder, cellItemModel: Any, columnPosition: Int, rowPosition: Int) {
        super.onBindCellViewHolder(holder, cellItemModel, columnPosition, rowPosition)

        val row = getRowHeaderItem(rowPosition) as? SymbolCell
        val column = getColumnHeaderItem(columnPosition)
        val cell = cellItemModel as Cell

        val contentDescriptionWithoutSymbol = "${column.displayTextContentDescription}, ${cell.displayText}".formatLetterRepresentationToNumber()

        val contentDescription = if (row != null) {
            "${row.getSymbolContentDescription()}, $contentDescriptionWithoutSymbol"
        } else {
            contentDescriptionWithoutSymbol
        }
        if (holder is DataCellViewHolder) {
            holder.contentDescription = contentDescription
        } else {
            holder.itemView.contentDescription = contentDescription
        }
    }

    override fun onCreateRowHeaderViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder =
        when (viewType) {
            SIMPLE_ROW_TYPE -> {
                val layout = inflater.inflate(R.layout.table_view_row_header_layout, parent, false)
                SymbolRowHeaderViewHolder(layout)
            }
            ROW_WITH_LABELS_TYPE -> {
                val layout = inflater.inflate(R.layout.table_view_row_header_layout_with_labels, parent, false)
                SymbolRowHeaderWithExtraHeightViewHolder(layout)
            }
            else -> throw IllegalArgumentException("Unknown row header view type")
        }

    override fun onCreateCellViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder =
        when (viewType) {
            SIMPLE_ROW_TYPE -> createSimpleCellViewHolder(parent, onSingleLineDataCellTextViewUpdated)
            ROW_WITH_LABELS_TYPE -> createSimpleCellViewHolder(parent, onDoubleLineDataCellTextViewUpdated)
            STARS_CELL -> {
                val view = inflater.inflate(R.layout.table_view_cell_layout_stars, parent, false)
                ResultDataCellWithStarsViewHolder(view)
            }
            else -> throw IllegalArgumentException("Unknown cell view type")
        }

    private fun createSimpleCellViewHolder(parent: ViewGroup, onTextViewUpdated: ((TextView, DataCell) -> Unit)): DataCellViewHolder {
        val view = inflater.inflate(R.layout.table_view_cell_layout_with_extra_height, parent, false)
        return DataCellViewHolder(
            view,
            onTextViewUpdated
        )
    }

    override fun bindRowHeaderViewHolder(holder: AbstractViewHolder, rowHeaderItemModel: Any, rowPosition: Int) {
        val item = rowHeaderItemModel as ResultSymbolCell
        when (holder) {
            is SymbolRowHeaderViewHolder -> {
                holder.cell = item
                holder.itemView.contentDescription = item.getSymbolContentDescription()
            }
            is SymbolRowHeaderWithExtraHeightViewHolder -> {
                holder.cell = item
                holder.itemView.contentDescription = item.getSymbolContentDescription()
            }
            else -> throw IllegalArgumentException("Unknown row header view type")
        }
    }

    override fun getAllRowHeaderViewTypes() = listOf(SIMPLE_ROW_TYPE, ROW_WITH_LABELS_TYPE)

    override fun getRowHeaderItemViewType(position: Int): Int = getRowType(position)

    override fun getCellItemViewType(row: Int, column: Int): Int {
        return getCellType(getCellItem(column, row), row, column)
    }

    override fun getCellItemViewType(cellItem: Cell?, row: Int, column: Int): Int {
        return getCellType(cellItem, row, column)
    }

    override fun getCellWidth(column: Int, headerCell: Cell): Int? = customColumnWidths[headerCell.displayText]

    override fun getAllCellViewTypes() = listOf(SIMPLE_ROW_TYPE, ROW_WITH_LABELS_TYPE, STARS_CELL)

    private fun getCellType(cellItem: Cell?, row: Int, column: Int): Int {
        val cell = cellItem as? ResultDataCell ?: getCellItem(column, row)
        return if (cell?.value is MorningstarRatingResult) {
            STARS_CELL
        } else {
            getRowType(row)
        }
    }

    private fun getRowType(row: Int): Int {
        val cell = getRowHeaderItem(row) as? ResultSymbolCell ?: rowItems[row] as ResultSymbolCell
        return if (cell.hasExtraHeight || rowHasCellsWithTwoLinedText(row)) {
            ROW_WITH_LABELS_TYPE
        } else {
            SIMPLE_ROW_TYPE
        }
    }

    private fun rowHasCellsWithTwoLinedText(row: Int): Boolean {
        val cellItems = runCatching {
            getCellRowItems(row)
        }.getOrNull() ?: dataItems[row]
        return cellItems.find { (it as? ResultDataCell)?.isWorthSplittingInTwoLines == true } != null
    }
}
