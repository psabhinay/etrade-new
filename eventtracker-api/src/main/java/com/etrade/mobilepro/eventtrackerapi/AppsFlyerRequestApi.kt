package com.etrade.mobilepro.eventtrackerapi

import com.etrade.mobilepro.eventtrackerapi.dto.AppsFlyerRequestDto
import com.etrade.mobilepro.eventtrackerapi.dto.AppsFlyerResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query

interface AppsFlyerRequestApi {
    @JsonMoshi
    @POST("/gdpr/opengdpr_requests")
    @Headers(
        "Content-Type: application/json",
        "Accept: application/json"
    )
    suspend fun makeAppsFlyerRequest(@Query("api_token") apiToken: String, @JsonMoshi @Body request: AppsFlyerRequestDto): AppsFlyerResponseDto
}
