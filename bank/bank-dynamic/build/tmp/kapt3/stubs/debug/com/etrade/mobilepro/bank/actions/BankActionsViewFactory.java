package com.etrade.mobilepro.bank.actions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\bH\u0016J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0002\u00a8\u0006\r"}, d2 = {"Lcom/etrade/mobilepro/bank/actions/BankActionsViewFactory;", "Lcom/etrade/mobilepro/dynamicui/DynamicUiViewFactory;", "()V", "createView", "Lcom/etrade/mobilepro/dynamicui/api/GenericLayout;", "dto", "", "meta", "", "toBankActionsViewModel", "Lcom/etrade/mobilepro/bank/actions/BankActionsViewModel;", "bankAccountId", "", "bank-dynamic_debug"})
public final class BankActionsViewFactory implements com.etrade.mobilepro.dynamicui.DynamicUiViewFactory {
    
    public BankActionsViewFactory() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.etrade.mobilepro.dynamicui.api.GenericLayout createView(@org.jetbrains.annotations.NotNull()
    java.lang.Object dto, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends java.lang.Object> meta) {
        return null;
    }
    
    private final com.etrade.mobilepro.bank.actions.BankActionsViewModel toBankActionsViewModel(java.lang.String bankAccountId) {
        return null;
    }
    
    public void setupValuesFromRequest(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.dynamicui.api.ScreenRequest screenRequest) {
    }
}