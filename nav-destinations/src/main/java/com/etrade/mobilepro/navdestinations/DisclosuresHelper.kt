package com.etrade.mobilepro.navdestinations

import android.content.Context
import org.chromium.customtabsclient.shared.R
import org.chromium.customtabsclient.shared.WebviewActivity

fun Context.openDisclosures() = WebviewActivity.intent(
    this,
    "https://us.etrade.com/l/f/disclosure-library",
    getString(R.string.disclosures),
    R.drawable.ic_close
)
