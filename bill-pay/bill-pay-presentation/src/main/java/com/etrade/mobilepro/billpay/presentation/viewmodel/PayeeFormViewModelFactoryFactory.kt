package com.etrade.mobilepro.billpay.presentation.viewmodel

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.billpay.presentation.common.ParcelablePayee
import com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.PayeeFormViewModel
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

interface PayeeFormViewModelFactoryFactory {
    fun create(billPaySessionId: String, payeeItem: ParcelablePayee?, isEditMode: Boolean): ViewModelProvider.Factory
}

@FlowPreview
@ExperimentalCoroutinesApi
class PayeeFormViewModelFactoryFactoryImp @Inject constructor(
    private val factory: PayeeFormViewModel.Factory
) : PayeeFormViewModelFactoryFactory {
    override fun create(billPaySessionId: String, payeeItem: ParcelablePayee?, isEditMode: Boolean): ViewModelProvider.Factory =
        viewModelFactory { factory.create(billPaySessionId, payeeItem, isEditMode) }
}
