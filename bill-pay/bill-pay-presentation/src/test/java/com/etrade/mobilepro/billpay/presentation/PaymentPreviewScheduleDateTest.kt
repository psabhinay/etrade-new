package com.etrade.mobilepro.billpay.presentation

import android.os.Parcel
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobile.accounts.billpay.BillPay
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.presentation.common.ParcelablePayee
import com.etrade.mobilepro.billpay.presentation.paymentpreview.PaymentPreview
import com.etrade.mobilepro.util.formatters.createAmericanSimpleDateFormatter
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import org.threeten.bp.LocalDate
import java.math.BigDecimal
import java.math.BigInteger

@RunWith(AndroidJUnit4::class)
@Config(application = android.app.Application::class)
class PaymentPreviewScheduleDateTest {

    @Test
    fun `payment schedule date should be the same after parcel and un-parcel`() {
        val localDate = LocalDate.of(2020, 5, 5)
        val objToBeParcel = PaymentPreview(
            originAccount = originAccount,
            destinationAccount = destinationAccount,
            scheduleDate = localDate,
            amount = BigDecimal("40.00"),
            memo = null
        )

        // Parcel and retrieve the date
        val parcel = Parcel.obtain()
        objToBeParcel.run { writeToParcel(parcel, describeContents()) }
        parcel.setDataPosition(0)
        val creator = PayeeParcelerTest.getParcelableCreator(PaymentPreview::class.java)
        val result = creator.createFromParcel(parcel) as PaymentPreview
        assertEquals(objToBeParcel.scheduleDate, result.scheduleDate)

        // Get the string representation for the same
        val expected = createAmericanSimpleDateFormatter().format(objToBeParcel.scheduleDate)
        val actual = createAmericanSimpleDateFormatter().format(result.scheduleDate)
        assertEquals(expected, actual)
    }

    private val originAccount = object : AccountDetails {
        override val accountIdKey: String
            get() = "AccountId"
        override val maskedAccountId: String
            get() = "MaskedAccountId"
        override val accountMode: String
            get() = "AccountMode"
        override val accountName: String
            get() = "AccountName"
        override val accountShortName: String
            get() = "AccountShortName"
        override val accountLongName: String
            get() = "AccountLongName"
        override val mobileDescription: String
            get() = "MobileDescription"
        override val accountValue: BigDecimal?
            get() = null
        override val institutionType: String
            get() = "InstitutionType"
        override val accountStatus: String
            get() = "AccountStatus"
        override val restrictionLevel: String
            get() = "RestrictionLevel"
        override val closedDate: Long
            get() = 1L
        override val brokerage: AccountDetails.Brokerage?
            get() = object : AccountDetails.Brokerage {
                override val cashAvailableForWithdrawal: BigDecimal?
                    get() = null
                override val marginAvailableForWithdrawal: BigDecimal?
                    get() = null
                override val purchasingPower: BigDecimal?
                    get() = null
                override val totalAvailableForWithdrawal: BigDecimal?
                    get() = null
                override val marginBalance: BigDecimal?
                    get() = null
                override val netCashBalance: BigDecimal?
                    get() = null
                override val cmAccount: Boolean?
                    get() = null
                override val contributionDetails: List<AccountDetails.ContributionDetails>
                    get() = emptyList()
            }
        override val billPay: BillPay?
            get() = null
        override val accountId: String
            get() = "AccountId"
        override val nickname: String?
            get() = "NickName"
        override val instNo: BigInteger
            get() = BigInteger("1")
        override val accountType: String
            get() = "AccountType"
        override val accountCategory: String
            get() = "AccountCategory"
    }

    private val destinationAccount = ParcelablePayee(
        payeeId = "dest1",
        name = "dest1",
        nickName = null,
        phoneNumber = null,
        accountNumber = "dest1",
        leadDays = -1,
        isAddressAvailable = null,
        address = null
    )
}
