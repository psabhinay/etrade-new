package com.etrade.mobilepro.trade.presentation.form.delegate.conditional

import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.trade.presentation.usecase.CreateConditionalTradeFormParameter

internal interface ConditionalFormFactoryDelegate {

    fun createForm(
        parameter: CreateConditionalTradeFormParameter,
        symbolChangedListener: SymbolChangedListener,
        initialSymbol: WithSymbolInfo? = null
    ): Form

    interface SymbolChangedListener {
        fun onSymbolChange(withSymbolInfo: WithSymbolInfo)
    }
}
