package com.etrade.mobilepro.util.formatters

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.eo.core.util.MarketDataFormatter.EMPTY_FORMATTED_VALUE
import java.math.BigDecimal

private val VOLUME_THRESHOLD = BigDecimal("1000000000000000")

fun safeFormatVolume(volume: BigDecimal?) = if (volume != null && volume < VOLUME_THRESHOLD) MarketDataFormatter.formatVolume(volume) else EMPTY_FORMATTED_VALUE
