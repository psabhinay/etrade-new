package com.etrade.mobilepro.searching.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(strict = false, name = "OptionMonthsList")
data class OptionMonthsDto(
    @field:ElementList(name = "ED", inline = true)
    var dateList: List<EDDateStructDto>? = null
)

@Root(strict = false, name = "ED")
data class EDDateStructDto(
    @field:Element(name = "Year")
    var year: Int? = null,
    @field:Element(name = "Month")
    var month: Int? = null,
    @field:Element(name = "Day")
    var day: Int? = null,
    @field:Element(name = "SettlementSessionCd")
    var settlementSessionCd: Int? = null
)
