package com.etrade.mobilepro.dynamic.form.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.value.MutableInputValuesSnapshot

sealed class Group(
    val components: List<Component>,
    override val isVisible: LiveData<Boolean> = MutableLiveData(true)
) : BaseInputHolder(components.mapInputs()), Component, InputValueHolder, InputHolder {

    final override fun collectValuesTo(snapshot: MutableInputValuesSnapshot) {
        components {
            if (isVisible.value == true) {
                snapshot[id] = parameterValue.value
            }
        }
    }

    final override fun injectValuesFrom(map: InputValuesSnapshot) {
        components {
            map[id]?.let(::setParameterValue)
        }
    }

    final override fun disabledFields(list: List<String>) {
        components {
            if (list.contains(this.id)) {
                disableInput()
            }
        }
    }

    final override fun resetValues() {
        components(Input<Any>::resetValue)
    }
}

class HorizontalGroup(components: List<Component>, override val isVisible: LiveData<Boolean>) : Group(components)

class VerticalGroup(components: List<Component>, override val isVisible: LiveData<Boolean>) : Group(components)

internal fun Iterable<Component>.mapInputs(): Map<String, Input<Any>> {
    val map = mutableMapOf<String, Input<Any>>()
    forEach {
        @Suppress("UNCHECKED_CAST")
        when (it) {
            is Input<*> -> map[it.id] = it as Input<Any>
            is Group -> map.putAll(it.inputMap)
        }
    }
    return map.toMap()
}

private fun Group.components(block: Input<Any>.() -> Unit) {
    components.forEach {
        @Suppress("UNCHECKED_CAST")
        when (it) {
            is Input<*> -> (it as Input<Any>).block()
            is Group -> it.components(block)
        }
    }
}
