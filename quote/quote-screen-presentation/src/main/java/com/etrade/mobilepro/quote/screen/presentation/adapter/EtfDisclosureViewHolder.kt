package com.etrade.mobilepro.quote.screen.presentation.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemEtfDisclosureBinding
import com.etrade.mobilepro.quoteapi.QuoteItem

class EtfDisclosureViewHolder(private val binding: QuoteItemEtfDisclosureBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bindTo(
        item: QuoteItem.QuoteEtfDisclosureItem?,
        disclosureClickListener: (() -> Unit)? = null
    ) {
        item?.let {
            binding.disclosureLink.setOnClickListener {
                disclosureClickListener?.invoke()
            }
        }
    }
}
