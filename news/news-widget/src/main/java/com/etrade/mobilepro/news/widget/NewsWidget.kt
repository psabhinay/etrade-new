package com.etrade.mobilepro.news.widget

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.itemsIndexed
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.etColors
import com.etrade.mobilepro.compose.components.DisclosuresItem
import com.etrade.mobilepro.news.api.model.NewsItem
import kotlinx.coroutines.ExperimentalCoroutinesApi

@OptIn(ExperimentalCoroutinesApi::class)
@Composable
fun NewsWidget(
    items: LazyPagingItems<NewsItem>,
    onNewsTap: (NewsItem.Text, Int) -> Unit,
    onSymbolTap: (String) -> Unit,
    onDisclosuresTap: () -> Unit,
    wasRead: (String) -> Boolean,
) {
    LazyColumn {
        NewsWidgetItem(items, onNewsTap, onSymbolTap, onDisclosuresTap, wasRead)
    }
}

@Suppress("FunctionNaming")
@OptIn(ExperimentalCoroutinesApi::class)
fun LazyListScope.NewsWidgetItem(
    itemsList: LazyPagingItems<NewsItem>,
    onNewsTap: (NewsItem.Text, Int) -> Unit,
    onSymbolTap: (String) -> Unit,
    onDisclosuresTap: () -> Unit,
    wasRead: (String) -> Boolean,
) {
    ThickDividerItem()
    if (itemsList.hasNoNews() && itemsList.loadState.refresh !is LoadState.Loading) {
        NoNewsItem()
    }
    if (itemsList.loadState.refresh is LoadState.Loading) {
        LoadingIndicatorItem()
    } else {
        itemsIndexed(itemsList) { index, item ->
            (item as? NewsItem.Text)?.let {
                NewsCell(
                    item = it,
                    wasRead = wasRead(it.docId),
                    modifier = Modifier.clickable { onNewsTap(it, index) },
                    onSymbolTap = onSymbolTap
                )
                if (index < itemsList.itemCount) {
                    Divider(color = MaterialTheme.etColors.lightGrey, thickness = 1.dp)
                }
            }
        }
    }
    if (itemsList.loadState.append is LoadState.Loading) {
        LoadingIndicatorItem()
    }
    DisclosuresItem(onDisclosuresTap)
}

@Suppress("FunctionNaming")
private fun LazyListScope.ThickDividerItem() = item {
    Box(
        modifier = Modifier
            .height(Dimens.Size16dp)
            .fillMaxWidth()
            .background(colorResource(R.color.light_grey))
    )
}

@Suppress("FunctionNaming")
private fun LazyListScope.NoNewsItem() = item {
    Text(
        stringResource(R.string.empty_news),
        modifier = Modifier.fillMaxWidth().padding(Dimens.Size16dp),
        textAlign = TextAlign.Center,
        color = MaterialTheme.etColors.grey
    )
}

@Suppress("FunctionNaming")
private fun LazyListScope.LoadingIndicatorItem() =
    item { NewsLoadingCell(Modifier.fillMaxWidth().padding(Dimens.Size16dp)) }

fun LazyPagingItems<NewsItem>.hasNoNews(): Boolean =
    this.itemCount == 0 || (this.itemCount == 1 && this[0] is NewsItem.Empty)
