package com.etrade.mobilepro.watchlist.news.filter

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.sp
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.etColors
import com.etrade.mobilepro.watchlist.news.R

@Composable
fun WatchlistFilter(title: String, isLoading: Boolean, modifier: Modifier, onTap: () -> Unit) {
    Row(modifier.clickable(onClick = onTap), verticalAlignment = Alignment.CenterVertically) {
        Text(
            stringResource(R.string.filter),
            modifier = Modifier.padding(Dimens.Size16dp)
                .requiredWidth(IntrinsicSize.Min),
            style = captionStyle(),
        )
        if (isLoading) {
            Box(
                Modifier
                    .weight(1f)
                    .height(Dimens.Size16dp * 2)
                    .padding(end = Dimens.Size16dp),
                contentAlignment = Alignment.CenterEnd
            ) {
                CircularProgressIndicator(color = MaterialTheme.etColors.purple)
            }
        } else {
            Text(
                title,
                modifier = modifier.padding(Dimens.Size16dp).weight(1f),
                style = filterTitleStyle(),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                textAlign = TextAlign.End
            )
        }
    }
}

// once all et redesign language is ported to Compose use that and remove these
@Suppress("MagicNumber")
@Composable
private fun filterTitleStyle() = MaterialTheme.typography.subtitle2.copy(
    color = MaterialTheme.etColors.purple,
    fontWeight = FontWeight.Normal,
    fontSize = 16.sp,
)

@Suppress("MagicNumber")
@Composable
private fun captionStyle() = MaterialTheme.typography.subtitle2.copy(
    fontWeight = FontWeight.Normal,
    fontSize = 14.sp
)
