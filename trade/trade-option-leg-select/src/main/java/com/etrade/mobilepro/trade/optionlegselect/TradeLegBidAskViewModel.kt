package com.etrade.mobilepro.trade.optionlegselect

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.etrade.eo.netbidaskcalculator.api.NetBidAskCalculator
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.livedata.OutsourcedLiveData
import com.etrade.mobilepro.livedata.sourceFor
import com.etrade.mobilepro.optionschains.api.Option
import com.etrade.mobilepro.orders.api.signedQuantity
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quoteapi.QuoteBidAskItem
import com.etrade.mobilepro.quoteapi.QuotePriceWidgetItem
import com.etrade.mobilepro.quoteapi.QuoteWidgetItem
import com.etrade.mobilepro.quoteapi.usecase.GetMobileQuotesParameter
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.api.option.isOption
import com.etrade.mobilepro.util.invoke
import com.etrade.mobilepro.util.safeParseBigDecimal
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import javax.inject.Inject

class TradeLegBidAskViewModel @Inject constructor(
    private val getOptionQuotesUseCase: GetOptionQuotesUseCase,
    private val netBidAskCalculator: NetBidAskCalculator,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    private val streamingStatusController: StreamingStatusController,
    private val resources: Resources
) : ViewModel() {

    val quoteBidAskItem = object : QuoteBidAskItem {
        override val bidPrice: LiveData<QuotePriceWidgetItem>
            get() = _bidPrice
        override val askPrice: LiveData<QuotePriceWidgetItem>
            get() = _askPrice
    }

    private val quote: OutsourcedLiveData<QuoteWidgetItem?, QuoteWidgetItem> = OutsourcedLiveData { quote, liveData ->
        if (quote != null) {
            liveData.value = quote
        }
    }

    var quoteWidgetViewModel: QuotesWidgetViewModel? by sourceFor(quote) { quote }

    private val stockAskPrice: LiveData<QuotePriceWidgetItem> = quote.switchMap { it.askPrice }
    private val stockBidPrice: LiveData<QuotePriceWidgetItem> = quote.switchMap { it.bidPrice }

    private var _normalizedRatio = MutableLiveData<String>()
    val normalizedRatio: LiveData<String>
        get() = _normalizedRatio

    private val netBidAskObserver = Observer<Any> { updateNetBidAsk() }

    private val _askPrice: MediatorLiveData<QuotePriceWidgetItem> = MediatorLiveData()
    private val _bidPrice: MediatorLiveData<QuotePriceWidgetItem> = MediatorLiveData()

    private val optionStreamFields = setOf(
        Level1Field.ASK,
        Level1Field.ASK_SIZE,
        Level1Field.BID,
        Level1Field.BID_SIZE
    )

    private val streams = CompositeDisposable()

    private val logger: Logger = LoggerFactory.getLogger(TradeLegBidAskViewModel::class.java)

    private var currentLegs: Collection<BidAskSource> = emptyList()
        set(value) {
            field = value
            updateNetBidAsk()
        }

    override fun onCleared() {
        super.onCleared()
        streams.clear()
    }

    fun subscribeToBidAsk(legs: Iterable<TradeLeg>, streamOptions: Boolean = true, shouldRunBlocking: Boolean = false) {
        if (currentLegs.isNotEmpty()) {
            unsubscribeBidAsk()
        }

        val noOptionDataLegs = legs.filter { leg ->
            leg.isOption && leg !is OptionTradeLegWithOption
        }

        if (noOptionDataLegs.isEmpty()) {
            logger.debug("All option legs have option data.")
            subscribe(legs, streamOptions)
        } else {
            logger.debug("Some option legs do not have option data.")
            if (shouldRunBlocking) {
                runBlocking {
                    getQuotesAndSubscribe(noOptionDataLegs, legs, streamOptions)
                }
            } else {
                viewModelScope.launch {
                    getQuotesAndSubscribe(noOptionDataLegs, legs, streamOptions)
                }
            }
        }
    }

    private suspend fun getQuotesAndSubscribe(noOptionDataLegs: List<TradeLeg>, legs: Iterable<TradeLeg>, streamOptions: Boolean) {
        // Trade page requires requireExchangeCode to be set to true,
        // this will force the service to return real time quotes irrespective of user subscriptions
        getOptionQuotesUseCase(
            GetMobileQuotesParameter(
                noOptionDataLegs.map { it.symbol },
                requireExchangeCode = true
            )
        )
        getOptionQuotesUseCase(GetMobileQuotesParameter(noOptionDataLegs.map { it.symbol }, requireExchangeCode = true))
            .onSuccess { subscribeIfLegsValid(it, legs, streamOptions) }
            .onFailure { logger.error("Can't load option quote details", it) }
    }

    private fun subscribeIfLegsValid(optionsList: List<Option>, legs: Iterable<TradeLeg>, streamOptions: Boolean) {
        val options = optionsList.associateBy { option -> option.symbol }
        val formattedLegs: List<TradeLeg> = legs.mapNotNull { leg ->
            if (leg.isOption && leg !is OptionTradeLegWithOption) {
                options[leg.symbol]?.createOptionTradeLeg(leg)
            } else {
                leg
            }
        }
        if (formattedLegs.size == legs.count()) {
            subscribe(formattedLegs, streamOptions)
        }
    }

    fun unsubscribeBidAsk() {
        streams.clear()
        currentLegs.forEach {
            _askPrice.removeSource(it.ask)
            _bidPrice.removeSource(it.bid)
        }
        currentLegs = emptyList()
    }

    private fun subscribe(legs: Iterable<TradeLeg>, streamOptions: Boolean) {
        currentLegs = legs.map { it.toBidAskSource() }
            .onEach { source ->
                _askPrice.addSource(source.ask, netBidAskObserver)
                _bidPrice.addSource(source.bid, netBidAskObserver)
            }

        if (streamOptions) {
            stream(legs.filterIsInstance<OptionTradeLegWithOption>())
        }
    }

    private fun stream(options: Iterable<OptionTradeLegWithOption>) {
        if (streamingStatusController.isStreamingToggleEnabled) {
            options.map { option ->
                val stream = streamingQuoteProvider.getQuote(option.symbol, InstrumentType.OPTN, optionStreamFields).subscribe { data ->
                    option.option.updateItem(data)
                }
                streams.add(stream)
            }
        }
    }

    private fun updateNetBidAsk() {
        val spreadLegs = currentLegs
            .asSequence()
            .map { it.spreadLeg }
            .filter { it.quantity != 0 }
            .toList()

        if (spreadLegs.isNotEmpty()) {
            val isMultiLeg = spreadLegs.size > 1
            val netBidAsk = netBidAskCalculator.calculateNetBidAsk(spreadLegs)
            _askPrice.value = createQuotePriceWidgetItem(netBidAsk.netAsk, isMultiLeg) { spreadLegs[0].askSize }
            _bidPrice.value = createQuotePriceWidgetItem(netBidAsk.netBid, isMultiLeg) { spreadLegs[0].bidSize }

            val normalizedRatioList = netBidAsk.normalizedRatios

            _normalizedRatio.value = if (!netBidAsk.isInRatio) {
                formatRatio(normalizedRatioList, netBidAsk.greatestCommonFactor)
            } else null
        }
    }

    private fun createQuotePriceWidgetItem(
        price: BigDecimal,
        isMultiLeg: Boolean,
        size: () -> String?
    ): QuotePriceWidgetItem {
        return QuotePriceWidgetItem(
            price = price.abs().toString(),
            size = if (isMultiLeg) null else size(),
            isNet = isMultiLeg
        )
    }

    private fun Option.createOptionTradeLeg(prototype: TradeLeg): OptionTradeLegWithOption {
        return OptionTradeLegWithOption(
            this,
            prototype.transactionType,
            prototype.quantity,
            prototype.symbol,
            prototype.displaySymbol,
            prototype.isAMOption,
            false
        )
    }

    private fun TradeLeg.toBidAskSource(): BidAskSource {
        return if (isOption) {
            when (this) {
                is OptionTradeLegWithOption -> OptionLegBidAskSource(this)
                else -> throw UnsupportedOperationException("Unknown type: $this")
            }
        } else {
            StockLegBidAskSource(this)
        }
    }

    private fun formatRatio(normalizedRatio: List<Int>, greatestCommonFactor: Int): String {
        // The number of shares is in index 0 in normalizedRatio
        val optionRatios = StringBuilder(resources.getQuantityString(R.plurals.trade_shares, normalizedRatio.first(), normalizedRatio.first()))
        for (index in 1 until normalizedRatio.size) {
            optionRatios.append(":").append(normalizedRatio[index])
        }
        return resources.getString(
            R.string.trade_order_ratio,
            optionRatios,
            resources.getQuantityString(R.plurals.trade_spreads, greatestCommonFactor, greatestCommonFactor)
        )
    }

    private interface BidAskSource {
        val ask: LiveData<*>
        val bid: LiveData<*>
        val spreadLeg: InternalSpreadLeg
    }

    private class OptionLegBidAskSource(private val optionTradeLeg: OptionTradeLegWithOption) : BidAskSource {
        override val ask: LiveData<*> = optionTradeLeg.option.streamAsk
        override val bid: LiveData<*> = optionTradeLeg.option.streamBid
        override val spreadLeg: InternalSpreadLeg
            get() = OptionSpreadLeg(optionTradeLeg)
    }

    private inner class StockLegBidAskSource(private val stockTradeLeg: TradeLeg) : BidAskSource {
        override val ask: LiveData<*> = stockAskPrice
        override val bid: LiveData<*> = stockBidPrice
        override val spreadLeg: InternalSpreadLeg
            get() {
                val askPrice = stockAskPrice.value
                val bidPrice = stockBidPrice.value

                return StockSpreadLeg(
                    ask = askPrice?.price?.safeParseBigDecimal() ?: BigDecimal.ZERO,
                    bid = bidPrice?.price?.safeParseBigDecimal() ?: BigDecimal.ZERO,
                    askSize = askPrice?.size,
                    bidSize = bidPrice?.size,
                    quantity = stockTradeLeg.transactionType.signedQuantity(stockTradeLeg.quantity)
                )
            }
    }
}
