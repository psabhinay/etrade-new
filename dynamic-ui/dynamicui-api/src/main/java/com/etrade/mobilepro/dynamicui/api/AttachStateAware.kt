package com.etrade.mobilepro.dynamicui.api

import androidx.recyclerview.widget.RecyclerView

interface AttachStateAware {
    fun onAttached(holder: RecyclerView.ViewHolder)
    fun onDetached(holder: RecyclerView.ViewHolder)
}
