package com.etrade.mobilepro.chat.presentation.message.viewHolder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class ChatMessageTransferQueueViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
