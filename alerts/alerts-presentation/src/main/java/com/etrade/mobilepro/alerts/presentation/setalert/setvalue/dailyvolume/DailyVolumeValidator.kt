package com.etrade.mobilepro.alerts.presentation.setalert.setvalue.dailyvolume

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import java.util.regex.Pattern

// based on https://regexr.com/2rios
private val pattern: Pattern by lazy { Pattern.compile("""[\d]{1,8}""") }

internal class DailyVolumeValidator(private val genericValidationErrorDesc: String) : InputFieldManager.Validator {
    override fun validate(value: String): InputFieldManager.Value {
        if (value.isEmpty()) {
            return InputFieldManager.Value.Valid(value)
        }
        return if (pattern.matcher(value).matches()) {
            InputFieldManager.Value.Valid(value)
        } else {
            InputFieldManager.Value.Invalid(value, genericValidationErrorDesc)
        }
    }
}
