package com.etrade.mobilepro.order.details.dto;

import com.etrade.mobilepro.backends.neo.BaseDataDto;
import com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto;
import com.etrade.mobilepro.orders.data.dto.AuditInfoDto;
import com.etrade.mobilepro.orders.data.dto.HeaderDto;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\bT\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B\u0093\u0002\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0001\u0010\b\u001a\u00020\u0003\u0012\b\b\u0001\u0010\t\u001a\u00020\u0003\u0012\b\b\u0001\u0010\n\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u000b\u001a\u00020\u0003\u0012\b\b\u0001\u0010\f\u001a\u00020\u0003\u0012\b\b\u0001\u0010\r\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u000e\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u000f\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0010\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0011\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0012\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0013\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0014\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0015\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0016\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0017\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0018\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0019\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u001a\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u001b\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u001c\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u001d\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u001eJ\t\u0010;\u001a\u00020\u0003H\u00c6\u0003J\t\u0010<\u001a\u00020\u0003H\u00c6\u0003J\t\u0010=\u001a\u00020\u0003H\u00c6\u0003J\t\u0010>\u001a\u00020\u0003H\u00c6\u0003J\t\u0010?\u001a\u00020\u0003H\u00c6\u0003J\t\u0010@\u001a\u00020\u0003H\u00c6\u0003J\t\u0010A\u001a\u00020\u0003H\u00c6\u0003J\t\u0010B\u001a\u00020\u0003H\u00c6\u0003J\t\u0010C\u001a\u00020\u0003H\u00c6\u0003J\t\u0010D\u001a\u00020\u0003H\u00c6\u0003J\t\u0010E\u001a\u00020\u0003H\u00c6\u0003J\t\u0010F\u001a\u00020\u0003H\u00c6\u0003J\t\u0010G\u001a\u00020\u0003H\u00c6\u0003J\t\u0010H\u001a\u00020\u0003H\u00c6\u0003J\t\u0010I\u001a\u00020\u0003H\u00c6\u0003J\t\u0010J\u001a\u00020\u0003H\u00c6\u0003J\t\u0010K\u001a\u00020\u0003H\u00c6\u0003J\t\u0010L\u001a\u00020\u0003H\u00c6\u0003J\t\u0010M\u001a\u00020\u0003H\u00c6\u0003J\t\u0010N\u001a\u00020\u0003H\u00c6\u0003J\t\u0010O\u001a\u00020\u0003H\u00c6\u0003J\t\u0010P\u001a\u00020\u0003H\u00c6\u0003J\t\u0010Q\u001a\u00020\u0003H\u00c6\u0003J\t\u0010R\u001a\u00020\u0003H\u00c6\u0003J\t\u0010S\u001a\u00020\u0003H\u00c6\u0003J\t\u0010T\u001a\u00020\u0003H\u00c6\u0003J\t\u0010U\u001a\u00020\u0003H\u00c6\u0003J\u0097\u0002\u0010V\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u00032\b\b\u0003\u0010\u0006\u001a\u00020\u00032\b\b\u0003\u0010\u0007\u001a\u00020\u00032\b\b\u0003\u0010\b\u001a\u00020\u00032\b\b\u0003\u0010\t\u001a\u00020\u00032\b\b\u0003\u0010\n\u001a\u00020\u00032\b\b\u0003\u0010\u000b\u001a\u00020\u00032\b\b\u0003\u0010\f\u001a\u00020\u00032\b\b\u0003\u0010\r\u001a\u00020\u00032\b\b\u0003\u0010\u000e\u001a\u00020\u00032\b\b\u0003\u0010\u000f\u001a\u00020\u00032\b\b\u0003\u0010\u0010\u001a\u00020\u00032\b\b\u0003\u0010\u0011\u001a\u00020\u00032\b\b\u0003\u0010\u0012\u001a\u00020\u00032\b\b\u0003\u0010\u0013\u001a\u00020\u00032\b\b\u0003\u0010\u0014\u001a\u00020\u00032\b\b\u0003\u0010\u0015\u001a\u00020\u00032\b\b\u0003\u0010\u0016\u001a\u00020\u00032\b\b\u0003\u0010\u0017\u001a\u00020\u00032\b\b\u0003\u0010\u0018\u001a\u00020\u00032\b\b\u0003\u0010\u0019\u001a\u00020\u00032\b\b\u0003\u0010\u001a\u001a\u00020\u00032\b\b\u0003\u0010\u001b\u001a\u00020\u00032\b\b\u0003\u0010\u001c\u001a\u00020\u00032\b\b\u0003\u0010\u001d\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010W\u001a\u00020X2\b\u0010Y\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010Z\u001a\u00020[H\u00d6\u0001J\t\u0010\\\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u001a\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0011\u0010\u0018\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010 R\u0011\u0010\u001c\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010 R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010 R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010 R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010 R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010 R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010 R\u0011\u0010\u0010\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010 R\u0011\u0010\b\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010 R\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010 R\u0011\u0010\n\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b+\u0010 R\u0011\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b,\u0010 R\u0011\u0010\f\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b-\u0010 R\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010 R\u0011\u0010\u001d\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b/\u0010 R\u0011\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b0\u0010 R\u0011\u0010\u0011\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u0010 R\u0011\u0010\u0012\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b2\u0010 R\u0011\u0010\u0013\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b3\u0010 R\u0011\u0010\u0014\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b4\u0010 R\u0011\u0010\u0015\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b5\u0010 R\u0011\u0010\u000f\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b6\u0010 R\u0011\u0010\u001b\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b7\u0010 R\u0011\u0010\u0019\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b8\u0010 R\u0011\u0010\u0016\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b9\u0010 R\u0011\u0010\u0017\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b:\u0010 \u00a8\u0006]"}, d2 = {"Lcom/etrade/mobilepro/order/details/dto/LegDetailsDto;", "", "averageFillPrice", "", "commissionAmount", "displaySymbol", "filledQuantity", "instrumentName", "legNumber", "optCallPut", "optExpireDate", "optExpireYearMonth", "optStrikePrice", "orderAction", "otherFee", "secFee", "iofFee", "otherFeeCode", "positionEffect", "qtyReserveShow", "quantityType", "quantityValue", "typeCode", "underlyingProductId", "authMs", "shortSaleCd", "authMSEST", "shortSaleAuthID", "authStatusCd", "originalSymbol", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAuthMSEST", "()Ljava/lang/String;", "getAuthMs", "getAuthStatusCd", "getAverageFillPrice", "getCommissionAmount", "getDisplaySymbol", "getFilledQuantity", "getInstrumentName", "getIofFee", "getLegNumber", "getOptCallPut", "getOptExpireDate", "getOptExpireYearMonth", "getOptStrikePrice", "getOrderAction", "getOriginalSymbol", "getOtherFee", "getOtherFeeCode", "getPositionEffect", "getQtyReserveShow", "getQuantityType", "getQuantityValue", "getSecFee", "getShortSaleAuthID", "getShortSaleCd", "getTypeCode", "getUnderlyingProductId", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component23", "component24", "component25", "component26", "component27", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "orders-details-data"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class LegDetailsDto {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String averageFillPrice = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String commissionAmount = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String displaySymbol = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String filledQuantity = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String instrumentName = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String legNumber = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String optCallPut = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String optExpireDate = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String optExpireYearMonth = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String optStrikePrice = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String orderAction = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String otherFee = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String secFee = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String iofFee = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String otherFeeCode = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String positionEffect = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String qtyReserveShow = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String quantityType = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String quantityValue = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String typeCode = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String underlyingProductId = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String authMs = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String shortSaleCd = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String authMSEST = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String shortSaleAuthID = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String authStatusCd = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String originalSymbol = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.LegDetailsDto copy(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "averageFillPrice")
    java.lang.String averageFillPrice, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "commissionAmount")
    java.lang.String commissionAmount, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "displaySymbol")
    java.lang.String displaySymbol, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "filledQuantity")
    java.lang.String filledQuantity, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "instrumentName")
    java.lang.String instrumentName, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "legNumber")
    java.lang.String legNumber, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "optCallPut")
    java.lang.String optCallPut, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "optExpireDate")
    java.lang.String optExpireDate, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "optExpireYearMonth")
    java.lang.String optExpireYearMonth, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "optStrikePrice")
    java.lang.String optStrikePrice, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderAction")
    java.lang.String orderAction, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "otherFee")
    java.lang.String otherFee, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "secFee")
    java.lang.String secFee, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "iofFee")
    java.lang.String iofFee, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "otherFeeCode")
    java.lang.String otherFeeCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "positionEffect")
    java.lang.String positionEffect, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "qtyReserveShow")
    java.lang.String qtyReserveShow, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "quantityType")
    java.lang.String quantityType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "quantityValue")
    java.lang.String quantityValue, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "typeCode")
    java.lang.String typeCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "underlyingProductId")
    java.lang.String underlyingProductId, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "authMs")
    java.lang.String authMs, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "shortSaleCd")
    java.lang.String shortSaleCd, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "authMSEST")
    java.lang.String authMSEST, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "shortSaleAuthID")
    java.lang.String shortSaleAuthID, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "authStatusCd")
    java.lang.String authStatusCd, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "originalSymbol")
    java.lang.String originalSymbol) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public LegDetailsDto(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "averageFillPrice")
    java.lang.String averageFillPrice, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "commissionAmount")
    java.lang.String commissionAmount, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "displaySymbol")
    java.lang.String displaySymbol, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "filledQuantity")
    java.lang.String filledQuantity, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "instrumentName")
    java.lang.String instrumentName, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "legNumber")
    java.lang.String legNumber, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "optCallPut")
    java.lang.String optCallPut, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "optExpireDate")
    java.lang.String optExpireDate, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "optExpireYearMonth")
    java.lang.String optExpireYearMonth, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "optStrikePrice")
    java.lang.String optStrikePrice, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderAction")
    java.lang.String orderAction, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "otherFee")
    java.lang.String otherFee, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "secFee")
    java.lang.String secFee, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "iofFee")
    java.lang.String iofFee, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "otherFeeCode")
    java.lang.String otherFeeCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "positionEffect")
    java.lang.String positionEffect, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "qtyReserveShow")
    java.lang.String qtyReserveShow, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "quantityType")
    java.lang.String quantityType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "quantityValue")
    java.lang.String quantityValue, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "typeCode")
    java.lang.String typeCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "underlyingProductId")
    java.lang.String underlyingProductId, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "authMs")
    java.lang.String authMs, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "shortSaleCd")
    java.lang.String shortSaleCd, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "authMSEST")
    java.lang.String authMSEST, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "shortSaleAuthID")
    java.lang.String shortSaleAuthID, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "authStatusCd")
    java.lang.String authStatusCd, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "originalSymbol")
    java.lang.String originalSymbol) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAverageFillPrice() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCommissionAmount() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDisplaySymbol() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFilledQuantity() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getInstrumentName() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLegNumber() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOptCallPut() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOptExpireDate() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component9() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOptExpireYearMonth() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component10() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOptStrikePrice() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component11() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOrderAction() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component12() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOtherFee() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component13() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSecFee() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component14() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getIofFee() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component15() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOtherFeeCode() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component16() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPositionEffect() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component17() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQtyReserveShow() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component18() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQuantityType() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component19() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQuantityValue() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component20() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTypeCode() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component21() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUnderlyingProductId() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component22() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAuthMs() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component23() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getShortSaleCd() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component24() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAuthMSEST() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component25() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getShortSaleAuthID() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component26() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAuthStatusCd() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component27() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOriginalSymbol() {
        return null;
    }
}