package com.etrade.mobilepro.instrument

enum class PricingParameterCode(private vararg val typeCodes: String) {
    FRONT_END_LOAD("F"),
    CONTINGENT_DEFERRED("C"), // https://www.investopedia.com/terms/c/cdsc.asp
    NON_FRONT_END_LOAD("N");

    companion object {
        fun from(typeCode: String?): PricingParameterCode = typeCode?.let {
            values().firstOrNull { it.typeCodes.contains(typeCode) }
        } ?: NON_FRONT_END_LOAD
    }
}
