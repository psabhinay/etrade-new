package com.etrade.mobilepro.quotelookup.adapter

import com.etrade.mobilepro.quote.search.presentation.OnSearchResultItemClickListener
import com.etrade.mobilepro.quote.search.presentation.SectionTitleItemAdapterDelegate
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class OptionsSearchResultsAdapter(symbolSelectListener: OnSearchResultItemClickListener? = null) : ListDelegationAdapter<MutableList<SearchResultItem>>() {
    private var optionItemAdapterDelegate: OptionItemAdapterDelegate = OptionItemAdapterDelegate(symbolSelectListener)

    init {
        delegatesManager.addDelegate(optionItemAdapterDelegate)
        delegatesManager.addDelegate(SectionTitleItemAdapterDelegate())
        items = mutableListOf()
    }

    fun updateData(newList: List<SearchResultItem>) {
        dispatchUpdates(items, newList)
        items.clear()
        items.addAll(newList)
    }
}
