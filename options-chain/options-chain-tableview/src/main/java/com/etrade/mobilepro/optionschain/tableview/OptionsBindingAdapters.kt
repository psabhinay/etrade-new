package com.etrade.mobilepro.optionschain.tableview

import android.content.Context
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.TextAppearanceSpan
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.util.android.binding.processContentDescription
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import java.math.BigDecimal

@Suppress("LongParameterList", "LongMethod")
@BindingAdapter(value = ["bidOrAsk", "bidOrAskSize", "itemSelected", "optionTypeLabel", "bidOrAskLabel", "inTheMoney"], requireAll = false)
internal fun TextView.setBidAskSpan(
    bidOrAsk: BigDecimal?,
    bidOrAskSize: String?,
    itemSelected: Boolean?,
    optionTypeLabel: String?,
    bidOrAskLabel: String?,
    inTheMoney: Boolean?
) {
    if (bidOrAsk != null) {
        val bidOrAskString = MarketDataFormatter.formatMarketDataCustom(bidOrAsk)
        text = getCellText(context, bidOrAskString, bidOrAskSize, itemSelected)
        contentDescription = StringBuilder().apply {
            appendLine(optionTypeLabel)
            appendLine("$bidOrAskLabel ${context.getString(R.string.options_chain_price, bidOrAskString)}")
            appendLine("$bidOrAskLabel ${context.getString(R.string.options_chain_size, bidOrAskSize)}")
            if (inTheMoney == true) {
                append(context.getString(R.string.options_chain_in_the_money))
            }
        }
        processContentDescription()
    } else {
        val colorRes = when (itemSelected) {
            true -> R.color.white
            else -> R.color.light_black
        }
        setTextColor(ContextCompat.getColor(context, colorRes))
    }
}

private fun getCellText(context: Context, bidOrAskString: String, bidOrAskSize: String?, isSelected: Boolean?): CharSequence {
    val indexToStart = bidOrAskString.length
    return SpannableStringBuilder().apply {
        append(bidOrAskString)
        append("\nx$bidOrAskSize")
        setSpan(TextAppearanceSpan(context, R.style.OptionsChain_Subtitle), indexToStart, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        val selectionColor = if (isSelected != null && isSelected) {
            R.color.white
        } else {
            R.color.light_black
        }
        setSpan(ForegroundColorSpan(ContextCompat.getColor(context, selectionColor)), 0, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }
}

@BindingAdapter(value = ["optionsChange", "changeContentLabel"], requireAll = false)
internal fun TextView.setChange(optionsChange: BigDecimal?, contentLabel: String?) {
    optionsChange?.let {
        val color = when {
            it.signum() > 0 -> ContextCompat.getColor(context, R.color.green)
            it.signum() < 0 -> ContextCompat.getColor(context, R.color.red)
            else -> ContextCompat.getColor(context, R.color.light_black)
        }
        text = MarketDataFormatter.formatMarketDataCustom(it)
        contentDescription = "$contentLabel $text"
        setTextColor(color)
    }
}
