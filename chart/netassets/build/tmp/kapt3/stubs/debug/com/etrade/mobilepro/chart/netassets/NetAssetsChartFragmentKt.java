package com.etrade.mobilepro.chart.netassets;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0000\u001a\u0010\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0000\u00a8\u0006\u0005"}, d2 = {"formatXAxis", "", "value", "", "formatYAxis", "netassets_debug"})
public final class NetAssetsChartFragmentKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String formatXAxis(float value) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String formatYAxis(float value) {
        return null;
    }
}