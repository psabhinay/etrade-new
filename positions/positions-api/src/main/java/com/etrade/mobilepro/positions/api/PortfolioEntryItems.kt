package com.etrade.mobilepro.positions.api

import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import java.math.BigDecimal

data class PortfolioEntry(
    val accountId: String,
    val totalCash: BigDecimal,
    val positions: List<PortfolioPosition>
)

data class PortfolioPosition(
    override val recordId: String,
    override val symbol: String,
    override val symbolDescription: String,
    override val displaySymbol: String,
    override val instrumentType: InstrumentType,
    override val daysGain: BigDecimal?,
    override val totalGain: BigDecimal?,
    override val pricePaid: BigDecimal?,
    override val commission: BigDecimal?,
    override val fees: BigDecimal?,
    override val quantity: BigDecimal?,
    override val displayQuantity: BigDecimal?,
    override val ask: BigDecimal?,
    override val bid: BigDecimal?,
    override val daysGainPercentage: BigDecimal?,
    override val priceEarningRation: BigDecimal?,
    override val totalGainPercentage: BigDecimal?,
    override val earningsPerShare: BigDecimal?,
    override val fiftyTwoWeekHigh: BigDecimal?,
    override val fiftyTwoWeekLow: BigDecimal?,
    override val lastPrice: BigDecimal?,
    override val tickIndicator: Short? = null,
    override val marketCap: BigDecimal?,
    override val marketValue: BigDecimal?,
    override val todayClose: BigDecimal?,
    override val volume: BigDecimal?,
    override val daysChange: BigDecimal?,
    override val daysChangePercentage: BigDecimal?,
    override val mark: BigDecimal?,
    override val delta: BigDecimal?,
    override val gamma: BigDecimal?,
    override val theta: BigDecimal?,
    override val vega: BigDecimal?,
    override val impliedVolatilityPercentage: BigDecimal?,
    override val baseSymbolAndPrice: String?,
    override val daysToExpire: Int?,
    override val isTaxLot: Boolean = false,
    override val underlyingSymbolInfo: WithSymbolInfo?,
    override val inTheMoneyFlag: Boolean? = null
) : PortfolioRecord, Instrument, Position {
    override val positionId: String
        get() = recordId
    override val displayName: String
        get() = symbol
    override val position: Position
        get() = this
    override val instrument: Instrument
        get() = this
    override val previousClose: BigDecimal?
        get() = instrument.previousClose
}
