package com.etrade.mobilepro.util.formatters

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class DeepLinkFormatterTest {

    @Test
    fun testFormatStringToWebDeepLink() {
        val url = "https://uat.etrade.com/knowledge/investing-basics"
        val expectedDeepLink = "etrade://web/$url"
        val deepLink = url.toWebDeepLink()
        Assertions.assertEquals(deepLink, expectedDeepLink)
    }

    @Test
    fun testFormatNullStringToWebDeepLink() {
        val url = null
        val expectedDeepLink = null
        val deepLink = url.toWebDeepLink()
        Assertions.assertEquals(deepLink, expectedDeepLink)
    }

    @Test
    fun testFormatEmptyStringToWebDeepLink() {
        val url = ""
        val expectedDeepLink = null
        val deepLink = url.toWebDeepLink()
        Assertions.assertEquals(deepLink, expectedDeepLink)
    }

    @Test
    fun testFormatStringToWebViewDeepLink() {
        val baseUrl = "https://uat.etrade.com"
        val relativeUrl = "/knowledge/investing-basics"
        val title = "Knowledge Base"
        val encodedTitle = title
        val encodedUrl = baseUrl.plus(relativeUrl)
        val expectedDeepLink = "etrade://webview?title=$encodedTitle&url=$encodedUrl"
        val deepLink = relativeUrl.toWebViewDeepLink(
            baseUrl = baseUrl,
            webViewTitle = title
        )
        Assertions.assertEquals(deepLink, expectedDeepLink)
    }

    @Test
    fun testFormatStringWithExternalWebViewTitleToWebViewDeepLink() {
        val baseUrl = "https://uat.etrade.com"
        val relativeUrl = "/knowledge/investing-basics"
        val externalTitle = "Knowledge Base"
        val encodedExternalTitle = externalTitle
        val encodedUrl = baseUrl.plus(relativeUrl)
        val expectedDeepLink = "etrade://webview?title=$encodedExternalTitle&url=$encodedUrl"
        val deepLink = relativeUrl.toWebViewDeepLink(
            baseUrl = baseUrl,
            externalWebViewTitle = externalTitle
        )
        Assertions.assertEquals(deepLink, expectedDeepLink)
    }

    @Test
    fun testFormatNullStringToWebViewDeepLink() {
        val url = null
        val expectedDeepLink = null
        val deepLink = url.toWebViewDeepLink()
        Assertions.assertEquals(deepLink, expectedDeepLink)
    }

    @Test
    fun testFormatNullWebViewTitleToWebViewDeepLink() {
        val url = "https://uat.etrade.com"
        val webViewTitle = null
        val expectedDeepLink = "etrade://webview?title=$url&url=$url"
        val deepLink = url.toWebViewDeepLink(
            webViewTitle = webViewTitle
        )
        Assertions.assertEquals(deepLink, expectedDeepLink)
    }

    @Test
    fun testFormatNullExternalWebViewTitleToWebViewDeepLink() {
        val url = "https://uat.etrade.com"
        val externalWebViewTitle = null
        val expectedDeepLink = "etrade://webview?title=$url&url=$url"
        val deepLink = url.toWebViewDeepLink(
            externalWebViewTitle = externalWebViewTitle
        )
        Assertions.assertEquals(deepLink, expectedDeepLink)
    }

    @Test
    fun testFormatNullWebViewTitlesToWebViewDeepLink() {
        val url = "https://uat.etrade.com"
        val externalWebViewTitle = null
        val webViewTitle = null
        val expectedDeepLink = "etrade://webview?title=$url&url=$url"
        val deepLink = url.toWebViewDeepLink(
            webViewTitle = webViewTitle,
            externalWebViewTitle = externalWebViewTitle
        )
        Assertions.assertEquals(deepLink, expectedDeepLink)
    }

    @Test
    fun testFormatEmptyStringToWebViewDeepLink() {
        val url = ""
        val expectedDeepLink = null
        val deepLink = url.toWebViewDeepLink()
        Assertions.assertEquals(deepLink, expectedDeepLink)
    }
}
