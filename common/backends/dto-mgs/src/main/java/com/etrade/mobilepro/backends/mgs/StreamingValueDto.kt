package com.etrade.mobilepro.backends.mgs

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

const val MOVEMENT_TYPE_GAIN = "gain"
const val MOVEMENT_TYPE_LOSS = "loss"
const val MOVEMENT_TYPE_NEUTRAL = "neutral"

@JsonClass(generateAdapter = true)
data class StreamingValueDto(
    @Json(name = "initial")
    val initial: String?,
    @Json(name = "stream_id")
    val streamId: String?,
    @Json(name = "movement_type")
    val movementType: String?
)
