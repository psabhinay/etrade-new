package com.etrade.mobilepro.checkdeposit.presentation.details

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.checkdeposit.presentation.R
import com.etrade.mobilepro.checkdeposit.presentation.databinding.FragmentDepositDetailsBinding
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.util.android.binding.viewBinding
import javax.inject.Inject

@RequireLogin
class DepositDetailsFragment @Inject constructor(
    private val viewModelProvider: ViewModelProvider.Factory,
    imageLoader: ImageLoader
) : Fragment(R.layout.fragment_deposit_details) {

    private val binding by viewBinding(FragmentDepositDetailsBinding::bind)

    private val viewModel: DepositDetailsViewModel by viewModels { viewModelProvider }
    private val args: DepositDetailsFragmentArgs by navArgs()
    private val detailsAdapter = DepositDetailsAdapter(imageLoader)

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        binding.progress.show()

        with(binding.recyclerView) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = detailsAdapter
        }

        viewModel.detailsItems.observe(
            viewLifecycleOwner,
            Observer {
                binding.progress.hide()

                detailsAdapter.addItems(it)
            }
        )

        if (savedInstanceState == null) {
            viewModel.setDepositDetails(args.deposit, args.accountName)
        }
    }
}
