package com.etrade.mobilepro.news.presentation.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class DisclosuresItemViewHolder(
    itemView: View,
    private val disclosuresItemClickListener: (() -> Unit)?
) : RecyclerView.ViewHolder(itemView) {

    init {
        itemView.setOnClickListener {
            disclosuresItemClickListener?.invoke()
        }
    }
}
