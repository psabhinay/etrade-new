package com.etrade.mobilepro.movers.portfolio

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.home.overview.viewmodel.OVERVIEW_CACHE_EXPIRATION_TIME
import com.etrade.mobilepro.markets.movers.EmptyQuotesException
import com.etrade.mobilepro.movers.portfolio.caching.PortfolioSnapshotRepo
import com.etrade.mobilepro.portfoliosnapshot.AccountId
import com.etrade.mobilepro.portfoliosnapshot.PORTFOLIO_SNAPSHOT_URL
import com.etrade.mobilepro.portfoliosnapshot.PortfolioSnapshotPosition
import com.etrade.mobilepro.portfoliosnapshot.PortfolioSnapshotRequestBody
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.util.UseCase
import com.etrade.mobilepro.util.executeBlocking
import io.reactivex.Observable
import io.reactivex.rxkotlin.zipWith
import kotlinx.coroutines.rx2.awaitLast
import javax.inject.Inject

interface GetPortfolioMoversUseCase : UseCase<GetPortfolioMoversParameter, ETResult<Pair<List<MobileQuote>, List<PortfolioSnapshotPosition>>>>

class GetPortfolioMoversParameter(
    val accountId: String?,
    val forceRefresh: Boolean
)

class GetPortfolioMoversUseCaseImpl @Inject constructor(
    private val portfolioSnapshotRepo: PortfolioSnapshotRepo,
    private val quoteRepo: MobileQuoteRepo,
    private val isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase
) : GetPortfolioMoversUseCase {

    override suspend fun execute(parameter: GetPortfolioMoversParameter): ETResult<Pair<List<MobileQuote>, List<PortfolioSnapshotPosition>>> {
        val requestBody = PortfolioSnapshotRequestBody(AccountId(parameter.accountId ?: ""))
        val request = object : ScreenRequest {
            override val url: String
                get() = PORTFOLIO_SNAPSHOT_URL
            override val contentKey: String
                get() = PORTFOLIO_SNAPSHOT_URL + parameter.accountId

            override fun createRequestBody(): Any = requestBody
        }

        return runCatchingET {
            val positions = portfolioSnapshotRepo.getPortfolioMovers(
                request = request,
                cacheExpiration = OVERVIEW_CACHE_EXPIRATION_TIME.takeUnless { parameter.forceRefresh }
            ).awaitLast().data?.data?.portfolioData?.portfolioSnapshotPosition

            if (positions.isNullOrEmpty()) {
                throw EmptyQuotesException()
            }

            val symbols = positions.joinToString(",") { position -> position.symbol }
            val isExtendedHours = isExtendedHoursOnUseCase.executeBlocking()
            quoteRepo.loadMobileQuotes(symbols, extendedHours = isExtendedHours).zipWith(Observable.just(positions)).awaitLast()
        }
    }
}
