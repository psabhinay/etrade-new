package com.etrade.mobilepro.trade.form.api.value

import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.util.MutualConverter

interface OrderTermValueId {
    val extendedHoursDay: String
    val extendedHoursImmediateOrCancel: String
    val fillOrKill: String
    val goodForDay: String
    val goodUntilCancel: String
    val immediateOrCancel: String
    val converter: MutualConverter<String, OrderTerm>
}
