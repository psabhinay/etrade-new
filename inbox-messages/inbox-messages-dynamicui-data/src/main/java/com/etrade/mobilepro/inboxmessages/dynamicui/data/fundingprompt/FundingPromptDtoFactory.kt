package com.etrade.mobilepro.inboxmessages.dynamicui.data.fundingprompt

import com.etrade.mobile.accounts.dto.AccountDto
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.etrade.mobilepro.inboxmessages.InboxAttributes
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.dynamicui.api.InboxMessagesWidgetPlace
import com.etrade.mobilepro.inboxmessages.dynamicui.api.fundingprompt.FundingPrompt
import com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt.FundingPromptViewDto
import com.etrade.mobilepro.util.guard
import javax.inject.Inject

interface FundingPromptDtoFactory {
    fun create(place: InboxMessagesWidgetPlace, account: AccountDto, message: InboxMessage, view: BaseScreenView): FundingPromptViewDto?
}

class DefaultFundingPromptDtoFactory @Inject constructor() : FundingPromptDtoFactory {
    override fun create(place: InboxMessagesWidgetPlace, account: AccountDto, message: InboxMessage, view: BaseScreenView): FundingPromptViewDto? {
        validate(place, account) guard {
            return null
        }

        if (!message.hasThumbnail) {
            return null
        }
        val imageUrl = message.thumbnailUrl
        val inboxMap = message.rawAttributes
        val fundingPrompt = when (place) {
            InboxMessagesWidgetPlace.COMPLETE_VIEW -> FundingPrompt.SmallWithAccountName(account, imageUrl)
            InboxMessagesWidgetPlace.OVERVIEW -> {
                when (inboxMap[InboxAttributes.ASSET_HEIGHT]) {
                    FundingPrompt.Small.HEIGHT -> FundingPrompt.Small(account, imageUrl)
                    FundingPrompt.Medium.HEIGHT -> FundingPrompt.Medium(account, imageUrl)
                    FundingPrompt.Large.HEIGHT -> FundingPrompt.Large(account, imageUrl)
                    else -> FundingPrompt.Small(account, imageUrl)
                }
            }
        }

        val ctaDto = CallToActionDto(
            label = "",
            clickActionDto = ClickActionDto(
                appUrl = message.deepLink
            )
        )
        return FundingPromptViewDto(fundingPrompt, listOf(ctaDto), view.clickActionDto)
    }

    private fun validate(place: InboxMessagesWidgetPlace, account: AccountDto): Unit? {
        if (place == InboxMessagesWidgetPlace.COMPLETE_VIEW && !belongsToCompleteView(account.promptsForFunding)) {
            return null
        }
        if (place == InboxMessagesWidgetPlace.OVERVIEW && !belongsToOverviewPortfolio(account.promptsForFunding)) {
            return null
        }
        if (!isFundingPrompt(account.promptsForFunding)) {
            return null
        }
        return Unit
    }

    private fun belongsToCompleteView(promptCode: String?): Boolean = promptCode?.contains(InboxMessagesWidgetPlace.COMPLETE_VIEW.code) == true
    private fun belongsToOverviewPortfolio(promptCode: String?): Boolean = promptCode?.contains(InboxMessagesWidgetPlace.OVERVIEW.code) == true
    private fun isFundingPrompt(promptCode: String?): Boolean = promptCode?.contains(FundingPrompt.PROMPT_INBOX_CODE) == true
}
