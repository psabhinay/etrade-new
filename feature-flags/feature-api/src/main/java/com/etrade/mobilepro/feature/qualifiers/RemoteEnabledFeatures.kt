package com.etrade.mobilepro.feature.qualifiers

import com.etrade.mobilepro.feature.Feature
import javax.inject.Qualifier

/**
 * Dependency qualifier for a [Set] of [Feature]s that are allowed to be resolved by remote feature
 * flag providers.
 */
@Qualifier
annotation class RemoteEnabledFeatures
