package com.etrade.mobilepro.common.readstatus

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ReadableEntityDb(
    @PrimaryKey
    var entityId: String = "",
    var wasReadAt: Long = 0,
    var type: String = ""
) : RealmObject()
