package com.etrade.mobilepro.util.android.snackbar

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.etrade.mobilepro.util.android.R

fun SnackBarUtil.Companion.buttonWithCarrot(context: Context, titleStringId: Int, action: () -> Unit): View =
    LayoutInflater.from(context).inflate(R.layout.view_button_with_carrot, null).apply {
        setOnClickListener {
            action()
        }
        findViewById<TextView>(R.id.button).setText(titleStringId)
    }
