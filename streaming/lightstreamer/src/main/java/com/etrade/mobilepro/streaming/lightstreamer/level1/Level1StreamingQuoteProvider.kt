package com.etrade.mobilepro.streaming.lightstreamer.level1

import com.etrade.eo.streaming.SubscriptionMode
import com.etrade.eo.streaming.UpdateInfo
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamIdFactory
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.base.SubscriptionManagerHelper
import com.etrade.mobilepro.streaming.lightstreamer.LightstreamerFieldMapper
import com.etrade.mobilepro.streaming.lightstreamer.map
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Streaming provider for level 1 quotes.
 *
 * @param fieldMapper The mapper that will provide string representation of fields that [subscriptionManagerHelper] supports.
 * @param streamIdFactory The factory to create stream ids for level 1 quotes.
 * @param subscriptionManagerHelper The helper for a subscription manager.
 */
class Level1StreamingQuoteProvider @Inject constructor(
    private val fieldMapper: LightstreamerFieldMapper<Level1Field>,
    private val streamIdFactory: StreamIdFactory<Any>,
    private val subscriptionManagerHelper: SubscriptionManagerHelper<UpdateInfo>
) : StreamingQuoteProvider<Level1Field, Level1Data, Any> {

    override fun getQuote(symbol: String, instrumentType: InstrumentType, fields: Set<Level1Field>, payload: Any?): Observable<Level1Data> {
        val isOption = instrumentType.isOption
        val actualFields = fieldMapper.map(fields, isOption)
        return subscriptionManagerHelper.subscribeTo(streamIdFactory.createStreamId(symbol, instrumentType), actualFields, SubscriptionMode.MERGE)
            .map {
                if (isOption) {
                    LightstreamerOptionData(it, fields)
                } else {
                    LightstreamerStockData(it, fields, instrumentType)
                }
            }
    }
}
