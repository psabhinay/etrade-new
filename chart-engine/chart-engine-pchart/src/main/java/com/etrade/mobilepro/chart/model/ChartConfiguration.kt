package com.etrade.mobilepro.chart.model

data class ChartConfiguration(
    val timeFrame: ChartTimeFrame,
    val yAxisMin: Double,
    val yAxisMax: Double,
    val maxTimeInMillis: Long,
    val showPercentage: Boolean = false,
    val listOfLabels: MutableList<DisplayLabel>? = null,
    val isOption: Boolean = false
)
