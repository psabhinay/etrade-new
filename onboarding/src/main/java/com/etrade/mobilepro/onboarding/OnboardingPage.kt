package com.etrade.mobilepro.onboarding

import androidx.annotation.ColorRes
import androidx.annotation.StringRes

data class OnboardingPage(
    @StringRes val title: Int? = null,
    @StringRes val subtitle: Int,
    @ColorRes val subtitleColor: Int = R.color.white,
    val image: String
)
