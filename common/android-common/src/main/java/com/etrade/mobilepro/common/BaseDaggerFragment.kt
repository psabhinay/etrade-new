package com.etrade.mobilepro.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * If possible, it is recommended to not introduce fragment inheritance hierarchies, but instead using composition.
 * View model construction is made easy with [androidx.fragment.app.FragmentViewModelLazy]'s [Fragment.viewModels] delegate property
 * Dagger constructor injection is possible (see app examples)
 * Snackbar automatic handling (after view destruction) is handled by [SnackbarUtilFactory]
 */
abstract class BaseDaggerFragment<VM : ViewModel> : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var snackbarUtilFactory: SnackbarUtilFactory

    protected val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    protected lateinit var viewModel: VM
    protected abstract val viewModelClass: Class<VM>
    private fun viewModelProvider() = ViewModelProvider(this, viewModelFactory)

    protected abstract val layoutRes: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = viewModelProvider().get(viewModelClass)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }
}
