package com.etrade.mobilepro.bank.transactions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0004"}, d2 = {"IN_PROCESS_LABEL", "", "LOCALDATE_DISPLAYED_DATE_FORMAT", "Lorg/threeten/bp/format/DateTimeFormatter;", "bank-dynamic_debug"})
public final class BankTransactionGenericLayoutKt {
    private static final org.threeten.bp.format.DateTimeFormatter LOCALDATE_DISPLAYED_DATE_FORMAT = null;
    private static final java.lang.String IN_PROCESS_LABEL = "In Process";
}