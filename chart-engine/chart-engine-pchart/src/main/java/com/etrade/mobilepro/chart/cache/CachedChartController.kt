package com.etrade.mobilepro.chart.cache

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.chartengine.SymbolChartData
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CachedChartController @Inject constructor() : SimpleCachedResource.Controller<SymbolChartData> {
    override fun shouldRefresh(itemFromCache: SymbolChartData): Boolean {
        return itemIsOutdated(itemFromCache)
    }

    private fun itemIsOutdated(fetchedItem: SymbolChartData): Boolean {
        val currentTimeInMillis = System.currentTimeMillis()
        return if (fetchedItem.cachedAt > currentTimeInMillis) {
            true
        } else {
            currentTimeInMillis - fetchedItem.cachedAt >= TimeUnit.SECONDS.toMillis(fetchedItem.shouldCachedTimeInSeconds)
        }
    }
}
