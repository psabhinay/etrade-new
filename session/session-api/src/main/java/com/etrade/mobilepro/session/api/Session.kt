package com.etrade.mobilepro.session.api

/**
 * This data class represents a user session and contains the data that is received from the login service.
 *
 */

interface Session {

    /**
     * @property principalId Contains a unique id for the session.
     */
    val principalId: String

    /**
     * @property token This token can be user for authentication to E-TRADE services.
     * You can use this in the network request headers as "stk1" and/or "token" in case of using OH adapter.
     */
    val token: String

    /**
     * @property userId A unique id for a user.
     */
    val userId: Long

    // TODO - Server response missing field.
    //  https://jira.corp.etradegrp.com/browse/ETAND-4607
    /**
     * @property currentServerTime The time in the server in milliseconds. This can be used to synchronize time on the client.
     */
    val currentServerTime: Long

    /**
     * @property timeZoneOffset The offset between the server time and UTC in milliseconds.
     */
    val timeZoneOffset: Long

    /**
     * @property level1StreamerUrl The level1 server endpoint URL that application can use to connect to LightStreamer for level1.
     */
    val level1StreamerUrl: String?

    /**
     * @property level2StreamerUrl The level2 server endpoint URL that application can use to connect to LightStreamer for level 2.
     */
    val level2StreamerUrl: String?

    /**
     * @property isRealTimeQuotes Indicates whether the user is eligible to see real time quotes. When enabled the user has accepted the Market Data Agreement
     * and may access the level 2 quotes page
     */
    var isRealTimeQuotes: Boolean

    /**
     * @property isInternationalUser Indicates whether the user is international, for GDPR compliance.
     */
    val isInternationalUser: Boolean

    /**
     * @property aatId user id to be used with LocaLytics in place of user id, 1:1 mapping from backend should never be null or empty
     */
    val aatId: String?

    /**
     * @property numberOfAccounts total number of brokerage accounts under this login
     */
    val numberOfAccounts: Int

    /**
     * @property consumerKey environment - SIT, UAT, PROD.. specific consumer key which is used to access api endpoints
     */
    val consumerKey: String

    /**
     * @property notificationToken token used for alert message notification
     */
    val notificationToken: String

    /**
     * @property webSessionTimeout session timeout duration set by the user for the web login
     */
    var webSessionTimeout: Int

    /**
     * @property defaultAccountId default account to be shown when user logs in.
     */
    val defaultAccountId: String

    /**
     * @property isPET Indicates whether user is a Pro Elite Trading user
     */
    val isPET: Boolean

    /**
     * @property isNYSE
     */
    val isNYSE: Boolean

    /**
     * @property isMobileProConsentGiven Indicates whether user consented for mobile app usage
     */
    val isMobileProConsentGiven: Boolean

    /**
     * @property isAndroidConsentGiven Indicates whether user consented for Android mobile app usage
     */
    val isAndroidConsentGiven: Boolean

    /**
     * @property isTradingEnabled Indicates whether user is allowed to use trading feature
     */
    val isTradingEnabled: Boolean

    /**
     * @property isQuickTransferEnabled Indicates whether user is allowed to use quick transfer feature
     */
    val isQuickTransferEnabled: Boolean

    /**
     * @property isAdvancedOrderDisclosure Indicates whether user has read and acknowledged terms and conditions for Conditional/Advanced Order trading
     */
    val isAdvancedOrderDisclosure: Boolean

    /**
     * @property isConditionalOrderDisclosure Indicates whether user has read and acknowledged terms and conditions for Conditional Order trading
     */
    val isConditionalOrderDisclosure: Boolean

    /**
     * @property isPreclearanceRequired Indicates whether ple clearance required for this user to use mobile app
     */
    val isPreclearanceRequired: Boolean

    /**
     * @property isRSATokenUser Indicates if user has setup RSA token for authentication
     */
    val isRSATokenUser: Boolean

    /**
     * @property isExtendedHoursAccepted Indicates if the user has accepted the Extended Hours Trade Agreement
     */
    val isExtendedHoursAccepted: Boolean

    /**
     * @property realTimeOfLastTouchMillis Indicates last time user interact with the app
     */
    val realTimeOfLastTouchMillis: Long

    fun touchSession(timeStamp: Long)
}
