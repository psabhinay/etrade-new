package com.etrade.mobilepro.appwidget.indices

import android.content.Context
import androidx.annotation.WorkerThread
import com.etrade.mobilepro.appwidget.api.WidgetViewStateRepo
import com.etrade.mobilepro.appwidget.common.BaseRemoteViewsAdapter
import com.etrade.mobilepro.appwidget.common.BaseRemoteViewsFactory
import com.etrade.mobilepro.appwidget.common.formatIndexChange
import com.etrade.mobilepro.util.android.ChangeColorPicker
import kotlinx.coroutines.runBlocking

@WorkerThread
internal class IndicesRemoteViewsAdapter(
    val repo: WidgetViewStateRepo,
    context: Context,
    changeColorPicker: ChangeColorPicker
) : BaseRemoteViewsAdapter<IndexItem>(context) {

    override val remoteViewsFactory: BaseRemoteViewsFactory<IndexItem> = IndicesRemoteViewsFactory(
        context = context,
        changeColorPicker = changeColorPicker
    )

    override fun getUpdatedItems(): List<IndexItem> = runBlocking {
        (repo.getIndicesViewState().indices ?: listOf())
            .mapIndexed { position, item ->
                IndexItem(
                    index = item,
                    position = position,
                    formattedIndexChange = formatIndexChange(context, item.change, item.percentChange)
                )
            }
    }
}
