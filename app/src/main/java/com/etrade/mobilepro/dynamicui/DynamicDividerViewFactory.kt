package com.etrade.mobilepro.dynamicui

import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

data class DynamicDividerViewModel(val compact: Boolean) : DynamicViewModel(R.layout.dynamic_divider) {
    override val variableId: Int = BR.obj
}

class DynamicDividerViewFactory @Inject constructor() : DynamicUiViewFactory {
    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        return object : GenericLayout {

            private val isCompact: Boolean = ((dto as? DynamicDividerViewDto)?.data as? Boolean) ?: false

            override val viewModelClass = DynamicDividerViewModel::class.java

            override val viewModelFactory = viewModelFactory {
                DynamicDividerViewModel(isCompact)
            }

            override val uniqueIdentifier: String = "${javaClass.name}_$isCompact"
        }
    }
}
