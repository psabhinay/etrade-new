package com.etrade.mobilepro.menu

import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.di.OpenNewAccount
import com.etrade.mobilepro.userviewmodel.UserViewModel
import javax.inject.Inject

class VerifyAccountBeforeOpenAccountDialogFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    userViewModel: UserViewModel,
    @OpenNewAccount val openNewAccountUrl: String
) : VerifyAccountBeforeNavDialogFragment(viewModelFactory, userViewModel) {

    override fun getDirections(withLogin: Boolean): NavDirections {
        val webViewTitle = getString(R.string.open_new_account)
        val webViewUrl = openNewAccountUrl
        return if (withLogin) {
            VerifyAccountBeforeWebViewDialogFragmentDirections.actionMenuLoginRequiredWebView(webViewTitle, webViewUrl)
        } else {
            VerifyAccountBeforeWebViewDialogFragmentDirections.actionNewAccountWebView(webViewTitle, webViewUrl)
        }
    }
}
