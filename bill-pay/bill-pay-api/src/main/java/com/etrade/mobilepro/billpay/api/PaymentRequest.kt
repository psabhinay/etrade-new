package com.etrade.mobilepro.billpay.api

import org.threeten.bp.LocalDate
import java.math.BigDecimal

data class PaymentRequest(
    val paymentId: String? = null,
    val amount: BigDecimal,
    val scheduleDate: LocalDate,
    val memo: String? = null,
    val originAccount: OriginAccount,
    val destinationAccount: DestinationAccount,
    val isNewPayment: Boolean = paymentId.isNullOrEmpty()
) {

    data class OriginAccount(
        val accountId: String,
        val instNo: String
    )

    data class DestinationAccount(
        val payeeId: String,
        val leadDays: Int
    )
}
