package com.etrade.mobilepro.tableviewutils.presentation

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ViewConfiguration
import com.evrencoskun.tableview.preference.Preferences
import kotlin.math.absoluteValue
import kotlin.math.sign

/**
 * This is a RestrictedTableView that designed to be used in a horizontal ViewPager2 to have a better
 * scrolling behavior because ViewPager2 is a RecyclerView and some conflicting touch event handling
 *
 * We disallows intercept touch events on parent when scrolling in the horizontal direction
 * and the child can scroll in that direction.
 *
 * When the motion is vertical dy > dx, we allow parent to intercept all touch events.
 *
 * When the motion is horizontal and if the child cannot scroll in the same horizontal direction,
 * we also disallow based on location of the TableView.
 *
 * When on the very left or the very right, disallow intercept when trying to scroll more left or more right,
 * the reason for this is if we don't disallow, parent intercepts and then steals all touch events that follow.
 * So users can start a scroll motion at the very edge and then scrolls to the other side without lifting finger.
 * If we allowed parent to intercept, we'll no longer get any events passed to us.
 *
 * When position is in middle, we allow parent to intercept if the child can no longer scroll in that direction.
 *
 * Also added a fix for scroll position restoration on this recyclerView detaching and re-attaching when used inside a ViewPager
 * switching tabs.
 */
class ViewPagerRestrictedTableView : RestrictedTableView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    enum class PositionInViewPager {
        FIRST,
        MIDDLE,
        LAST,
        NOT_IN_VIEWPAGER
    }

    var position: PositionInViewPager = PositionInViewPager.NOT_IN_VIEWPAGER

    private var preferencesToRestore: Preferences? = null

    private var touchSlop = 0
    private var initialX = 0f
    private var initialY = 0f

    init {
        touchSlop = ViewConfiguration.get(context).scaledTouchSlop
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        if (position != PositionInViewPager.NOT_IN_VIEWPAGER) {
            handleInterceptTouchEvent(ev)
        }
        return super.onInterceptTouchEvent(ev)
    }

    private fun canChildScroll(delta: Float): Boolean {
        return columnHeaderRecyclerView?.canScrollHorizontally(-delta.sign.toInt()) ?: false
    }

    private fun handleInterceptTouchEvent(e: MotionEvent?) {
        if (e != null) {
            /**
             * There is some strange issue causing scroll to not work if we ever request disallowIntercept
             * of parent when the touch event is on the rowHeaderRecyclerView, so here we just don't
             * proces the touch event if it's on the rowHeaderRecyclerView
             */
            val rowHeaderRect = Rect()
            rowHeaderRecyclerView.getHitRect(rowHeaderRect)
            if (rowHeaderRect.contains(e.x.toInt(), e.y.toInt())) {
                return
            }
        }
        if (e?.action == MotionEvent.ACTION_DOWN) {
            initialX = e.x
            initialY = e.y
            parent?.requestDisallowInterceptTouchEvent(true)
        } else if (e?.action == MotionEvent.ACTION_MOVE) {
            val dx = e.x - initialX
            val dy = e.y - initialY

            val absDx = dx.absoluteValue
            val absDy = dy.absoluteValue

            if (absDx > touchSlop || absDy > touchSlop) {
                if (absDy > absDx) {
                    // Gesture is vertical, allow all parents to intercept
                    parent?.requestDisallowInterceptTouchEvent(false)
                } else {
                    handleHorizontalMotionEvent(dx)
                }
            }
        }
    }

    private fun handleHorizontalMotionEvent(dx: Float) {
        // Gesture is horizontal, query child if movement in that direction is possible
        if (canChildScroll(dx)) {
            // Child can scroll, disallow all parents to intercept
            parent?.requestDisallowInterceptTouchEvent(true)
        } else {
            /**
             * Child cannot scroll, allow all parents to intercept only if this table view is in the middle
             * Disallow parent to intercept if this is the first item in ViewPager and user is trying to swipe left
             * Disallow parent to intercept if this is the last item in ViewPager and user is trying to move right
             */
            val disallowIntercept = when (position) {
                PositionInViewPager.FIRST -> dx.sign >= 0f
                PositionInViewPager.LAST -> dx.sign <= 0f
                PositionInViewPager.MIDDLE, PositionInViewPager.NOT_IN_VIEWPAGER -> false
            }
            parent?.requestDisallowInterceptTouchEvent(disallowIntercept)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        preferencesToRestore?.let {
            restoreScrollState(it)
            preferencesToRestore = null
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        preferencesToRestore = getScrollState()
    }
}
