package com.etrade.mobilepro.trade.disclosures

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.util.android.extension.loadFormattedData

private const val ADDITIONAL_CONTENT_IDENTIFIER_TAG = "\$AdditionalDisclosureContent"

@RequireLogin
class MutualFundTradeDisclosuresFragment : Fragment() {

    private val fundsArgs: MutualFundTradeDisclosuresFragmentArgs? by navArgs()
    private var additionalContent = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_data_replacement_webview, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fundsArgs?.conditions?.let {

            if (it.hasEarlyRedemptionFee) {
                additionalContent = additionalContent.concatText(R.string.disclosures_has_redemption_fee)
            }
            if (it.isFrontEndOrSalesFeeFund) {
                additionalContent = additionalContent.concatText(R.string.disclosures_front_end_sales_fund)
            }
            if (it.isMoneyMarketFund) {
                additionalContent = additionalContent.concatText(R.string.disclosures_mmf)
            }
            if (it.isNoLoadNoTransactionFeeFund) {
                additionalContent = additionalContent.concatText(R.string.disclosures_no_load_fund)
            }
        }

        val replacementData = mapOf(ADDITIONAL_CONTENT_IDENTIFIER_TAG to additionalContent)
        val webView = view.findViewById<WebView>(org.chromium.customtabsclient.shared.R.id.web_view)
        webView.loadFormattedData(R.raw.mftrading_disclosure_template, replacementData)
    }

    private fun String.concatText(@StringRes textId: Int): String = this.plus(resources.getString(textId))
}
