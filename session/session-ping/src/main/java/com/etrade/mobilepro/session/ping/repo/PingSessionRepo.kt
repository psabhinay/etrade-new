package com.etrade.mobilepro.session.ping.repo

import com.etrade.mobilepro.session.ping.PingMessage

interface PingSessionRepo {

    /**
     * Ping to E-TRADE systems at regular interval of 60 seconds
     * This is done in order to preserve the server side session when user is idle or App is not interacting with the server
     *
     */
    suspend fun ping(): PingMessage
}
