package com.etrade.mobilepro.trade.data.common

import com.etrade.mobilepro.instrument.getDisplaySymbolFromOsiKey
import com.etrade.mobilepro.instrument.isSymbolLongOption
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
open class LegDetailsCommonDto : TradeLeg {

    @field:Json(name = "symbol")
    override var symbol: String = ""
    @field:Json(name = "quantity")
    override var quantity: Int = 0
    @field:Json(name = "amOption")
    override var isAMOption: Boolean = false
    @field:Json(name = "transaction")
    var transactionCode: String = ""
    @field:Json(name = "optionType")
    var optionType: String? = null
    @field:Json(name = "expiryDay")
    var expiryDay: String? = null
    @field:Json(name = "expiryMonth")
    var expiryMonth: String? = null
    @field:Json(name = "expiryYear")
    var expiryYear: String? = null
    @field:Json(name = "strikePrice")
    var strikePrice: String? = null
    @field:Json(name = "cancelQty")
    var cancelQty: String? = null
    @field:Json(name = "productType")
    var productType: String? = null

    override val transactionType: TransactionType
        get() = TransactionType.fromTypeCode(transactionCode.toIntOrNull(), isSymbolLongOption(symbol))

    override val displaySymbol: String
        get() = getDisplaySymbolFromOsiKey(symbol) ?: symbol
}
