package com.etrade.mobilepro.trade.data

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.MarketDestination

internal class TradeOrderArguments(
    val params: InputValuesSnapshot,
    val marketDestination: MarketDestination
)
