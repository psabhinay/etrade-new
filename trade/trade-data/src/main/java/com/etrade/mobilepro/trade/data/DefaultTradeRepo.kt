package com.etrade.mobilepro.trade.data

import com.etrade.mobilepro.common.di.IsTablet
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.order.details.api.OrderDetailsRepo
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.api.OrderRequest
import com.etrade.mobilepro.trade.api.OrderResponse
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.TradeRepo
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.data.advanced.AdvancedTradingApiClient
import com.etrade.mobilepro.trade.data.conditional.NewConditionalOrderTradeDelegate
import com.etrade.mobilepro.trade.data.contingent.ContingentOrderTradeDelegate
import com.etrade.mobilepro.trade.data.customorder.CustomOrderApiClient
import com.etrade.mobilepro.trade.data.form.input.DefaultOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.mutualfunds.MutualFundsOrderTradingApiClient
import com.etrade.mobilepro.trade.data.mutualfunds.NewMutualFundsOrderTradeDelegate
import com.etrade.mobilepro.trade.data.option.ContingentOptionTradeRequestParamsUpdater
import com.etrade.mobilepro.trade.data.option.NewOptionTradeDelegate
import com.etrade.mobilepro.trade.data.option.SavedOptionTradeDelegate
import com.etrade.mobilepro.trade.data.stock.ContingentStockTradeRequestParamsUpdater
import com.etrade.mobilepro.trade.data.stock.NewStockOrderTradeDelegate
import com.etrade.mobilepro.trade.data.stock.SavedStockOrderTradeDelegate
import com.etrade.mobilepro.trade.data.stock.StockTradingApiClient
import com.etrade.mobilepro.trade.data.stoploss.StopLossOrderTradeDelegate
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormInputIdMap
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.extractTradeOrderType
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ConditionalTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ContingentOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ContingentStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossStockTradeFormInputId
import com.etrade.mobilepro.util.ValueCoder
import com.squareup.moshi.Moshi
import retrofit2.Retrofit
import java.util.UUID
import javax.inject.Inject

class DefaultTradeRepo @Inject constructor(
    @MobileTrade retrofit: Retrofit,
    @IsTablet private val isTablet: Boolean,
    moshi: Moshi,
    user: User,
    conditionalOrderTradeFormInputId: ConditionalOrderTradeFormInputId,
    conditionalTradeFormInputId: ConditionalTradeFormInputId,
    contingentStockTradeFormInputId: ContingentStockTradeFormInputId,
    contingentOptionTradeFormInputId: ContingentOptionTradeFormInputId,
    optionTradeFormInputId: OptionTradeFormInputId,
    stockTradeFormInputId: StockTradeFormInputId,
    stopLossStockTradeFormInputId: StopLossStockTradeFormInputId,
    stopLossOptionTradeFormInputId: StopLossOptionTradeFormInputId,
    orderDetailsRepo: OrderDetailsRepo,
    private val tradeFormInputIdMap: TradeFormInputIdMap,
    private val tradeLegsCoder: ValueCoder<List<TradeLeg>>
) : TradeRepo {

    private val newOrderDelegates: Map<TradeFormType, TradeDelegate>

    private val tradeWithSavedOrderDelegates: Map<TradeFormType, TradeDelegate>

    private val activeOrders: MutableMap<String, Order> = mutableMapOf()

    init {
        val advancedTradingApiClient: AdvancedTradingApiClient = retrofit.create(AdvancedTradingApiClient::class.java)
        val customOrderApiClient: CustomOrderApiClient = retrofit.create(CustomOrderApiClient::class.java)
        val mutualFundsOrderTradingApiClient: MutualFundsOrderTradingApiClient = retrofit.create(MutualFundsOrderTradingApiClient::class.java)
        val newOptionTradeDelegate = NewOptionTradeDelegate(customOrderApiClient, moshi, optionTradeFormInputId, tradeLegsCoder, isTablet)
        val stockTradingApiClient: StockTradingApiClient = retrofit.create(StockTradingApiClient::class.java)

        newOrderDelegates = mapOf(
            TradeFormType(SecurityType.CONDITIONAL) to NewConditionalOrderTradeDelegate(
                advancedTradingApiClient,
                conditionalOrderTradeFormInputId,
                conditionalTradeFormInputId,
                isTablet
            ),
            TradeFormType(SecurityType.MUTUAL_FUND) to NewMutualFundsOrderTradeDelegate(mutualFundsOrderTradingApiClient, isTablet, moshi),
            TradeFormType(SecurityType.OPTION) to newOptionTradeDelegate,
            TradeFormType(SecurityType.STOCK) to NewStockOrderTradeDelegate(stockTradingApiClient, stockTradeFormInputId, isTablet, orderDetailsRepo, user),
            TradeFormType(SecurityType.STOCK, AdvancedTradeType.Contingent) to ContingentOrderTradeDelegate(
                contingentStockTradeFormInputId,
                advancedTradingApiClient,
                ContingentStockTradeRequestParamsUpdater(),
                moshi
            ),
            TradeFormType(SecurityType.OPTION, AdvancedTradeType.Contingent) to ContingentOrderTradeDelegate(
                contingentOptionTradeFormInputId,
                advancedTradingApiClient,
                ContingentOptionTradeRequestParamsUpdater(),
                moshi
            ),
            TradeFormType(SecurityType.STOCK, AdvancedTradeType.StopLoss) to StopLossOrderTradeDelegate(
                advancedTradingApiClient,
                stopLossStockTradeFormInputId
            ),
            TradeFormType(SecurityType.OPTION, AdvancedTradeType.StopLoss) to StopLossOrderTradeDelegate(
                advancedTradingApiClient,
                stopLossOptionTradeFormInputId
            ),
            TradeFormType(SecurityType.STOCK_AND_OPTIONS) to newOptionTradeDelegate
        )

        val savedOptionTradeDelegate = SavedOptionTradeDelegate(customOrderApiClient, moshi, optionTradeFormInputId, tradeLegsCoder, isTablet)

        tradeWithSavedOrderDelegates = mapOf(
            TradeFormType(SecurityType.OPTION) to savedOptionTradeDelegate,
            TradeFormType(SecurityType.STOCK) to SavedStockOrderTradeDelegate(stockTradingApiClient, isTablet, user),
            TradeFormType(SecurityType.STOCK_AND_OPTIONS) to savedOptionTradeDelegate
        )
    }

    override suspend fun previewOrder(
        map: InputValuesSnapshot,
        marketDestination: MarketDestination,
        changeOrder: Boolean
    ): ETResult<TradeOrder> {
        val tradeFormType = tradeFormInputIdMap.extractTradeOrderType(map)
        val delegate = if (changeOrder) {
            tradeWithSavedOrderDelegates[tradeFormType]
        } else {
            newOrderDelegates[tradeFormType]
        }
        val inputId = tradeFormInputIdMap.getInputId(tradeFormType)
        if (delegate == null || inputId == null) {
            return ETResult.failure(IllegalArgumentException("Unknown security type: $tradeFormType"))
        }
        val accountId = map[inputId.accountId] as? String ?: return ETResult.failure(IllegalArgumentException("Trade requires account"))
        return delegate.previewOrder(map, marketDestination).saveOrderPreview(accountId, map, marketDestination, delegate)
    }

    override suspend fun placeOrder(orderId: String): ETResult<TradeOrder> {
        val order = activeOrders[orderId] ?: return ETResult.failure(IllegalArgumentException("Invalid previewId: $orderId"))
        return order.delegate.placeOrder(order.arguments, order.state).updateOrder(order)
    }

    override suspend fun loadOrder(orderId: String): ETResult<TradeOrder> {
        val order = activeOrders[orderId]
        return if (order != null) {
            ETResult.success(order.state)
        } else {
            ETResult.failure(IllegalArgumentException("Order $orderId not found."))
        }
    }

    override suspend fun clearOrder(orderId: String): ETResult<Unit> {
        activeOrders.remove(orderId)
        return ETResult.success(Unit)
    }

    private fun ETResult<OrderResponse>.saveOrderPreview(
        accountId: String,
        map: InputValuesSnapshot,
        marketDestination: MarketDestination,
        delegate: TradeDelegate
    ): ETResult<TradeOrder> {

        return map { response ->
            val orderId = UUID.randomUUID().toString()
            val legDetails = map[DefaultOptionTradeFormInputId.tradeLegs]?.let(tradeLegsCoder::decode).orEmpty()
            TradeOrder(orderId, accountId, response, OrderRequest(legDetails)).also { state ->
                activeOrders[orderId] = Order(TradeOrderArguments(map, marketDestination), delegate, state)
            }
        }
    }

    private fun ETResult<OrderResponse>.updateOrder(order: Order): ETResult<TradeOrder> {
        return map { response ->
            val legDetails = order.state.orderRequest?.legDetails.orEmpty()
            order.state.copy(response = response, orderRequest = OrderRequest(legDetails)).also { state ->
                activeOrders[order.state.orderId] = order.copy(state = state)
            }
        }
    }

    private data class Order(
        val arguments: TradeOrderArguments,
        val delegate: TradeDelegate,
        val state: TradeOrder
    )
}
