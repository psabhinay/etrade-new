@file:Suppress("TopLevelPropertyNaming", "MagicNumber")

package com.etrade.mobilepro.common.compose.theme

import androidx.compose.material.Colors
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.ui.graphics.Color

/**
 * Colors to be used in color palettes
 */
private val Purple = Color(0xFF5627D8)
private val PurpleNight = Color(0xFFAB80FF)
private val Plum = Color(0xFF241056)
private val PlumNight = Color(0xFF19233C)
private val LightPurple = Color(0xFFAB80FF)
private val LightPurpleNight = Color(0xFFAB80FF)
private val LightGrey = Color(0xFFE4E4E4)
private val LightGreyNight = Color(0xFF424A5F)
private val LightestGrey = Color(0xFFF8F8F8)
private val LightestGreyNight = Color(0xFF131A2C)
private val LightBlack = Color(0xFF444444)
private val LightBlackNight = Color(0xFFFFFFFF)
private val LightBlackOpacity30 = Color(0xAA444444)
private val LightBlackOpacity30Night = Color(0xAAFFFFFF)
private val Green = Color(0xFF008000)
private val GreenNight = Color(0xFF23E1A7)
private val Red = Color(0xFFE00000)
private val RedNight = Color(0xFFFF623E)
private val BlackOpacity60 = Color.Black.copy(alpha = 0.6F)
private val lightBlackOpacity50 = Color(0x80FFFFFF)
private val WhiteOpacity60 = Color.White.copy(alpha = 0.6F)

/**
 * Color Palettes for Compose components
 */
private val DarkColors = darkColors(
    primary = Plum,
    onPrimary = Color.White,
    secondary = PlumNight,
    onSecondary = Color.White,
    background = Color.Transparent,
)

private val LightColors = lightColors(
    primary = Plum,
    onPrimary = Color.Black,
    secondary = LightGrey,
    onSecondary = Color.White,
    background = lightBlackOpacity50,
)

data class EtColors(
    internal val material: Colors,
    val purple: Color,
    val plum: Color,
    val lightPurple: Color,
    val green: Color,
    val red: Color,
    val grey: Color,
    val lightGrey: Color,
    val lightBlackOpacity30: Color,
    val bottomSheetSurface: Color,
    val bottomSheetTextColor: Color,
    val progressIndicator: Color,
    val infoDialogMessage: Color,
    val footerDateTime: Color,
)

val LightColorPalette = EtColors(
    LightColors,
    Purple,
    Plum,
    LightPurple,
    Green,
    Red,
    LightBlack,
    LightGrey,
    LightBlackOpacity30,
    Color.White,
    Color.Black,
    Purple,
    BlackOpacity60,
    LightBlack
)

val DarkColorPalette = EtColors(
    DarkColors,
    PurpleNight,
    PlumNight,
    LightPurpleNight,
    GreenNight,
    RedNight,
    LightBlackNight,
    LightGreyNight,
    LightBlackOpacity30Night,
    PlumNight,
    LightPurple,
    LightPurple,
    WhiteOpacity60,
    Color.White,
)
