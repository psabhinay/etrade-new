package com.etrade.mobilepro.util.android.clicklisteners

import android.content.DialogInterface
import com.etrade.mobilepro.util.android.extension.lifecycleScope
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * Ignore all the click events until [timeoutMillis] pass after a valid click event
 * Use this with [MaterialAlertDialogBuilder.setPositiveButton] [MaterialAlertDialogBuilder.setNegativeButton] [MaterialAlertDialogBuilder.setNeutralButton]
 *
 * @param clickListener clickListener to be get called for valid click event
 * @param timeoutMillis duration to ignore all other click events after a valid click
 */
class OnDialogDebounceClickListener(
    private val clickListener: DialogInterface.OnClickListener,
    private val timeoutMillis: Long = 1000
) : DialogInterface.OnClickListener {

    private var canClick = true

    override fun onClick(dialog: DialogInterface?, which: Int) {
        if (canClick) {
            canClick = false
            dialog.lifecycleScope?.launch(Dispatchers.Main) {
                clickListener.onClick(dialog, which)
                delay(timeoutMillis)
                canClick = true
            }
        }
    }
}
