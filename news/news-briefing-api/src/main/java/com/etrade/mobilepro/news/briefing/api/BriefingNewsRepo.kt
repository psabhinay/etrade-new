package com.etrade.mobilepro.news.briefing.api

import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable

interface BriefingNewsRepo {

    fun getBriefingNews(cacheExpiration: Long?): Observable<Resource<List<NewsItem>>>
}
