package com.etrade.mobilepro.chart.data

import com.etrade.mobilepro.chart.api.ChartPoint
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ChartDto(
    @Json(name = "accountData")
    val accounts: List<AccountDataDto>?,
    @Json(name = "indexData")
    val indexes: List<IndexDataDto>?
)

@JsonClass(generateAdapter = true)
class AccountDataDto(
    @Json(name = "accountNo")
    val accountNumber: String?,
    @Json(name = "pointData")
    val points: List<PointDataDto>
)

@JsonClass(generateAdapter = true)
class IndexDataDto(
    @Json(name = "index")
    val index: String,
    @Json(name = "pointData")
    val points: List<PointDataDto>
)

@JsonClass(generateAdapter = true)
class PointDataDto(
    @Json(name = "date")
    val date: String,
    @Json(name = "value")
    val value: String
) : ChartPoint {
    override val rawX: String
        get() = date
    override val rawY: String
        get() = value
}
