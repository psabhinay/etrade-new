package com.etrade.mobilepro.alerts.presentation.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.presentation.R
import com.etrade.mobilepro.alerts.presentation.adapter.viewholder.AlertDividerViewHolder
import com.etrade.mobilepro.util.android.extension.inflate
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class AlertDividerAdapterDelegate : AbsListItemAdapterDelegate<AlertItem.Divider, AlertItem, AlertDividerViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): AlertDividerViewHolder {
        val view = parent.inflate(R.layout.thick_divider)
        return AlertDividerViewHolder(view)
    }

    override fun isForViewType(item: AlertItem, items: MutableList<AlertItem>, position: Int): Boolean {
        return item is AlertItem.Divider
    }

    override fun onBindViewHolder(
        item: AlertItem.Divider,
        holder: AlertDividerViewHolder,
        payloads: MutableList<Any>
    ) {
        holder.bindTo(item)
    }
}
