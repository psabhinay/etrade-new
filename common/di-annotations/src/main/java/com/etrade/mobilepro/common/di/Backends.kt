package com.etrade.mobilepro.common.di

import javax.inject.Qualifier

@Qualifier
annotation class MobileTrade

@Qualifier
annotation class Web

@Qualifier
annotation class EDocWeb

@Qualifier
annotation class Express

@Qualifier
annotation class Mod

@Qualifier
annotation class AppsFlyer

@Qualifier
annotation class Api

@Qualifier
annotation class Level1Streamer

@Qualifier
annotation class Level2Streamer

@Qualifier
annotation class MobileEtrade

@Qualifier
annotation class MoshiConverter

@Qualifier
annotation class SimpleXmlConverter
