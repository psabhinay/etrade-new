package com.etrade.mobilepro.marketscalendar.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.calendar.CalendarMode
import com.etrade.mobilepro.calendar.MIDDLE_OF_CALENDAR_RANGE
import org.threeten.bp.LocalDate
import javax.inject.Inject

interface SharedCalendarState {
    val selectedDate: LocalDate
    val selectedDateLiveData: LiveData<LocalDate>
    val selectedCalendarRange: LiveData<Triple<LocalDate, LocalDate, Int>>
    fun updateSelectedDate(date: LocalDate)
    fun updateSelectedCalendarRange(range: Triple<LocalDate, LocalDate, Int>)
    fun reset()
}

class DefaultSharedCalendarState @Inject constructor() : SharedCalendarState {
    private val currentCalendarRange = Triple(
        CalendarMode.TWO_WEEKS.getFirstDateOfRange(),
        CalendarMode.TWO_WEEKS.getLastDateOfRange(),
        MIDDLE_OF_CALENDAR_RANGE
    )
    private var _selectedDate: MutableLiveData<LocalDate> = MutableLiveData<LocalDate>().apply { value = LocalDate.now() }
    private val _selectedCalendarRange = MutableLiveData(currentCalendarRange)

    override val selectedCalendarRange: LiveData<Triple<LocalDate, LocalDate, Int>>
        get() = _selectedCalendarRange

    override val selectedDateLiveData: LiveData<LocalDate>
        get() = _selectedDate

    override val selectedDate: LocalDate
        get() {
            return _selectedDate.value!!
        }

    override fun reset() {
        updateSelectedDate(LocalDate.now())
        updateSelectedCalendarRange(currentCalendarRange)
    }

    override fun updateSelectedDate(date: LocalDate) {
        _selectedDate.value = date
    }

    override fun updateSelectedCalendarRange(range: Triple<LocalDate, LocalDate, Int>) {
        _selectedCalendarRange.value = range
    }
}
