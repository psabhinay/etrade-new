package com.etrade.mobilepro.dynamicui.api

interface PositionAware {
    var currentPosition: Int?
}
