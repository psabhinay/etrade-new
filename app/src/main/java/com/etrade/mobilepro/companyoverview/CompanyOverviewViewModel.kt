package com.etrade.mobilepro.companyoverview

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.etrade.eo.core.util.nullToEmpty
import com.etrade.mobilepro.R
import com.etrade.mobilepro.mod.api.CompanyOverview
import com.etrade.mobilepro.overlay.TextOverlayViewModel
import com.etrade.mobilepro.util.Resource
import javax.inject.Inject

class CompanyOverviewViewModel @Inject constructor(
    private val resources: Resources,
    private val companyOverviewProvider: CompanyOverviewProvider
) : ViewModel(), TextOverlayViewModel {

    private var companyOverview: LiveData<Resource<String>>? = null

    override val text = MediatorLiveData<Resource<CharSequence?>>()

    override val title = MutableLiveData<String>().apply { value = resources.getString(R.string.title_company_overview) }

    override val icon = MutableLiveData<Int>().apply { value = R.drawable.ic_arrow_back }

    fun showCompanyOverview(symbol: String) {
        companyOverview?.let {
            text.removeSource(it)
        }
        val overview = createCompanyOverviewTextLiveData(symbol)
        companyOverview = overview
        text.addSource(overview) {
            text.value = it.map { overview -> overview }
        }
    }

    private fun createCompanyOverviewTextLiveData(symbol: String): LiveData<Resource<String>> {
        return Transformations.map(companyOverviewProvider.createCompanyOverviewLiveData(symbol)) { resource ->
            resource.map { it?.let { companyOverview -> createCompanyOverviewText(companyOverview) } ?: resources.getString(R.string.company_overview_not_found) }
        }
    }

    private fun createCompanyOverviewText(companyOverview: CompanyOverview): String? {
        return companyOverview.overview?.let { overview ->
            val sectorAndIndustry = "${companyOverview.sector?.nullToEmpty()}: ${companyOverview.industry?.nullToEmpty()}"
            val header = "$sectorAndIndustry\n${companyOverview.webAddress?.nullToEmpty()}"
            "$header\n\n$overview"
        }
    }
}
