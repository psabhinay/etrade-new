package com.etrade.mobilepro.quote.screen.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.user.session.manager.SessionState
import com.etrade.mobilepro.user.session.manager.SessionStateChangeListener
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import javax.inject.Inject

class QuoteTabsViewModel @Inject constructor(
    private val sessionState: SessionState
) : ViewModel(), SessionStateChangeListener {

    init {
        sessionState.addListener(this)
    }

    public override fun onCleared() {
        sessionState.removeListener(this)
    }

    val scrollToPageEvent: LiveData<ConsumableLiveEvent<Int>>
        get() = _navEvent
    private val _navEvent = MutableLiveData<ConsumableLiveEvent<Int>>()
    var indexToNavigateTo: Int? = null

    override fun onSessionStateChange(state: SessionStateChange) {
        when (state) {
            SessionStateChange.AUTHENTICATED -> {
                indexToNavigateTo?.let { navigateToIndex(it) }
                indexToNavigateTo = null
            }
            else -> {
                // NO OP
            }
        }
    }

    fun navigateToIndex(index: Int) {
        _navEvent.value = index.consumable()
    }
}
