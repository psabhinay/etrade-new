package com.etrade.mobilepro.portfolio.model

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobilepro.positions.api.TaxLotPosition
import com.etrade.mobilepro.positions.data.mapper.BOOLEAN_STRING_VALUE_TRUE
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

@JsonClass(generateAdapter = true)
class TaxLotReferenceDto(
    @Json(name = "positionId")
    override val positionId: String,
    @Json(name = "positionLotId")
    override val taxLotId: String,
    @Json(name = "acquiredDate")
    override val acquiredDateMs: Long,
    @Json(name = "price")
    val price: String,
    @Json(name = "termCode")
    val termCode: String,
    @Json(name = "daysGain")
    val daysGainValue: String,
    @Json(name = "daysGainPct")
    val daysGainPct: String,
    @Json(name = "marketValue")
    val marketValueRaw: String,
    @Json(name = "totalCost")
    val totalCost: String,
    @Json(name = "totalCostForGainPct")
    val totalCostForGainPct: String,
    @Json(name = "totalGain")
    val totalGainValue: String,
    @Json(name = "lotSourceCode")
    val lotSourceCode: String,
    @Json(name = "originalQty")
    val originalQty: String,
    @Json(name = "remainingQty")
    val remainingQty: String,
    @Json(name = "availableQty")
    val availableQty: String,
    @Json(name = "orderNo")
    val orderNo: String,
    @Json(name = "legNo")
    val legNo: String,
    @Json(name = "locationCode")
    val locationCode: String,
    @Json(name = "exchangeRate")
    val exchangeRate: String,
    @Json(name = "settlementCurrency")
    val settlementCurrency: String,
    @Json(name = "paymentCurrency")
    val paymentCurrency: String,
    @Json(name = "adjPrice")
    val adjPrice: String,
    @Json(name = "commPerShare")
    val commPerShare: String,
    @Json(name = "feesPerShare")
    val feesPerShare: String,
    @Json(name = "shortType")
    val shortType: String,
    @Json(name = "inTheMoneyFlag")
    val inTheMoneyFlagRaw: String?

) : TaxLotPosition {

    override val symbol: String
        get() = DateFormattingUtils.formatLocaleIndependentShortDateWithSlashes(TimeUnit.SECONDS.toMillis(acquiredDateMs))

    override val daysGain: BigDecimal?
        get() = daysGainValue.safeParseBigDecimal()

    override val totalGain: BigDecimal?
        get() = totalGainValue.safeParseBigDecimal()

    override val pricePaid: BigDecimal?
        get() = price.safeParseBigDecimal()

    override val commission: BigDecimal?
        get() = commPerShare.safeParseBigDecimal()

    override val fees: BigDecimal?
        get() = feesPerShare.safeParseBigDecimal()

    override val quantity: BigDecimal?
        get() = remainingQty.safeParseBigDecimal()

    override val displayQuantity: BigDecimal?
        get() = quantity

    override val daysGainPercentage: BigDecimal?
        get() = daysGainPct.safeParseBigDecimal()

    override val totalGainPercentage: BigDecimal?
        get() = totalCostForGainPct.safeParseBigDecimal()

    override val todayClose: BigDecimal? = null

    override val baseSymbolAndPrice: String? = null

    override val mark: BigDecimal? = null

    override val marketValue: BigDecimal?
        get() = marketValueRaw.safeParseBigDecimal()

    override val inTheMoneyFlag: Boolean
        get() = inTheMoneyFlagRaw == BOOLEAN_STRING_VALUE_TRUE
}
