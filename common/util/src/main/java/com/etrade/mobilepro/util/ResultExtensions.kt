package com.etrade.mobilepro.util

import com.etrade.mobilepro.common.result.ETResult

inline fun <R, T> ETResult<T>.flatMap(transform: (value: T) -> ETResult<R>): ETResult<R> {
    return fold(
        onSuccess = { transform(it) },
        onFailure = { ETResult.failure(it) }
    )
}

/**
 * Calls the specified function [block] as a side effect.
 *
 * Returns the original `ETResult.success` unchanged or in case of failure the original `ETResult.failure` or the side effect `ETResult.failure`.
 */
inline fun <R, T> ETResult<T>.applySideEffect(block: (value: T) -> ETResult<R>): ETResult<T> {
    return fold(
        onSuccess = { value ->
            block(value).fold(
                onSuccess = { ETResult.success(value) },
                onFailure = { ETResult.failure(it) }
            )
        },
        onFailure = { ETResult.failure(it) }
    )
}
