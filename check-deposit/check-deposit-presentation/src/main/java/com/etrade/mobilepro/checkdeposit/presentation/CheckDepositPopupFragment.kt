package com.etrade.mobilepro.checkdeposit.presentation

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.util.android.textutil.TextUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder

@RequireLogin
class CheckDepositPopupFragment : DialogFragment() {

    private val args: CheckDepositPopupFragmentArgs by navArgs()

    override fun onCreateDialog(savedInstanceState: Bundle?) =
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(TextUtil.convertTextWithHmlTagsToSpannable(args.message))
            .setPositiveButton(R.string.ok) { _, _ ->
                dismiss()
            }
            .create()
            .also { isCancelable = false }
}
