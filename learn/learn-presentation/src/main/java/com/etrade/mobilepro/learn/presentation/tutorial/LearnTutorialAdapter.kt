package com.etrade.mobilepro.learn.presentation.tutorial

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.learn.presentation.R

class LearnTutorialAdapter(
    val context: Context,
    private val tutorialWalkThroughRouting: (LearnTutorialItem) -> Unit
) : RecyclerView.Adapter<LearnTutorialItemViewHolder>() {

    private var _tutorialItems = emptyList<LearnTutorialItem>()

    internal fun setTutorialItems(newTutorialItems: List<LearnTutorialItem>) {
        _tutorialItems = newTutorialItems
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LearnTutorialItemViewHolder {
        val itemHolder = LayoutInflater.from(parent.context)
            .inflate(R.layout.learn_tutorial_recyclerview_item, parent, false)

        return LearnTutorialItemViewHolder(itemHolder)
    }

    override fun onBindViewHolder(holder: LearnTutorialItemViewHolder, position: Int) {
        holder.bind(_tutorialItems[position], tutorialWalkThroughRouting)
    }

    override fun getItemCount(): Int {
        return _tutorialItems.size
    }
}
