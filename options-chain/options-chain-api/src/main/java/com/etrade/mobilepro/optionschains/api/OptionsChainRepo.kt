package com.etrade.mobilepro.optionschains.api

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.instrument.ExpirationDate

/**
 * Provider for the options chains api
 */
interface OptionsChainRepo {

    /**
     * Gets options chain data for a specific [symbol].
     *
     * @param symbol The symbol to get options chain.
     *
     * @param expirationDate The selected expiration date you want options from.
     *
     * @return Result containing an options chain or an error.
     */
    suspend fun getOptionsChain(symbol: String, expirationDate: ExpirationDate? = null): ETResult<OptionsChain>
}
