package com.etrade.mobilepro.quotelookup.options.call

import androidx.lifecycle.Observer
import com.etrade.mobilepro.quotelookup.options.StockOptionLookupBaseFragment
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.util.android.consume

class StockCallOptionLookupFragment : StockOptionLookupBaseFragment() {

    override fun observeCurrentSymbol() {
        viewModel.currentCallSymbol.observe(
            viewLifecycleOwner,
            Observer { currentCallSymbol ->
                currentCallSymbol.consume { searchByQuery(it) }
            }
        )
    }

    override val optionSearchType: SearchType
        get() = SearchType.CALL_OPTION
}
