package com.etrade.mobilepro.chart.performance;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J%\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\f2\u0006\u0010\u000f\u001a\u00020\u0010H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00020\u0007H\u0002JH\u0010\u0014\u001a\u00020\u0015*\u00020\u000e2\u0006\u0010\u0016\u001a\u00020\u00172\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0016\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u001ej\u0002`\u001f\u0012\u0004\u0012\u00020\u00150\u001dH\u0002J\u001a\u0010 \u001a\b\u0012\u0004\u0012\u00020\u000e0\r*\u00020!2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J(\u0010\"\u001a\u00020#*\u00020\u00172\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006*\b\u0012\u0004\u0012\u00020\b0\u00068BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\n\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006%"}, d2 = {"Lcom/etrade/mobilepro/chart/performance/GetPerformanceChartLineDataUseCase;", "Lcom/etrade/mobilepro/chart/data/GetChartLineDataUseCase;", "performanceChartRepo", "Lcom/etrade/mobilepro/chart/performance/data/PerformanceChartRepo;", "(Lcom/etrade/mobilepro/chart/performance/data/PerformanceChartRepo;)V", "chartIds", "", "", "Lcom/etrade/mobilepro/chart/data/ChartParams;", "getChartIds", "(Ljava/util/List;)Ljava/util/List;", "execute", "Lio/reactivex/Observable;", "Lcom/etrade/mobilepro/util/Resource;", "Lcom/etrade/mobilepro/chart/model/ChartLineData;", "parameter", "Lcom/etrade/mobilepro/chart/data/GetChartLineDataParameter;", "(Lcom/etrade/mobilepro/chart/data/GetChartLineDataParameter;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "normalizeChartId", "rawChartId", "addLineSet", "", "line", "Lcom/etrade/mobilepro/chart/api/ChartLine;", "chartParams", "", "chartTimeFrame", "Lcom/etrade/mobilepro/chart/model/ChartTimeFrame;", "onException", "Lkotlin/Function1;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "toChartLineData", "Lcom/etrade/mobilepro/chart/performance/model/PerformanceChartData;", "toLineSet", "Lcom/etrade/mobilepro/chart/model/ChartLineSet;", "params", "performance_debug"})
public final class GetPerformanceChartLineDataUseCase implements com.etrade.mobilepro.chart.data.GetChartLineDataUseCase {
    private final com.etrade.mobilepro.chart.performance.data.PerformanceChartRepo performanceChartRepo = null;
    
    @javax.inject.Inject()
    public GetPerformanceChartLineDataUseCase(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.chart.performance.data.PerformanceChartRepo performanceChartRepo) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object execute(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.chart.data.GetChartLineDataParameter parameter, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super io.reactivex.Observable<com.etrade.mobilepro.util.Resource<com.etrade.mobilepro.chart.model.ChartLineData>>> continuation) {
        return null;
    }
    
    private final java.util.List<java.lang.String> getChartIds(java.util.List<com.etrade.mobilepro.chart.data.ChartParams> $this$chartIds) {
        return null;
    }
    
    private final com.etrade.mobilepro.util.Resource<com.etrade.mobilepro.chart.model.ChartLineData> toChartLineData(com.etrade.mobilepro.chart.performance.model.PerformanceChartData $this$toChartLineData, com.etrade.mobilepro.chart.data.GetChartLineDataParameter parameter) {
        return null;
    }
    
    private final com.etrade.mobilepro.chart.model.ChartLineSet toLineSet(com.etrade.mobilepro.chart.api.ChartLine $this$toLineSet, java.util.Map<java.lang.String, com.etrade.mobilepro.chart.data.ChartParams> params, com.etrade.mobilepro.chart.model.ChartTimeFrame chartTimeFrame) {
        return null;
    }
    
    private final void addLineSet(com.etrade.mobilepro.chart.model.ChartLineData $this$addLineSet, com.etrade.mobilepro.chart.api.ChartLine line, java.util.Map<java.lang.String, com.etrade.mobilepro.chart.data.ChartParams> chartParams, com.etrade.mobilepro.chart.model.ChartTimeFrame chartTimeFrame, kotlin.jvm.functions.Function1<? super java.lang.Exception, kotlin.Unit> onException) {
    }
    
    private final java.lang.String normalizeChartId(java.lang.String rawChartId) {
        return null;
    }
}