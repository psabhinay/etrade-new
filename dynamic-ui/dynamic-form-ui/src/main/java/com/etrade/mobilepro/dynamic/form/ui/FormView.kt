package com.etrade.mobilepro.dynamic.form.ui

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.dynamic.form.api.Component
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.Group
import com.etrade.mobilepro.dynamic.form.ui.factory.ComponentViewFactory
import com.etrade.mobilepro.dynamic.form.ui.factory.ComponentViewFactoryProvider
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import kotlin.reflect.KClass

class FormView : RecyclerView {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val groupTopMargin: Int = context.resources.getDimensionPixelSize(R.dimen.spacing_small)
    private val groupBottomMargin: Int = context.resources.getDimensionPixelSize(R.dimen.spacing_medium)

    private val viewFactoryProvider: ComponentViewFactoryProvider = ComponentViewFactoryProvider()

    init {
        layoutManager = LinearLayoutManager(context)
        setupRecyclerViewDivider(R.drawable.thick_divider)
    }

    fun <C : Component, V : ComponentView<C>> addViewFactory(cls: KClass<C>, viewFactory: ComponentViewFactory<C, V>) {
        viewFactoryProvider.addViewFactory(cls, viewFactory)
    }

    fun render(form: Form, owner: LifecycleOwner) {
        adapter = GroupAdapter(form.groups, owner)
    }

    private inner class GroupAdapter(
        private val groups: List<Group>,
        private val owner: LifecycleOwner
    ) : Adapter<GroupViewHolder>() {

        override fun getItemCount(): Int = groups.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupViewHolder = GroupViewHolder(parent.context)

        override fun onBindViewHolder(holder: GroupViewHolder, position: Int) = holder.bindTo(groups[position], owner)
    }

    private inner class GroupViewHolder(context: Context) : ViewHolder(GroupView(context)) {

        private val groupView: GroupView = itemView as GroupView

        init {
            groupView.viewFactoryProvider = viewFactoryProvider
            groupView.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply {
                topMargin = groupTopMargin
                bottomMargin = groupBottomMargin
            }
        }

        fun bindTo(group: Group, owner: LifecycleOwner) = groupView.render(group, owner)
    }
}
