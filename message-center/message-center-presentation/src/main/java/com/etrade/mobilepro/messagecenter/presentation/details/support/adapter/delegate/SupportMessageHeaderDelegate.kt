package com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.messagecenter.presentation.databinding.MessageCenterItemSupportMessageHeaderBinding
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.Header
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.SupportMessageItem
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.viewholder.SupportMessageHeaderViewHolder
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

internal class SupportMessageHeaderDelegate : AbsListItemAdapterDelegate<Header, SupportMessageItem, SupportMessageHeaderViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): SupportMessageHeaderViewHolder =
        SupportMessageHeaderViewHolder(
            parent.viewBinding(
                MessageCenterItemSupportMessageHeaderBinding::inflate
            )
        )

    override fun isForViewType(item: SupportMessageItem, items: MutableList<SupportMessageItem>, position: Int): Boolean =
        item is Header

    override fun onBindViewHolder(item: Header, holder: SupportMessageHeaderViewHolder, payloads: MutableList<Any>) =
        holder.bindTo(item)
}
