@file:Suppress("LongParameterList")
package com.etrade.mobilepro.etcompose.tabhorizontalpager

import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.DecayAnimationSpec
import androidx.compose.animation.rememberSplineBasedDecay
import androidx.compose.foundation.gestures.FlingBehavior
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter

/**
 * Contains the default values used by [ETHorizontalPager].
 */
object PagerDefaults {
    /**
     * Remember the default [FlingBehavior] that represents the scroll curve.
     *
     * @param state The [ETPagerState] to update.
     * @param decayAnimationSpec The decay animation spec to use for decayed flings.
     * @param snapAnimationSpec The animation spec to use when snapping.
     */
    @Composable
    fun flingBehavior(
        state: ETPagerState,
        decayAnimationSpec: DecayAnimationSpec<Float> = rememberSplineBasedDecay(),
        snapAnimationSpec: AnimationSpec<Float> = ETSnappingFlingBehaviorDefaults.snapAnimationSpec,
    ): FlingBehavior = rememberETSnappingFlingBehavior(
        lazyListState = state.lazyListState,
        decayAnimationSpec = decayAnimationSpec,
        snapAnimationSpec = snapAnimationSpec,
    )

    @Deprecated(
        "Replaced with PagerDefaults.flingBehavior()",
        ReplaceWith("PagerDefaults.flingBehavior(state, decayAnimationSpec, snapAnimationSpec)")
    )
    @Composable
    fun rememberPagerFlingConfig(
        state: ETPagerState,
        decayAnimationSpec: DecayAnimationSpec<Float> = rememberSplineBasedDecay(),
        snapAnimationSpec: AnimationSpec<Float> = ETSnappingFlingBehaviorDefaults.snapAnimationSpec,
    ): FlingBehavior = flingBehavior(state, decayAnimationSpec, snapAnimationSpec)
}

/**
 * A horizontally scrolling layout that allows users to flip between items to the left and right.
 *
 * @sample com.google.accompanist.sample.pager.HorizontalPagerSample
 *
 * @param count the number of pages.
 * @param modifier the modifier to apply to this layout.
 * @param state the state object to be used to control or observe the pager's state.
 * @param reverseLayout reverse the direction of scrolling and layout, when `true` items will be
 * composed from the end to the start and [ETPagerState.currentPage] == 0 will mean
 * the first item is located at the end.
 * @param itemSpacing horizontal spacing to add between items.
 * @param flingBehavior logic describing fling behavior.
 * @param key the scroll position will be maintained based on the key, which means if you
 * add/remove items before the current visible item the item with the given key will be kept as the
 * first visible one.
 * @param onScrollFinished callback is called when scroll action finishes
 * @param content a block which describes the content. Inside this block you can reference
 * [ETPagerScope.currentPage] and other properties in [ETPagerScope].
 */

@Composable
fun ETHorizontalPager(
    count: Int,
    modifier: Modifier = Modifier,
    state: ETPagerState = rememberPagerState(0),
    reverseLayout: Boolean = false,
    itemSpacing: Dp = 0.dp,
    flingBehavior: FlingBehavior = PagerDefaults.flingBehavior(state),
    verticalAlignment: Alignment.Vertical = Alignment.CenterVertically,
    key: ((page: Int) -> Any)? = null,
    contentPadding: PaddingValues = PaddingValues(0.dp),
    onScrollFinished: () -> Unit,
    content: @Composable ETPagerScope.(page: Int) -> Unit,
) {
    Pager(
        count = count,
        state = state,
        modifier = modifier,
        reverseLayout = reverseLayout,
        itemSpacing = itemSpacing,
        verticalAlignment = verticalAlignment,
        flingBehavior = flingBehavior,
        key = key,
        contentPadding = contentPadding,
        onScrollFinished = onScrollFinished,
        content = content
    )
}

@Composable
internal fun Pager(
    count: Int,
    modifier: Modifier,
    state: ETPagerState,
    reverseLayout: Boolean,
    itemSpacing: Dp,
    flingBehavior: FlingBehavior,
    key: ((page: Int) -> Any)?,
    contentPadding: PaddingValues,
    verticalAlignment: Alignment.Vertical = Alignment.CenterVertically,
    horizontalAlignment: Alignment.Horizontal = Alignment.CenterHorizontally,
    onScrollFinished: () -> Unit,
    content: @Composable ETPagerScope.(page: Int) -> Unit,
) {
    require(count >= 0) { "pageCount must be >= 0" }

    // Provide our PagerState with access to the SnappingFlingBehavior animation target
    state.flingAnimationTarget = {
        (flingBehavior as? ETSnappingFlingBehavior)?.animationTarget
    }

    // Once a fling (scroll) has finished, notify the state
    LaunchedEffect(state) {
        // When a 'scroll' has finished, notify the state
        snapshotFlow { state.isScrollInProgress }
            .filter { !it }
            .collect {
                state.onScrollFinished()
                onScrollFinished()
            }
    }

    val pagerScope = remember(state) { ETPagerScopeImpl(state) }
    LazyRow(
        state = state.lazyListState,
        verticalAlignment = verticalAlignment,
        horizontalArrangement = Arrangement.spacedBy(itemSpacing, horizontalAlignment),
        flingBehavior = flingBehavior,
        reverseLayout = reverseLayout,
        contentPadding = contentPadding,
        modifier = modifier,
    ) {
        items(
            count = count,
            key = key,
        ) { page ->
            Box(
                Modifier
                    // We don't any nested flings to continue in the pager, so we add a
                    // connection which consumes them.
                    // See: https://github.com/google/accompanist/issues/347
                    .nestedScroll(connection = ConsumeFlingNestedScrollConnection)
                    // Constraint the content to be <= than the size of the pager.
                    .fillParentMaxSize()
            ) {
                pagerScope.content(page)
            }
        }
    }
}

private object ConsumeFlingNestedScrollConnection : NestedScrollConnection {
    override fun onPostScroll(
        consumed: Offset,
        available: Offset,
        source: NestedScrollSource
    ): Offset = when (source) {
        // We can consume all resting fling scrolls so that they don't propagate up to the
        // Pager
        NestedScrollSource.Fling -> available
        else -> Offset.Zero
    }

    override suspend fun onPostFling(consumed: Velocity, available: Velocity): Velocity {
        // We can consume all post fling velocity so that it doesn't propagate up to the Pager
        return available
    }
}

/**
 * Scope for [HorizontalPager] content.
 */
@Stable
interface ETPagerScope {
    /**
     * Returns the current selected page
     */
    val currentPage: Int

    /**
     * The current offset from the start of [currentPage], as a ratio of the page width.
     */
    val currentPageOffset: Float
}

private class ETPagerScopeImpl(
    private val state: ETPagerState,
) : ETPagerScope {
    override val currentPage: Int get() = state.currentPage
    override val currentPageOffset: Float get() = state.currentPageOffset
}

/**
 * Calculate the offset for the given [page] from the current scroll position. This is useful
 * when using the scroll position to apply effects or animations to items.
 *
 * The returned offset can positive or negative, depending on whether which direction the [page] is
 * compared to the current scroll position.
 *
 * @sample com.google.accompanist.sample.pager.HorizontalPagerWithOffsetTransition
 */

fun ETPagerScope.calculateCurrentOffsetForPage(page: Int): Float {
    return (currentPage + currentPageOffset) - page
}
