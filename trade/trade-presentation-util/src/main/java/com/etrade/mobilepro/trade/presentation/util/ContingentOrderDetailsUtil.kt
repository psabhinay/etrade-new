package com.etrade.mobilepro.trade.presentation.util

import android.content.res.Resources
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.trade.api.advanced.ContingentOrderDetails
import com.etrade.mobilepro.trade.presentation.util.adapters.sectionItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.util.safeParseBigDecimal

fun ContingentOrderDetails.getSectionItems(resources: Resources): List<TableSectionItemLayout> {
    return listOfNotNull(
        sectionItem(resources, R.string.qualifier, qualifier.getLabel()),
        sectionItem(resources, R.string.condition, condition.getLabel()),
        sectionItem(resources, R.string.value, MarketDataFormatter.formatMoneyValueWithCurrency(conditionValue.safeParseBigDecimal()))
    )
}
