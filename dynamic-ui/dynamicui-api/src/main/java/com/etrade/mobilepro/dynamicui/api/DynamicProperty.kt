package com.etrade.mobilepro.dynamicui.api

interface DynamicProperty {

    var shouldHide: Boolean
}

fun Boolean.flip() = !this
