package com.etrade.mobilepro.billpay.usecase

import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.PaymentStatus
import org.threeten.bp.Instant

data class PaymentListRequestParameters(
    val status: PaymentStatus,
    val numOfDays: Int,
    val startDate: Instant,
    val sessionId: String,
    val allAccounts: Map<String, AccountDetails>
)
