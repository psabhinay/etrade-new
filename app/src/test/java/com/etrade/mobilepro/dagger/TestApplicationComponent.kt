package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.BaseApiTest
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        TestApplicationModule::class,
        NetworkModule::class
    ]
)

interface TestApplicationComponent {
    fun inject(baseTest: BaseApiTest)
}
