package com.etrade.mobilepro.trade.form.api

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.extractAdvancedOrderType
import com.etrade.mobilepro.trade.form.api.value.SecurityTypeValueId

interface TradeFormInputIdMap {

    val advancedTradeInputId: Map<SecurityType, WithAdvancedTradeFormInputId>

    val securityTypeId: String

    val securityTypeValueId: SecurityTypeValueId

    fun getInputId(tradeFormType: TradeFormType?): TradeFormInputId?
}

fun TradeFormInputIdMap.extractTradeOrderType(snapshot: InputValuesSnapshot): TradeFormType? {
    return getSecurityType(snapshot[securityTypeId])?.let { securityType ->
        val inputId = advancedTradeInputId[securityType]
        val advancedTradeType = if (inputId is WithAdvancedTradeFormInputId) {
            inputId.extractAdvancedOrderType(snapshot)
        } else {
            AdvancedTradeType.None
        }
        TradeFormType(securityType, advancedTradeType)
    }
}

fun TradeFormInputIdMap.getInputId(snapshot: InputValuesSnapshot): TradeFormInputId? = getInputId(extractTradeOrderType(snapshot))

fun TradeFormInputIdMap.getSecurityType(value: Any?): SecurityType? {
    return (value as? String)?.let {
        securityTypeValueId.converter.toValue(it)
    }
}

fun TradeFormInputIdMap.getSecurityType(snapshot: InputValuesSnapshot): SecurityType? {
    return getSecurityType(snapshot[securityTypeId])
}
