package com.etrade.mobilepro.screener.presentation.etf.portfolio

import com.etrade.mobilepro.screener.api.filter.etf.EtfPortfolioFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class EtfTotalNetAsset : ScreenerFilterEvent<EtfPortfolioFilter, EtfPortfolioViewModel.ViewState, Boolean> {
    LESS_THAN_100M {
        override fun update(viewState: EtfPortfolioViewModel.ViewState, eventValue: Boolean): EtfPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(netAssetsLessThan100 = eventValue))
    },
    ONE_HUNDRED_M_TO_500M {
        override fun update(viewState: EtfPortfolioViewModel.ViewState, eventValue: Boolean): EtfPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(netAssets100to500 = eventValue))
    },
    FIVE_HUNDRED_M_TO_1B {
        override fun update(viewState: EtfPortfolioViewModel.ViewState, eventValue: Boolean): EtfPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(netAssets500to1 = eventValue))
    },
    ONE_B_TO_2B {
        override fun update(viewState: EtfPortfolioViewModel.ViewState, eventValue: Boolean): EtfPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(netAssets1to2 = eventValue))
    },
    GREATER_THAN_2B {
        override fun update(viewState: EtfPortfolioViewModel.ViewState, eventValue: Boolean): EtfPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(netAssetsGreaterThan2 = eventValue))
    }
}

internal enum class EtfYield : ScreenerFilterEvent<EtfPortfolioFilter, EtfPortfolioViewModel.ViewState, Boolean> {
    BOTTOM_20 {
        override fun update(viewState: EtfPortfolioViewModel.ViewState, eventValue: Boolean): EtfPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(yieldBottom20 = eventValue))
    },
    BELOW_MID_20 {
        override fun update(viewState: EtfPortfolioViewModel.ViewState, eventValue: Boolean): EtfPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(yieldBelowMid20 = eventValue))
    },
    MID_20 {
        override fun update(viewState: EtfPortfolioViewModel.ViewState, eventValue: Boolean): EtfPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(yieldMid20 = eventValue))
    },
    ABOVE_MID_20 {
        override fun update(viewState: EtfPortfolioViewModel.ViewState, eventValue: Boolean): EtfPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(yieldAboveMid20 = eventValue))
    },
    TOP_20 {
        override fun update(viewState: EtfPortfolioViewModel.ViewState, eventValue: Boolean): EtfPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(yieldTop20 = eventValue))
    }
}
