package com.etrade.mobilepro.preferences

data class LandingPageParams(
    val pageId: String? = null,
    val pageUuid: String? = null,
    val accountId: String? = null
)
