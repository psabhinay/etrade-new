package com.etrade.mobilepro.screener.data.json.stock

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_EQUALS
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN_OR_EQUAL_TO
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.stock.MarketSegmentFilter
import com.etrade.mobilepro.screener.api.mergeFollowingClauses
import javax.inject.Inject

private const val CAP_FIELD = "MarketCapitalization"
private const val SECTOR_FIELD = "TRBCSectorCode"
private const val INDUSTRY_FIELD = "TRBCIndustryCode"

class MarketSegmentFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val marketSegmentFilter = screenerFilter as MarketSegmentFilter
        val arguments = mutableListOf<Argument>()

        if (marketSegmentFilter.isCapSelected()) {
            arguments.add(Argument(getCapClauses(marketSegmentFilter), CAP_FIELD))
        }

        if (!marketSegmentFilter.sector.isAllItemsElement) {
            arguments.add(Argument(listOf(getSectorClause(marketSegmentFilter)), SECTOR_FIELD))
            // industry can be added only if sector is added
            if (!marketSegmentFilter.industry.isAllItemsElement) {
                arguments.add(Argument(listOf(getIndustryClause(marketSegmentFilter)), INDUSTRY_FIELD))
            }
        }

        return PartialScreenerParams(arguments)
    }

    private fun getCapClauses(marketSegmentFilter: MarketSegmentFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (marketSegmentFilter.microCap) {
            clauses.addAll(microCapClauses)
        }
        if (marketSegmentFilter.smallCap) {
            clauses.addAll(smallCapClauses)
        }
        if (marketSegmentFilter.midCap) {
            clauses.addAll(midCapClauses)
        }
        if (marketSegmentFilter.largeCap) {
            clauses.addAll(largeCapClauses)
        }
        if (marketSegmentFilter.megaCap) {
            clauses.addAll(megaCapClauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private val microCapClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("1")),
        Clause(OPERATOR_LESS_THAN, listOf("300"))
    )
    private val smallCapClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("300")),
        Clause(OPERATOR_LESS_THAN, listOf("2000"))
    )
    private val midCapClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("2000")),
        Clause(OPERATOR_LESS_THAN, listOf("10000"))
    )
    private val largeCapClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("10000")),
        Clause(OPERATOR_LESS_THAN, listOf("200000"))
    )
    private val megaCapClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("200000")),
        Clause(OPERATOR_LESS_THAN, listOf("15000000"))
    )

    private fun getSectorClause(marketSegmentFilter: MarketSegmentFilter) = Clause(OPERATOR_EQUALS, listOf(marketSegmentFilter.sector.id))
    private fun getIndustryClause(marketSegmentFilter: MarketSegmentFilter) = Clause(OPERATOR_EQUALS, listOf(marketSegmentFilter.industry.id))
}
