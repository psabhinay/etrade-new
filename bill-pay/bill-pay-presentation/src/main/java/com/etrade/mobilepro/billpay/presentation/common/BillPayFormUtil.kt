package com.etrade.mobilepro.billpay.presentation.common

import android.content.res.Resources
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.presentation.R
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.formatters.formatAccountName
import com.etrade.mobilepro.util.formatters.formatPayeeName
import java.math.BigDecimal
import java.math.RoundingMode

private val MAXIMUM_VALID_AMOUNT = BigDecimal("99999.99")
private val MINIMUM_VALID_AMOUNT = BigDecimal("1.00")

internal const val EMPTY_STRING = ""

internal val Resources.payeeNameFormatter: (Payee) -> String
    get() = { item ->
        item.formatName() ?: getString(R.string.empty_default)
    }

internal fun Resources.getAmountError(amount: BigDecimal) =
    when {
        !amount.isValidMinimumAmount() -> getString(R.string.bill_pay_invalid_min_amount_error)
        !amount.isValidMaximumAmount() -> getString(R.string.bill_pay_invalid_max_amount_error)
        else -> EMPTY_STRING
    }

internal fun Resources.accountDetailsContentDescriptionFormatter(account: AccountDetails): String {
    return accountNameContentDescription(account.accountShortName) ?: account.accountShortName
}

internal fun Resources.payeeContentDescriptionFormatter(payee: Payee): String {
    return accountNameContentDescription(payeeNameFormatter.invoke(payee)) ?: payeeNameFormatter.invoke(payee)
}

internal fun formatCurrency(balanceAmount: BigDecimal?): String = balanceAmount?.let {
    MarketDataFormatter.formatCurrencyValue(it)
} ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE

internal fun AccountDetails.formatAccountName() = formatAccountName(accountName, accountId)

internal fun Payee.formatName() = formatPayeeName(payeeName = name, accountNumber = accountNumber, payeeNickname = nickName)

internal fun BigDecimal?.isValidAmount(): Boolean = (
    this?.let {
        isValidMaximumAmount() && isValidMinimumAmount()
    }
    ) ?: false

internal fun BigDecimal?.isValidMaximumAmount(): Boolean = (this?.let { it <= MAXIMUM_VALID_AMOUNT }) ?: false

internal fun BigDecimal?.isValidMinimumAmount(): Boolean = (this?.let { it >= MINIMUM_VALID_AMOUNT }) ?: false

internal fun BigDecimal?.formatAmount(): BigDecimal? = this?.setScale(2, RoundingMode.HALF_UP)
