package com.etrade.mobilepro.dropdown.viewmodel

import androidx.lifecycle.LiveData

interface DropDownViewModel<T> {

    val dropDown: LiveData<DropDownModel<T>>

    fun selectDropDownItem(item: T)
}
