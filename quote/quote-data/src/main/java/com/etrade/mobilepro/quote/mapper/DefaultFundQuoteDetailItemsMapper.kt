package com.etrade.mobilepro.quote.mapper

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.quote.getFundRatingsDrawable
import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.quoteapi.FundQuoteItem
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.MutualFundDetails
import com.etrade.mobilepro.quoteapi.QuoteChangeWidgetItem
import com.etrade.mobilepro.quoteapi.QuoteTemplateResources
import com.etrade.mobilepro.quoteapi.isAffiliatedProduct
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.etrade.mobilepro.util.toSentence
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class DefaultFundQuoteDetailItemsMapper @Inject constructor(private val templateResources: QuoteTemplateResources) : FundQuoteDetailItemsMapper {
    override fun getMutualFundQuoteDetail(quote: MobileQuote): FundQuoteItem.MutualFundDetailItem {
        val quotePrice = quote.quotePrice
        val lastTradeTime = quote.fundDetails?.timeOfLastTradeInMillis?.let {
            DateFormattingUtils.formatToShortDate(TimeUnit.SECONDS.toMillis(it))
        }

        val timeStamp = lastTradeTime?.let { String.format(templateResources.nasdaqAsOfDateTimeFormat, it) }
        val changeVal = quotePrice.change.toDoubleOrNull() ?: 0.0
        val changeColor = if (changeVal >= 0) {
            templateResources.positiveChangeColorRes
        } else {
            templateResources.negativeChangeColorRes
        }
        val change =
            QuoteChangeWidgetItem(
                change = quotePrice.change,
                changePercent = quotePrice.percentChange
            )

        return FundQuoteItem.MutualFundDetailItem(
            quote.fundDetails?.netAssetValue ?: templateResources.valueMissing,
            change,
            changeColor,
            quote.fundDetails?.publicOfferPrice ?: templateResources.valueMissing,
            quote.fundDetails?.previousClose ?: templateResources.valueMissing,
            quote.fundDetails?.familyName?.toSentence() ?: templateResources.valueMissing,
            timeStamp
        )
    }

    override fun getMoneyMarketFundQuoteDetail(quote: MobileQuote): FundQuoteItem.MoneyMarketFundDetail {
        val lastTradeTime = quote.fundDetails?.timeOfLastTradeInMillis?.let {
            DateFormattingUtils.formatToShortDate(TimeUnit.SECONDS.toMillis(it))
        }
        val timeStamp = lastTradeTime?.let { String.format(templateResources.nasdaqAsOfDateTimeFormat, it) }

        return FundQuoteItem.MoneyMarketFundDetail(
            quote.fundDetails?.sevenDayCurrentYield ?: templateResources.valueMissing,
            quote.fundDetails?.annualTotalReturn ?: templateResources.valueMissing,
            quote.fundDetails?.weightedAvgMaturity ?: templateResources.valueMissing,
            quote.fundDetails?.familyName?.toSentence() ?: templateResources.valueMissing,
            timeStamp
        )
    }

    override fun getMutualFundClassSection(quote: MobileQuote?): FundQuoteItem.FundClassSection = FundQuoteItem.FundClassSection(
        templateResources.fundClassTitle,
        quote?.fundDetails?.fundClassInfoList?.withIndex()?.firstOrNull {
            it.value.symbol == quote.symbol
        },
        quote?.fundDetails?.fundClassInfoList ?: emptyList()
    )

    override fun getFundIconSection(quote: MobileQuote): FundQuoteItem.FundIconSection = FundQuoteItem.FundIconSection(
        quote.fundDetails?.hasNoTransactionsFee ?: false,
        quote.fundDetails?.isAllStarFund ?: false,
        quote.isAffiliatedProduct()
    )

    override fun getMorningStarRatings(mutualFundDetails: MutualFundDetails?) = FundQuoteItem.MorningStarRatings(
        DateFormattingUtils.formatUsDateToDisplayedDate(mutualFundDetails?.morningStarAsOfDate) ?: templateResources.valueMissing,
        mutualFundDetails?.numFundsInOverallRating ?: 0,
        getFundRatingsDrawable(mutualFundDetails?.morningStarRating),
        mutualFundDetails?.numFundsInMsRatingFor3y ?: 0,
        getFundRatingsDrawable(mutualFundDetails?.threeYearMorningStarRating),
        mutualFundDetails?.numFundsInMsRatingFor5y ?: 0,
        getFundRatingsDrawable(mutualFundDetails?.fiveYearMorningStarRating),
        mutualFundDetails?.numFundsInMsRatingFor10y ?: 0,
        getFundRatingsDrawable(mutualFundDetails?.tenYearMorningStarRating)
    )

    override fun getMorningStarDetails(mutualFundDetails: MutualFundDetails?) = FundQuoteItem.MorningStarDetails(
        mutualFundDetails?.morningStarCategory ?: templateResources.valueMissing,
        mutualFundDetails?.availability ?: templateResources.valueMissing
    )

    override fun getQuarterlyReturns(mutualFundDetails: MutualFundDetails?) = FundQuoteItem.QuarterlyTrailingReturnsSection(
        DateFormattingUtils.formatUsDateToDisplayedDate(mutualFundDetails?.qtrlyPerformanceAsOfDate) ?: templateResources.valueMissing,
        mutualFundDetails?.quarterlyTrailingReturnYtd ?: templateResources.valueMissing,
        mutualFundDetails?.quarterlyTrailingReturn1y ?: templateResources.valueMissing,
        mutualFundDetails?.quarterlyTrailingReturn5y ?: templateResources.valueMissing,
        mutualFundDetails?.quarterlyTrailingReturn10y ?: templateResources.valueMissing
    )

    override fun getSummarySection(mutualFundDetails: MutualFundDetails?) = FundQuoteItem.SummarySection(
        mutualFundDetails?.netExpenseRatio ?: templateResources.valueMissing,
        mutualFundDetails?.grossExpenseRatio ?: templateResources.valueMissing,
        mutualFundDetails?.totalNetAssets ?: templateResources.valueMissing,
        DateFormattingUtils.formatUsDateToDisplayedDate(mutualFundDetails?.inceptionDate) ?: templateResources.valueMissing,
        mutualFundDetails?.thirtyDaySecYield ?: templateResources.valueMissing,
        mutualFundDetails?.oneYearSecYield ?: templateResources.valueMissing,
        mutualFundDetails?.initialInvestment ?: templateResources.valueMissing,
        mutualFundDetails?.subsequentInvestment ?: templateResources.valueMissing,
        mutualFundDetails?.salesCharge ?: templateResources.valueMissing,
        mutualFundDetails?.fundRedemptionFee ?: templateResources.valueMissing,
        mutualFundDetails?.etradeTransactionFee ?: templateResources.valueMissing
    )

    @Suppress("LongMethod")
    override fun getFundCategoryPerformanceItem(quote: MobileQuote) = FundQuoteItem.FundPerformanceItem(
        symbol = quote.symbol,
        fundCategoryName = quote.mutualFundPerformance?.fundCategoryName?.let { String.format(templateResources.fundPerformanceCategoryFormat, it) }
            ?: templateResources.valueMissing,

        nonAdjMonthlyTrailingReturnYtd = quote.mutualFundPerformance?.nonAdjMonthlyTrailingReturnYtd ?: templateResources.valueMissing,
        nonAdjMonthlyTrailingReturn1y = quote.mutualFundPerformance?.nonAdjMonthlyTrailingReturn1y ?: templateResources.valueMissing,
        nonAdjMonthlyTrailingReturn5y = quote.mutualFundPerformance?.nonAdjMonthlyTrailingReturn5y ?: templateResources.valueMissing,
        nonAdjMonthlyTrailingReturn10y = quote.mutualFundPerformance?.nonAdjMonthlyTrailingReturn10y ?: templateResources.valueMissing,

        adjMonthlyTrailingReturnYtd = quote.mutualFundPerformance?.adjMonthlyTrailingReturnYtd ?: templateResources.valueMissing,
        adjMonthlyTrailingReturn1y = quote.mutualFundPerformance?.adjMonthlyTrailingReturn1y ?: templateResources.valueMissing,
        adjMonthlyTrailingReturn5y = quote.mutualFundPerformance?.adjMonthlyTrailingReturn5y ?: templateResources.valueMissing,
        adjMonthlyTrailingReturn10y = quote.mutualFundPerformance?.adjMonthlyTrailingReturn10y ?: templateResources.valueMissing,

        nonAdjQuarterlyTrailingReturnYtd = quote.mutualFundPerformance?.nonAdjQuarterlyTrailingReturnYtd ?: templateResources.valueMissing,
        nonAdjQuarterlyTrailingReturn1y = quote.mutualFundPerformance?.nonAdjQuarterlyTrailingReturn1y ?: templateResources.valueMissing,
        nonAdjQuarterlyTrailingReturn5y = quote.mutualFundPerformance?.nonAdjQuarterlyTrailingReturn5y ?: templateResources.valueMissing,
        nonAdjQuarterlyTrailingReturn10y = quote.mutualFundPerformance?.nonAdjQuarterlyTrailingReturn10y ?: templateResources.valueMissing,

        adjQuarterlyTrailingReturnYtd = quote.mutualFundPerformance?.adjQuarterlyTrailingReturnYtd ?: templateResources.valueMissing,
        adjQuarterlyTrailingReturn1y = quote.mutualFundPerformance?.adjQuarterlyTrailingReturn1y ?: templateResources.valueMissing,
        adjQuarterlyTrailingReturn5y = quote.mutualFundPerformance?.adjQuarterlyTrailingReturn5y ?: templateResources.valueMissing,
        adjQuarterlyTrailingReturn10y = quote.mutualFundPerformance?.adjQuarterlyTrailingReturn10y ?: templateResources.valueMissing,

        monthlyLoadAdjTrailingReturnYtd = quote.mutualFundPerformance?.monthlyLoadAdjTrailingReturnYtd ?: templateResources.valueMissing,
        monthlyLoadAdjTrailingReturn1y = quote.mutualFundPerformance?.monthlyLoadAdjTrailingReturn1y ?: templateResources.valueMissing,
        monthlyLoadAdjTrailingReturn5y = quote.mutualFundPerformance?.monthlyLoadAdjTrailingReturn5y ?: templateResources.valueMissing,
        monthlyLoadAdjTrailingReturn10y = quote.mutualFundPerformance?.monthlyLoadAdjTrailingReturn10y ?: templateResources.valueMissing,

        quarterlyLoadAdjTrailingReturnYtd = quote.mutualFundPerformance?.quarterlyLoadAdjTrailingReturnYtd ?: templateResources.valueMissing,
        quarterlyLoadAdjTrailingReturn1y = quote.mutualFundPerformance?.quarterlyLoadAdjTrailingReturn1y ?: templateResources.valueMissing,
        quarterlyLoadAdjTrailingReturn5y = quote.mutualFundPerformance?.quarterlyLoadAdjTrailingReturn5y ?: templateResources.valueMissing,
        quarterlyLoadAdjTrailingReturn10y = quote.mutualFundPerformance?.quarterlyLoadAdjTrailingReturn10y ?: templateResources.valueMissing,

        monthlyTrailingReturnYtd = quote.mutualFundPerformance?.monthlyTrailingReturnYtd ?: templateResources.valueMissing,
        monthlyTrailingReturn1y = quote.mutualFundPerformance?.monthlyTrailingReturn1y ?: templateResources.valueMissing,
        monthlyTrailingReturn5y = quote.mutualFundPerformance?.monthlyTrailingReturn5y ?: templateResources.valueMissing,
        monthlyTrailingReturn10y = quote.mutualFundPerformance?.monthlyTrailingReturn10y ?: templateResources.valueMissing,

        quarterlyTrailingReturnYtd = quote.mutualFundPerformance?.quarterlyTrailingReturnYtd ?: templateResources.valueMissing,
        quarterlyTrailingReturn1y = quote.mutualFundPerformance?.quarterlyTrailingReturn1y ?: templateResources.valueMissing,
        quarterlyTrailingReturn5y = quote.mutualFundPerformance?.quarterlyTrailingReturn5y ?: templateResources.valueMissing,
        quarterlyTrailingReturn10y = quote.mutualFundPerformance?.quarterlyTrailingReturn10y ?: templateResources.valueMissing,

        inceptionDate = DateFormattingUtils.formatUsDateToDisplayedDate(quote.mutualFundPerformance?.inceptionDate) ?: templateResources.valueMissing,
        performanceAsOfDate = DateFormattingUtils.formatUsDateToDisplayedDate(quote.mutualFundPerformance?.performanceAsOfDate)
            ?: templateResources.valueMissing,
        quarteryPerformanceAsOfDate = DateFormattingUtils.formatUsDateToDisplayedDate(quote.mutualFundPerformance?.quarteryPerformanceAsOfDate)
            ?: templateResources.valueMissing,
        monthlyLoadAdjTrailingReturnSinceInception = MarketDataFormatter.formatTrailingReturn(
            quote.mutualFundPerformance?.monthlyLoadAdjTrailingReturnSinceInception?.safeParseBigDecimal()
        ),
        monthlyTrailingReturnSinceInception = MarketDataFormatter.formatTrailingReturn(
            quote.mutualFundPerformance?.monthlyTrailingReturnSinceInception?.safeParseBigDecimal()
        ),
        quarterlyLoadAdjTrailingReturnSinceInception = MarketDataFormatter.formatTrailingReturn(
            quote.mutualFundPerformance?.quarterlyLoadAdjTrailingReturnSinceInception?.safeParseBigDecimal()
        ),
        quarterlyTrailingReturnSinceInception = MarketDataFormatter.formatTrailingReturn(
            quote.mutualFundPerformance?.quarterlyTrailingReturnSinceInception?.safeParseBigDecimal()
        )
    )

    override fun getQuoteFundFooterItem(quote: MobileQuote) = FundQuoteItem.QuoteFundsFooterItem(
        descriptionText = String.format(
            templateResources.fundDescriptionFormat,
            quote.fundDetails?.morningStarCategory ?: templateResources.valueMissing,
            quote.fundDetails?.fundDescription ?: templateResources.fundDescriptionNotFound
        )
    )

    override fun getFeesAndExpensesItem(mutualFundDetails: MutualFundDetails?) = FundQuoteItem.FeesAndExpensesItem(
        salesCharge = mutualFundDetails?.salesCharge ?: templateResources.valueMissing,
        fundRedemptionFee = mutualFundDetails?.fundRedemptionFee ?: templateResources.valueMissing,
        etradeTransactionFee = mutualFundDetails?.etradeTransactionFee ?: templateResources.valueMissing,
        etradeEarlyRedemptionFee = mutualFundDetails?.etradeEarlyRedemptionFee ?: templateResources.valueMissing,
        additionalFeeDetails = mutualFundDetails?.additionalFeeDetails ?: Pair(null, emptyList()),
        availability = mutualFundDetails?.availability ?: templateResources.valueMissing,
        initialInvestment = MarketDataFormatter.formatMoneyValueWithCurrency(mutualFundDetails?.initialInvestment?.toBigDecimalOrNull()),
        subsequentInvestment = MarketDataFormatter.formatMoneyValueWithCurrency(mutualFundDetails?.subsequentInvestment?.toBigDecimalOrNull()),
        initialIra = MarketDataFormatter.formatMoneyValueWithCurrency(mutualFundDetails?.initialIra?.toBigDecimalOrNull()),
        subsequentIra = MarketDataFormatter.formatMoneyValueWithCurrency(mutualFundDetails?.subsequentIra?.toBigDecimalOrNull()),
        netExpenseRatio = MarketDataFormatter.formatMarketPercData(mutualFundDetails?.netExpenseRatio?.safeParseBigDecimal()),
        grossExpenseRatio = MarketDataFormatter.formatMarketPercData(mutualFundDetails?.grossExpenseRatio?.safeParseBigDecimal()),
        managementFee = MarketDataFormatter.formatMarketPercData(mutualFundDetails?.managementFee?.safeParseBigDecimal()),
        administrationFee = MarketDataFormatter.formatMarketPercData(mutualFundDetails?.administrationFee?.safeParseBigDecimal()),
        twelveB1Fee = MarketDataFormatter.formatMarketPercData(mutualFundDetails?.twelveBOneFee?.safeParseBigDecimal())
    )
}
