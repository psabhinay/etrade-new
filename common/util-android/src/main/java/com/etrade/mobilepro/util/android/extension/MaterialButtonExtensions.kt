package com.etrade.mobilepro.util.android.extension

import android.os.Handler
import androidx.core.content.ContextCompat
import com.etrade.mobilepro.util.android.R
import com.google.android.material.button.MaterialButton

fun MaterialButton.toggleCheckedState(isChecked: Boolean, handler: Handler) {
    this.isChecked = isChecked
    handler.post { // there are issues with icon drawables of buttons inside scrollview but not yet visible
        context?.let { context ->
            val backgroundColor = if (isChecked) {
                ContextCompat.getColor(context, R.color.purple)
            } else {
                ContextCompat.getColor(context, R.color.white)
            }
            val textColor = if (isChecked) {
                ContextCompat.getColor(context, R.color.white)
            } else {
                ContextCompat.getColor(context, R.color.purple)
            }
            this.background.setTint(backgroundColor)
            this.setTextColor(textColor)
            this.icon.setTint(textColor)
        }
    }
}
