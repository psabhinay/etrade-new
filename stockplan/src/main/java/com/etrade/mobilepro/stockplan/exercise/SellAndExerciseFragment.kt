package com.etrade.mobilepro.stockplan.exercise

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.di.IsTablet
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.order.details.router.OrderDetailsRouter
import com.etrade.mobilepro.stockplan.AccountIndexHandler
import com.etrade.mobilepro.stockplan.StockPlanWebViewFragment
import com.etrade.mobilepro.stockplan.StockPlanWebviewPage
import javax.inject.Inject

@RequireLogin
class SellAndExerciseFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    @Web private val baseWebUrl: String,
    @DeviceType private val deviceType: String,
    @IsTablet private val isTabletView: Boolean,
    private val accountIndexHandler: AccountIndexHandler,
    mainNavigation: MainNavigation,
    orderDetailsRouter: OrderDetailsRouter
) : StockPlanWebViewFragment(viewModelFactory, deviceType, mainNavigation, orderDetailsRouter) {

    override fun getUrl(): String {
        return accountIndexHandler.getAccountIndex()?.let {
            return StockPlanWebviewPage.getUrl(StockPlanWebviewPage.SELL, it, baseWebUrl, isTabletView)
        } ?: baseWebUrl + StockPlanWebviewPage.SELL.defaultPlanUrl
    }
}
