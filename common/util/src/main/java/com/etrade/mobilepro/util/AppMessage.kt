package com.etrade.mobilepro.util

sealed class AppMessage

class ErrorMessage(
    val message: CharSequence? = null,
    val retryAction: (() -> Unit)? = null
) : AppMessage()

class TextMessage(
    val message: CharSequence,
    val duration: Duration = Duration.INDEFINITE
) : AppMessage() {

    enum class Duration {
        INDEFINITE,
        LONG,
        SHORT
    }
}
