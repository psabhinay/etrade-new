package com.etrade.mobilepro.menu.fragment.recyclerview.delegate

import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menu.TextMenuItem
import com.etrade.mobilepro.menu.fragment.recyclerview.holder.TextMenuItemViewHolder
import com.etrade.mobilepro.util.android.extension.inflate
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class TextMenuItemDelegate(
    @LayoutRes private val layout: Int,
    private val type: TextMenuItem.Type,
    private val onMenuItemClickListener: (TextMenuItem) -> Unit
) : AbsListItemAdapterDelegate<TextMenuItem, MenuItem, TextMenuItemViewHolder>() {

    override fun isForViewType(item: MenuItem, items: MutableList<MenuItem>, position: Int): Boolean {
        return item is TextMenuItem && item.type == type
    }

    override fun onCreateViewHolder(parent: ViewGroup): TextMenuItemViewHolder {
        return TextMenuItemViewHolder(parent.inflate(layout), onMenuItemClickListener)
    }

    override fun onBindViewHolder(item: TextMenuItem, holder: TextMenuItemViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }
}
