package com.etrade.mobilepro.review

import androidx.core.app.NotificationCompat
import com.localytics.androidx.InAppCampaign
import com.localytics.androidx.InAppConfiguration
import com.localytics.androidx.MessagingListenerV2
import com.localytics.androidx.PlacesCampaign
import com.localytics.androidx.PushCampaign

private const val REQUEST_FEEDBACK = "requestFeedback"
private const val REQUEST_FEEDBACK_VALUE = "1"

class AppReviewLocalyticsMessagingListener(private val callBack: () -> Unit) : MessagingListenerV2 {

    override fun localyticsShouldShowInAppMessage(inAppCampaign: InAppCampaign): Boolean {
        return inAppCampaign.attributes[REQUEST_FEEDBACK]?.let {
            if (it == REQUEST_FEEDBACK_VALUE) {
                callBack()

                false
            } else {
                true
            }
        } ?: true
    }

    override fun localyticsWillDisplayInAppMessage(inAppCampaign: InAppCampaign, inAppConfiguration: InAppConfiguration): InAppConfiguration {
        return inAppConfiguration
    }

    override fun localyticsDidDisplayInAppMessage() {
        // np
    }

    override fun localyticsWillDismissInAppMessage() {
        // np
    }

    override fun localyticsDidDismissInAppMessage() {
        // np
    }

    override fun localyticsShouldDelaySessionStartInAppMessages(): Boolean {
        return true
    }

    override fun localyticsShouldShowPushNotification(pushCampaign: PushCampaign): Boolean {
        return true
    }

    override fun localyticsShouldShowPlacesPushNotification(placesCampaign: PlacesCampaign): Boolean {
        return true
    }

    override fun localyticsWillShowPushNotification(
        builder: NotificationCompat.Builder,
        pushCampaign: PushCampaign
    ): NotificationCompat.Builder {
        return builder
    }

    override fun localyticsWillShowPlacesPushNotification(
        builder: NotificationCompat.Builder,
        placesCampaign: PlacesCampaign
    ): NotificationCompat.Builder {
        return builder
    }
}
