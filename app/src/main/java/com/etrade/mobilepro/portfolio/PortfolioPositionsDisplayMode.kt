package com.etrade.mobilepro.portfolio

/**
 * Portfolio positions table mode.
 *
 * @param index index of a mode
 */
enum class PortfolioPositionsDisplayMode(val index: Int) {

    TABLE(0),
    LIST(1),
    TILE(2);

    companion object {

        /**
         * Gets [PortfolioPositionsDisplayMode] from [index].
         *
         * @param index index of a mode
         *
         * @return table mode
         */
        fun from(index: Int): PortfolioPositionsDisplayMode {
            return values().find { it.index == index } ?: TABLE
        }
    }
}
