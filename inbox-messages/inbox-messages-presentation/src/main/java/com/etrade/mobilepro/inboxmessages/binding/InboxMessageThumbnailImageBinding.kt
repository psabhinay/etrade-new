package com.etrade.mobilepro.inboxmessages.binding

import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.R

class InboxMessageThumbnailImageBinding(
    private val imageLoader: ImageLoader,
    private val onItemClickListener: ((InboxMessage) -> Unit)? = null
) : InboxMessageBinding {

    override val layoutRes: Int = R.layout.inbox_message_dynamic_navigation_thumbnail_image_view

    override fun bind(item: InboxMessage, view: View) {
        if (view is AppCompatImageView) {
            imageLoader.showAndSetBackground(item.thumbnailUrl, view)
        }

        view.setOnClickListener {
            onItemClickListener?.invoke(item)
        }
    }
}
