package com.etrade.mobilepro.trade.presentation.form.component.builder

import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.form.api.input.WithTradeLegsTradeFormInputId
import com.etrade.mobilepro.trade.presentation.form.component.TradeLegsInput
import com.etrade.mobilepro.util.ValueCoder

internal fun GroupBuilder.tradeLegs(inputId: WithTradeLegsTradeFormInputId, tradeLegsCoder: ValueCoder<List<TradeLeg>>) {
    custom { TradeLegsInput(inputId.tradeLegs, tradeLegsCoder) }
}
