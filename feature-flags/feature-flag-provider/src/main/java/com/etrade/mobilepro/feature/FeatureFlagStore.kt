package com.etrade.mobilepro.feature

/**
 * A [FeatureFlagProvider] that is writeable.
 */
interface FeatureFlagStore : FeatureFlagProvider {

    /**
     * Remove all features from this store
     */
    suspend fun clearFeatures()

    /**
     * Remove the feature from this store.
     */
    suspend fun removeFeature(feature: Feature)

    /**
     * Set the value of this feature.
     */
    suspend fun setFeatureEnabled(feature: Feature, enabled: Boolean)
}
