package com.etrade.mobilepro.screener.api.filter.mutualfund

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

private val customExtraResultFields = listOf(
    "PortfolioTurnover", "PctAssetsInTop10Holdings", "AvgMarketCap"
)

data class PortfolioFilter(
    val netAssetsLessThan100: Boolean = false,
    val netAssets100to500: Boolean = false,
    val netAssets500to1: Boolean = false,
    val netAssets1to2: Boolean = false,
    val netAssetsGreaterThan2: Boolean = false,

    val distributionYieldLessThan3: Boolean = false,
    val distributionYield3to5: Boolean = false,
    val distributionYieldGreaterThan5: Boolean = false
) : ScreenerFilter {
    fun isNetAssetsSelected(): Boolean = netAssetsLessThan100 || netAssets100to500 ||
        netAssets500to1 || netAssets1to2 || netAssetsGreaterThan2

    fun isYieldSelected(): Boolean = distributionYieldLessThan3 ||
        distributionYield3to5 || distributionYieldGreaterThan5

    override fun isEmpty(): Boolean = !isNetAssetsSelected() && !isYieldSelected()

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.MUTUAL_FUND

    override fun customResultFields(): List<String> = customExtraResultFields
}
