package com.etrade.mobilepro.checkdeposit.presentation

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.checkdeposit.presentation.databinding.FragmentCheckDepositLandingBinding
import com.etrade.mobilepro.checkdeposit.presentation.deposit.CheckDepositViewModel
import com.etrade.mobilepro.checkdeposit.presentation.deposit.ViewState
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.RestoreTabPositionDelegate
import com.etrade.mobilepro.common.di.CheckDepositServiceAgreement
import com.etrade.mobilepro.tracking.initTabsTracking
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject
import com.etrade.mobilepro.common.R as pool

/**
 * A simple [Fragment] subclass.
 */
@RequireLogin
class CheckDepositLandingFragment @Inject constructor(
    private val viewModelProvider: ViewModelProvider.Factory,
    @CheckDepositServiceAgreement private val checkDepositServiceAgreementUrl: String,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val restoreTabPositionDelegate: RestoreTabPositionDelegate
) : Fragment(R.layout.fragment_check_deposit_landing) {

    private val binding by viewBinding(FragmentCheckDepositLandingBinding::bind)

    private val args: CheckDepositLandingFragmentArgs by navArgs()
    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private val viewModel: CheckDepositViewModel by navGraphViewModels(R.id.check_deposit_graph) { viewModelProvider }
    private val navigationFlowViewModel: CheckDepositNavigationFlowViewModel by navGraphViewModels(R.id.check_deposit_graph)

    private var depositStateInProgress = true
    private var termsStateInProgress = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        CheckDepositFragmentPagerAdapter(this).attach(binding.tabLayout, binding.mcdViewpager)
        binding.mcdViewpager.initTabsTracking(viewModel)

        restoreTabPositionDelegate.init(
            this,
            viewPager = binding.mcdViewpager,
            rootDestinations = setOf(pool.id.menuFragment, pool.id.bankFragment)
        )

        if (savedInstanceState == null) { viewModel.grabAccounts(args.bankAccountId) }
        observeDepositViewState()
        observeTermsState()

        navigationFlowViewModel.switchToDepositActivity.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.also {
                    binding.mcdViewpager.setCurrentItem(ACTIVITY_TAB, false)
                }
            }
        )
    }

    private fun observeDepositViewState() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is ViewState.Loading -> {
                        depositStateInProgress = true
                        updateInProgress()
                    }
                    is ViewState.AccountFetchFailed -> {
                        depositStateInProgress = false
                        updateInProgress()
                        snackBarUtil.retrySnackbar(getString(R.string.error_message_general)) {
                            viewModel.grabAccounts(args.bankAccountId)
                        }
                    }
                    else -> {
                        depositStateInProgress = false
                        updateInProgress()
                    }
                }
            }
        )
    }

    private fun observeTermsState() {
        viewModel.termsSigned.observe(
            viewLifecycleOwner,
            Observer { termsSigned ->
                if (termsSigned || args.skipTermsSignedCheck) {
                    binding.mcdViewpager.visibility = View.VISIBLE
                    termsStateInProgress = false
                    updateInProgress()
                } else {
                    termsStateInProgress = true
                    updateInProgress()

                    binding.mcdViewpager.visibility = View.GONE

                    findNavController().navigate(
                        CheckDepositLandingFragmentDirections.openCheckServiceAgreement(
                            title = getString(R.string.service_agreement),
                            url = checkDepositServiceAgreementUrl
                        )
                    )
                }
            }
        )
    }

    private fun updateInProgress() {
        if (termsStateInProgress || depositStateInProgress) {
            binding.progress.show()
        } else {
            binding.progress.hide()
        }
    }
}
