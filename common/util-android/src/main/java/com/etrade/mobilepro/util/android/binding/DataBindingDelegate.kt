package com.etrade.mobilepro.util.android.binding

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

inline fun <T : ViewDataBinding> AppCompatActivity.dataBinding(crossinline factory: (LayoutInflater) -> T) =
    lazy(LazyThreadSafetyMode.NONE) {
        factory(layoutInflater).also { binding ->
            binding.lifecycleOwner = this
        }
    }

/**
 * A property delegate that initialize the ViewDataBinding and observes Fragment's viewLifecycle.
 * On the view lifecycle ON_DESTROY_EVENT, the backing property is cleared. Note that the lifecycle
 * event is called before onDestroyEvent, so any binding cleanup should occur in the [cleanup] lambda.
 */
fun <T : ViewDataBinding> Fragment.dataBinding(
    factory: (View) -> T,
    cleanup: T.() -> Unit = {}
): ReadOnlyProperty<Fragment, T> =
    object : ReadOnlyProperty<Fragment, T>, LifecycleObserver {

        // A backing property to hold our value
        @Suppress("StaticFieldLeak")
        private var binding: T? = null

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy() {
            binding?.cleanup()
            // Clear out backing property just before onDestroyView
            binding = null
            viewLifecycleOwner.lifecycle.removeObserver(this)
        }

        override fun getValue(thisRef: Fragment, property: KProperty<*>): T =
            // Return the backing property if it's set, otherwise initialize
            binding ?: factory(requireView()).also {
                if (viewLifecycleOwner.lifecycle.currentState.isAtLeast(Lifecycle.State.INITIALIZED)) {
                    it.lifecycleOwner = viewLifecycleOwner
                    binding = it
                    viewLifecycleOwner.lifecycle.addObserver(this)
                }
            }
    }

inline val ViewDataBinding.resources: Resources
    get() = root.resources
