package com.etrade.mobilepro.chart.linechart

import com.etrade.mobilepro.chart.data.GetChartLineDataParameter
import com.etrade.mobilepro.chart.data.GetChartLineDataUseCase
import com.etrade.mobilepro.chart.model.ChartLineData
import com.etrade.mobilepro.chart.util.getChartLineDataWithDisplayDto
import com.etrade.mobilepro.chartengine.ChartRepo
import com.etrade.mobilepro.chartengine.ChartRequest
import com.etrade.mobilepro.chartengine.SymbolChartData
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable
import javax.inject.Inject

class GetSymbolChartLineDataUseCase @Inject constructor(private val chartRepo: ChartRepo) : GetChartLineDataUseCase {

    override suspend fun execute(parameter: GetChartLineDataParameter): Observable<Resource<ChartLineData>> {
        val requests = parameter.indexes.map {
            ChartRequest(
                ticker = it.chartId,
                timeFrame = parameter.chartTimeFrame.timeFrame,
                isRealTime = true
            )
        }

        return chartRepo.getAggregatedCharts(requests).map {
            it.mapToChartLineDataResource(parameter)
        }
    }

    private fun createEmptyLineData(parameter: GetChartLineDataParameter): ChartLineData = ChartLineData(parameter.chartTimeFrame, true)

    private fun Resource<List<SymbolChartData>>.mapToChartLineDataResource(parameter: GetChartLineDataParameter): Resource<ChartLineData> {
        return when (this) {
            is Resource.Loading -> Resource.Loading()
            is Resource.Success -> Resource.Success(mergeSymbolChartData(parameter, data))
            is Resource.Failed -> Resource.Failed(error = error)
        }
    }

    private fun mergeSymbolChartData(parameter: GetChartLineDataParameter, chartData: List<SymbolChartData>?): ChartLineData {
        val result = createEmptyLineData(parameter)
        chartData?.forEachIndexed { index, item ->
            val marketIndex = parameter.indexes[index]
            result.addLineSet(
                getChartLineDataWithDisplayDto(
                    marketIndex.chartId,
                    marketIndex.lineColor,
                    item,
                    parameter.chartTimeFrame
                )
            )
        }

        return result
    }
}
