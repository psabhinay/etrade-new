package com.etrade.mobilepro.dynamic.form.api

sealed class TextInputType {
    object Text : TextInputType()
    object Integer : TextInputType()
    class Decimal(val maxFractionDigits: Int) : TextInputType()
    class CurrencyDecimal(val maxFractionDigits: Int) : TextInputType()
}
