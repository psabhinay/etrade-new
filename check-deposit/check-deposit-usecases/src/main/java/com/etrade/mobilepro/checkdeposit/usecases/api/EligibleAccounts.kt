package com.etrade.mobilepro.checkdeposit.usecases.api

import com.etrade.mobilepro.fundsflow.api.FundsFlowAccount

interface EligibleAccounts {
    val termsSigned: Boolean
    val eligibleAccounts: List<FundsFlowAccount>
}
