package com.etrade.mobilepro.trade.presentation

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.dynamicui.api.flip
import com.etrade.mobilepro.trade.conditionalOrderDisclosure.api.ConditionalOrderDisclosureRepo
import com.etrade.mobilepro.trade.conditionalOrderDisclosure.presentation.BaseConditionalOrderDisclosureViewModel
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import javax.inject.Inject

class TradeConditionalOrderDisclosureViewModel @Inject constructor(
    repository: ConditionalOrderDisclosureRepo,
    resources: Resources
) : BaseConditionalOrderDisclosureViewModel(repository, resources) {

    private val _resetAdvancedOrderSwitch = MutableLiveData<ConsumableLiveEvent<Boolean>>()
    private val _resetPriceType = MutableLiveData<ConsumableLiveEvent<String>>()
    private val _resetSecurityType = MutableLiveData<ConsumableLiveEvent<SecurityType>>()

    internal val resetAdvancedOrderSwitch: LiveData<ConsumableLiveEvent<Boolean>>
        get() = _resetAdvancedOrderSwitch
    internal val resetPriceType: LiveData<ConsumableLiveEvent<String>>
        get() = _resetPriceType
    internal val resetSecurityType: LiveData<ConsumableLiveEvent<SecurityType>>
        get() = _resetSecurityType

    internal fun onTrailingPriceTypeSelected(previousPriceType: String) {
        checkIfTrailingOrderAllowed { isAllowed ->
            if (isAllowed.not()) {
                _resetPriceType.value = previousPriceType.consumable()
            }
        }
    }

    internal fun onAdvancedOrderSwitchedOn() {
        checkDisclosureState { isSigned ->
            _resetAdvancedOrderSwitch.value = isSigned.flip().consumable()
        }
    }

    internal fun onConditionalOrderSelected(previous: SecurityType) {
        checkDisclosureState { isSigned ->
            val securityType: SecurityType = if (isSigned) {
                SecurityType.CONDITIONAL
            } else {
                previous
            }
            _resetSecurityType.value = securityType.consumable()
        }
    }
}
