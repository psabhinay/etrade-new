package com.etrade.mobilepro.landing.repo

import android.content.res.Resources
import androidx.annotation.StringRes
import com.etrade.mobilepro.landing.R
import com.etrade.mobilepro.landing.api.LandingPage
import com.etrade.mobilepro.landing.api.UserAccounts
import com.etrade.mobilepro.landing.api.repo.LandingPagesDataSource
import com.etrade.mobilepro.landing.api.repo.UserAccountsDataSource
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import javax.inject.Inject

class DefaultLandingPagesDataSource @Inject constructor(
    private val userAccountsDataSource: UserAccountsDataSource,
    private val resources: Resources
) : LandingPagesDataSource {
    private val lazyUserAccounts by lazy { userAccountsDataSource.getUserAccounts() }
    override fun getUserAccounts(): UserAccounts = lazyUserAccounts

    override fun getLandingPages(): List<LandingPage> {
        val pages = mutableListOf<LandingPage>()
        pages.add(createPageFrom(LandingPage.ALL_ACCOUNTS_ID))
        val brokerageAccountsSize = lazyUserAccounts.brokerageAccounts.count()
        if (brokerageAccountsSize > 1) {
            pages.add(createPageFrom(LandingPage.ALL_BROKERAGE_ID))
        }
        if (brokerageAccountsSize > 0) {
            pages.add(
                createPageFrom(
                    id = LandingPage.PORTFOLIO_ID,
                    subItems = lazyUserAccounts.brokerageAccounts.map {
                        val title = it.title
                        createPageFrom(
                            id = LandingPage.PORTFOLIO_ID,
                            uuid = it.uuid,
                            accountId = it.accountId,
                            title = it.title,
                            contentDescription = resources.accountNameContentDescription(title)
                        )
                    }.takeIf { it.isNotEmpty() }
                )
            )
        }
        pages.add(createPageFrom(LandingPage.WATCHLIST_ID))
        return pages
    }

    override fun getDefaultPage(): LandingPage = createPageFrom(LandingPage.ALL_ACCOUNTS_ID)

    @Suppress("LongMethod")
    override fun createPageFrom(
        id: String,
        uuid: String,
        accountId: String?,
        title: String?,
        contentDescription: String?,
        subItems: List<LandingPage>?
    ): LandingPage =
        when (id) {
            LandingPage.ALL_BROKERAGE_ID -> LandingPage.AllBrokerage(
                title = resources.getString(R.string.landing_page_all_brokerage_view)
            )
            LandingPage.PORTFOLIO_ID -> {
                val finalTitle = getFinalTitle(title, uuid, R.string.landing_page_portfolio)
                LandingPage.Portfolio(
                    uuid = uuid,
                    accountId = accountId,
                    title = finalTitle,
                    subItems = subItems,
                    contentDescription = contentDescription ?: finalTitle

                )
            }
            LandingPage.WATCHLIST_ID -> LandingPage.Watchlist(
                title = resources.getString(R.string.landing_page_watch_lists)
            )
            else -> {
                LandingPage.AllAccounts(
                    title = resources.getString(R.string.landing_page_complete_view_landing)
                )
            }
        }

    private fun getFinalTitle(title: String?, uuid: String, @StringRes titleRes: Int) =
        title ?: lazyUserAccounts.getTitleOfAccount(uuid) ?: resources.getString(titleRes)
}
