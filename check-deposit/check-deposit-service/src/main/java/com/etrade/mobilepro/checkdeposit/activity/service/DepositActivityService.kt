package com.etrade.mobilepro.checkdeposit.activity.service

import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface DepositActivityService {
    @JsonMoshi
    @GET("v1/accounts/deposits/check.json")
    suspend fun getDepositActivity(
        @Header("DataToken") consumerKey: String?
    ): Response<DepositActivityScreenDto>
}
