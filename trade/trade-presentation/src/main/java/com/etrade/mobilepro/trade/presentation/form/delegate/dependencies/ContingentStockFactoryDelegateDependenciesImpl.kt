package com.etrade.mobilepro.trade.presentation.form.delegate.dependencies

import android.content.res.Resources
import com.etrade.mobilepro.trade.form.api.input.ContingentStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.trade.presentation.R

internal class ContingentStockFactoryDelegateDependenciesImpl(
    inputId: ContingentStockTradeFormInputId,
    resources: Resources
) : StockFactoryDelegateDependenciesImpl<StockTradeFormInputId>(inputId, resources) {

    private val termValueId: OrderTermValueId = inputId.termValueId

    override val saveAndPreviewActionRequiredInputs: Set<String> = setOf(
        inputId.accountId,
        inputId.securityType,
        inputId.symbol,
        inputId.priceType,
        inputId.term,
        inputId.limitPrice,
        inputId.limitPriceForStopLimit,
        inputId.stopPrice,
        inputId.stopPriceForStopLimit,
        inputId.advancedOrderType,
        inputId.contingentOrderInputId.symbol,
        inputId.contingentOrderInputId.condition,
        inputId.contingentOrderInputId.qualifier,
        inputId.contingentOrderInputId.conditionValue
    )

    override val limitOrderTermLabelMap = mapOf(
        termValueId.goodUntilCancel to resources.getString(R.string.trade_term_gtc),
        termValueId.fillOrKill to resources.getString(R.string.trade_term_fok),
        termValueId.immediateOrCancel to resources.getString(R.string.trade_term_ioc)
    )
}
