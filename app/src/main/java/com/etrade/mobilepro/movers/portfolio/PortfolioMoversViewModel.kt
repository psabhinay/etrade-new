package com.etrade.mobilepro.movers.portfolio

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamicui.api.Diffable
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.markets.movers.EmptyQuotesException
import com.etrade.mobilepro.movers.MoverItem
import com.etrade.mobilepro.movers.Movers
import com.etrade.mobilepro.movers.MoversRouter
import com.etrade.mobilepro.portfoliosnapshot.PortfolioSnapshotPosition
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.quote.deriveDayChange
import com.etrade.mobilepro.quote.deriveDayChangePercent
import com.etrade.mobilepro.quote.getFormattedLastPrice
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.addLeadingZerosToDate
import com.etrade.mobilepro.util.android.CallToAction
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

private const val EMPTY_VALUE = "0"

@Suppress("LongParameterList")
class PortfolioMoversViewModel(
    private val getPortfolioMoversUseCase: GetPortfolioMoversUseCase,
    moversRouter: MoversRouter,
    streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    streamingStatusController: StreamingStatusController,
    title: String,
    callToAction: CallToAction? = null,
    itemsComparator: Comparator<MoverItem>,
    resources: Resources,
    private val userViewModel: UserViewModel,
    private val userPreferences: UserPreferences
) : Movers(
    layoutId = R.layout.layout_movers,
    title = title,
    showActionView = false,
    callToAction = callToAction,
    moversRouter = moversRouter,
    streamingQuoteProvider = streamingQuoteProvider,
    streamingStatusController = streamingStatusController,
    itemsComparator = itemsComparator,
    resources = resources,
    shouldDisplayOptionsData = true
),
    Diffable {

    private var selectedAccount = userViewModel.accountList.firstOrNull {
        it.account?.accountUuid == (userPreferences.pfMoversWidgetAccountUuid ?: userViewModel.defaultAccountUuid)
    }?.account

    override val subtitle = MutableLiveData(selectedAccount?.accountShortName)

    override val variableId: Int = BR.obj

    override val showAccountSelection = userViewModel.hasMoreThanOneBrokerageAccount

    private val _openAccountSelection: MutableLiveData<ConsumableLiveEvent<String?>> = MutableLiveData()
    val openAccountSelection: LiveData<ConsumableLiveEvent<String?>> = _openAccountSelection

    override suspend fun fetchData(): ETResult<List<MoverItem>> {
        return getPortfolioMoversUseCase.execute(GetPortfolioMoversParameter(selectedAccount?.accountId, true)).fold(
            onSuccess = {
                val result = if (it.first.isEmpty()) {
                    emptyList()
                } else {
                    onSuccess(it)
                }
                ETResult.success(result)
            },
            onFailure = {
                ETResult.failure(it)
            }
        )
    }

    // Assumes that there will be only one PortfolioMoversViewModel in a list
    override fun isSameItem(other: Any): Boolean = other.javaClass == javaClass

    override fun hasSameContents(other: Any): Boolean = false

    private fun onSuccess(response: Pair<List<MobileQuote>, List<PortfolioSnapshotPosition>>): List<MoverItem> {
        val quotes = response.first.sortedBy { it.symbol }
        val positions = response.second.sortedBy { it.symbol }
        return positions.map { position ->
            val optionUnderlier = position.optionUnderlier
            val displaySymbol = optionUnderlier?.optionSymbol ?: position.symbol
            val quote = quotes.find {
                (it.symbol == optionUnderlier?.optionSymbol || it.symbol == position.symbol) &&
                    it.typeCode.typeCode == position.securityType
            }
            createMoverItem(
                symbol = position.symbol,
                displaySymbol = displaySymbol,
                price = quote?.getFormattedLastPrice() ?: EMPTY_VALUE,
                change = quote?.deriveDayChange() ?: EMPTY_VALUE,
                decimalPercentChange = (quote?.deriveDayChangePercent() ?: EMPTY_VALUE).toBigDecimalOrNull(),
                optionExpiryDate = optionUnderlier?.expiryDate?.addLeadingZerosToDate(),
                optionsStrikePricePlusType = optionUnderlier?.let { it.strikePrice + it.optionType }
            )
        }
    }

    override fun openAccountSelection() {
        _openAccountSelection.value = ConsumableLiveEvent(selectedAccount?.accountUuid)
    }

    override fun handleGetMoversFailure(e: Throwable): Boolean {
        if (e is EmptyQuotesException) {
            updateViewState {
                postValue(DynamicUiViewState.Warning(resources.getString(R.string.portfolio_movers_no_positions)))
            }
            return true
        }
        return false
    }

    fun onAccountSelection(account: Account) {
        if (account.accountUuid != selectedAccount?.accountUuid) {
            subtitle.value = account.accountShortName
            userPreferences.pfMoversWidgetAccountUuid = account.accountUuid
            resetOnAccountSelection()
            selectedAccount = account
            getMovers()
        }
    }
}
