package com.etrade.mobilepro.etcompose.tabhorizontalpager

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Tab
import androidx.compose.material.TabRow
import androidx.compose.material.TabRowDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.FontSizes
import com.etrade.mobilepro.common.compose.theme.LightColorPalette
import com.etrade.mobilepro.common.compose.theme.etColors
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

typealias PagerContent = @Composable (StateFlow<Int>) -> Unit

/**
 * Tab with a horizontal viewpager
 * @param tabTitles the titles that we want the tabs to have
 * @param pagerDestination the Composable components that we want to display in the viewpager corresponding to the index of tabTitles.
 *                         StateFlow<Int> lamda param is a signal of the scroll finished event, its value increases every time the event happens
 * @param onTabChangedListener listener that is invoked when tab changes from one to another
 * @param onTabClickListener listener to when a tab gets clicked on, returns true if we suppose to scroll to that tab, false
 *                          to not scroll yet.
 * @param animateScroll boolean to enable animate the scrolling when switching tabs.
 */
@Composable
@Suppress("LongMethod", "LongParameterList")
fun CombinedTabHorizontalPager(
    items: List<PagerItem>,
    modifier: Modifier = Modifier,
    pagerState: ETPagerState = rememberPagerState(0),
    onTabChangedListener: ((Int) -> Unit)? = null,
    animateScroll: Boolean = true
) {
    val coroutineScope = rememberCoroutineScope()
    val scrollFinishedFlow = remember { MutableStateFlow(0) }
    val scrollListener = remember { { scrollFinishedFlow.value = scrollFinishedFlow.value.inc() } }
    Column(modifier) {
        onTabChangedListener?.invoke(pagerState.currentPage)
        TabRow(
            selectedTabIndex = pagerState.currentPage,
            indicator = @Composable { tabPositions ->
                TabRowDefaults.Indicator(
                    modifier = Modifier.etPagerTabIndicatorOffset(pagerState, tabPositions),
                    color = LightColorPalette.lightPurple,
                    height = Dimens.Size4dp
                )
            }
        ) {
            items.forEachIndexed { index, item ->
                val selected = pagerState.currentPage == index
                val fontWeight = if (selected) FontWeight.Medium else FontWeight.Normal
                Tab(
                    selected = selected,
                    modifier = Modifier.background(MaterialTheme.etColors.plum),
                    onClick = {
                        coroutineScope.launch {
                            if (animateScroll) {
                                pagerState.animateScrollToPage(index)
                            } else {
                                pagerState.scrollToPage(index)
                            }
                        }
                    },
                    text = { TabText(item, fontWeight) }
                )
            }
        }
        ETHorizontalPager(
            state = pagerState,
            count = items.size,
            verticalAlignment = Alignment.Top,
            modifier = Modifier.fillMaxSize(),
            onScrollFinished = scrollListener,
        ) { page ->
            items[page].content
                .invoke(scrollFinishedFlow)
        }
    }
}

@Composable
private fun TabText(item: PagerItem, fontWeight: FontWeight) {
    Text(
        text = item.title,
        fontWeight = fontWeight,
        fontSize = FontSizes.Size16sp,
        color = Color.White
    )
}

class PagerItem(
    val title: String,
    val content: PagerContent
)

@Preview
@Composable
private fun CombinedHoriontalPagerPreview() {
    val pagerItems = listOf(
        PagerItem("tab 1") {},
        PagerItem("tab 2") {},
        PagerItem("tab 3") {}
    )
    CombinedTabHorizontalPager(
        items = pagerItems
    )
}
