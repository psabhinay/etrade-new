package com.etrade.mobilepro.quote.screen.presentation.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.dropdown.DropDownManager
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quote.screen.presentation.R
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemMutualFundClassSectionBinding
import com.etrade.mobilepro.quoteapi.FundClassInfo
import com.etrade.mobilepro.quoteapi.FundQuoteItem

class FundClassSectionViewHolder(private val binding: QuoteItemMutualFundClassSectionBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bindTo(
        section: FundQuoteItem.FundClassSection?,
        fundClassBottomSheetSelector: DropDownManager<FundClassInfo>,
        fetchQuoteAction: ((String?, InstrumentType) -> Unit)?
    ) {
        section?.let {
            setUpClassDropdown(it, fundClassBottomSheetSelector, fetchQuoteAction)
        }
    }

    @Suppress("LongMethod")
    private fun setUpClassDropdown(
        section: FundQuoteItem.FundClassSection,
        fundClassBottomSheetSelector: DropDownManager<FundClassInfo>,
        fetchQuoteAction: ((String?, InstrumentType) -> Unit)?
    ) {
        with(binding) {
            fundClassBottomSheetSelector.init(
                "QUOTES FUND CLASS",
                section.title,
                section.fundClassInfoList,
                section.selectedFundClass?.index
            )
            if (section.fundClassInfoList.isNotEmpty() && section.selectedFundClass != null) {
                fundClassValuePicker.setOnClickListener {
                    fundClassBottomSheetSelector.openDropDown()
                }
                fundClassBottomSheetSelector.setListener { _, item ->
                    fundClassValuePicker.value = item.fundClass
                    fetchQuoteAction?.invoke(item.symbol, item.typeCode)
                }
                section.selectedFundClass?.value?.fundClass?.let {
                    fundClassValuePicker.value = it
                    fundClassRootView.contentDescription =
                        "${itemView.context.getString(R.string.quote_fund_class)} $it"
                }
            } else {
                fundClassValuePicker.hint = ""
                fundClassValuePicker.isEnabled = false
            }
        }
    }
}
