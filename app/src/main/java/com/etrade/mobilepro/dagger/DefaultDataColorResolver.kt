package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.util.color.DataColor
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.util.isNegativeNumber
import com.etrade.mobilepro.util.isPositiveNumber
import java.math.BigDecimal

class DefaultDataColorResolver constructor(
    private val streamingStatusController: StreamingStatusController
) : DataColorResolver {
    override fun resolveDataColor(value: Any?): DataColor {
        return when (value) {
            is Short -> selectValueColor(value, { it < 0 }, { it > 0 })
            is Int -> selectValueColor(value, { it < 0 }, { it > 0 })
            is Long -> selectValueColor(value, { it < 0 }, { it > 0 })
            is Float -> selectValueColor(value, { it < 0f }, { it > 0f })
            is Double -> selectValueColor(value, { it < 0.0 }, { it > 0.0 })
            is BigDecimal -> selectValueColor(value, { it < BigDecimal.ZERO }, { it > BigDecimal.ZERO })
            is String -> selectValueColor(value, { it.isNegativeNumber() }, { it.isPositiveNumber() })
            else -> DataColor.NEUTRAL
        }
    }

    override fun resolveFilledDataColor(value: Any?): DataColor {
        val color = resolveDataColor(value)

        return if (streamingStatusController.isStreamingToggleEnabled) {
            when (color) {
                DataColor.NEGATIVE -> DataColor.FILLED_NEGATIVE
                DataColor.POSITIVE -> DataColor.FILLED_POSITIVE
                else -> DataColor.NEUTRAL
            }
        } else {
            color
        }
    }

    private inline fun <T> selectValueColor(value: T, isLessThanZero: (T) -> Boolean, isGreaterThanZero: (T) -> Boolean): DataColor {
        return when {
            isLessThanZero(value) -> DataColor.NEGATIVE
            isGreaterThanZero(value) -> DataColor.POSITIVE
            else -> DataColor.NEUTRAL
        }
    }
}
