package com.etrade.mobilepro.trade.presentation.form.delegate

import android.os.Build
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.dynamic.form.api.inputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.data.form.input.DefaultStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormType
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.O_MR1])
@RunWith(AndroidJUnit4::class)
class StockTradeFormFactoryDelegateTest {

    private val tradeFormType = TradeFormType(SecurityType.STOCK)

    private val sut: TradeFormFactoryDelegate? = mockDelegates.getFormDelegate(tradeFormType)

    private val form = sut?.createForm(
        mockTradeAccounts,
        mockSelectedAccountDelegate("31459185"),
        mockParameter,
        object : TradeFormFactoryDelegate.OnFormChangeListener {
            override fun onSecurityType(securityType: SecurityType) =
                Unit

            override fun onAdvancedOrderType(
                formType: TradeFormType,
                snapshot: InputValuesSnapshot
            ) = Unit
        }
    )

    @Test
    fun `check StockTradeFormFactoryDelegate is configured for Stock trade type`() {
        assertTrue(mockDelegates.getFormDelegate(tradeFormType) is StockTradeFormFactoryDelegate)
    }

    @Test
    fun `StockTradeFormFactoryDelegate creates form successfully`() {
        assertNotNull(form)

        // Top level sections : Account, Stock form, Advanced order
        assertTrue(form?.groups?.size == VERTICAL_GROUP_SECTIONS)
    }

    @Test
    // Buttons : Preview, Save
    fun `StockTrade Form check if button state is valid`() {
        assertValidActionButtons(form?.actions)
    }

    @Test
    fun `StockTrade Form mandatory view components`() {
        val formIds = form?.inputValuesSnapshot?.keys ?: emptySet()
        DefaultStockTradeFormInputId.run {
            assertTrue(formIds.contains(accountId))
            assertTrue(formIds.contains(action))
            assertTrue(formIds.contains(priceType))
            assertTrue(formIds.contains(quantity))
            assertTrue(formIds.contains(symbol))
            assertTrue(formIds.contains(term))
            assertTrue(formIds.contains(advancedOrder))
        }
    }

    @Test
    fun `check Stock form content for injected user input`() {
        val testStockInput = with(DefaultStockTradeFormInputId) {
            mapOf(
                securityType to securityTypeValueId.stock,
                accountId to "31459185",
                symbol to "FB:EQ",
                priceType to priceTypeValueId.limit,
                limitPrice to "0.45",
                term to termValueId.goodForDay
            )
        }

        form?.resetValues()
        form?.injectValuesFrom(testStockInput)
        assertFormValues(testStockInput, form)
    }

    @Test
    fun `check tradeFormDelegates are configured correctly`() {
        assertTrue(mockDelegates.getFormDelegate(TradeFormType(SecurityType.MUTUAL_FUND)) is MutualFundTradeFormDelegate)
        assertTrue(mockDelegates.getFormDelegate(TradeFormType(SecurityType.CONDITIONAL)) is ConditionalTradeFormFactoryDelegate)
        assertTrue(
            mockDelegates.getFormDelegate(
                TradeFormType(
                    SecurityType.STOCK,
                    AdvancedTradeType.Contingent
                )
            ) is ContingentStockTradeFormFactoryDelegate
        )
        assertTrue(
            mockDelegates.getFormDelegate(
                TradeFormType(
                    SecurityType.OPTION,
                    AdvancedTradeType.Contingent
                )
            ) is ContingentOptionTradeFormFactoryDelegate
        )
        assertTrue(
            mockDelegates.getFormDelegate(
                TradeFormType(
                    SecurityType.OPTION,
                    AdvancedTradeType.StopLoss
                )
            ) is StopLossOptionTradeFormFactoryDelegate
        )
    }
}
