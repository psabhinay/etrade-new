package com.etrade.mobilepro.trade.presentation

import android.content.Intent
import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.navigation.NavController
import com.etrade.mobilepro.common.R
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.viewmodel.AppDialogViewModel
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.api.inputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.viewmodel.CreateFormUseCase
import com.etrade.mobilepro.dynamic.form.viewmodel.FormViewModel
import com.etrade.mobilepro.orders.api.TradeOrderType
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.tooltip.AppTooltip
import com.etrade.mobilepro.tooltip.viewmodel.AppTooltipViewModel
import com.etrade.mobilepro.trade.presentation.form.TradeFormAction
import com.etrade.mobilepro.trade.presentation.form.delegate.InboxMessagesDelegate
import com.etrade.mobilepro.trade.presentation.form.util.injectSymbol
import com.etrade.mobilepro.trade.presentation.form.util.parseSymbol
import com.etrade.mobilepro.trade.presentation.usecase.TradeFormParameter
import com.etrade.mobilepro.trade.presentation.util.SharedTradeViewModel
import com.etrade.mobilepro.trade.router.TradeRouter
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.safeLet
import com.etrade.mobilepro.viewmodel.NavigationSignal
import com.etrade.mobilepro.viewmodel.NavigationSignalViewModel
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import org.chromium.customtabsclient.shared.internalUrlToUri

abstract class BaseTradeFormViewModel(
    createFormUseCase: CreateFormUseCase,
    resources: Resources,
    val inboxMessagesDelegate: InboxMessagesDelegate,
    protected val tradeRouter: TradeRouter,
    private val baseUrl: String
) : FormViewModel(resources, createFormUseCase), AppDialogViewModel, AppTooltipViewModel, NavigationSignalViewModel {

    protected val mutableDialog = MutableLiveData<AppDialog?>()
    final override val dialog: LiveData<ConsumableLiveEvent<AppDialog?>?> =
        mutableDialog.map { ConsumableLiveEvent(it) }
    final override val navigationSignal: LiveData<ConsumableLiveEvent<NavigationSignal>>
        get() = _navigationSignal
    final override val tooltip: LiveData<ConsumableLiveEvent<AppTooltip>>
        get() = _tooltip

    val symbolLookupSignal: LiveData<ConsumableLiveEvent<Intent>>
        get() = _symbolLookupSignal

    var allowConsumeFormInputValues: Boolean = true
    var tradeOrderType: TradeOrderType = TradeOrderType.NEW

    internal lateinit var advancedOrderBidAskViewModel: QuotesWidgetViewModel
    internal lateinit var advancedQuoteWidgetViewModel: QuotesWidgetViewModel
    internal lateinit var disclosureViewModel: TradeConditionalOrderDisclosureViewModel
    internal lateinit var sharedTradeViewModel: SharedTradeViewModel
    internal lateinit var quoteWidgetViewModel: QuotesWidgetViewModel
    internal lateinit var walkthroughViewModel: WalkthroughStatesViewModel

    private val _tooltip: MutableLiveData<ConsumableLiveEvent<AppTooltip>> = MutableLiveData()

    private val _navigationSignal: MutableLiveData<ConsumableLiveEvent<NavigationSignal>> = MutableLiveData()
    private val _symbolLookupSignal: MutableLiveData<ConsumableLiveEvent<Intent>> = MutableLiveData()

    private val _symbol: MutableLiveData<String?> = MutableLiveData()

    var pendingSymbolInputId: String? = null
        private set

    private var pendingSymbol: SearchResultItem.Symbol? = null

    init {
        form.observeForever {
            injectPendingValues(it)
            _symbol.value = it.inputValuesSnapshot.parseSymbol()
        }
        inboxMessagesDelegate.observeFormSymbolChanges(_symbol)
    }

    fun onSymbolSelectedForOptionsType() {
        form.value?.inputValuesSnapshot?.let {
            // Symbol value is observed by inboxMessagesDelegate to display trade halt banner.
            _symbol.value = it.parseSymbol()
        }
    }

    final override fun onActionClick(actionId: Any) {
        val action = actionId as? TradeFormAction ?: return

        when (action) {
            TradeFormAction.Calculator -> navigateTo { tradeRouter.openCalculator(it) }
            is TradeFormAction.Dialog -> mutableDialog.value = action.dialog
            is TradeFormAction.DisplayPriceTypeDescriptions -> navigateTo { tradeRouter.openPriceTypeDescriptions(it) }
            is TradeFormAction.StockExchangeKeys -> navigateTo { it.navigateToUri(action.title) }
            is TradeFormAction.SymbolLookup -> {
                pendingSymbolInputId = action.symbolInputId
                _symbolLookupSignal.value = action.intent.consumable()
            }
            is TradeFormAction.Tooltip -> { _tooltip.value = action.tooltip.consumable() }
            else -> handleAction(action)
        }
    }

    override fun addDefaultValuesIfNeeded(form: Form, initialValues: InputValuesSnapshot): InputValuesSnapshot = initialValues.let { snapshot ->
        pendingSymbolInputId?.updateMapWithSymbol(snapshot) ?: snapshot
    }

    private fun String.updateMapWithSymbol(snapshot: InputValuesSnapshot): InputValuesSnapshot {
        val mutableMap = snapshot.toMutableMap()
        mutableMap.putIfAbsent(this, pendingSymbol)
        return mutableMap.toMap()
    }

    abstract override fun createParameter(
        initialValues: InputValuesSnapshot,
        defaultValues: InputValuesSnapshot,
        disableIfNoChange: Boolean,
        onActionClickListener: OnActionClickListener
    ): TradeFormParameter

    fun injectSymbol(symbol: SearchResultItem.Symbol?, pendingSymbolId: String?) {
        if (symbol == null) {
            return
        }

        if (isFormLoading) {
            pendingSymbol = symbol
            pendingSymbolInputId = pendingSymbolId
        } else {
            (pendingSymbolInputId ?: pendingSymbolId)?.let {
                form.value?.injectSymbol(it, symbol)
                pendingSymbolInputId = null
                _symbol.value = symbol.underlyerSymbol
            }
        }
    }

    protected open fun handleAction(action: TradeFormAction) = Unit

    protected fun navigateTo(block: (NavController) -> Unit) {
        _navigationSignal.value = ConsumableLiveEvent(object : NavigationSignal {
            override fun invoke(navController: NavController) {
                block(navController)
            }
        })
    }

    private fun injectPendingValues(form: Form) {
        safeLet(pendingSymbol, pendingSymbolInputId) { symbol, inputId ->
            form.injectSymbol(inputId, symbol)
            pendingSymbolInputId = null
            pendingSymbol = null
        }
        form.injectValuesFrom(addDefaultValuesIfNeeded(form, form.inputValuesSnapshot))
    }

    private fun NavController.navigateToUri(title: String) {
        navigate(
            internalUrlToUri(
                title = title,
                url = "$baseUrl/e/t/estation/help?id=1307044130",
                upIconResId = R.drawable.ic_arrow_back
            )
        )
    }
}
