package com.etrade.mobilepro.dynamic.form.fragment

import com.squareup.moshi.Moshi
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class InputValuesTest {

    @Test
    fun `check form values argument create and restore`() {
        val moshi = Moshi.Builder().build()
        val expected = mapOf(
            "id1" to true,
            "id3" to 1.2345,
            "id4" to "value",
            "id5" to listOf(
                "item1",
                "item2",
                "item3"
            ),
            "id6" to mapOf(
                "id7" to false,
                "id8" to "item",
                "id9" to 0.9876
            )
        )
        val actual = restoreInputValuesSnapshot(moshi, createFormValuesArgument(moshi, expected))
        assertFalse(expected === actual)
        assertEquals(expected, actual)
    }
}
