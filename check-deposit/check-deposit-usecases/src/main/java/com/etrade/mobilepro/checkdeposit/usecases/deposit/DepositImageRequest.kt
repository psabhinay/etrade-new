package com.etrade.mobilepro.checkdeposit.usecases.deposit

data class DepositImageRequest(
    val amount: String,
    val accountNo: String,
    val instNo: String,
    val workflowId: String = "",
    val frontImageToken: String? = null,
    val frontImage: String? = null,
    val rearImageToken: String? = null,
    val rearImage: String? = null,
    val iraDepositType: String?
)
