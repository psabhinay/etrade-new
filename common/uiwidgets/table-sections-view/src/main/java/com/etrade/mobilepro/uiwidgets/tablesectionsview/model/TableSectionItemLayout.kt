package com.etrade.mobilepro.uiwidgets.tablesectionsview.model

import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import java.math.BigDecimal

interface TableSectionItemLayout {
    fun getRowView(linearParams: LinearLayout.LayoutParams, parent: ViewGroup): View
}

fun onValid(value: String?, onTrue: () -> Unit) {
    if (!(value.isNullOrBlank() || value.replace(",", "").toBigDecimal().compareTo(BigDecimal.ZERO) == 0)) {
        onTrue()
    }
}
