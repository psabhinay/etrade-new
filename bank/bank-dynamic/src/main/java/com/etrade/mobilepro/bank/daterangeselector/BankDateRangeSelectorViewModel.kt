package com.etrade.mobilepro.bank.daterangeselector

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.bank.BR
import com.etrade.mobilepro.bank.BankHomeFragmentDirections
import com.etrade.mobilepro.bank.R
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.util.formatters.createAmericanSimpleDateFormatter
import org.threeten.bp.LocalDate

class BankDateRangeSelectorViewModel(
    val resources: Resources,
    private val startDate: LiveData<LocalDate>,
    private val endDate: LiveData<LocalDate>
) : DynamicViewModel(R.layout.bank_date_range_selector) {

    override val variableId: Int = BR.obj

    val formattedStartDate: LiveData<String> = startDate.map { it.format(createAmericanSimpleDateFormatter()) }
    val formattedEndDate: LiveData<String> = endDate.map { it.format(createAmericanSimpleDateFormatter()) }

    val startDateContentDescription: LiveData<String> = formattedStartDate.map {
        resources.getString(R.string.accessibility_label_from, it)
    }

    val endDateContentDescription: LiveData<String> = formattedEndDate.map {
        resources.getString(R.string.accessibility_label_to, it)
    }

    fun onStartDateClicked() {
        clickEvents.value = CtaEvent.NavDirectionsNavigation(
            BankHomeFragmentDirections.openDatePicker(
                requireNotNull(startDate.value),
                requireNotNull(endDate.value),
                true
            )
        )
    }

    fun onEndDateClicked() {
        clickEvents.value = CtaEvent.NavDirectionsNavigation(
            BankHomeFragmentDirections.openDatePicker(
                requireNotNull(startDate.value),
                requireNotNull(endDate.value),
                false
            )
        )
    }
}
