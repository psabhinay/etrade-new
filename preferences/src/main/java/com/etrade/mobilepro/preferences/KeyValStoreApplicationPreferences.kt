package com.etrade.mobilepro.preferences

import androidx.lifecycle.MutableLiveData
import com.etrade.eo.userprefrences.api.KeyValueStorage
import kotlin.properties.Delegates
import kotlin.properties.ReadOnlyProperty

private const val EULA_PART_DISPLAY_ACCEPTANCE = "displayEulaAcceptance"
private const val EULA_PART_VERSION_NO = "2.0"

const val KEY_DISPLAY_EULA = EULA_PART_DISPLAY_ACCEPTANCE + EULA_PART_VERSION_NO

private const val KEY_ADVANCED_ORDER_DISCLOSURE = "advancedOrderDisclosure"
private const val KEY_API_URL = "apiUrl"
private const val KEY_APP_ENVIRONMENT = "appServerEnv"
private const val KEY_APP_VERSION = "appVersion"
private const val KEY_BARCODE_TERMS = "barcodeTerms"
private const val KEY_CONDITIONAL_DISCLOSURE = "conditionalDisclosure"
private const val KEY_CONNECTION_TIMEOUT_SEC = "connectionTimeoutSec"
private const val KEY_DEVICE_ID = "deviceId"
private const val KEY_EXPRESS_URL = "expressUrl"
private const val KEY_LAST_LOGIN_MAJOR_MINOR_VERSION = "lastLoginMajorMinorVersion"
private const val KEY_LAST_VISITED_ACCOUNTS_TAB_ACCOUNT_ID = "lastVisitedAccountsTabAccountId"
private const val KEY_LAST_VISITED_ACCOUNTS_TAB_INDEX = "lastVisitedAccountsTabIndex"
private const val KEY_WATCHLIST_SELECTED_INDEX = "watchlistSelectedIndex"
private const val KEY_LEVEL1_STREAMER_URL = "level1StreamerUrl"
private const val KEY_LEVEL2_STREAMER_URL = "level2StreamerUrl"
private const val KEY_LIMIT_SESSION_TO_30_SEC = "limitSessionTo30Sec"
private const val KEY_HOME_LANDING_COUNT = "homeLandingCount"
private const val KEY_MARKET_CIRCUIT_BREAKER_ENABLED = "marketCircuitBreakerOn"
private const val KEY_MARKET_ON_DEMAND_URL = "marketOnDemandUrl"
private const val KEY_PREFIX_MENU_ITEM_CLICK_COUNTS = "menuItemClickCounts"
private const val KEY_MOBILE_TRADE_URL = "mobileTradeUrl"
private const val KEY_MOBILE_ETRADE_URL = "mobileETradeUrl"
private const val KEY_LABEL_PILL_MENU_PAGE_SHOWING_COUNT = "labelPillMenuPageShowingCount"
private const val KEY_READ_TIMEOUT_SEC = "readTimeoutSec"
private const val KEY_SHOULD_DISPLAY_ACCOUNT_TAB_HOST_TOOLTIP = "shouldDisplayAccountTabHostTooltip"
private const val KEY_SHOULD_DISPLAY_ACCOUNTS_ORGANIZE_BOARDING = "shouldShowAccountsOrganizeBoarding"
private const val KEY_SHOULD_DISPLAY_MARKETS_TOOLTIP = "shouldDisplayMarketsTooltip"
private const val KEY_SHOULD_DISPLAY_MENU_TOOLTIP = "shouldDisplayMenuTooltip"
private const val KEY_SHOULD_DISPLAY_COMPLETE_VIEW_TOOLTIP = "shouldDisplayCompleteViewTooltip"
private const val KEY_SHOULD_DISPLAY_ONBOARDING = "shouldDisplayOnboarding"
private const val KEY_LOCALYTICS_ONBOARDING_SHOW = "isLocalyticsOnboardingShow"
private const val KEY_SHOULD_DISPLAY_WATCHLIST_TOOLTIP = "shouldDisplayWatchListTooltip"
private const val KEY_SHOULD_SHOW_LANDING_SELECTION = "shouldShowLandingSelection"
private const val KEY_SHOULD_SYNC_CHART_IQ_DATA = "shouldSyncChartIqData"
private const val KEY_SHOULD_DISPLAY_QUOTE_LOOKUP_ON_COMPANY_NEWS_TAB =
    "shouldDisplayQuoteLookupOnCompanyNewsTab"
private const val KEY_VERSION_LOGIN_COUNT = "versionLoginCount"
private const val KEY_WATCH_LIST_SORT_STATE = "watchlistSortState"
private const val KEY_WEB_URL = "webUrl"
private const val KEY_E_DOC_WEB_URL = "eDocWebUrl"
private const val KEY_WRITE_TIMEOUT_SEC = "writeTimeoutSec"
private const val KEY_ONBOARDING_AB = "onboardingABvalue"
private const val KEY_SHOULD_TRACK_WITH_APPSFLYER = "shouldTrackWithAppsFlyer"
private const val KEY_SHOULD_DISPLAY_PORTFOLIO_CONTROL_BAR_TOOLTIP = "shouldDisplayPortfolioControlBarTooltip"
private const val KEY_SHOULD_DISPLAY_PORTFOLIO_SETTINGS_TOOLTIP = "shouldDisplayPortfolioSettingsTooltip"
private const val KEY_SHOULD_DISPLAY_PORTFOLIO_MORE_INFO_TOOLTIP = "shouldDisplayPortfolioMoreInfoTooltip"

const val KEY_SHOULD_DISPLAY_PORTFOLIO_TOOLTIP = "shouldDisplayPortfolioTooltip"

class KeyValStoreApplicationPreferences(
    private val keyValueStorage: KeyValueStorage,
    private val androidIdProvider: () -> String
) : ApplicationPreferences {

    override var appVersion: Int by keyValueStorage.preference(KEY_APP_VERSION)

    override var isBarcodeTermsAgreed: Boolean by keyValueStorage.preference(KEY_BARCODE_TERMS)

    override val deviceId: String
        get() {
            if (keyValueStorage.getStringValue(KEY_DEVICE_ID)?.isEmpty() == true) {
                keyValueStorage.putStringValue(KEY_DEVICE_ID, androidIdProvider.invoke())
            }
            return keyValueStorage.getStringValue(KEY_DEVICE_ID, "") ?: ""
        }

    override var shouldSyncChartIqData: Boolean by keyValueStorage.preference(KEY_SHOULD_SYNC_CHART_IQ_DATA, true)

    override var shouldDisplayOnboarding: Boolean by keyValueStorage.preference(KEY_SHOULD_DISPLAY_ONBOARDING, true)

    override var isLocalyticsOnboardingShow: Boolean by keyValueStorage.preference(KEY_LOCALYTICS_ONBOARDING_SHOW, false)

    override val shouldDisplayAccountTabHostTooltip: Boolean by keyValueStorage.tooltipPreference(KEY_SHOULD_DISPLAY_ACCOUNT_TAB_HOST_TOOLTIP)

    override val shouldDisplayMarketsTooltip: Boolean by keyValueStorage.tooltipPreference(KEY_SHOULD_DISPLAY_MARKETS_TOOLTIP)

    override var shouldDisplayWatchListTooltip: Boolean by keyValueStorage.preference(KEY_SHOULD_DISPLAY_WATCHLIST_TOOLTIP, true)

    override val shouldDisplayMenuTooltip: Boolean by keyValueStorage.tooltipPreference(KEY_SHOULD_DISPLAY_MENU_TOOLTIP)

    override val shouldDisplayCompleteViewTooltip: Boolean by keyValueStorage.tooltipPreference(KEY_SHOULD_DISPLAY_COMPLETE_VIEW_TOOLTIP)

    override var shouldDisplayEULA: Boolean by keyValueStorage.preference(KEY_DISPLAY_EULA, true)

    override var shouldDisplayQuoteLookupOnCompanyNewsTab: Boolean by keyValueStorage.preference(KEY_SHOULD_DISPLAY_QUOTE_LOOKUP_ON_COMPANY_NEWS_TAB, true)

    override var shouldShowAccountsOrganizeBoarding: Boolean by Delegates.observable(
        keyValueStorage.getBooleanValue(
            KEY_SHOULD_DISPLAY_ACCOUNTS_ORGANIZE_BOARDING,
            true
        )
    ) { _, _, newValue ->
        keyValueStorage.putBooleanValue(KEY_SHOULD_DISPLAY_ACCOUNTS_ORGANIZE_BOARDING, newValue)

        displayPortfolioTooltip.value = !newValue
    }

    override val displayPortfolioTooltip: MutableLiveData<Boolean> = object : MutableLiveData<Boolean>(!shouldShowAccountsOrganizeBoarding) {
        override fun getValue(): Boolean {
            return (super.getValue() ?: true && keyValueStorage.getBooleanValue(KEY_SHOULD_DISPLAY_PORTFOLIO_TOOLTIP, true))
        }

        override fun setValue(value: Boolean?) {
            keyValueStorage.putBooleanValue(KEY_SHOULD_DISPLAY_PORTFOLIO_TOOLTIP, value == true)
            super.setValue(value)
        }
    }

    override var displayPortfolioControlBarToolTip: Boolean by keyValueStorage.preference(
        KEY_SHOULD_DISPLAY_PORTFOLIO_CONTROL_BAR_TOOLTIP, default = true
    )

    override var displayPortfolioSettingsToolTip: Boolean by keyValueStorage.preference(
        KEY_SHOULD_DISPLAY_PORTFOLIO_SETTINGS_TOOLTIP, default = true
    )

    override var displayPortfolioMoreInfoToolTip: Boolean by keyValueStorage.preference(
        KEY_SHOULD_DISPLAY_PORTFOLIO_MORE_INFO_TOOLTIP, default = true
    )

    override var homeLandingCount: Int by keyValueStorage.preference(KEY_HOME_LANDING_COUNT, 0)

    override var shouldShowLandingSelection: Boolean by keyValueStorage.preference(KEY_SHOULD_SHOW_LANDING_SELECTION, true)

    override var appEnvironment: String by keyValueStorage.preference(KEY_APP_ENVIRONMENT, "")

    override var connectionTimeoutSec: Int by keyValueStorage.preference(KEY_CONNECTION_TIMEOUT_SEC, 0)

    override var readTimeoutSec: Int by keyValueStorage.preference(KEY_READ_TIMEOUT_SEC, 0)

    override var writeTimeoutSec: Int by keyValueStorage.preference(KEY_WRITE_TIMEOUT_SEC, 0)

    override var limitSessionTo30Sec: Boolean by keyValueStorage.preference(KEY_LIMIT_SESSION_TO_30_SEC, false)

    override var isCircuitBreakEnabled: Boolean by keyValueStorage.preference(KEY_MARKET_CIRCUIT_BREAKER_ENABLED, false)

    override var isAdvancedOrderDisclosureSigned: Boolean by keyValueStorage.preference(KEY_ADVANCED_ORDER_DISCLOSURE, false)

    override var isConditionalOrderDisclosureSigned: Boolean by keyValueStorage.preference(KEY_CONDITIONAL_DISCLOSURE, false)

    override var lastVisitedAccountsTabAccountId: String by keyValueStorage.preference(KEY_LAST_VISITED_ACCOUNTS_TAB_ACCOUNT_ID)

    override var lastVisitedAccountsTabIndex: Int by keyValueStorage.preference(KEY_LAST_VISITED_ACCOUNTS_TAB_INDEX)

    override var watchlistSelectedIndex: Int by keyValueStorage.preference(KEY_WATCHLIST_SELECTED_INDEX, 0)

    override var webUrl: String by keyValueStorage.preference(KEY_WEB_URL)

    override var eDocWebUrl: String by keyValueStorage.preference(KEY_E_DOC_WEB_URL)

    override var expressUrl: String by keyValueStorage.preference(KEY_EXPRESS_URL)

    override var mobileTradeUrl: String by keyValueStorage.preference(KEY_MOBILE_TRADE_URL)

    override var mobileETradeUrl: String by keyValueStorage.preference(KEY_MOBILE_ETRADE_URL)

    override var marketOnDemandUrl: String by keyValueStorage.preference(KEY_MARKET_ON_DEMAND_URL)

    override var apiUrl: String by keyValueStorage.preference(KEY_API_URL)

    override var level1StreamerUrl: String by keyValueStorage.preference(KEY_LEVEL1_STREAMER_URL)

    override var level2StreamerUrl: String by keyValueStorage.preference(KEY_LEVEL2_STREAMER_URL)

    override val onboardingABValue: Int
        get() {
            if (keyValueStorage.getIntValue(KEY_ONBOARDING_AB, defaultValue = -1) == -1) {
                keyValueStorage.putIntValue(KEY_ONBOARDING_AB, getOnboardingAbValue())
            }
            return keyValueStorage.getIntValue(KEY_ONBOARDING_AB)
        }

    override var shouldTrackWithAppsFlyer: Boolean by keyValueStorage.preference(KEY_SHOULD_TRACK_WITH_APPSFLYER, true)

    override val menuItemClickCounts = object : MenuItemClickCountsPreference {
        override fun get(menuItemKey: String): Int {
            return keyValueStorage.getIntValue("$KEY_PREFIX_MENU_ITEM_CLICK_COUNTS/$menuItemKey", 0)
        }

        override fun set(menuItemKey: String, count: Int) {
            keyValueStorage.putIntValue("$KEY_PREFIX_MENU_ITEM_CLICK_COUNTS/$menuItemKey", count)
        }
    }

    override var lastLoginMajorMinorVersion: String
        by keyValueStorage.preference(KEY_LAST_LOGIN_MAJOR_MINOR_VERSION)

    override var versionLoginCount: Int by keyValueStorage.preference(KEY_VERSION_LOGIN_COUNT)

    override var watchlistSortState: String by keyValueStorage.preference(KEY_WATCH_LIST_SORT_STATE)
}

private fun KeyValueStorage.tooltipPreference(key: String): ReadOnlyProperty<Any, Boolean> =
    ReadOnlyProperty { _, _ ->
        val result = getBooleanValue(key, true)

        if (result) {
            putBooleanValue(key, false)
        }

        result
    }
