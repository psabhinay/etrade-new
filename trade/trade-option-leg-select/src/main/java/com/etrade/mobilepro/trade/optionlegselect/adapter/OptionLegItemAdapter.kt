package com.etrade.mobilepro.trade.optionlegselect.adapter

import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.trade.optionlegselect.adapter.viewholder.DeletableOptionLegItemViewHolder
import com.etrade.mobilepro.trade.optionlegselect.adapter.viewholder.OnOptionLegItemDeleteInteractionListener
import com.etrade.mobilepro.trade.optionlegselect.adapter.viewholder.OnOptionLegItemDeleteListener
import com.etrade.mobilepro.trade.optionlegselect.adapter.viewholder.OnOptionLegItemDeletePendingListener
import com.etrade.mobilepro.trade.optionlegselect.adapter.viewholder.OnOptionLegItemQuantityChangedListener
import com.etrade.mobilepro.trade.optionlegselect.adapter.viewholder.OptionLegItemViewHolder
import com.etrade.mobilepro.trade.optionlegselect.databinding.TradeItemOptionLegDeletableBinding
import com.etrade.mobilepro.util.android.extension.viewBinding

internal class OptionLegItemAdapter(
    private val lifecycleOwner: LifecycleOwner,
    private val quantityChangedListener: OnOptionLegItemQuantityChangedListener,
    private val itemDeleteListener: OnOptionLegItemDeleteListener,
    private val itemDeleteInteractionListener: OnOptionLegItemDeleteInteractionListener,
    private val itemDeletePendingListener: OnOptionLegItemDeletePendingListener
) : BaseOptionLegItemAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionLegItemViewHolder {
        return DeletableOptionLegItemViewHolder(
            binding = parent.viewBinding(TradeItemOptionLegDeletableBinding::inflate),
            lifecycleOwner = lifecycleOwner,
            getItemAtPosition = {
                if (it < underlyingItems.size) {
                    underlyingItems[it]
                } else {
                    null
                }
            },
            quantityChangedListener = quantityChangedListener,
            itemDeleteListener = itemDeleteListener,
            itemDeleteInteractionListener = itemDeleteInteractionListener,
            itemDeletePendingListener = itemDeletePendingListener
        )
    }
}
