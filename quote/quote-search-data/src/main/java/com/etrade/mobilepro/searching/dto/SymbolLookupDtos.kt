package com.etrade.mobilepro.searching.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(strict = false, name = "SymbolLookup")
data class SymbolLookupDto(
    @field:Element(name = "Symbol")
    var symbol: String? = null,
    @field:Element(name = "Display_Symbol")
    var symbolDisplay: String? = null,
    @field:Element(name = "ProductType")
    var productType: String? = null,
    @field:Element(name = "settlementSessionCd")
    var settlementSessionCd: Int? = null,
    @field:Element(name = "Strike")
    var strike: String? = null,
    @field:Element(name = "Type")
    var type: String? = null,
    @field:Element(name = "AF")
    var AF: String? = null,
    @field:Element(name = "Expire")
    var expire: DataStructDto? = null
)

@Root(strict = false, name = "Expire")
data class DataStructDto(
    @field:Element(name = "Day")
    var day: String? = null,
    @field:Element(name = "Month")
    var month: String? = null,
    @field:Element(name = "Year")
    var year: String? = null
)
