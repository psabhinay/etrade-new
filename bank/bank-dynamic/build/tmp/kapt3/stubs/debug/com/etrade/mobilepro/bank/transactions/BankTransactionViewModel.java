package com.etrade.mobilepro.bank.transactions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u000f\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u0004\u0018\u00010\u0006X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\b\u000e\u0010\bR\u0012\u0010\u000f\u001a\u00020\u0006X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\bR\u0012\u0010\u0011\u001a\u00020\u0006X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\b\u0012\u0010\bR\u0012\u0010\u0013\u001a\u00020\u0006X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\b\u0014\u0010\bR\u000e\u0010\u0003\u001a\u00020\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0015\u001a\u00020\u0006X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\b\u0016\u0010\bR\u0014\u0010\u0017\u001a\u00020\nX\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\f\u00a8\u0006\u0019"}, d2 = {"Lcom/etrade/mobilepro/bank/transactions/BankTransactionViewModel;", "Lcom/etrade/mobilepro/dynamicui/api/DynamicViewModel;", "Lcom/etrade/mobilepro/transactions/Transaction;", "transaction", "(Lcom/etrade/mobilepro/transactions/Transaction;)V", "amount", "", "getAmount", "()Ljava/lang/String;", "amountColorResource", "", "getAmountColorResource", "()I", "amountContentDescription", "getAmountContentDescription", "date", "getDate", "description", "getDescription", "formattedAmount", "getFormattedAmount", "type", "getType", "variableId", "getVariableId", "bank-dynamic_debug"})
public final class BankTransactionViewModel extends com.etrade.mobilepro.dynamicui.api.DynamicViewModel implements com.etrade.mobilepro.transactions.Transaction {
    private final com.etrade.mobilepro.transactions.Transaction transaction = null;
    private final int variableId = 0;
    
    public BankTransactionViewModel(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.transactions.Transaction transaction) {
        super(0);
    }
    
    @java.lang.Override()
    public int getVariableId() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.String getAmount() {
        return null;
    }
    
    @java.lang.Override()
    public int getAmountColorResource() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.String getAmountContentDescription() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getDate() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getDescription() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getFormattedAmount() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getType() {
        return null;
    }
}