package com.etrade.mobilepro.news.detail.helper

import android.app.Application
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.presentation.detail.helper.DefaultTextNewsContentLoader
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsContentLoader
import com.etrade.mobilepro.util.NetworkState
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode

@RunWith(AndroidJUnit4::class)
@Suppress("DEPRECATION")
@LooperMode(value = LooperMode.Mode.LEGACY)
@Config(application = Application::class)
class DefaultTextNewsContentLoaderTest {

    @Test
    fun `check content loading by id`() {
        checkLoading(
            NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"),
            listOf(NetworkState.Loading, NetworkState.Success),
            listOf(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
        ) {
            load("docId")
        }
    }

    @Test
    fun `check content loading by content`() {
        checkLoading(
            emptyList(),
            listOf(NetworkState.Success),
            listOf(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
        ) {
            load(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
        }
    }

    @Test
    fun `check loading of empty content`() {
        checkLoading(
            emptyList(),
            listOf(NetworkState.Success),
            listOf(NewsItem.Empty)
        ) {
            loadEmpty()
        }
    }

    @Test
    fun `check retry when loading by id fails`() {
        val expectedException = RuntimeException()
        val loader = DefaultTextNewsContentLoader(
            mock {
                on { textContent("docId") }
                    .thenReturn(Observable.error(expectedException))
                    .thenReturn(Observable.just(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId")))
            }
        )

        checkLoader(
            loader,
            listOf(NetworkState.Loading, NetworkState.Failed(expectedException)),
            listOf(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
        ) {
            load("docId")
            load("docId")
        }
    }

    @Test
    fun `check loading by id after it was already loaded by id`() {
        checkLoading(
            NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"),
            listOf(NetworkState.Loading, NetworkState.Success),
            listOf(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
        ) {
            load("docId")
            load("docId")
        }
    }

    @Test
    fun `check loading by id after it was already loaded by content`() {
        checkLoading(
            NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"),
            listOf(NetworkState.Success),
            listOf(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
        ) {
            load(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
            load("docId")
        }
    }

    @Test
    fun `check loading by id after another was already loaded by id`() {
        checkLoading(
            listOf(
                NewsItem.Text("newsDateTimeSource1", "title1", "url1", emptyList(), "content1", "docId1"),
                NewsItem.Text("newsDateTimeSource2", "title2", "url2", emptyList(), "content2", "docId2")
            ),
            listOf(NetworkState.Loading, NetworkState.Success, NetworkState.Loading, NetworkState.Success),
            listOf(
                NewsItem.Text("newsDateTimeSource1", "title1", "url1", emptyList(), "content1", "docId1"),
                NewsItem.Text("newsDateTimeSource2", "title2", "url2", emptyList(), "content2", "docId2")
            )
        ) {
            load("docId1")
            load("docId2")
        }
    }

    @Test
    fun `check loading by id after another was already loaded by content`() {
        checkLoading(
            NewsItem.Text("newsDateTimeSource2", "title2", "url2", emptyList(), "content2", "docId2"),
            listOf(NetworkState.Success, NetworkState.Loading, NetworkState.Success),
            listOf(NewsItem.Text("newsDateTimeSource1", "title1", "url1", emptyList(), "content1", "docId1"))
        ) {
            load(NewsItem.Text("newsDateTimeSource1", "title1", "url1", emptyList(), "content1", "docId1"))
            load("docId2")
        }
    }

    @Test
    fun `check loading by id after empty was loaded`() {
        checkLoading(
            NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"),
            listOf(NetworkState.Success, NetworkState.Loading, NetworkState.Success),
            listOf<NewsItem>(NewsItem.Empty, NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
        ) {
            loadEmpty()
            load("docId")
        }
    }

    @Test
    fun `check loading by content after it was already loaded by id`() {
        checkLoading(
            NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"),
            listOf(NetworkState.Loading, NetworkState.Success),
            listOf(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
        ) {
            load("docId")
            load(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
        }
    }

    @Test
    fun `check loading by content after it was already loaded by content`() {
        checkLoading(
            emptyList(),
            listOf(NetworkState.Success),
            listOf(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
        ) {
            load(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
            load(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
        }
    }

    @Test
    fun `check loading by content after another was already loaded by id`() {
        checkLoading(
            NewsItem.Text("newsDateTimeSource1", "title1", "url1", emptyList(), "content1", "docId1"),
            listOf(NetworkState.Loading, NetworkState.Success, NetworkState.Success),
            listOf(
                NewsItem.Text("newsDateTimeSource1", "title1", "url1", emptyList(), "content1", "docId1"),
                NewsItem.Text("newsDateTimeSource2", "title2", "url2", emptyList(), "content2", "docId2")
            )
        ) {
            load("docId1")
            load(NewsItem.Text("newsDateTimeSource2", "title2", "url2", emptyList(), "content2", "docId2"))
        }
    }

    @Test
    fun `check loading by content after another was already loaded by content`() {
        checkLoading(
            emptyList(),
            listOf(NetworkState.Success, NetworkState.Success),
            listOf(
                NewsItem.Text("newsDateTimeSource1", "title1", "url1", emptyList(), "content1", "docId1"),
                NewsItem.Text("newsDateTimeSource2", "title2", "url2", emptyList(), "content2", "docId2")
            )
        ) {
            load(NewsItem.Text("newsDateTimeSource1", "title1", "url1", emptyList(), "content1", "docId1"))
            load(NewsItem.Text("newsDateTimeSource2", "title2", "url2", emptyList(), "content2", "docId2"))
        }
    }

    @Test
    fun `check loading by content after empty was loaded`() {
        checkLoading(
            emptyList(),
            listOf(NetworkState.Success, NetworkState.Success),
            listOf<NewsItem>(NewsItem.Empty, NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
        ) {
            loadEmpty()
            load(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
        }
    }

    @Test
    fun `check loading empty after content was loaded by id`() {
        checkLoading(
            NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"),
            listOf(NetworkState.Loading, NetworkState.Success, NetworkState.Success),
            listOf<NewsItem>(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"), NewsItem.Empty)
        ) {
            load("docId")
            loadEmpty()
        }
    }

    @Test
    fun `check loading empty after content was loaded by content`() {
        checkLoading(
            emptyList(),
            listOf(NetworkState.Success, NetworkState.Success),
            listOf<NewsItem>(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"), NewsItem.Empty)
        ) {
            load(NewsItem.Text("newsDateTimeSource", "title", "url", emptyList(), "content", "docId"))
            loadEmpty()
        }
    }

    @Test
    fun `check loading empty after empty was already loaded`() {
        checkLoading(
            emptyList(),
            listOf(NetworkState.Success),
            listOf<NewsItem>(NewsItem.Empty)
        ) {
            loadEmpty()
            loadEmpty()
        }
    }

    private fun checkLoading(
        response: NewsItem.Text,
        expectedNetworkStates: List<NetworkState>,
        expectedContents: List<NewsItem>,
        testBody: TextNewsContentLoader.() -> Unit
    ) {
        checkLoading(listOf(response), expectedNetworkStates, expectedContents, testBody)
    }

    private fun checkLoading(
        response: Iterable<NewsItem.Text>,
        expectedNetworkStates: List<NetworkState>,
        expectedContents: List<NewsItem>,
        testBody: TextNewsContentLoader.() -> Unit
    ) {
        checkLoader(createLoader(response), expectedNetworkStates, expectedContents, testBody)
    }

    private fun checkLoader(
        loader: TextNewsContentLoader,
        expectedNetworkStates: List<NetworkState>,
        expectedContents: List<NewsItem>,
        testBody: TextNewsContentLoader.() -> Unit
    ) {
        val actualNetworkStates = mutableListOf<NetworkState>()
        loader.networkState.observeForever { actualNetworkStates.add(it) }

        val actualContents = mutableListOf<NewsItem.Text>()
        loader.content.observeForever { actualContents.add(it) }

        loader.testBody()

        assertEquals(expectedNetworkStates, actualNetworkStates)
        assertEquals(expectedContents, expectedContents)
    }

    private fun createLoader(items: Iterable<NewsItem.Text>): TextNewsContentLoader {
        return DefaultTextNewsContentLoader(
            mock {
                for (item in items) {
                    on { textContent(item.docId) }.thenReturn(Observable.just(item))
                }
            }
        )
    }
}
