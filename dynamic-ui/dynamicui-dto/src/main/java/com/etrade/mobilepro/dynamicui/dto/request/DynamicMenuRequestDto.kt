package com.etrade.mobilepro.dynamicui.dto.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class DynamicMenuRequestDto(
    @Json(name = "mainMenu")
    val mainMenu: MainMenuRequestDto
)

@JsonClass(generateAdapter = true)
class MainMenuRequestDto(
    @Json(name = "enableTaxDocuments")
    val enableTaxDocuments: Boolean = false,
    @Json(name = "enableLearnMenu")
    val enableLearnMenu: Boolean = false,
    @Json(name = "enableDisclosuresMenu")
    val enableDisclosuresMenu: Boolean = false
)
