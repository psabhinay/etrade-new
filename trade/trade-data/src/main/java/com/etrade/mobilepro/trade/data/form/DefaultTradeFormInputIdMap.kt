package com.etrade.mobilepro.trade.data.form

import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.SecurityType.CONDITIONAL
import com.etrade.mobilepro.trade.form.api.SecurityType.MUTUAL_FUND
import com.etrade.mobilepro.trade.form.api.SecurityType.OPTION
import com.etrade.mobilepro.trade.form.api.SecurityType.STOCK
import com.etrade.mobilepro.trade.form.api.SecurityType.STOCK_AND_OPTIONS
import com.etrade.mobilepro.trade.form.api.TradeFormInputIdMap
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ContingentOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ContingentStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.MutualFundTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.SecurityTypeValueId
import javax.inject.Inject

class DefaultTradeFormInputIdMap @Inject constructor(
    conditionalTradeFormInputId: ConditionalOrderTradeFormInputId,
    mutualFundTradeFormInputId: MutualFundTradeFormInputId,
    optionTradeFormInputId: OptionTradeFormInputId,
    stockTradeFormInputId: StockTradeFormInputId,
    contingentStockTradeFormInputId: ContingentStockTradeFormInputId,
    contingentOptionTradeFormInputId: ContingentOptionTradeFormInputId,
    stopLossStockTradeFormInputId: StopLossStockTradeFormInputId,
    stopLossOptionTradeFormInputId: StopLossOptionTradeFormInputId,
    override val securityTypeValueId: SecurityTypeValueId
) : TradeFormInputIdMap {

    private val inputIds: Map<TradeFormType, TradeFormInputId> = mapOf(
        TradeFormType(CONDITIONAL) to conditionalTradeFormInputId,
        TradeFormType(MUTUAL_FUND) to mutualFundTradeFormInputId,
        TradeFormType(OPTION) to optionTradeFormInputId,
        TradeFormType(OPTION, AdvancedTradeType.Contingent) to contingentOptionTradeFormInputId,
        TradeFormType(STOCK) to stockTradeFormInputId,
        TradeFormType(STOCK, AdvancedTradeType.Contingent) to contingentStockTradeFormInputId,
        TradeFormType(STOCK, AdvancedTradeType.StopLoss) to stopLossStockTradeFormInputId,
        TradeFormType(OPTION, AdvancedTradeType.StopLoss) to stopLossOptionTradeFormInputId,
        TradeFormType(STOCK_AND_OPTIONS) to optionTradeFormInputId
    )

    override val advancedTradeInputId: Map<SecurityType, WithAdvancedTradeFormInputId> = mapOf(
        STOCK to stockTradeFormInputId,
        OPTION to optionTradeFormInputId
    )

    override val securityTypeId: String = requireValue(stockTradeFormInputId.securityType)

    override fun getInputId(tradeFormType: TradeFormType?): TradeFormInputId? = inputIds[tradeFormType]

    private fun requireValue(securityType: String): String {
        require(inputIds.values.all { it.securityType == securityType }) {
            "Integrity check failed: ID for securityType fields must be the same for all TradeFormInputIds"
        }
        return securityType
    }
}
