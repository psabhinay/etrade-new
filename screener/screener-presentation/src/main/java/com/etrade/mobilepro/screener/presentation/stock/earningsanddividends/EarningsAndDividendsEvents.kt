package com.etrade.mobilepro.screener.presentation.stock.earningsanddividends

import com.etrade.mobilepro.screener.api.filter.stock.EarningsDividendsFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class EpsAnnualGrowth : ScreenerFilterEvent<EarningsDividendsFilter, EarningsAndDividendsViewModel.ViewState, Boolean> {
    ZERO_5 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthAnnual0to5 = eventValue))
    },
    FIVE_10 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthAnnual5to10 = eventValue))
    },
    TEN_15 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthAnnual10to15 = eventValue))
    },
    FIFTEEN_25 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthAnnual15to25 = eventValue))
    },
    TWENTY_FIVE_50 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthAnnual25to50 = eventValue))
    },
    GREATER_THAN_50 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthAnnualGreaterThan50 = eventValue))
    },
    POSITIVE {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthAnnualPositiveChange = eventValue))
    },
    NEGATIVE {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthAnnualNegativeChange = eventValue))
    };

    override fun conditionalUpdate(
        viewState: EarningsAndDividendsViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<EarningsAndDividendsViewModel.ViewState> {
        var newState = update(viewState, eventValue)
        if (!newState.filter.isEpsGrowthAnnualValuesSelected()) {
            newState = clearEpsAnnualGrowthPeriod(newState)
        }
        if (newState.filter.epsGrowthAnnualPositiveChange) {
            newState = clearEpsAnnualGrowthPartialPositiveValues(newState)
        }
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = (newState.filter.isEpsGrowthAnnualValuesSelected() && newState.filter.isEpsGrowthAnnualPeriodSelected()) ||
                (!viewState.filter.isEpsGrowthAnnualBlockEmpty() && newState.filter.isEpsGrowthAnnualBlockEmpty())
        )
    }

    private fun clearEpsAnnualGrowthPeriod(viewState: EarningsAndDividendsViewModel.ViewState): EarningsAndDividendsViewModel.ViewState =
        viewState.copy(
            filter = viewState.filter.copy(
                epsGrowthAnnualForCurrentFiscalYear = false,
                epsGrowthAnnualForNextFiscalYear = false
            )
        )

    private fun clearEpsAnnualGrowthPartialPositiveValues(viewState: EarningsAndDividendsViewModel.ViewState): EarningsAndDividendsViewModel.ViewState =
        viewState.copy(
            filter = viewState.filter.copy(
                epsGrowthAnnual0to5 = false,
                epsGrowthAnnual5to10 = false,
                epsGrowthAnnual10to15 = false,
                epsGrowthAnnual15to25 = false,
                epsGrowthAnnual25to50 = false,
                epsGrowthAnnualGreaterThan50 = false
            )
        )
}

internal enum class EpsAnnualGrowthPeriod : ScreenerFilterEvent<EarningsDividendsFilter, EarningsAndDividendsViewModel.ViewState, Boolean> {
    CURRENT_FISCAL_YEAR {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    epsGrowthAnnualForCurrentFiscalYear = eventValue && viewState.filter.isEpsGrowthAnnualValuesSelected()
                )
            )
    },
    NEXT_FISCAL_YEAR {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    epsGrowthAnnualForNextFiscalYear = eventValue && viewState.filter.isEpsGrowthAnnualValuesSelected()
                )
            )
    };

    override fun conditionalUpdate(
        viewState: EarningsAndDividendsViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<EarningsAndDividendsViewModel.ViewState> {
        val newState = update(viewState, eventValue)
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = (newState.filter.isEpsGrowthAnnualValuesSelected() && newState.filter.isEpsGrowthAnnualPeriodSelected()) ||
                (!viewState.filter.isEpsGrowthAnnualBlockEmpty() && newState.filter.isEpsGrowthAnnualBlockEmpty())
        )
    }
}

internal enum class RevenueAnnualGrowth : ScreenerFilterEvent<EarningsDividendsFilter, EarningsAndDividendsViewModel.ViewState, Boolean> {
    ZERO_5 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthAnnual0to5 = eventValue))
    },
    FIVE_10 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthAnnual5to10 = eventValue))
    },
    TEN_15 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthAnnual10to15 = eventValue))
    },
    FIFTEEN_25 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthAnnual15to25 = eventValue))
    },
    TWENTY_FIVE_50 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthAnnual25to50 = eventValue))
    },
    GREATER_THAN_50 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthAnnualGreaterThan50 = eventValue))
    },
    POSITIVE {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthAnnualPositiveChange = eventValue))
    },
    NEGATIVE {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthAnnualNegativeChange = eventValue))
    };

    override fun conditionalUpdate(
        viewState: EarningsAndDividendsViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<EarningsAndDividendsViewModel.ViewState> {
        var newState = update(viewState, eventValue)
        if (!newState.filter.isRevenueGrowthAnnualValuesSelected()) {
            newState = clearRevenueAnnualGrowthPeriod(newState)
        }
        if (newState.filter.revenueGrowthAnnualPositiveChange) {
            newState = clearRevenueAnnualGrowthPartialPositiveValues(newState)
        }
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = (newState.filter.isRevenueGrowthAnnualValuesSelected() && newState.filter.isRevenueGrowthAnnualPeriodSelected()) ||
                (!viewState.filter.isRevenueGrowthAnnualBlockEmpty() && newState.filter.isRevenueGrowthAnnualBlockEmpty())
        )
    }

    private fun clearRevenueAnnualGrowthPeriod(viewState: EarningsAndDividendsViewModel.ViewState): EarningsAndDividendsViewModel.ViewState =
        viewState.copy(
            filter = viewState.filter.copy(
                revenueGrowthAnnualForCurrentFiscalYear = false,
                revenueGrowthAnnualForNextFiscalYear = false
            )
        )

    private fun clearRevenueAnnualGrowthPartialPositiveValues(viewState: EarningsAndDividendsViewModel.ViewState): EarningsAndDividendsViewModel.ViewState =
        viewState.copy(
            filter = viewState.filter.copy(
                revenueGrowthAnnual0to5 = false,
                revenueGrowthAnnual5to10 = false,
                revenueGrowthAnnual10to15 = false,
                revenueGrowthAnnual15to25 = false,
                revenueGrowthAnnual25to50 = false,
                revenueGrowthAnnualGreaterThan50 = false
            )
        )
}

internal enum class RevenueAnnualGrowthPeriod : ScreenerFilterEvent<EarningsDividendsFilter, EarningsAndDividendsViewModel.ViewState, Boolean> {
    CURRENT_FISCAL_YEAR {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    revenueGrowthAnnualForCurrentFiscalYear = eventValue && viewState.filter.isRevenueGrowthAnnualValuesSelected()
                )
            )
    },
    NEXT_FISCAL_YEAR {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    revenueGrowthAnnualForNextFiscalYear = eventValue && viewState.filter.isRevenueGrowthAnnualValuesSelected()
                )
            )
    };

    override fun conditionalUpdate(
        viewState: EarningsAndDividendsViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<EarningsAndDividendsViewModel.ViewState> {
        val newState = update(viewState, eventValue)
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = (newState.filter.isRevenueGrowthAnnualValuesSelected() && newState.filter.isRevenueGrowthAnnualPeriodSelected()) ||
                (!viewState.filter.isRevenueGrowthAnnualBlockEmpty() && newState.filter.isRevenueGrowthAnnualBlockEmpty())
        )
    }
}

internal enum class EpsQuarterlyGrowth : ScreenerFilterEvent<EarningsDividendsFilter, EarningsAndDividendsViewModel.ViewState, Boolean> {
    ZERO_5 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthQuarterly0to5 = eventValue))
    },
    FIVE_10 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthQuarterly5to10 = eventValue))
    },
    TEN_15 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthQuarterly10to15 = eventValue))
    },
    FIFTEEN_25 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthQuarterly15to25 = eventValue))
    },
    TWENTY_FIVE_50 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthQuarterly25to50 = eventValue))
    },
    GREATER_THAN_50 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthQuarterlyGreaterThan50 = eventValue))
    },
    POSITIVE {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthQuarterlyPositiveChange = eventValue))
    },
    NEGATIVE {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(epsGrowthQuarterlyNegativeChange = eventValue))
    };

    override fun conditionalUpdate(
        viewState: EarningsAndDividendsViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<EarningsAndDividendsViewModel.ViewState> {
        var newState = update(viewState, eventValue)
        if (newState.filter.epsGrowthQuarterlyPositiveChange) {
            newState = clearEpsQuarterlyGrowthPartialPositiveValues(newState)
        }
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = true
        )
    }

    private fun clearEpsQuarterlyGrowthPartialPositiveValues(viewState: EarningsAndDividendsViewModel.ViewState): EarningsAndDividendsViewModel.ViewState =
        viewState.copy(
            filter = viewState.filter.copy(
                epsGrowthQuarterly0to5 = false,
                epsGrowthQuarterly5to10 = false,
                epsGrowthQuarterly10to15 = false,
                epsGrowthQuarterly15to25 = false,
                epsGrowthQuarterly25to50 = false,
                epsGrowthQuarterlyGreaterThan50 = false
            )
        )
}

internal enum class RevenueQuarterlyGrowth : ScreenerFilterEvent<EarningsDividendsFilter, EarningsAndDividendsViewModel.ViewState, Boolean> {
    ZERO_5 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthQuarterly0to5 = eventValue))
    },
    FIVE_10 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthQuarterly5to10 = eventValue))
    },
    TEN_15 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthQuarterly10to15 = eventValue))
    },
    FIFTEEN_25 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthQuarterly15to25 = eventValue))
    },
    TWENTY_FIVE_50 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthQuarterly25to50 = eventValue))
    },
    GREATER_THAN_50 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthQuarterlyGreaterThan50 = eventValue))
    },
    POSITIVE {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthQuarterlyPositiveChange = eventValue))
    },
    NEGATIVE {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(revenueGrowthQuarterlyNegativeChange = eventValue))
    };

    override fun conditionalUpdate(
        viewState: EarningsAndDividendsViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<EarningsAndDividendsViewModel.ViewState> {
        var newState = update(viewState, eventValue)
        if (newState.filter.revenueGrowthQuarterlyPositiveChange) {
            newState = clearRevenueQuarterlyGrowthPartialPositiveValues(newState)
        }
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = true
        )
    }

    private fun clearRevenueQuarterlyGrowthPartialPositiveValues(viewState: EarningsAndDividendsViewModel.ViewState): EarningsAndDividendsViewModel.ViewState =
        viewState.copy(
            filter = viewState.filter.copy(
                revenueGrowthQuarterly0to5 = false,
                revenueGrowthQuarterly5to10 = false,
                revenueGrowthQuarterly10to15 = false,
                revenueGrowthQuarterly15to25 = false,
                revenueGrowthQuarterly25to50 = false,
                revenueGrowthQuarterlyGreaterThan50 = false
            )
        )
}

internal enum class DividendYield : ScreenerFilterEvent<EarningsDividendsFilter, EarningsAndDividendsViewModel.ViewState, Boolean> {
    POSITIVE {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(dividendYieldPositive = eventValue))
    },
    ZERO_2 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(dividendYield0to2 = eventValue))
    },
    TWO_4 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(dividendYield2to4 = eventValue))
    },
    FOUR_6 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(dividendYield4to6 = eventValue))
    },
    SIX_8 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(dividendYield6to8 = eventValue))
    },
    GREATER_THAN_8 {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(dividendYieldGreaterThan8 = eventValue))
    },
    BELOW_AVG {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(dividendYieldBelowIndustryAvg = eventValue, dividendYieldAboveIndustryAvg = false))
    },
    ABOVE_AVG {
        override fun update(viewState: EarningsAndDividendsViewModel.ViewState, eventValue: Boolean): EarningsAndDividendsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(dividendYieldAboveIndustryAvg = eventValue, dividendYieldBelowIndustryAvg = false))
    }
}
