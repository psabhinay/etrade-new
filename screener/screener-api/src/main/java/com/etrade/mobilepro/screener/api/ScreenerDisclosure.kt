package com.etrade.mobilepro.screener.api

enum class ScreenerDisclosure {
    STOCK_PRIMARY,
    STOCK_RESULTS,
    MUTUAL_FUND_PRIMARY,
    MUTUAL_FUND_RESULTS,
    ETF_PRIMARY,
    ETF_RESULTS
}
