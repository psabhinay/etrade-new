package org.chromium.customtabsclient.shared

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.browser.customtabs.CustomTabsClient
import androidx.browser.customtabs.CustomTabsIntent
import androidx.browser.customtabs.CustomTabsServiceConnection
import androidx.browser.customtabs.CustomTabsSession
import javax.inject.Inject

/**
 * This is a helper class to launch url in custom tab or webview.
 */
class WebViewHelper @Inject constructor(var context: Context) : CustomTabServiceConnectionCallback {

    private var customTabsSession: CustomTabsSession? = null
    private var client: CustomTabsClient? = null
    private var connection: CustomTabsServiceConnection? = null
    private var connectionCallback: ConnectionCallback? = null

    /**
     * Creates or retrieves an exiting CustomTabsSession.
     *
     * @return a CustomTabsSession.
     */
    val session: CustomTabsSession?
        get() {
            if (client == null) {
                customTabsSession = null
            } else if (customTabsSession == null) {
                customTabsSession = client!!.newSession(null)
            }
            return customTabsSession
        }

    /**
     * Unbinds the Custom Tabs Service.
     */
    fun unbindCustomTabsService() {
        if (connection == null) return
        context.unbindService(connection!!)
        client = null
        customTabsSession = null
        connection = null
    }

    /**
     * Binds to the Custom Tabs Service.
     */
    fun bindCustomTabsService() {
        if (client != null) return

        val packageName = CustomTabsHelper.getPackageNameToUse(context) ?: return

        connection = CustomServiceConnectionImpl(this)
        CustomTabsClient.bindCustomTabsService(context, packageName, connection)
    }

    override fun onServiceConnected(client: CustomTabsClient) {
        this.client = client
        this.client!!.warmup(0L)
        if (connectionCallback != null) connectionCallback!!.onCustomTabsConnected()
    }

    override fun onServiceDisconnected() {
        client = null
        customTabsSession = null
        if (connectionCallback != null) connectionCallback!!.onCustomTabsDisconnected()
    }

    /**
     * A Callback for when the service is connected or disconnected. Use those callbacks to
     * handle UI changes when the service is connected or disconnected.
     */
    interface ConnectionCallback {
        /**
         * Called when the service is connected.
         */
        fun onCustomTabsConnected()

        /**
         * Called when the service is disconnected.
         */
        fun onCustomTabsDisconnected()
    }

    /**
     * Opens the URL on a Custom Tab if possible. Otherwise fallback to opening it on a WebView.
     * has an optional flag to open it in browser instead of WebView if needed.
     *
     * @param uri The url to be loaded
     * @param colorToolbar the color of toolbar
     * @param externalBrowser fall back to be external browser or not
     *
     */
    fun openCustomTab(
        uri: String,
        colorToolbar: Int? = null,
        externalBrowser: Boolean = false
    ) {
        val packageName = CustomTabsHelper.getPackageNameToUse(context)

        // If we cant find a package name, it means there is no browser that supports
        // Chrome Custom Tabs installed. So, we fallback to the webview or external browser with an ACTION_VIEW intent
        if (packageName == null) {
            val intent = if (externalBrowser) {
                getExternalBrowserIntent(uri)
            } else {
                WebviewActivity.intent(context, uri)
            }
            context.startActivity(intent)
        } else {
            if (externalBrowser) {
                context.startActivity(getExternalBrowserIntent(uri, packageName))
            } else {
                getCustomTabsIntent(colorToolbar, packageName)?.launchUrl(context, Uri.parse(uri))
            }
        }
    }

    private fun getCustomTabsIntent(
        colorToolbar: Int?,
        packageName: String?
    ): CustomTabsIntent? {
        val customTabsIntent = CustomTabsIntent.Builder().apply {
            colorToolbar?.let { setToolbarColor(it) }
        }.build()
        customTabsIntent.intent.setPackage(packageName)
        customTabsIntent.intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        customTabsIntent.intent.putExtra(CustomTabsIntent.EXTRA_ENABLE_URLBAR_HIDING, false)
        return customTabsIntent
    }

    private fun getExternalBrowserIntent(uri: String, packageName: String? = null): Intent {
        return Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(uri)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            packageName?.let { setPackage(it) }
        }
    }
}
