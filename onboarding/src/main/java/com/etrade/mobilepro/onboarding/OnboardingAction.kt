package com.etrade.mobilepro.onboarding

import android.app.Activity

interface OnboardingAction {

    fun logonAction(activity: Activity)

    fun openAnAccountAction(activity: Activity)
}
