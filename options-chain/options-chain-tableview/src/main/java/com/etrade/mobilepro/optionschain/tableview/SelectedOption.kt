package com.etrade.mobilepro.optionschain.tableview

import com.etrade.mobilepro.optionschains.api.Option

data class SelectedOption(val option: Option, val isSelected: Boolean)
