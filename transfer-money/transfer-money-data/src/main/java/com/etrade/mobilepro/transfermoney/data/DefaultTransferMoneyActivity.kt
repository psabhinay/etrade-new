package com.etrade.mobilepro.transfermoney.data

import com.etrade.mobilepro.transfermoney.api.TransferMoneyActivity
import com.etrade.mobilepro.transfermoney.api.TransferType

@SuppressWarnings("LongParameterList")
internal class DefaultTransferMoneyActivity(
    override val amount: String,
    override val date: String,
    override val frequency: String,
    override val fromAccount: String,
    override val confirmationNumber: String,
    override val referenceNumber: String,
    override val status: String,
    override val toAccount: String,
    override val transferType: TransferType?,
    override val id: String,
    override val appName: String,
    override val memo: String
) : TransferMoneyActivity
