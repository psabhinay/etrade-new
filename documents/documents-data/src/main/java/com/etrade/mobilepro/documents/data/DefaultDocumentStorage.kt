package com.etrade.mobilepro.documents.data

import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.documents.Document
import com.etrade.mobilepro.documents.DocumentStorage
import com.etrade.mobilepro.documents.data.decoding.DocumentDecoder
import com.etrade.mobilepro.documents.data.decoding.DocumentFilenameGenerator
import com.etrade.mobilepro.documents.data.mapping.toRequest
import com.etrade.mobilepro.documents.data.rest.DocumentDetailsApiClient
import com.etrade.mobilepro.util.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Retrofit
import java.io.File
import javax.inject.Inject
import kotlin.concurrent.thread

class DefaultDocumentStorage @Inject constructor(
    @MobileTrade retrofit: Retrofit,
    private val applicationInfo: ApplicationInfo,
    private val decoder: DocumentDecoder,
    private val filenameGenerator: DocumentFilenameGenerator
) : DocumentStorage {
    private val apiClient = retrofit.create(DocumentDetailsApiClient::class.java)
    private val cachedDocuments = mutableMapOf<String, Document>()

    @ExperimentalCoroutinesApi
    override suspend fun getDocumentData(document: Document): Flow<Resource<Document>> = flow {
        emit(Resource.Loading(document))
        runCatching {
            apiClient.getDocumentDetails(document.toRequest(applicationInfo.appVersionName())).holder?.documents?.firstOrNull()?.data
        }.mapCatching {
            val base64Data = it ?: throw IllegalArgumentException("No payload data for document $document")
            val fileName = filenameGenerator.generateFilename(document)
            val file = decoder.decodeToFile(fileName, base64Data)
            document.updateFilename(filename = file.getOrThrow().absolutePath)
        }.fold(
            onSuccess = {
                cachedDocuments[it.documentId] = it
                emit(Resource.Success(it))
            },
            onFailure = {
                emit(Resource.Failed(document))
            }
        )
    }

    override fun getCachedDocument(documentId: String): Document? = cachedDocuments[documentId]

    override fun getCachedDocuments(documents: List<Document>): List<Document> =
        documents.filter { cachedDocuments[it.documentId] != null }

    @Suppress("TooGenericExceptionCaught")
    override fun clearAllCachedDocuments() {
        thread {
            try {
                cachedDocuments.values.forEach {
                    it.filename?.let { name ->
                        File(name).delete()
                    }
                }
            } catch (e: Exception) {
                // nop
            } finally {
                cachedDocuments.clear()
            }
        }
    }
}
