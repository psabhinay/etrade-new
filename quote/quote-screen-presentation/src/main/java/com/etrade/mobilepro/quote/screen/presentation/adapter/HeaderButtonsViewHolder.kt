package com.etrade.mobilepro.quote.screen.presentation.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quote.screen.presentation.R
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemHeaderBinding
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.util.android.binding.resources

class HeaderButtonsViewHolder(
    private val binding: QuoteItemHeaderBinding,
    private val tradeClickListener: (() -> Unit)?,
    private val setAlertClickListener: (() -> Unit)?,
    private val addToWatchlistClickListener: (() -> Unit)?
) : RecyclerView.ViewHolder(binding.root) {

    @Suppress("LongMethod")
    fun bindTo(item: QuoteItem.HeaderButtons?) {
        item ?: return
        with(binding) {
            // TODO At the top of the page, I see Trade CTA's (not functioning in this story)
            setAlertBtn.setOnClickListener {
                setAlertClickListener?.invoke()
            }
            outlinedAddWatchListBtn.setOnClickListener {
                addToWatchlistClickListener?.invoke()
            }
            filledAddWatchListBtn.setOnClickListener {
                addToWatchlistClickListener?.invoke()
            }
            tradeBtn.setOnClickListener {
                tradeClickListener?.invoke()
            }
            tradeBtn.visibility = View.VISIBLE
            outlinedAddWatchListBtn.visibility = View.VISIBLE
            outlinedAddWatchListBtn.text = resources.getString(R.string.plus_watch_list_2_lines)
            filledAddWatchListBtn.visibility = View.GONE
            setAlertBtn.visibility = View.VISIBLE
            when (item.type) {
                InstrumentType.INDX -> {
                    tradeBtn.visibility = View.GONE
                    outlinedAddWatchListBtn.visibility = View.GONE
                    filledAddWatchListBtn.visibility = View.VISIBLE
                    filledAddWatchListBtn.text = resources.getString(R.string.plus_watch_list)
                }
                InstrumentType.OPTN, InstrumentType.MF, InstrumentType.MMF -> {
                    setAlertBtn.visibility = View.GONE
                    outlinedAddWatchListBtn.text = resources.getString(R.string.plus_watch_list)
                }
                else -> { }
            }
        }
    }
}
