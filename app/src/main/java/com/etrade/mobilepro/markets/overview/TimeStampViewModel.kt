package com.etrade.mobilepro.markets.overview

import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel

class TimeStampViewModel(
    val timestamp: String
) : DynamicViewModel(R.layout.overview_list_footer_timestamp) {
    override val variableId: Int = BR.obj
}
