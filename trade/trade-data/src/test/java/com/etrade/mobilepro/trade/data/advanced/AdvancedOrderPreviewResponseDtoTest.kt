package com.etrade.mobilepro.trade.data.advanced

import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.trade.api.ConfirmedOrder
import com.etrade.mobilepro.trade.api.action
import com.etrade.mobilepro.trade.api.advanced.AdvancedStockOrder
import com.etrade.mobilepro.util.ObjectArrayMismatchAdapter
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.math.BigDecimal

class AdvancedOrderPreviewResponseDtoTest {

    @Test
    fun `check multiple errors preview response parsing`() {
        val response = readPreviewResponse("advanced-order-preview-response-errors.json")
        assertEquals(2, response.errors.size)
        assertTrue(response.orders.isEmpty())
        assertTrue(response.warnings.isEmpty())

        response.errors.forEach { error ->
            assertEquals(ServerMessage.Type.ERROR, error.type)
            assertEquals("2055", error.code)
            assertEquals(
                "The trailing parameter you have specified is invalid. Please modify your entry, making sure you enter a positive number that doesn't " +
                    "include any letters or special characters.",
                error.text
            )
        }
    }

    @Test
    fun `check single error preview response parsing`() {
        val response = readPreviewResponse("advanced-order-preview-response-error.json")
        assertEquals(1, response.errors.size)
        assertTrue(response.orders.isEmpty())
        assertTrue(response.warnings.isEmpty())

        val error = response.errors.first()
        assertEquals(ServerMessage.Type.ERROR, error.type)
        assertEquals("9999", error.code)
        assertEquals("The service you requested is not available at this time. Please try again.", error.text)
    }

    @Test
    fun `check single warning preview response parsing`() {
        val response = readPreviewResponse("advanced-order-preview-response-warning.json")
        assertTrue(response.errors.isEmpty())
        assertEquals(2, response.orders.size)
        assertEquals(1, response.warnings.size)
        assertEquals("545913938106", response.previewId)

        val error = response.warnings.first()
        assertEquals(ServerMessage.Type.WARN, error.type)
        assertEquals("8501", error.code)
        assertEquals(
            "Fractional shares present for sell. Please note that you can now enter orders to sell your full fractional quantity directly on the trade " +
                "ticket. Orders with decimal quantities can only be Good for the Day, can only be Market or Limit orders, and must be a Market order if " +
                "the quantity is less than one share.",
            error.text
        )
    }

    @Test
    fun `check removal of special sequence of characters from a server message`() {
        val response = readPreviewResponse("advanced-order-preview-response-error-masked.json")
        assertEquals(1, response.errors.size)

        val error = response.errors.first()
        assertEquals(
            error.text,
            "<b>IMPORTANT: </b>The stop price must be at least \$0.01 above the current ask price for buy orders (\$0.01 above the most " +
                "recent closing price if the market is closed). Please adjust the stop price and resubmit your order."
        )
    }

    @Test
    fun `check successful preview response parsing`() {
        val response = readPreviewResponse("advanced-order-preview-response-successful.json")
        assertTrue(response.errors.isEmpty())
        assertEquals(2, response.orders.size)
        assertTrue(response.warnings.isEmpty())
        assertEquals("540957650106", response.previewId)

        val orders = response.orders
        orders.forEach {
            require(it is AdvancedStockOrder)
            assertFalse(it is ConfirmedOrder)
            assertEquals("Individual_Brokerage -1187", it.account)
            assertEquals(TransactionType.BUY_OPEN, it.action)
            assertEquals(PriceType.MARKET, it.priceType)
            assertEquals(OrderTerm.GOOD_FOR_DAY, it.term)
            assertEquals(OrderType.EQUITY, it.orderType)
            assertEquals(BigDecimal.ZERO, it.fee)
            assertFalse(it.allOrNone)
            assertNull(it.limitPrice)
            assertNull(it.stopPrice)
        }

        val iterator = orders.iterator()

        var order = iterator.next()
        require(order is AdvancedStockOrder)
        var leg = order.legs.first()

        assertEquals("AAPL", leg.symbol)
        assertEquals(BigDecimal("123"), leg.quantity)
        assertEquals(BigDecimal("13810.44"), order.cost)

        order = iterator.next()
        require(order is AdvancedStockOrder)
        leg = order.legs.first()

        assertEquals("FB", leg.symbol)
        assertEquals(BigDecimal("456"), leg.quantity)
        assertEquals(BigDecimal("116197.92"), order.cost)
    }

    private fun readPreviewResponse(path: String): AdvancedOrderPreviewResponseDto {
        return AdvancedOrderPreviewResponseDto::class.getObjectFromJson(
            resourcePath = path,
            type = AdvancedOrderPreviewResponseDto::class.java,
            adapters = listOf(ObjectArrayMismatchAdapter.newFactory(AdvancedOrderServerMessageDto::class.java))
        )
    }
}
