package com.etrade.mobilepro.order.details.dto

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.mapper.OrderDetailDescriptionMapper
import com.etrade.mobilepro.order.details.mapper.OrderDetailsMapper
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.testutil.getObjectFromJson
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import java.math.BigDecimal

internal class OptionOrdersResponseDtoTest {

    @Test
    fun `test options orders mapper`() {
        val response = getOptionOrderDetailsResponse()
        val orderDetailsDto = response.data?.viewOrderEvents
        val orderDetailsItemDto = orderDetailsDto?.orderDetails
        assertNotNull(orderDetailsItemDto)

        val orders = orderDetailsItemDto?.orders
        assertNotNull(orders)
        Assertions.assertFalse(orders?.isEmpty() ?: false)

        val firstOrder = orderDetailsItemDto?.orders?.first()
        assertNotNull(firstOrder)
        val orderType = firstOrder?.header?.orderType?.let {
            OrderType.fromTypeCode(it)
        }
        assertNotNull(orderType)
        Assertions.assertNotEquals(orderType, OrderType.UNKNOWN)
        val legDetailsDto = firstOrder?.legDetails

        assertNotNull(legDetailsDto)
        Assertions.assertFalse(legDetailsDto?.isEmpty() ?: false)

        if (orderDetailsDto != null) {
            val orderDetailsItems = OrderDetailsMapper(OrderDetailDescriptionMapper()::map).map(orderDetailsDto)
            assertEquals(orderDetailsItems.size, 1)
            Assertions.assertTrue(orderDetailsItems.first() is BaseOrderDetailsItem.OrderDetailsItem)
            val orderDetailsItem = orderDetailsItems.first() as BaseOrderDetailsItem.OrderDetailsItem
            assertEquals(BigDecimal.valueOf(193.007559), orderDetailsItem.estimatedTotalCost)
            assertEquals(BigDecimal.valueOf(6.9924), orderDetailsItem.commission)
            assertEquals(OrderStatus.EXECUTED, orderDetailsItem.orderStatus)
            assertEquals(BigDecimal.valueOf(2), orderDetailsItem.priceExecuted)
            assertEquals(BigDecimal.valueOf(2), orderDetailsItem.limitPrice)
            assertEquals("4", orderDetailsItem.quantity)
            assertEquals(PriceType.NET_CREDIT, orderDetailsItem.priceType)
            assertEquals(OrderType.IRON_BUTTERFLY, orderDetailsItem.orderType)
            assertEquals(OrderTerm.GOOD_FOR_DAY, orderDetailsItem.term)
            assertEquals(StrategyType.IRON_BUTTERFLY, orderDetailsItem.strategyType)
            assertEquals(4, orderDetailsItem.orderDescriptions.size)
            val orderDescFirst = orderDetailsItem.orderDescriptions.getOrNull(0)
            orderDescFirst?.apply {
                assertEquals("1", displayQuantity)
                assertEquals("1", fillQuantity)
                assertEquals("1", quantity)
                assertEquals(TransactionType.BUY_OPEN_OPT, transactionType)
                assertEquals("AAPL May 03 '19 \$202.50 Put", displaySymbol)
                assertEquals("AAPL--190503P00202500", originalSymbol)
                assertEquals("AAPL", underlyingProductId)
            }
            assertEquals(4, orderDetailsItem.tradeLegs.size)
            val tradeLegFirst = orderDetailsItem.tradeLegs.getOrNull(0)
            tradeLegFirst?.apply {
                assertEquals(orderDescFirst?.originalSymbol, symbol)
                assertEquals(orderDescFirst?.displaySymbol, displaySymbol)
                assertEquals(orderDescFirst?.transactionType, transactionType)
                assertEquals(orderDescFirst?.quantity?.toInt(), quantity)
            }
        }
    }

    private fun getOptionOrderDetailsResponse(): ServerResponseDto<OrderDetailsResponseDto> =
        OptionOrdersResponseDtoTest::class.getObjectFromJson("options_order_details.json", ServerResponseDto::class.java, OrderDetailsResponseDto::class.java)
}
