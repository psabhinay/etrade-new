package com.etrade.mobilepro.util.formatters

val String.majorMinorVersion: String
    get() = """\d+\.\d+""".toRegex().find(this)?.value ?: ""
