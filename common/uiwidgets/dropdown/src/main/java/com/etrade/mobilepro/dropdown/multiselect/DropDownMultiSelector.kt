package com.etrade.mobilepro.dropdown.multiselect

import android.graphics.drawable.Drawable
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.etrade.mobilepro.common.ToolbarActionEnd
import com.etrade.mobilepro.dropdown.DropDownSelector
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import java.lang.ref.WeakReference

interface DropDownMultiSelector<T> : DropDownSelector {

    data class Selectable<T>(
        val title: String,
        val value: T,
        val selected: Boolean,
        val enabled: Boolean = true,
        val titleColor: Int? = null,
        val titleFormatter: ((T) -> String)? = null,
        val descriptionFormatter: ((T) -> String)? = null,
        val contentDescriptionFormatter: ((T) -> String)? = null
    )

    /**
     * Settings for drop down multi selector
     * @param tag - Tag for the selection fragment
     * @param multiSelectEnabled - defines, whether multiselection feature will be enabled in the selection fragment
     * @param title - title for the selection fragment
     * @param itemsData - LiveData with the list of items which will be displayed in the selection fragment
     * @param selectionFlow - Object with additional dependencies, it is used when we need some processing before or after selection
     * @param snackbarUtilFactory - Factory for creating snack bars in case of errors
     * @param onCancelListener is called when user closes drop down in any way(close button, swipe down), first param is passed to open() fallBackValues
     * @param eventListener - listener for CLEAR_ALL, SAVE buttons tap events(will be called only in multiSelectEnabled = true case)
     * @param selectListener - listener for selection event
     * @param actionButtonSettings - Object containing a title and action for the action button
     * @param loadingState - Object containing information about the loading state of the data
     * @param ghostText - String to display as tooltip at the bottom of the drop down
     * @param saveButtonText - String to display for save button, if null, button text is "Saved"
     * @param actionItemSettings - Object containing information about action item
     */
    class Settings<T>(
        val tag: String,
        val multiSelectEnabled: Boolean,
        val title: String,
        val itemsData: LiveData<List<Selectable<T>>>,
        val selectionFlow: SelectionFlow<T>? = null,
        val snackbarUtilFactory: SnackbarUtilFactory? = null,
        val onCancelListener: ((Set<T>?) -> Unit)? = null,
        val eventListener: ((Event, WeakReference<BottomSheetDialogFragment>) -> Unit)? = null,
        val selectListener: (Int, Selectable<T>) -> Unit,
        val actionButtonSettings: ActionButtonSettings? = null,
        val loadingState: LiveData<Resource<T>>? = null,
        val ghostText: String? = null,
        val saveButtonText: String? = null,
        val actionItemSettings: ActionItemSettings? = null,
        val toolbarActionEnd: ToolbarActionEnd? = null
    ) {
        enum class Event {
            CLEAR_ALL, SAVE
        }
    }

    /**
     * Additional group of non obligatory dependencies. This is used when some processing before or after selection is needed.
     * When this is presented in Settings, the flow of DropDownMultiSelector will be as the following:
     *  1. Fragment is shown, fragment subscribes to stageData. Depending on type of incoming Resource fragment updates its UI:
     *          Resource.Loading - list hides, progress bar shows
     *          Resource.Failed - list shows, snackbar shows(if snackbarUtilFactory was provided), progress bar hides
     *          Resource.Success with data == null - list shows, progress bar hides, can be thought of as "Idle" or "Reset" state
     *          Resource.Success with data != null - list shows, progress bar hides,
     *                                               selectListener is called.
     *                                               If multiSelectEnabled == false then fragment dismisses.
     *                                               didSelectListener is called
     *  2. User taps on the item in the list
     *  3. willSelectListener is called. Here some async operations can be started. It is a client responsibility to update stageData accordingly
     *      during this async operations
     *
     * @param willSelectListener is called when user taps on an item. Good place to start some async operations
     * @param didSelectListener is called after selectListener. Good place to dispose resources/subscriptions, update stageDate if needed
     * @param stageData indicator of DropDown ViewState, also carries data
     * @param onError is called when stageData brings Resource.Failed
     * @param onRetry is called when user presses snackBar retry button
     */
    class SelectionFlow<T>(
        val willSelectListener: (Int, Selectable<T>) -> Unit,
        val didSelectListener: (Int, Selectable<T>) -> Unit,
        val stageData: LiveData<Resource<Pair<Int, Selectable<T>>>>,
        val onError: ((Int, Selectable<T>) -> Unit)? = null,
        val onRetry: ((Int, Selectable<T>) -> Unit)? = null
    )

    /**
     * Additional group of optional settings for the action button. This is used when it is desired to display an action button at the bottom of the list
     *
     * @param actionButtonListener the action to call on button click
     * @param buttonTitle the display text for the button
     */
    class ActionButtonSettings(
        val buttonTitle: String,
        val actionButtonListener: (() -> Unit)
    )

    class ActionItemSettings(
        val title: String,
        val action: () -> Unit,
        val startIcon: Drawable? = null,
        val endIcon: Drawable? = null
    )
}

class DefaultDropDownMultiSelector<T>(
    private val supportFragmentManager: FragmentManager,
    private val settings: DropDownMultiSelector.Settings<T>,
    private val fallBackValuesProvider: () -> Set<T>? = { null }
) : DropDownMultiSelector<T> {

    init {
        // this class is config.change - dependent, so we need to (re)apply settings on its (re)creation
        applySettings(supportFragmentManager, settings, fallBackValuesProvider)
    }

    override fun open() {
        val dialogFragment = supportFragmentManager.findFragmentByTag(settings.tag) as? OverlayDropDownFragment ?: OverlayDropDownFragment()
        dialogFragment.applySettings(settings, fallBackValuesProvider.invoke())
        if (!dialogFragment.isAdded) {
            dialogFragment.show(supportFragmentManager, settings.tag)
        }
    }

    override fun close() {
        (supportFragmentManager.findFragmentByTag(settings.tag) as? OverlayDropDownFragment)?.dismiss()
    }

    companion object {

        fun <T> applySettings(
            fragmentManager: FragmentManager,
            settings: DropDownMultiSelector.Settings<T>,
            fallBackValuesProvider: () -> Set<T>? = { null }
        ) {
            val dialogFragment = fragmentManager.findFragmentByTag(settings.tag) as? OverlayDropDownFragment
            dialogFragment?.applySettings(settings, fallBackValuesProvider.invoke())
        }
    }
}
