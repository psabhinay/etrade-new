package com.etrade.mobilepro.feature.presentation.compose

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.etrade.mobilepro.feature.BaseFeatureCategory
import com.etrade.mobilepro.feature.FeatureCategory

@Composable
internal fun FeatureCategoryHeader(category: FeatureCategory) = Column(
    Modifier
        .fillMaxWidth()
        .wrapContentHeight()
        .background(MaterialTheme.colors.primary)
        .padding(8.dp)
) {
    Text(
        text = category.title,
        style = MaterialTheme.typography.h6,
        color = MaterialTheme.colors.onPrimary
    )
    Text(
        text = category.explanation,
        style = MaterialTheme.typography.subtitle1,
        color = MaterialTheme.colors.onPrimary
    )
}

@Preview
@Composable
private fun LightModeFeatureCategoryHeader() =
    FeatureCategoryHeader(category = BaseFeatureCategory("Title", "Explanation"))
