package com.etrade.mobilepro.trade.presentation.account

import com.etrade.mobilepro.trade.api.AccountMode
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.presentation.form.account.DefaultSelectedAccountDelegate
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations.openMocks

class TestDefaultSelectedAccountDelegate {

    @Mock
    lateinit var tradingDefaults: TradingDefaultsPreferences

    @Before
    fun setup() {
        openMocks(this)
    }

    @Test
    fun `returns fallback when fallback valid, no valid default, and no account selected`() {
        val sut = createAccountDelegate("bravo")
        assertThat(sut.getCurrentAccountId(SecurityType.STOCK), `is`("bravo"))
    }

    @Test
    fun `returns first item when fallback invalid, no valid default, and no account selected`() {
        val sut = createAccountDelegate("echo")
        assertThat(sut.getCurrentAccountId(SecurityType.STOCK), `is`("alpha"))
    }

    @Test
    fun `returns correctly when stock account has valid default, and no account selected`() {
        val sut = createAccountDelegate("bravo")
        whenever(tradingDefaults.stocksAccountId).thenReturn("charlie")

        assertThat(sut.getCurrentAccountId(SecurityType.STOCK), `is`("charlie"))
        assertThat(sut.getCurrentAccountId(SecurityType.OPTION), `is`("bravo"))
        assertThat(sut.getCurrentAccountId(SecurityType.STOCK_AND_OPTIONS), `is`("bravo"))
    }

    @Test
    fun `returns correctly when options account has default, and no account selected`() {
        val sut = createAccountDelegate("bravo")
        whenever(tradingDefaults.optionsAccountId).thenReturn("delta")

        assertThat(sut.getCurrentAccountId(SecurityType.STOCK), `is`("bravo"))
        assertThat(sut.getCurrentAccountId(SecurityType.OPTION), `is`("delta"))
        assertThat(sut.getCurrentAccountId(SecurityType.STOCK_AND_OPTIONS), `is`("bravo"))
    }

    @Test
    fun `returns correctly when both have valid defaults, and no account selected`() {
        val sut = createAccountDelegate("bravo")
        whenever(tradingDefaults.stocksAccountId).thenReturn("charlie")
        whenever(tradingDefaults.optionsAccountId).thenReturn("delta")

        assertThat(sut.getCurrentAccountId(SecurityType.STOCK), `is`("charlie"))
        assertThat(sut.getCurrentAccountId(SecurityType.OPTION), `is`("delta"))
        assertThat(sut.getCurrentAccountId(SecurityType.STOCK_AND_OPTIONS), `is`("bravo"))
    }

    @Test
    fun `returns correctly when both have valid defaults, and any account selected`() {
        val sut = createAccountDelegate("bravo")
        whenever(tradingDefaults.stocksAccountId).thenReturn("charlie")
        whenever(tradingDefaults.optionsAccountId).thenReturn("delta")

        sut.setUserSelectedAccountId(SecurityType.STOCK, "alpha")

        assertThat(sut.getCurrentAccountId(SecurityType.STOCK), `is`("alpha"))
        assertThat(sut.getCurrentAccountId(SecurityType.OPTION), `is`("alpha"))
        assertThat(sut.getCurrentAccountId(SecurityType.STOCK_AND_OPTIONS), `is`("alpha"))
    }

    @Test
    fun `onSecurityType resets account only when security has valid default`() {
        val sut = createAccountDelegate("bravo")
        whenever(tradingDefaults.stocksAccountId).thenReturn("charlie")
        whenever(tradingDefaults.optionsAccountId).thenReturn("echo")

        sut.setUserSelectedAccountId(SecurityType.OPTION, "alpha")
        sut.onSecurityType(SecurityType.OPTION)
        assertThat(sut.getCurrentAccountId(SecurityType.OPTION), `is`("alpha"))

        sut.onSecurityType(SecurityType.STOCK)
        assertThat(sut.getCurrentAccountId(SecurityType.STOCK), `is`("charlie"))
    }

    private fun createAccountDelegate(fallback: String): DefaultSelectedAccountDelegate =
        DefaultSelectedAccountDelegate(tradingDefaults, testAccounts, fallback)

    private val testAccounts = listOf(
        tradeAccount("alpha"),
        tradeAccount("bravo"),
        tradeAccount("charlie"),
        tradeAccount("delta")
    )

    private fun tradeAccount(_id: String): TradeAccount = object : TradeAccount {
        override val id = _id
        override val name = _id
        override val accountMode = AccountMode.UNKNOWN
        override val approvalLevel = 0
    }
}
