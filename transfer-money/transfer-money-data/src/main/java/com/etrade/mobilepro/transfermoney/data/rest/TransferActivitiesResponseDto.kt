package com.etrade.mobilepro.transfermoney.data.rest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TransferActivitiesResponseDto(
    @Json(name = "mobile_response")
    val mobileResponse: TransferActivitiesDto?
)

@JsonClass(generateAdapter = true)
data class TransferActivitiesDto(
    @Json(name = "views")
    val views: TransferActivitiesViewsDto?,
    @Json(name = "references")
    val references: List<TransferActivitiesReferenceDto?>
)

@JsonClass(generateAdapter = true)
data class TransferActivitiesViewsDto(
    @Json(name = "type")
    val viewType: String?,
    @Json(name = "data")
    val viewData: TransferActivityIdsDto?
)

@JsonClass(generateAdapter = true)
data class TransferActivityIdsDto(
    @Json(name = "transferActivityIds")
    val transferActivityIds: List<String?>
)

@JsonClass(generateAdapter = true)
data class TransferActivitiesReferenceDto(
    @Json(name = "type")
    val referenceType: String?,
    @Json(name = "data")
    val referenceData: List<AccountsAndTransferActivitiesDto?>
)

@JsonClass(generateAdapter = true)
data class AccountsAndTransferActivitiesDto(
    @Json(name = "fromAccountShortName")
    val fromAccountShortName: String?,
    @Json(name = "toAccountShortName")
    val toAccountShortName: String?,
    @Json(name = "accountUuid")
    val accountUuid: String?,
    @Json(name = "transferType")
    val transferType: String?,
    @Json(name = "status")
    val status: String?,
    @Json(name = "from")
    val from: String?,
    @Json(name = "to")
    val to: String?,
    @Json(name = "amount")
    val amount: String?,
    @Json(name = "date")
    val date: String?,
    @Json(name = "repeat")
    val repeat: String?,
    @Json(name = "confirmationNumber")
    val confirmationNumber: String?,
    @Json(name = "activityId")
    val reference: String?,
    @Json(name = "appName")
    val appName: String?,
    @Json(name = "paymentId")
    val id: String?,
    @Json(name = "memo")
    val memo: String?
)
