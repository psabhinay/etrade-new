package com.etrade.mobilepro.trade.form.api.input

interface ConditionalTradeFormInputId : TradeFormInputId {
    val order1: String
    val order2: String
    val order3: String
}
