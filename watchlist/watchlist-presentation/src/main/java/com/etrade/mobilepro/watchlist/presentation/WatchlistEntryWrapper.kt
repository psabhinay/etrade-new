package com.etrade.mobilepro.watchlist.presentation

import com.etrade.mobilepro.editlist.EditListItem
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.util.android.textutil.prepareSymbolSpan
import com.etrade.mobilepro.watchlistapi.WatchlistEntry

/**
 * Wraps [WatchlistEntry] with a [EditListItem] interface.
 *
 * @param entry wrapped entry
 */
data class WatchlistEntryWrapper(val entry: WatchlistEntry) : EditListItem {

    override val displayText: CharSequence
        get() = prepareSymbolSpan(entry.ticker, entry.typeCode ?: InstrumentType.UNKNOWN, entry.description ?: "")

    override val isChecked: Boolean = true
}
