package com.etrade.mobilepro.news.presentation.customview

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.annotation.StyleRes
import androidx.core.widget.TextViewCompat.setTextAppearance
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.news.presentation.databinding.ItemNewsSymbolBinding
import com.etrade.mobilepro.news.presentation.databinding.ViewTextNewsCellBinding
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.android.extension.layoutInflater
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager

class TextNewsCellView : RelativeLayout {

    constructor(ctx: Context) : this(ctx, null)
    constructor(ctx: Context, attributeSet: AttributeSet?) : this(ctx, attributeSet, 0)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle) {
        val layoutManager = FlexboxLayoutManager(context)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.flexWrap = FlexWrap.WRAP
        binding.symbolList.layoutManager = layoutManager
        binding.symbolList.adapter = symbolsAdapter
    }

    private val symbolsAdapter = NewsSymbolsAdapter(
        listener = {
            symbolSelectListener?.invoke(it)
        }
    )

    var symbolSelectListener: ((String) -> Unit)? = null

    private val binding = ViewTextNewsCellBinding.inflate(layoutInflater, this, true)

    private var isLarge = false
    private var wasRead = false

    fun toggleDisclosureArrowVisibility(isVisible: Boolean) {
        binding.arrowRightIv.visibility = if (isVisible) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    fun bindTo(item: NewsItem?, wasRead: Boolean? = null, titleMaxLinesLimit: Int? = null) {
        if (item is NewsItem.Text) {

            with(binding.newsTitleTv) {
                text = item.title.trim()
                titleMaxLinesLimit?.let {
                    maxLines = it
                    ellipsize = TextUtils.TruncateAt.END
                }
            }

            binding.dateTimeSourceTv.text = item.newsDateTimeSource

            symbolsAdapter.setItems(item.symbols, wasRead)

            with(binding) {
                contentDescription =
                    "${newsTitleTv.text}, ${dateTimeSourceTv.text}, ${item.symbols.joinToString().characterByCharacter()}"
            }

            toggleReadStatus(wasRead)
        }
    }

    fun toggleLargeFont(isLarge: Boolean) {
        this.isLarge = isLarge
        setStyle(isLarge, wasRead)
    }

    fun toggleReadStatus(wasRead: Boolean?) {
        wasRead?.let {
            this.wasRead = it
            setStyle(isLarge, it)
        }
    }

    private fun setStyle(isLarge: Boolean, wasRead: Boolean) {

        val titleStyleResId = titleStyle.resolve(isLarge, wasRead)
        val dateTimeStyleResId = dateTimeStyle.resolve(isLarge, wasRead)

        setTextAppearance(binding.newsTitleTv, titleStyleResId)
        setTextAppearance(binding.dateTimeSourceTv, dateTimeStyleResId)
        symbolsAdapter.toggleLargeFont(isLarge)
    }

    private companion object {

        val titleStyle = NewsCellFontStyle(
            regular = R.style.Account_Title,
            regularRead = R.style.Account_Title_Read,
            large = R.style.Account_Title_Larger,
            largeRead = R.style.Account_Title_Larger_Read,
        )

        val dateTimeStyle = NewsCellFontStyle(
            regular = R.style.TextAppearance_Etrade_Text_Regular_Medium,
            regularRead = R.style.TextAppearance_Etrade_Text_Regular_Medium_Read,
            large = R.style.TextAppearance_Etrade_Text_Regular_Medium_Plus,
            largeRead = R.style.TextAppearance_Etrade_Text_Regular_Medium_Plus_Read,
        )

        class NewsCellFontStyle(
            @StyleRes val regular: Int,
            @StyleRes val regularRead: Int,
            @StyleRes val large: Int,
            @StyleRes val largeRead: Int,
        ) {
            fun resolve(isLarge: Boolean, wasRead: Boolean): Int =
                when {
                    isLarge && wasRead -> largeRead
                    isLarge -> large
                    wasRead -> regularRead
                    else -> regular
                }
        }
    }
}

private class NewsSymbolsAdapter(private val listener: ((String) -> Unit)? = null) :
    RecyclerView.Adapter<NewsSymbolsAdapter.SymbolViewHolder>() {
    private val items = ArrayList<String>()
    private var wasRead: Boolean? = null
    private var isLargeFont: Boolean = false

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(newItems: List<String>, wasRead: Boolean?) {
        this.wasRead = wasRead
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun toggleLargeFont(isLargeFont: Boolean) {
        this.isLargeFont = isLargeFont
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SymbolViewHolder {
        return SymbolViewHolder(parent.viewBinding(ItemNewsSymbolBinding::inflate))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: SymbolViewHolder, position: Int) {
        holder.bindTo(items[position], wasRead, isLargeFont)
    }

    inner class SymbolViewHolder(private val binding: ItemNewsSymbolBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private var symbol: String? = null

        init {
            binding.root.setOnClickListener {
                symbol?.let { nonNullSymbol ->
                    listener?.invoke(nonNullSymbol)
                }
            }
        }

        fun bindTo(symbol: String, wasRead: Boolean?, isLargeFont: Boolean) {
            val titleStyle = if (wasRead == true) {
                if (isLargeFont) {
                    R.style.NewsSymbolTitle_Large_Read
                } else {
                    R.style.NewsSymbolTitle_Read
                }
            } else {
                if (isLargeFont) {
                    R.style.NewsSymbolTitle_Large
                } else {
                    R.style.NewsSymbolTitle
                }
            }
            setTextAppearance(binding.symbolTitle, titleStyle)
            this.symbol = symbol
            binding.symbolTitle.text = symbol
            binding.symbolTitle.contentDescription = symbol.characterByCharacter()
        }
    }
}
