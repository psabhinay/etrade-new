package com.etrade.mobilepro.backends.neo

import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.api.ServerMessage
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
open class BaseDataDto(
    @field:Json(name = "Messages")
    var messages: List<MessageDto?>? = null,

    @field:Json(name = "Status")
    var status: String? = null
) {
    fun extractError(): ServerError {
        return if (messages?.any { messageTypeFromString(it?.type) == ServerMessage.Type.ERROR } == true) {
            ServerError.Known(messages = getAllMessages())
        } else {
            ServerError.NoError
        }
    }

    fun getAllMessages(): List<ServerMessage> {
        val messages = messages ?: return emptyList()
        return messages.mapNotNull { it?.toServerMessage() }
    }

    fun getFirstMessageCode(): String? = getAllMessages().firstOrNull()?.code

    fun getFirstMessageText(): String? = getAllMessages().firstOrNull()?.text

    fun hasNoMessages(): Boolean = messages?.isEmpty() == true || messages == null
}
