package com.etrade.mobilepro.dynamic.form.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.fragment.databinding.DynamicFormFragmentFormBinding
import com.etrade.mobilepro.dynamic.form.ui.ActionListView
import com.etrade.mobilepro.dynamic.form.ui.FormView
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.viewmodel.FormViewModel
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.viewdelegate.AppMessageViewDelegate
import com.squareup.moshi.Moshi

private const val KEY_FORM_VALUES = "FormFragment.formValues"
private const val KEY_INPUTS_DISABLED = "FormFragment.inputsDisabled"

abstract class FormFragment(
    snackBarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.dynamic_form_fragment_form) {

    protected abstract val formViewModel: FormViewModel
    protected abstract val moshi: Moshi

    protected val snackBarUtil by lazy { snackBarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }) { view } }

    private val formValues by lazy { arguments?.formValues(moshi) }

    private val appMessageViewDelegate: AppMessageViewDelegate by lazy { AppMessageViewDelegate(formViewModel, snackBarUtil) }

    private val binding by viewBinding(DynamicFormFragmentFormBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initFormView(binding.formView)
        initActionListView(binding.actionListView)

        beforeFormFetch()
        formViewModel.fetch(
            savedInstanceState?.formValues(moshi) ?: formValues,
            formValues,
            arguments?.getStringArray(KEY_INPUTS_DISABLED)?.toList()
        )

        formViewModel.form.observe(viewLifecycleOwner, { onForm(it) })
        formViewModel.loadingIndicator.observe(viewLifecycleOwner, { onLoadingIndicator(it) })

        appMessageViewDelegate.observe(viewLifecycleOwner)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBundle(KEY_FORM_VALUES, createFormValuesArgument(moshi, formViewModel.peekFormValueSnapshot))
    }

    protected open fun initFormView(formView: FormView) = Unit

    protected open fun initActionListView(actionListView: ActionListView) = Unit

    protected open fun beforeFormFetch() = Unit

    private fun onForm(form: Form) {
        binding.apply {
            formView.render(form, viewLifecycleOwner)
            actionListView.render(form.actions, viewLifecycleOwner)
            contentView.visibility = View.VISIBLE
        }
    }

    private fun onLoadingIndicator(loading: Boolean) {
        if (loading) {
            binding.loadingIndicator.show()
        } else {
            binding.loadingIndicator.hide()
        }
    }

    protected fun scrollFormToTop() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            binding.formView.scrollToPosition(0)
        }
    }
}

fun Bundle.formValues(moshi: Moshi): InputValuesSnapshot? {
    return getBundle(KEY_FORM_VALUES)?.let {
        restoreInputValuesSnapshot(moshi, it)
    }
}
