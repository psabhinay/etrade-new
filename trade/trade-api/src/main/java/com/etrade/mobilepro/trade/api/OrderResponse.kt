package com.etrade.mobilepro.trade.api

import com.etrade.mobilepro.backends.api.HasWarnings

interface OrderResponse : HasWarnings {
    val orders: List<Order>
}

val OrderResponse.symbols: List<String>
    get() = orders.flatMap { it.symbols }
