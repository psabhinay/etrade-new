package com.etrade.mobilepro.marketscalendar.api.model

class EventsEconomic(
    val ecEcondetName: String?,
    val ecEcondetConcensus: String?,
    val ecEcondetActual: String?,
    val ecSeStartdate: String?,
    val ecEconDesc: String?,
    val ecEconDefinition: String?,
    val ecEconName: String?
)
