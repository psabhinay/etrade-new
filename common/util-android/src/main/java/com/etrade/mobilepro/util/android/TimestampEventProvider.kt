package com.etrade.mobilepro.util.android

import androidx.lifecycle.LiveData

data class TimestampEvent(val timestamp: Long)

interface TimestampEventProvider {
    val timestampEvents: LiveData<ConsumableLiveEvent<TimestampEvent>>
}
