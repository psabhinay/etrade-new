-if class com.etrade.mobilepro.order.details.dto.OrderDeleteRequestValueDto
-keepnames class com.etrade.mobilepro.order.details.dto.OrderDeleteRequestValueDto
-if class com.etrade.mobilepro.order.details.dto.OrderDeleteRequestValueDto
-keep class com.etrade.mobilepro.order.details.dto.OrderDeleteRequestValueDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.etrade.mobilepro.order.details.dto.OrderDeleteRequestValueDto
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.etrade.mobilepro.order.details.dto.OrderDeleteRequestValueDto
-keepclassmembers class com.etrade.mobilepro.order.details.dto.OrderDeleteRequestValueDto {
    public synthetic <init>(java.lang.String,java.lang.String,java.lang.String,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
