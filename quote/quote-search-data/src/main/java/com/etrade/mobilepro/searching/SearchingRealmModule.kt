package com.etrade.mobilepro.searching

import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
class SearchingRealmModule
