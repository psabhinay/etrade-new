package com.etrade.mobilepro.iraaccount.api.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.iraaccount.api.IraContributionDetails

interface IraContributionDetailRepo {
    suspend fun getIraContributionDetails(iraAccountNumber: String, instrumentNumber: String): ETResult<IraContributionDetails>
}
