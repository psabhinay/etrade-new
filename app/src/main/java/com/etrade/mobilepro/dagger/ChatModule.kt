package com.etrade.mobilepro.dagger

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.chat.DefaultChatRouter
import com.etrade.mobilepro.chat.api.ChatParamsModel
import com.etrade.mobilepro.chat.api.message.repo.ChatMessagesRepo
import com.etrade.mobilepro.chat.api.setup.repo.ChatSetupRepo
import com.etrade.mobilepro.chat.data.messages.dto.ChatGetMessagesRequestDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatParamsRequestDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatPostMessageRequestDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatTerminateRequestDto
import com.etrade.mobilepro.chat.data.messages.repo.ChatMessagesRepoImpl
import com.etrade.mobilepro.chat.data.messages.repo.ChatRequestBodyManager
import com.etrade.mobilepro.chat.data.messages.repo.ChatRequestBodyManagerImpl
import com.etrade.mobilepro.chat.data.setup.repo.ChatSetupRepoImpl
import com.etrade.mobilepro.chat.presentation.ChatRouter
import com.etrade.mobilepro.chat.presentation.message.ChatMessagesViewModelFactory
import com.etrade.mobilepro.chat.presentation.message.ChatMessagesViewModelFactoryImpl
import com.etrade.mobilepro.chat.presentation.setup.ChatSetupViewModel
import com.etrade.mobilepro.chat.usecase.ChatRequestUseCase
import com.etrade.mobilepro.chat.usecase.ChatRequestUseCaseImpl
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.util.JsonSerializer
import com.etrade.mobilepro.util.Serializer
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
interface ChatModule {

    @Binds
    @IntoMap
    @ViewModelKey(ChatSetupViewModel::class)
    fun bindChatSetupViewModel(viewModel: ChatSetupViewModel): ViewModel

    @Binds
    fun bindChatSetupRepo(repo: ChatSetupRepoImpl): ChatSetupRepo

    @Binds
    fun bindChatRequestUseCase(repo: ChatRequestUseCaseImpl): ChatRequestUseCase

    @Binds
    fun bindChatMessagesRepo(repo: ChatMessagesRepoImpl): ChatMessagesRepo

    @Binds
    fun chatMessageViewModelFactory(factory: ChatMessagesViewModelFactoryImpl): ChatMessagesViewModelFactory

    @Binds
    fun bindChatRouter(chatRouterImpl: DefaultChatRouter): ChatRouter

    @Binds
    fun bindChatRequestBodyManager(chatRequestBodyManagerImpl: ChatRequestBodyManagerImpl): ChatRequestBodyManager

    companion object {

        @Provides
        fun provideChatParamsModel() = ChatParamsModel()

        @Provides
        fun providePostMessageConverter(moshi: Moshi): Serializer<ChatPostMessageRequestDto> =
            JsonSerializer<ChatPostMessageRequestDto>(moshi).apply { init<ChatPostMessageRequestDto>() }

        @Provides
        fun provideTerminateChatConverter(moshi: Moshi): Serializer<ChatTerminateRequestDto> =
            JsonSerializer<ChatTerminateRequestDto>(moshi).apply { init<ChatTerminateRequestDto>() }

        @Provides
        fun provideRetrieveMessagesConverter(moshi: Moshi): Serializer<ChatGetMessagesRequestDto> =
            JsonSerializer<ChatGetMessagesRequestDto>(moshi).apply { init<ChatGetMessagesRequestDto>() }

        @Provides
        fun provideChatParamsConverter(moshi: Moshi): Serializer<ChatParamsRequestDto> =
            JsonSerializer<ChatParamsRequestDto>(moshi).apply { init<ChatParamsRequestDto>() }
    }
}
