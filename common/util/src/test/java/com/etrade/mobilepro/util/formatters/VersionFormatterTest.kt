package com.etrade.mobilepro.util.formatters

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.Test

class VersionFormatterTest {

    @Test
    fun `majorMinorVersion parses valid major minor versions`() {
        assertThat("9.3".majorMinorVersion, `is`("9.3"))
        assertThat("9.3.4".majorMinorVersion, `is`("9.3"))
        assertThat("9.3.4-extra".majorMinorVersion, `is`("9.3"))
    }

    @Test
    fun `majorMinorVersion returns empty for invalid versions`() {
        assertThat("9".majorMinorVersion, `is`(""))
        assertThat("foo.bar.extra".majorMinorVersion, `is`(""))
        assertThat("9.bar".majorMinorVersion, `is`(""))
        assertThat("".majorMinorVersion, `is`(""))
    }
}
