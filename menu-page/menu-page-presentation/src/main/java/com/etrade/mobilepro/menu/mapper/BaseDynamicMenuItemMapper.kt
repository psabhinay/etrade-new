package com.etrade.mobilepro.menu.mapper

import android.content.Context
import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menu.NewMenuItemValidator
import com.etrade.mobilepro.menu.builder.MenuPageBuilder
import com.etrade.mobilepro.menu.builder.TextMenuItemBuilder
import com.etrade.mobilepro.menu.builder.menuPage
import com.etrade.mobilepro.menu.repo.DynamicMenuItem
import com.etrade.mobilepro.menu.repo.MenuItemType
import org.slf4j.LoggerFactory

open class BaseDynamicMenuItemMapper(val context: Context, val newMenuItemValidator: NewMenuItemValidator) : DynamicMenuItemMapper {

    private val logger by lazy { LoggerFactory.getLogger(javaClass) }

    override fun mapDynamicMenuItemsToPage(dynamicMenuItems: List<DynamicMenuItem>): List<MenuItem> {
        return menuPage(newMenuItemValidator) {
            dynamicMenuItems.forEach { mapDynamicItem(it, this) }
        }
    }

    protected fun TextMenuItemBuilder.dynamicSetup(dynamicMenuItem: DynamicMenuItem) {
        text = dynamicMenuItem.label
        subtext = dynamicMenuItem.subLabel
        icon = dynamicMenuItem.iconRes
        localyticsTag = dynamicMenuItem.localyticsTag
        dynamicMenuItem.ctaDeepLink?.let { appAction(it) }
        newMenuItemKey(dynamicMenuItem.ctaDeepLink)
    }

    open fun mapDynamicItem(dynamicMenuItem: DynamicMenuItem, builder: MenuPageBuilder) {
        builder.apply {
            when (dynamicMenuItem.menuItemType) {
                MenuItemType.Divider -> divider()
                MenuItemType.MenuItemView -> textItem { dynamicSetup(dynamicMenuItem) }
                MenuItemType.MenuItemViewWithArrow -> textWithArrowItem { dynamicSetup(dynamicMenuItem) }
                MenuItemType.MenuItemViewWithArrowAndSubtitle -> textWithArrowAndSubtitle { dynamicSetup(dynamicMenuItem) }
                MenuItemType.MenuItemViewWithArrowAndSubtitle_Bg_Purple -> highlight { dynamicSetup(dynamicMenuItem) }
                MenuItemType.MenuItemViewWithCount -> count { dynamicSetup(dynamicMenuItem) }
                else -> {
                    logger.error("Unknown menu item type ${dynamicMenuItem.menuItemType}")
                }
            }
        }
    }

    private val DynamicMenuItem.iconRes: Int?
        get() {
            if (icon.isNullOrBlank()) return null
            val id = context.resources.getIdentifier(icon, "drawable", context.packageName)
            if (id != 0) {
                return id
            }
            logger.error("Invalid menu item icon name! [$icon]")
            return null
        }
}
