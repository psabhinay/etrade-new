package com.etrade.mobilepro.messagecenter.api.model

data class ServiceMessage(
    val accountId: String = "",
    val subject: String = "",
    val payload: Map<String, String> = emptyMap(),
    val messages: List<String> = emptyList(),
    val errorDescription: String? = null
) {
    companion object {
        const val ACCOUNT_KEY = "Account"
    }
}
