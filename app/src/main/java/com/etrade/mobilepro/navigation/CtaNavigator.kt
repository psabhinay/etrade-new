package com.etrade.mobilepro.navigation

import android.net.Uri
import androidx.navigation.NavDirections
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import java.util.Locale

const val ACCOUNT_UUID_QUERY_PARAMETER = "uuid"

private const val ACCOUNTS = "account"
private const val OVERVIEW = "overview"
private const val PORTFOLIO = "portfolio"
private const val HOLDINGS = "holdings"
private const val ORDERS = "orders"
private const val MARKETS = "markets"
private const val MOVERS = "movers"

private const val ACCOUNT_ID_PARAM = 0
private const val TAB_PARAM = 1
private const val TAB_ARGS_PARAM = 2
private const val ORDER_STATUS_PARAM = 2

// Sample url "etrade://account/1234567/portfolio"
// sample orders url "etrade://account/1234567/orders/saved"
// sample unauth url - etrade://markets/movers
// currently very rough implementation
fun getTabDestination(ctaString: String): CtaEvent? =
    with(Uri.parse(ctaString)) {
        when (host) {
            ACCOUNTS -> {
                val tabTitle = pathSegments[TAB_PARAM]
                val tabArgs = pathSegments.getOrNull(TAB_ARGS_PARAM)

                when (tabTitle) {
                    ORDERS, PORTFOLIO -> {
                        CtaEvent.AccountsTabNavigation(pathSegments[ACCOUNT_ID_PARAM], tabTitle, tabArgs)
                    }
                    else -> null
                }
            }
            else -> getNavDirections(ctaString)
        }
    }

fun getNavDirections(ctaString: String, isBankAccount: Boolean = false): CtaEvent? =
    with(Uri.parse(ctaString)) {
        when (host) {
            ACCOUNTS -> toAccountsEvent(this, isBankAccount)
            MARKETS -> toMarketsEvent(this)
            else -> null
        }
    }

private fun toMarketsEvent(uri: Uri): CtaEvent.NavDirectionsNavigation? {
    return when (uri.pathSegments.getOrNull(ACCOUNT_ID_PARAM)) {
        MOVERS -> {
            CtaEvent.NavDirectionsNavigation(MainGraphDirections.openMarketMovers())
        }
        else -> null
    }
}

private fun toAccountsEvent(uri: Uri, isBankAccount: Boolean): CtaEvent? {
    return when (uri.pathSegments[TAB_PARAM]) {
        OVERVIEW, PORTFOLIO, HOLDINGS -> toAccountsNavDirections(uri, isBankAccount)
        ORDERS -> getBottomSheetDirections(uri)
        else -> null
    }
}

private fun toAccountsNavDirections(uri: Uri, isBankAccount: Boolean): CtaEvent {
    val accountId = uri.pathSegments[ACCOUNT_ID_PARAM]
    val accountUuid = uri.getQueryParameter(ACCOUNT_UUID_QUERY_PARAMETER).orEmpty()
    return CtaEvent.NavDirectionsNavigation(
        if (isBankAccount) {
            MainGraphDirections.actionHomeToBank(accountUuid, accountId)
        } else {
            MainGraphDirections.actionHomeToAccountTabs(
                accountId = accountId,
                accountUuid = accountUuid,
                tabTitle = uri.pathSegments.getOrNull(TAB_PARAM),
                tabArgs = uri.pathSegments.getOrNull(TAB_ARGS_PARAM)
            )
        }
    )
}

private fun getBottomSheetDirections(uri: Uri): CtaEvent.LaunchOrdersBottomSheet {
    val pathSegments = uri.pathSegments
    val orderStatus = pathSegments[ORDER_STATUS_PARAM].uppercase(Locale.getDefault())
    return CtaEvent.LaunchOrdersBottomSheet(pathSegments[ACCOUNT_ID_PARAM], orderStatus)
}

fun portfolioTabDirections(accountId: String, accountUuid: String): NavDirections {
    return MainGraphDirections.actionHomeToAccountTabs(accountId, accountUuid, PORTFOLIO)
}

fun appendUuidQueryParam(appUrl: String, uuid: String?): String {
    return if (appUrl.contains("uuid=")) {
        appUrl
    } else {
        Uri.parse(appUrl)
            .buildUpon()
            .appendQueryParameter(ACCOUNT_UUID_QUERY_PARAMETER, uuid)
            .build()
            .toString()
    }
}
