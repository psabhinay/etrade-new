package com.etrade.mobilepro.checkdeposit.deposit.service

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class DepositCaptureServiceAgreementResponseDto(
    @Json(name = "CaptureStatus")
    val captureStatus: Boolean?
) : BaseDataDto()
