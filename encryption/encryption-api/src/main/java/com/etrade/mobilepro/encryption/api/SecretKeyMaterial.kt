package com.etrade.mobilepro.encryption.api

import javax.crypto.SecretKey

class SecretKeyMaterial(
    val secretKey: SecretKey,
    val isUserAuthenticationRequired: Boolean
)
