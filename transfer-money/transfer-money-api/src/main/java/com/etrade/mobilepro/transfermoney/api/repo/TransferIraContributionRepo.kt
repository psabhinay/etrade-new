package com.etrade.mobilepro.transfermoney.api.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.transfermoney.api.TransferIraContribution

interface TransferIraContributionRepo {
    suspend fun getIraContributionInfo(fromAccountId: String): ETResult<TransferIraContribution>
}
