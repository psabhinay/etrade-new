package com.etrade.mobilepro.util

interface MutualConverter<K, V> {
    fun toKey(value: V): K?
    fun toValue(key: K): V?
}

class MapConverter<K, V>(private val keyToValue: Map<K, V>) : MutualConverter<K, V> {

    private val valueToKey: Map<V, K> = keyToValue.inverse()

    override fun toKey(value: V): K? = valueToKey[value]

    override fun toValue(key: K): V? = keyToValue[key]
}
