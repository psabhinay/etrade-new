package com.etrade.mobilepro.barchartview

import com.github.mikephil.charting.formatter.ValueFormatter

internal class ValueFormatterWrapper(private val valueFormatter: ((Float) -> String?)?) : ValueFormatter() {
    override fun getFormattedValue(value: Float): String {
        return valueFormatter?.invoke(value) ?: ""
    }
}
