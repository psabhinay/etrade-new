package com.etrade.mobilepro.personalnotifications.adapter.holder

import android.view.View
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.personalnotifications.R
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.Localytics
import com.etrade.mobilepro.personalnotifications.databinding.ViewBannerNotificationItemBinding

class BannerNotificationViewHolder(
    val binding: ViewBannerNotificationItemBinding,
    val clickListener: ((PersonalNotification) -> Unit)
) : RecyclerView.ViewHolder(binding.root) {
    private lateinit var item: PersonalNotification

    fun bind(
        item: Localytics.Banner,
        imageLoader: ImageLoader
    ) {
        this.item = item
        binding.root.setOnClickListener { clickListener.invoke(item) }

        // Icon
        binding.bannerIcon.visibility = getVisibility(item.message.icon && item.message.hasThumbnail)
        imageLoader.show(item.message.thumbnailUrl, binding.bannerIcon)

        // Title
        binding.title.visibility = getVisibility(!item.message.headLine.isNullOrEmpty())
        item.message.headLine?.let {
            binding.title.text = it
        }

        // Sub Title
        binding.subTitle.text = item.message.bodyText
        TextViewCompat.setTextAppearance(
            binding.subTitle,
            if (item.message.headLine.isNullOrEmpty()) {
                R.style.TextAppearance_Etrade_Text_Regular_Large
            } else {
                R.style.TextAppearance_Etrade_Text_Regular_Medium
            }
        )

        // Arrow
        binding.arrow.visibility = getVisibility(item.message.arrow)
    }

    private fun getVisibility(show: Boolean) = if (show) {
        View.VISIBLE
    } else {
        View.GONE
    }
}
