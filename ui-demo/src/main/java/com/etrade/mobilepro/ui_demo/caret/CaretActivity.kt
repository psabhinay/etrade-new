package com.etrade.mobilepro.ui_demo.caret

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.etrade.mobilepro.ui_demo.R
import com.etrade.mobilepro.ui_demo.databinding.ActivityCaretBinding
import com.etrade.mobilepro.ui_demo.databinding.FragmentSimplePageBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.google.android.material.tabs.TabLayoutMediator
import javax.inject.Inject

class CaretActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityCaretBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)

        val adapter = DemoViewPagerAdapter(this)
        val tabLayout = binding.tabContainer.tabLayout

        binding.viewPager.adapter = adapter
        TabLayoutMediator(tabLayout, binding.viewPager) { tab, position ->
            tab.text = adapter.getPageTitle(position)
        }.attach()
    }
}

private class DemoViewPagerAdapter(fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {

    private val titles = listOf(
        "Overview",
        "Portfolio",
        "Balances",
        "Orders",
        "Transactions",
        "Documents"
    )

    fun getPageTitle(position: Int): String = titles[position]

    override fun getItemCount() = titles.size

    override fun createFragment(position: Int) = SimplePageFragment.create(getPageTitle(position))
}

class SimplePageFragment @Inject constructor(): Fragment(R.layout.fragment_simple_page) {

    private val pageName
        get() = arguments?.getString(ARG_PAGE_NAME)

    private val binding by viewBinding(FragmentSimplePageBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.pageName.text = pageName
    }

    companion object {
        const val ARG_PAGE_NAME = "pageName"

        fun create(pageName: String): SimplePageFragment = SimplePageFragment().apply {
            arguments = bundleOf(ARG_PAGE_NAME to pageName)
        }
    }
}
