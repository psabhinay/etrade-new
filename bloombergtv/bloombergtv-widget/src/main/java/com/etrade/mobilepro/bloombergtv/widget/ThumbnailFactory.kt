package com.etrade.mobilepro.bloombergtv.widget

import android.graphics.Bitmap
import android.graphics.SurfaceTexture
import android.media.MediaPlayer
import android.os.Handler
import android.view.PixelCopy
import android.view.Surface
import kotlinx.coroutines.delay
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

private const val MAX_RETRIES = 3
private const val DELAY_BEFORE_THUMBNAILING = 300L

class ThumbnailFactory(
    private val handler: Handler,
    private val mediaPlayer: MediaPlayer,
    private val onThumbnailCreated: (Bitmap) -> Unit,
    private val onThumbnailingFailed: () -> Unit
) {
    suspend fun createThumbnail(
        surfaceTexture: SurfaceTexture?,
        width: Int,
        height: Int
    ) {
        val surface = Surface(surfaceTexture)
        mediaPlayer.setSurface(surface)
        mediaPlayer.setVolume(0f, 0f)
        mediaPlayer.start()
        mediaPlayer.pause()
        var tries = 0
        val bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        while (tries < MAX_RETRIES + 1) {
            if (tryCaptureThumbnail(bmp, surface, handler) == PixelCopy.SUCCESS) {
                onThumbnailCreated(bmp)
                tries = 0
                break
            } else {
                mediaPlayer.start()
                delay(DELAY_BEFORE_THUMBNAILING)
                mediaPlayer.pause()
                tries++
            }
        }
        if (tries > MAX_RETRIES) {
            onThumbnailingFailed()
        }
    }

    private suspend fun tryCaptureThumbnail(bmp: Bitmap, src: Surface, handler: Handler): Int {
        val latch = Mutex(locked = true)
        var result = 0
        PixelCopy.request(
            src, bmp,
            PixelCopy.OnPixelCopyFinishedListener { res ->
                result = res
                latch.unlock()
            },
            handler
        )
        return latch.withLock { result }
    }
}
