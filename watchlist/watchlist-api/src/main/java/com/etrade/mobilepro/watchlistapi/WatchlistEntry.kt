package com.etrade.mobilepro.watchlistapi

import com.etrade.mobilepro.instrument.InstrumentType
import java.math.BigDecimal

data class WatchlistEntry(
    val ticker: String,
    val description: String? = null,
    val entryID: String? = null,
    val underlyingSymbol: String? = null,
    val typeCode: InstrumentType? = null,
    val lastTradeTimeStamp: Long? = null,
    val lastTradeTime: String? = null,
    val lastPrice: BigDecimal? = null,
    val change: BigDecimal? = null,
    val changePercent: BigDecimal? = null,
    val volume: BigDecimal? = null,
    val bid: BigDecimal? = null,
    val ask: BigDecimal? = null,
    val dayHigh: BigDecimal? = null,
    val dayLow: BigDecimal? = null,
    val week52High: BigDecimal? = null,
    val week52Low: BigDecimal? = null,
    val priceEarningsRatio: BigDecimal? = null,
    val earningsPerShare: BigDecimal? = null,
    var watchlistId: String? = null
)
