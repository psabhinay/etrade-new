package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.ETApplication
import com.etrade.mobilepro.balance.dagger.BalancesModule
import com.etrade.mobilepro.bloombergtv.BloombergtvModule
import com.etrade.mobilepro.feedback.FeedbackModule
import com.etrade.mobilepro.login.dagger.LoginBinderModule
import com.etrade.mobilepro.news.NewsModule
import com.etrade.mobilepro.quotelookup.SearchModule
import com.etrade.mobilepro.transactions.TransactionsModule
import com.etrade.mobilepro.watchlist.WatchlistModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP CLASSES IN THIS FILE ORGANIZED ALPHABETICALLY */

@Singleton
@Component(
    modules = [
        AccountsModule::class,
        ActivityModule::class,
        AlertsModule::class,
        AndroidInjectionModule::class,
        AppConfigModule::class,
        ApplicationModule::class,
        AppStartModule::class,
        AppWidgetModule::class,
        BalancesModule::class,
        BarcodeModule::class,
        BillPayModule::class,
        BiometricModule::class,
        BloombergtvModule::class,
        BroadcastModule::class,
        ChartIqModule::class,
        ChartModule::class,
        ChatModule::class,
        CheckDepositModule::class,
        ColorsModule::class,
        DebugMenuModule::class,
        DocumentsModule::class,
        DynamicUiModule::class,
        EncryptionModule::class,
        EventTrackerModule::class,
        FeatureFlagsModule::class,
        FeedbackModule::class,
        FormatterModule::class,
        FormsAndApplicationsModule::class,
        FragmentModule::class,
        InboxMessagesModule::class,
        IraAccountModule::class,
        LandingsModule::class,
        LearnModule::class,
        LocalyticsModule::class,
        LoginBinderModule::class,
        MainActivityHelpersModule::class,
        MarketModule::class,
        MarketOnDemandModule::class,
        MarketsCalendarModule::class,
        MarketScheduleModule::class,
        MenuPageModule::class,
        MessageCenterModule::class,
        NetworkModule::class,
        NewsModule::class,
        OptionsChainModule::class,
        OrdersModule::class,
        PersonalNotificationsModule::class,
        PdfModule::class,
        PortfolioModule::class,
        PortfolioSnapshotModule::class,
        PositionHoldingsModule::class,
        PreferencesModule::class,
        PushNotificationsModule::class,
        QuoteModule::class,
        RestorableModule::class,
        SecurityModule::class,
        ServiceModule::class,
        SessionModule::class,
        ScreenersModule::class,
        ScreenersJsonGeneratorsModule::class,
        SearchModule::class,
        StockPlanModule::class,
        StreamingModule::class,
        SymbolChartModule::class,
        TransactionsModule::class,
        TransferMoneyModule::class,
        UserSubscriptionModule::class,
        UtilAndroidModule::class,
        UtilModule::class,
        WalkthroughModule::class,
        WatchlistModule::class,
        Watchlist2Module::class
    ]
)
interface ApplicationComponent {

    fun inject(application: ETApplication)
}
