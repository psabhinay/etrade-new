package com.etrade.mobilepro.tier

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET

class CustomerTierRepoImpl(
    private val customerTierService: CustomerTierService
) : CustomerTierRepo {

    override suspend fun getTier(): ETResult<CustomerTier> = runCatchingET {
        customerTierService.getTier().doIfSuccessfulOrThrow {
            CustomerTier.fromString(requireNotNull(it.detail?.tier))
        }
    }
}
