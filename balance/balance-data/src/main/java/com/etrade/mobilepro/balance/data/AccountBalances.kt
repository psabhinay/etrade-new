package com.etrade.mobilepro.balance.data

import com.etrade.mobilepro.balance.data.dto.BalanceDto

/**
 * Model that contains Account balance details.
 */
data class AccountBalances(val balances: BalanceDto)
