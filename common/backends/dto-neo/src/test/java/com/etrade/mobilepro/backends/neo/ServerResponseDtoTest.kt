package com.etrade.mobilepro.backends.neo

import com.etrade.mobilepro.backends.api.HasWarnings
import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.backends.api.ServerMessageException
import com.etrade.mobilepro.backends.api.ServerResponseException
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert
import org.hamcrest.collection.IsCollectionWithSize
import org.hamcrest.collection.IsEmptyCollection
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class ServerResponseDtoTest {

    @Test
    fun `toResponse throws error, when has no data`() {
        val response = getResponseFromFile("sample_response_error.json")
        val exception = assertThrows<ServerMessageException> {
            response.toResponse { _, warnings ->
                object : HasWarnings {
                    override val warnings: Collection<ServerMessage> = warnings
                }
            }
        }
        assertEquals(1, exception.messages.size)

        val message = exception.messages.first()
        assertEquals(0, message.priority)
        assertEquals("Not available", message.text)
        assertEquals(ServerMessage.Type.ERROR, message.type)
        assertNull(message.code)
    }

    @Test
    fun `toResponse throws error, when data has errors`() {
        val response = getResponseWithSeveralMessages()
        val exception = assertThrows<ServerMessageException> {
            response.toResponse { _, warnings ->
                object : HasWarnings {
                    override val warnings: Collection<ServerMessage> = warnings
                }
            }
        }
        assertEquals(2, exception.messages.size)

        exception.messages.forEachIndexed { index, message ->
            assertEquals(0, message.priority)
            assertEquals("This is text ${index + 1}", message.text)
            assertEquals(ServerMessage.Type.ERROR, message.type)
            assertEquals("${9004 + index}", message.code)
        }
    }

    @Test
    fun `toResponse passes warnings, when data has warnings`() {
        val response = getResponseWithWarnMessages().toResponse { _, warnings ->
            object : HasWarnings {
                override val warnings: Collection<ServerMessage> = warnings
            }
        }

        val warnings = response.warnings
        assertEquals(1, warnings.size)

        val message = warnings.first()
        assertEquals(0, message.priority)
        assertEquals("This fund is not available for purchase through E*TRADE Securities.", message.text)
        assertEquals(ServerMessage.Type.WARN, message.type)
        assertEquals("9004", message.code)
    }

    @Test
    fun `has messages, basic contents`() {
        val response = getResponseWithOneMessage()
        assertTrue(response.data?.getAllMessages()?.isNotEmpty() == true)
        assertEquals(response.smUser, "XXXX")
        assertEquals(response.hostName, "43w603m3")
        assertEquals(response.sysdate, "1553821603919")
        assertEquals(response.data?.sample?.symbol, "AAPL")
    }

    // ------ one message

    @Test
    fun `one message, message contents`() {
        val response = getResponseWithOneMessage()
        assertEquals(response.data?.getAllMessages()?.size, 1)
        val message = response.data?.getAllMessages()?.get(0)
        assertEquals(message?.code, "9004")
        assertEquals(message?.type, ServerMessage.Type.ERROR)
        assertEquals(message?.text, "This fund is not available for purchase through E*TRADE Securities.")
    }

    @Test
    fun `one message, extract error`() {
        val response = getResponseWithOneMessage()
        val error = response.extractError()
        assertTrue(error is ServerError.Known)
        assertEquals((error as ServerError.Known).messages.size, 1)
        assertEquals(error.messages[0].code, "9004")
    }

    @Test
    fun `one message, getFirstMessageCode`() {
        val response = getResponseWithOneMessage()
        val code = response.data?.getFirstMessageCode()
        assertEquals(code, "9004")
    }

    @Test
    fun `one message, throws`() {
        val response = getResponseWithOneMessage()
        assertThrows(ServerResponseException::class.java) {
            response.throwIfHasErrors()
        }
    }

    // ------ null messages

    @Test
    fun `null message, message contents`() {
        val response = getResponseWithNullMessages()
        assertTrue(response.data?.getAllMessages()?.isEmpty() == true)
    }

    @Test
    fun `null message, extract error`() {
        val response = getResponseWithNullMessages()
        val error = response.extractError()
        assertEquals(error, ServerError.NoError)
    }

    @Test
    fun `null message, getFirstMessageCode`() {
        val response = getResponseWithNullMessages()
        val code = response.data?.getFirstMessageCode()
        assertNull(code)
    }

    @Test
    fun `null message, not throws`() {
        val response = getResponseWithNullMessages()
        response.throwIfHasErrors()
    }

    // ------ empty messages

    @Test
    fun `empty message, message contents`() {
        val response = getResponseWithEmptyMessages()
        MatcherAssert.assertThat(response.data?.getAllMessages(), IsEmptyCollection())
    }

    @Test
    fun `empty message, extract error`() {
        val response = getResponseWithEmptyMessages()
        val error = response.extractError()
        assertEquals(error, ServerError.NoError)
    }

    @Test
    fun `empty message, getFirstMessageCode`() {
        val response = getResponseWithEmptyMessages()
        val code = response.data?.getFirstMessageCode()
        assertNull(code)
    }

    @Test
    fun `empty message, not throws`() {
        val response = getResponseWithEmptyMessages()
        response.throwIfHasErrors()
    }

    // ------ several messages

    @Test
    fun `several messages, message contents`() {
        val response = getResponseWithSeveralMessages()
        MatcherAssert.assertThat(response.data?.getAllMessages(), IsCollectionWithSize(equalTo(2)))
    }

    @Test
    fun `several messages, extract error`() {
        val response = getResponseWithSeveralMessages()
        val error = response.extractError()
        assertTrue(error is ServerError.Known)
        assertFalse(error is ServerError.NoError)
        assertTrue((error as ServerError.Known).messages.size == 2)
        assertEquals(error.messages[0].code, "9004")
        assertEquals(error.messages[1].code, "9005")
    }

    @Test
    fun `several messages, getFirstMessageCode`() {
        val response = getResponseWithSeveralMessages()
        val code = response.data?.getFirstMessageCode()
        assertEquals(code, "9004")
    }

    @Test
    fun `several messages, throws`() {
        val response = getResponseWithSeveralMessages()
        assertThrows(ServerResponseException::class.java) {
            response.throwIfHasErrors()
        }
    }

    // ------ warn messages
    @Test
    fun `warn messages, extract error`() {
        val response = getResponseWithWarnMessages()
        val error = response.extractError()
        assertTrue(error is ServerError.NoError)
        val code = response.data?.getFirstMessageCode()
        assertEquals(code, "9004")
    }

    private fun getResponseWithOneMessage(): ServerResponseDto<SampleResponseContainer> = getResponseFromFile("sample_response.json")

    private fun getResponseWithNullMessages(): ServerResponseDto<SampleResponseContainer> = getResponseFromFile("sample_response_null_messages.json")

    private fun getResponseWithEmptyMessages(): ServerResponseDto<SampleResponseContainer> = getResponseFromFile("sample_response_empty_messages.json")

    private fun getResponseWithSeveralMessages(): ServerResponseDto<SampleResponseContainer> = getResponseFromFile("sample_response_several_messages.json")

    private fun getResponseWithWarnMessages(): ServerResponseDto<SampleResponseContainer> = getResponseFromFile("sample_response_warn_message.json")

    private fun getResponseFromFile(filename: String): ServerResponseDto<SampleResponseContainer> {
        return SampleResponseContainer::class.getObjectFromJson(filename, ServerResponseDto::class.java, SampleResponseContainer::class.java)
    }
}

@JsonClass(generateAdapter = true)
data class SampleResponseContainer(
    @Json(name = "SampleResponse")
    val sample: SampleDto?
) : BaseDataDto()

@JsonClass(generateAdapter = true)
data class SampleDto(
    @Json(name = "symbol")
    val symbol: String?
)
