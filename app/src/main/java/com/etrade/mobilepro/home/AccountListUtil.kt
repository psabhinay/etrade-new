package com.etrade.mobilepro.home

import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.accounts.util.android.view.AccountViewType
import com.etrade.mobilepro.accounts.util.android.view.AccountsSpinnerModel
import com.etrade.mobilepro.accounts.util.android.view.getAccountViewType

internal tailrec fun List<AccountsSpinnerModel>.dividerPosition(candidate: Int = 0): Int? {
    if (candidate >= size) {
        return null
    }
    val viewType = get(candidate).accountViewType
    return if (viewType != AccountViewType.COMPLETE_MULTIPLE_ACCOUNT_VIEW && viewType != AccountViewType.ALL_BROKERAGE_ACCOUNT_VIEW) {
        (candidate - 1).takeUnless { it < 0 }
    } else {
        dividerPosition(candidate + 1)
    }
}

internal fun List<Account>.toAccountDropDownModel() = map {
    AccountsSpinnerModel(it.accountShortName, it.getAccountViewType(), it)
}
