package com.etrade.mobilepro.trade.api

import com.etrade.mobilepro.orders.api.TransactionType
import java.math.BigDecimal

interface Order {
    val account: String
    val legs: List<OrderLeg>
}

val Order.action: TransactionType
    get() = legs.firstOrNull()?.action ?: TransactionType.UNKNOWN

val Order.quantity: BigDecimal?
    get() = legs.firstOrNull()?.quantity

val Order.symbols: List<String>
    get() = legs.map { it.symbol }

val Order.ticker: String
    get() = legs.first().symbol
