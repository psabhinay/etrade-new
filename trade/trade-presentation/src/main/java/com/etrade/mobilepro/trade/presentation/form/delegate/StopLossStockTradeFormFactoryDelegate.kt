package com.etrade.mobilepro.trade.presentation.form.delegate

import android.content.Context
import android.content.res.Resources
import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.dynamic.form.api.FormBuilder
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.livedata.combineWithTwo
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.form.api.SecurityType.STOCK
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.delegate.advancedorder.StopLossFormInput
import com.etrade.mobilepro.trade.presentation.form.delegate.advancedorder.StopLossFormParams
import com.etrade.mobilepro.trade.presentation.form.delegate.advancedorder.hiddenAllOrNoneRule
import com.etrade.mobilepro.trade.presentation.form.delegate.advancedorder.stopLossFormHiddenInputAndLegDisplayText
import com.etrade.mobilepro.trade.presentation.form.delegate.advancedorder.stopLossOrderForm
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.FormFactoryDelegateDependencies
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.resetPricesOnVisibilityChange
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter

internal class StopLossStockTradeFormFactoryDelegate(
    private val context: Context,
    private val delegateDependencies: FormFactoryDelegateDependencies,
    private val inputId: StopLossStockTradeFormInputId,
    searchIntentFactory: SearchIntentFactory,
    tradeFormTypeUpdateDelegate: TradeFormTypeUpdateDelegate<StockTradeFormInputId>
) : StockTradeFormFactoryDelegate(
    context = context,
    inputId = inputId,
    dependencies = delegateDependencies,
    searchIntentFactory = searchIntentFactory,
    tradeFormTypeUpdateDelegate = tradeFormTypeUpdateDelegate
) {

    override val formType: TradeFormType = TradeFormType(STOCK, AdvancedTradeType.StopLoss)

    override fun hiddenInput(formBuilder: FormBuilder) {
        inputId.stopLossInputId.apply {
            formBuilder.hidden(action)
            formBuilder.hidden(quantity)
            formBuilder.hidden(symbol)
            formBuilder.hidden(allOrNone)
        }
        formBuilder.hidden(inputId.walkthroughFormCompleted)
    }

    override fun advancedOrderFormInputs(groupBuilder: GroupBuilder, parameter: CreateTradeFormParameter) {
        groupBuilder.stopLossOrderForm(
            advancedFormInputId = inputId,
            params = StopLossFormParams(
                dependencies = delegateDependencies,
                parameter = parameter,
                primaryInputId = inputId,
                resources = context.resources,
                securityType = STOCK,
                stopLossInputId = inputId.stopLossInputId
            )
        ) {
            stopLossStocksLegDisplayText(context.resources, inputId)
        }
    }

    override fun advancedOrderRules(ruleBuilder: RuleListBuilder, updateForm: TradeFormFactoryDelegate.OnFormChangeListener) {
        super.advancedOrderRules(ruleBuilder, updateForm)
        ruleBuilder.apply {
            resetPricesOnVisibilityChange(inputId.stopLossInputId, includeLimitPrice = false)
            hiddenAllOrNoneRule(inputId, inputId.stopLossInputId)
        }
    }

    private fun GroupBuilder.stopLossStocksLegDisplayText(resources: Resources, inputId: StopLossStockTradeFormInputId) {

        with(inputId) {
            dynamicText {
                title = resources.getString(R.string.order)
                displayValue {
                    inputValue<DropDownOption?>(action).combineWithTwo(
                        inputValue<String?>(quantity),
                        inputValue<WithSymbolInfo>(symbol)
                    ) { actionId, qty, sym ->
                        val transactionParams = inputId.actionValueId.toStockStopLossAction(actionId?.id)
                        stopLossFormHiddenInputAndLegDisplayText(inputId.stopLossInputId, qty, resources, sym, transactionParams)
                    }
                }

                visibleIf { requireAnyInput(action).hasValue }
            }
        }
    }

    // STOCK: Exit Strategy : Stop Loss is opposite type of primary action
    // Buy to Sell
    // Sell short -> Buy to cover
    private fun ActionValueId.toStockStopLossAction(action: String?): StopLossFormInput<TransactionType> = when (action) {
        buy -> StopLossFormInput(sell, TransactionType.SELL_CLOSE)
        sellShort -> StopLossFormInput(buyToCover, TransactionType.BUY_CLOSE)
        else -> StopLossFormInput(sell, TransactionType.SELL_CLOSE)
    }
}
