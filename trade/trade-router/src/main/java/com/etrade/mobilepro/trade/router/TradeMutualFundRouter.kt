package com.etrade.mobilepro.trade.router

import androidx.navigation.NavController
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.quoteapi.MutualFundsTradeQuotesResult
import com.etrade.mobilepro.searchingapi.items.SearchResultItem

interface TradeMutualFundRouter {

    fun openDisclosures(navController: NavController, data: MutualFundsTradeQuotesResult?)

    fun openProspectus(navController: NavController, ticker: String)

    fun openFundsPerformancePage(navController: NavController, symbol: SearchResultItem.Symbol)

    fun openMutualFundSell(navController: NavController, accountId: String)

    fun openMutualFundExchangeSell(navController: NavController, accountId: String)

    fun openMutualFundExchangeBuy(navController: NavController, accountId: String, symbol: WithSymbolInfo)
}
