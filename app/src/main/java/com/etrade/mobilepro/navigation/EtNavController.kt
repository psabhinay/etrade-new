package com.etrade.mobilepro.navigation

import android.net.Uri
import androidx.annotation.NonNull
import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.alerts.presentation.base.ALERTS_PATH
import com.etrade.mobilepro.brokerage.ALL_POSITIONS_PATH
import com.etrade.mobilepro.common.navigation.NavRouter
import com.etrade.mobilepro.common.viewmodel.PendingNavDirection
import com.etrade.mobilepro.common.viewmodel.navDirectionsPriorToPending
import com.etrade.mobilepro.common.viewmodel.pendingNavDirections
import com.etrade.mobilepro.feedback.presentation.FEEDBACK_PATH
import com.etrade.mobilepro.home.TRADE_PATH
import com.etrade.mobilepro.home.model.AppActionViewModel
import com.etrade.mobilepro.landing.api.LandingRouter
import com.etrade.mobilepro.landing.api.repo.LandingRepo
import com.etrade.mobilepro.menu.MenuDeepLink
import com.etrade.mobilepro.messagecenter.presentation.messagelist.MESSAGE_CENTER_PATH
import com.etrade.mobilepro.portfolio.PORTFOLIO_PATH
import com.etrade.mobilepro.quotes.QUOTE_PATH
import com.etrade.mobilepro.transfermoney.presentation.TRANSFER_MONEY_PATH
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.navigation.hasDestinationInBackStack
import com.etrade.mobilepro.util.android.navigation.navigateSafely
import com.etrade.mobilepro.util.formatters.toWebDeepLink
import com.etrade.mobilepro.util.guard
import com.etrade.mobilepro.watchlist.presentation.WatchlistContainerFragmentArgs
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.Locale

private const val SYMBOL = "symbol"
const val TRANSACTIONS_PATH = "/Transactions"
const val BALANCES_PATH = "/balances"
const val OVERVIEW_PATH = "/overview"
const val TAX_DOC_PATH = "/TaxDocs"

var pendingNavUri: Uri? = null
var hasBeenToLoginScreen = false

// contain rules related to navigation inside app
class EtNavController(
    private val navController: NavController,
    private val userViewModel: UserViewModel,
    private val landingRepo: LandingRepo? = null,
    private val landingRouter: LandingRouter? = null,
    private val appActionViewModel: AppActionViewModel? = null
) : NavRouter {

    val graphId: Int = navController.graph.id

    // keep in alpha-order to reduce merge conflicts
    private val loginRequiringDestinations = setOf(
        R.id.action_customer_service_menu_to_chat,
        R.id.action_customer_service_menu_to_message_center,
        R.id.action_global_tradeBottomSheetFragment,
        R.id.action_launch_bloombergtv,
        R.id.action_launch_trade,
        R.id.action_menu_login_required_web_view,
        R.id.action_to_alerts,
        R.id.action_to_market_data_agreement,
        R.id.action_to_set_alert_fragment,
        R.id.action_quote_launch_trade,
        R.id.action_quote_to_earnings_details,
        R.id.action_quote_to_level_2,
        R.id.action_quote_to_reports,
        R.id.action_quote_to_streaming_toggle_prompt_dialog,
        R.id.alertsFragment,
        R.id.bill_pay_nav_graph,
        R.id.check_deposit_graph,
        R.id.formsAndApplicationsActivity,
        R.id.menu_login_required_web_view_fragment,
        R.id.open_earnings_details_overlay,
        R.id.transfer_money_nav_graph,
        R.id.watchlistContainerFragment,
        R.id.action_news_with_login,
        R.id.action_wl_news_with_login,
        R.id.action_market_with_login
    )

    private val isolatedLoginRequiringDestinations = setOf(
        R.id.action_to_add_symbol_to_widget_watchlist,
        R.id.action_to_widget_settings
    )

    private val ignoreOnLoginQuoteDestination = setOf(
        R.id.action_quote_launch_trade,
        R.id.action_quote_to_earnings_details,
        R.id.action_quote_to_level_2,
        R.id.action_quote_to_reports,
        R.id.action_quote_to_show_chartiq_activity,
        R.id.action_quote_to_streaming_toggle_prompt_dialog
    )

    private val logger: Logger = LoggerFactory.getLogger(EtNavController::class.java)

    override fun hasDestinationInBackStack(destinationId: Int): Boolean = navController.hasDestinationInBackStack(destinationId)

    override fun navigate(directions: NavDirections) = navigate(directions, null)

    override fun navigate(uri: Uri) = navigateFromAppAction(uri)

    override fun navigate(action: () -> Unit) = action()

    override fun navigateToLandingPage() {
        val pendingNavigation = landingRouter?.getPendingNavigation() guard {
            navController.navigate(MainGraphDirections.actionToSelectedAccountTabIndexOrTitle())
            return
        }
        pendingNavigation.navPreparation?.invoke()
        navController.navigate(pendingNavigation.navDirections, pendingNavigation.navOptions)
    }

    private val loginRequiringUriDestinations = setOf(
        ALERTS_PATH,
        MESSAGE_CENTER_PATH,
        PORTFOLIO_PATH,
        TRANSACTIONS_PATH,
        BALANCES_PATH,
        TRADE_PATH,
        TRANSFER_MONEY_PATH,
        ALL_POSITIONS_PATH,
        TAX_DOC_PATH
    ).plus(MenuDeepLink.loginRequiredPaths)

    fun navigate(directions: NavDirections, directionsPriorToPending: NavDirections?) {
        if (!isLoginRequiringDestination(directions) || userViewModel.isAuthenticated) {
            navigateDirectionSafely(directions)
        } else {
            pendingNavDirections = PendingNavDirection(graphId, directions)
            navDirectionsPriorToPending = directionsPriorToPending
            if (navController.currentDestination?.id != R.id.overviewFragment || directions.actionId != R.id.action_to_alerts) {
                navController.navigate(R.id.activity_login)
            }
        }
    }

    override fun navigateFromAppAction(uri: Uri) {
        uri.path?.also {
            if (uri.toString().startsWith("http")) {
                navController.navigate(Uri.parse(uri.toString().toWebDeepLink()))
            } else {
                if (it == QUOTE_PATH) {
                    val ticker = uri.getQueryParameter(SYMBOL)?.uppercase(Locale.getDefault())
                    ticker?.let { symbol ->
                        appActionViewModel?.handleQuoteAction(symbol)
                    }
                } else {
                    if (!loginRequiringUriDestinations.contains(it) || userViewModel.isAuthenticated) {
                        handleAppActionNavigate(uri)
                    } else {
                        pendingNavDirections = null
                        pendingNavUri = uri

                        navController.navigate(R.id.activity_login)
                    }
                }
            }
        }
    }

    override fun onLoggedIn() {
        if (shouldIgnoreOnLogIn()) {
            return
        }
        if (isUserInternationalStateAndBloombergDirection()) {
            clearPendingDirections()
            return
        }
        pendingNavUri?.let { uri ->
            handleAppActionNavigate(uri)
            clearPendingAppActions()
        }

        navDirectionsPriorToPending?.run { navController.navigate(this) }
        navDirectionsPriorToPending = null

        pendingNavDirections?.run {
            if (shouldBeHandled()) {
                navigateDirectionSafely(this.navDirections)
                clearPendingDirections()
            }
        }
    }

    fun clearPendingDirections() {
        pendingNavDirections = null
        hasBeenToLoginScreen = false
    }

    fun clearPendingAppActions() {
        pendingNavUri = null
        hasBeenToLoginScreen = false
    }

    fun isIsolatedLoginRequiringDestination(directions: NavDirections?): Boolean {
        return directions?.actionId?.let(isolatedLoginRequiringDestinations::contains) ?: false
    }

    private fun handleAppActionNavigate(uri: Uri) {
        uri.path.let {
            when (it) {
                OVERVIEW_PATH, PORTFOLIO_PATH, TRANSACTIONS_PATH, BALANCES_PATH, TAX_DOC_PATH -> navigateToDefaultAccount(uri)
                ALL_POSITIONS_PATH -> navigateToAllPositions()
                MESSAGE_CENTER_PATH -> navigateToMessageCenter()
                FEEDBACK_PATH -> navigateToFeedback()
                ALERTS_PATH -> navigateToAlerts()
                else -> {
                    navController.navigateSafely(uri) { e ->
                        logger.error("Invalid uri: $uri", e)
                    }
                }
            }
        }
    }

    private fun navigateToAlerts() {
        navController.navigate(ActionOnlyNavDirections(R.id.alertsFragment))
    }

    private fun navigateToFeedback() {
        navController.navigate(ActionOnlyNavDirections(R.id.feedbackActivity))
    }

    private fun navigateToMessageCenter() {
        navController.navigate(MainGraphDirections.actionHomeToMessageCenterList())
    }

    fun navigateToAllPositions() {
        navController.navigate(R.id.accountsHomeFragment)
        val landingPage = landingRepo?.getLandingState()?.selected
        landingPage?.let {
            navController.navigate(MainGraphDirections.actionToSelectedAccountTabIndexOrTitle(accountId = it.uuid))
        }
        userViewModel.selectAllBrokerageAccountView()
    }

    private fun navigateToDefaultAccount(uri: Uri) {
        navController.navigate(R.id.action_home_to_completeview)
        appActionViewModel?.handleAccountTabAction(uri)
    }

    private fun shouldIgnoreOnLogIn() = pendingNavDirections?.navDirections?.actionId?.let {
        ignoreOnLoginQuoteDestination.contains(it) && navController.graph.id != R.id.quoteGraph
    } ?: false

    private fun isUserInternationalStateAndBloombergDirection(): Boolean {
        // bloomberg tv is not accessible by international user
        return userViewModel.userInternationalState.value == true &&
            pendingNavDirections?.navDirections?.actionId == R.id.action_launch_bloombergtv
    }

    private fun isLoginRequiringDestination(directions: NavDirections): Boolean {
        val isLoginRequiringDestination = loginRequiringDestinations.contains(directions.actionId)
        val isIsolatedLoginRequiringDestination = isolatedLoginRequiringDestinations.contains(directions.actionId)
        val watchlistId = WatchlistContainerFragmentArgs.fromBundle(directions.arguments).watchlistId
        val isLoginRequiringWatchlist = watchlistId != null && watchlistId != DEVICE_WATCHLIST_ID

        return isLoginRequiringDestination || isIsolatedLoginRequiringDestination || isLoginRequiringWatchlist
    }

    private fun PendingNavDirection.shouldBeHandled() = sourceNavControllerId == graphId

    private fun navigateDirectionSafely(@NonNull directions: NavDirections) {
        try {
            navController.navigate(directions)
        } catch (ex: IllegalArgumentException) {
            logger.error("failed to find destination", ex)
            // Navigate user to complete view page instead of doing nothing.
            navController.navigate(MainGraphDirections.actionHomeToCompleteview())
        }
    }
}
