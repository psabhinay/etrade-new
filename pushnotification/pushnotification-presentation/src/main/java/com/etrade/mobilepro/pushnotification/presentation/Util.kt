package com.etrade.mobilepro.pushnotification.presentation

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.pushnotification.api.PAYLOAD_KEY
import com.etrade.mobilepro.pushnotification.api.SmartAlert
import com.etrade.mobilepro.pushnotification.api.TYPE_KEY
import com.etrade.mobilepro.searchingapi.items.SearchResultItem

internal fun Map<String, String>.toSmartAlert(): SmartAlert? {
    val message = this[PAYLOAD_KEY]
    val alertType = this[TYPE_KEY]

    return SmartAlert.create(type = alertType, message = message)
}

internal fun SmartAlert.Stock.toSymbol(): SearchResultItem.Symbol {
    return SearchResultItem.Symbol(title = symbolTitle, type = InstrumentType.EQ)
}
