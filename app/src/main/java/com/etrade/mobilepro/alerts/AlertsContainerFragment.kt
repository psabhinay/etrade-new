package com.etrade.mobilepro.alerts

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.alerts.presentation.AlertContainerViewModel
import com.etrade.mobilepro.alerts.presentation.AlertSelectionViewModel
import com.etrade.mobilepro.alerts.presentation.AlertsSharedViewModel
import com.etrade.mobilepro.alerts.presentation.account.AccountAlertsViewModel
import com.etrade.mobilepro.alerts.presentation.base.AlertTab
import com.etrade.mobilepro.alerts.presentation.market.MarketAlertsViewModel
import com.etrade.mobilepro.alerts.presentation.withoutContentParcelable
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.RestoreTabPositionDelegate
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.common.setToolbarActionTextAndContentDescription
import com.etrade.mobilepro.common.setToolbarIcon
import com.etrade.mobilepro.common.toolbarTitleView
import com.etrade.mobilepro.common.toolbarView
import com.etrade.mobilepro.databinding.FragmentAlertsContainerBinding
import com.etrade.mobilepro.home.MainActivity
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.styledalertdialog.StyledAlertDialog
import com.etrade.mobilepro.tracking.initTabsTracking
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import javax.inject.Inject

@RequireLogin
class AlertsContainerFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val restoreTabPositionDelegate: RestoreTabPositionDelegate,
    private val readStatusRepo: ReadStatusRepo
) : Fragment(R.layout.fragment_alerts_container) {

    private val binding by viewBinding(FragmentAlertsContainerBinding::bind) { onDestroyBinding() }

    private val sharedViewModel: AlertsSharedViewModel by activityViewModels { viewModelFactory }
    private val alertSelectionViewModel: AlertSelectionViewModel by activityViewModels { viewModelFactory }
    private val viewModel: AlertContainerViewModel by viewModels { viewModelFactory }

    // TODO these two VMs and related workaround will be removed in scope of ETAND-5561 when alerts are moved to a separate module with their own navgraph
    private val marketAlertsViewModel: MarketAlertsViewModel by navGraphViewModels(R.id.mainGraph) { viewModelFactory }
    private val accountAlertsViewModel: AccountAlertsViewModel by navGraphViewModels(R.id.mainGraph) { viewModelFactory }

    private val args: AlertsContainerFragmentArgs by navArgs()
    private var alertFromNotification: ConsumableLiveEvent<String?>? = null
    private val adapter: AlertPagerAdapter
        get() = binding.alertListViewPager.adapter as AlertPagerAdapter

    private val pageSelectionListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabReselected(p0: TabLayout.Tab?) {
            // unused
        }

        override fun onTabUnselected(p0: TabLayout.Tab?) {
            // unused
        }

        override fun onTabSelected(tab: TabLayout.Tab?) {
            tab?.let {
                sharedViewModel.didSelectTab(AlertTab.values()[it.position])
            }
        }
    }

    private val backPressedCallback = object : OnBackPressedCallback(false) {
        override fun handleOnBackPressed() {
            if (sharedViewModel.inEditMode.value == true) { exitEditMode() }
        }
    }

    private var confirmDeleteDialogListener: View.OnClickListener? =
        View.OnClickListener { sharedViewModel.askConfirmDelete() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        alertFromNotification = ConsumableLiveEvent(args.alertMessageFromNotification)

        setHasOptionsMenu(true)
        (activity as? MainActivity)
            ?.onBackPressedDispatcher?.addCallback(this, backPressedCallback)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.marketAlertsViewModel = marketAlertsViewModel
        viewModel.accountAlertsViewModel = accountAlertsViewModel
        setupFragment()
        bindViewModel()
        binding.alertTabs.addOnTabSelectedListener(pageSelectionListener)

        if (savedInstanceState == null) {
            displayAccountAlertIfAvailable()
        }
    }

    fun FragmentAlertsContainerBinding.onDestroyBinding() {
        alertTabs.removeOnTabSelectedListener(pageSelectionListener)
    }

    private fun setupFragment() {

        with(binding) {
            alertListViewPager.initTabsTracking(viewModel)
            alertListViewPager.adapter = AlertPagerAdapter(
                this@AlertsContainerFragment,
                viewModelFactory = viewModelFactory,
                snackbarUtilFactory = snackbarUtilFactory,
                readStatusRepo = readStatusRepo
            )
            TabLayoutMediator(alertTabs, alertListViewPager) { tab, position ->
                tab.text = adapter.getPageTitle(position)
            }.attach()

            restoreTabPositionDelegate.init(
                this@AlertsContainerFragment,
                alertListViewPager,
                actionOnRestore = { prevTabIndex ->
                    val alertTabs = AlertTab.values()
                    val prevTab = if (prevTabIndex < alertTabs.size) {
                        prevTabIndex
                    } else {
                        0
                    }
                    alertListViewPager.setCurrentItem(prevTab, false)
                    sharedViewModel.didSelectTab(alertTabs[prevTab])
                }
            )
        }
    }

    private fun bindViewModel() {
        bindSharedViewModel()

        alertSelectionViewModel.navigateEvent
            .observeBy(viewLifecycleOwner) {
                val direction = MainGraphDirections.actionToAlertDetail(it.withoutContentParcelable())
                findNavController().navigate(direction)
            }
        marketAlertsViewModel
            .markedCountIsZero.observeBy(viewLifecycleOwner) {
                setEditIdleState(it, AlertTab.MARKET)
            }
        accountAlertsViewModel
            .markedCountIsZero.observeBy(viewLifecycleOwner) {
                setEditIdleState(it, AlertTab.ACCOUNT)
            }
    }

    private fun bindSharedViewModel() {
        sharedViewModel.selectedTab
            .observeBy(viewLifecycleOwner) {
                binding.alertListViewPager.currentItem = it.ordinal
                if (it == AlertTab.MARKET) {
                    marketAlertsViewModel.markedCountIsZero.value?.let { value ->
                        setEditIdleState(value, it)
                    }
                } else {
                    accountAlertsViewModel.markedCountIsZero.value?.let { value ->
                        setEditIdleState(value, it)
                    }
                }
            }
        sharedViewModel.askConfirmDelete
            .observeBy(viewLifecycleOwner) {
                if (it) { showConfirmDeleteDialog() }
            }
    }

    private fun setEditIdleState(isEditIdle: Boolean, currentTab: AlertTab) {
        if (sharedViewModel.selectedTab.value == currentTab) {
            changeDeleteButtonState(binding.editAlertsMenu.deleteAlerts, isEditIdle)
        }
    }

    private fun changeDeleteButtonState(view: TextView, isEditIdle: Boolean) {
        if (isEditIdle) {
            // keep enabled for feedback, but don't do anything
            view.isActivated = false
            view.setOnClickListener { }
        } else {
            view.isActivated = true
            view.setOnClickListener(confirmDeleteDialogListener)
        }
    }

    private fun showConfirmDeleteDialog() =
        with(sharedViewModel) {
            StyledAlertDialog(requireContext())
                .showAlert(
                    title = getString(R.string.alert_delete_dialog_title),
                    message = getString(R.string.do_you_want_to_delete_these_alerts),
                    positiveAnswerAction = {
                        when (selectedTab.value) {
                            AlertTab.MARKET -> marketAlertsViewModel.deleteMarkedAlerts()
                            AlertTab.ACCOUNT -> accountAlertsViewModel.deleteMarkedAlerts()
                        }
                    },
                    negativeAnswerAction = {},
                    dismissAction = { cancelDelete() }
                )
        }

    private fun displayAccountAlertIfAvailable() {
        alertFromNotification?.getNonConsumedContent()?.let {
            alertSelectionViewModel.selectAlertFromNotification(it)
            sharedViewModel.hasAlertFromPush = true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.options_menu_alerts, menu)
        menu.findItem(R.id.menu_alerts_edit)?.apply {
            actionView.setOnClickListener {
                isVisible = false
                startEditMode()
            }
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.menu_alerts_edit)?.apply {
            when (sharedViewModel.inEditMode.value) {
                true -> isVisible = false
                false -> {
                    actionView.findViewById<TextView>(R.id.alerts_action_label).apply {
                        text = getString(R.string.edit)
                        contentDescription = getString(R.string.button_content_description_edit)
                    }
                    isVisible = true
                }
            }
        }
    }

    private fun startEditMode() {
        sharedViewModel.enableEditMode(true)
        activity?.apply {
            toolbarView.setToolbarIcon(isLastPageInStack = true, isWhite = true)
        }
        binding.editAlertsMenu.apply {
            markAllAlerts.setOnClickListener {
                when (sharedViewModel.selectedTab.value) {
                    AlertTab.MARKET -> marketAlertsViewModel.markAllAlerts()
                    AlertTab.ACCOUNT -> accountAlertsViewModel.markAllAlerts()
                }
            }
        }
        resumeEditMode()
    }

    private fun resumeEditMode() {
        activity?.apply {
            toolbarTitleView?.setToolbarActionTextAndContentDescription(R.string.edit)
            toolbarView.setNavigationOnClickListener { exitEditMode() }
        }
        binding.editAlertsMenu.root.visibility = VISIBLE
        backPressedCallback.isEnabled = true
    }

    private fun suspendEditMode() {
        activity?.apply {
            toolbarView.setNavigationOnClickListener { onBackPressed() }
        }
        backPressedCallback.isEnabled = false
    }

    private fun exitEditMode() {
        sharedViewModel.enableEditMode(false)
        activity?.apply {
            invalidateOptionsMenu()
            toolbarTitleView?.setToolbarActionTextAndContentDescription(R.string.alerts)
            toolbarView.setToolbarIcon(isLastPageInStack = false, isWhite = true)
        }
        suspendEditMode()

        binding.editAlertsMenu.apply {
            markAllAlerts.setOnClickListener(null)
            root.visibility = GONE
        }
        backPressedCallback.remove()
    }

    override fun onStart() {
        super.onStart()
        if (sharedViewModel.inEditMode.value == true) { startEditMode() }
    }

    override fun onResume() {
        super.onResume()
        // this is needed for configuration changes
        if (sharedViewModel.inEditMode.value == true) { resumeEditMode() }
    }

    override fun onPause() {
        super.onPause()
        if (sharedViewModel.inEditMode.value == true) { suspendEditMode() }
    }

    override fun onDestroy() {
        sharedViewModel.reset()
        backPressedCallback.remove()
        confirmDeleteDialogListener = null
        super.onDestroy()
    }
}
