package com.etrade.mobilepro.calendar.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.calendar.databinding.AdapterWeekDayTitleBinding
import java.text.DateFormatSymbols
import java.util.Locale

class WeekDaysTitleAdapter : RecyclerView.Adapter<WeekDayTitleViewHolder>() {

    private val weekDays: List<String> by lazy {
        DateFormatSymbols
            .getInstance(Locale.US)
            .weekdays
            .filter { it.isNotEmpty() }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeekDayTitleViewHolder {
        val binding = AdapterWeekDayTitleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WeekDayTitleViewHolder(binding)
    }

    override fun getItemCount(): Int = weekDays.size

    override fun onBindViewHolder(holder: WeekDayTitleViewHolder, position: Int) {
        holder.bind(weekDays[position])
    }
}
