package com.etrade.mobilepro.tradingdefaults.viewholder

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.tradingdefaults.databinding.AdapterTradingDefaultsDividerBinding
import com.etrade.mobilepro.util.android.extension.viewBinding

class TradingDefaultsDividerViewHolder(binding: AdapterTradingDefaultsDividerBinding) :
    RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun from(parent: ViewGroup) =
            TradingDefaultsDividerViewHolder(parent.viewBinding(AdapterTradingDefaultsDividerBinding::inflate))
    }
}
