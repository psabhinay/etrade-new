package com.etrade.mobilepro.util.android.recyclerviewutil

import android.graphics.drawable.Drawable

/**
 * This ItemDecorator subclass removes the custom recycler divider for specific child {@code positions}
 *
 * @param positions = excluded positions
 * @param removeTopDivider = {@code true} removes top divider too {@code false} only removes bottom dividers
 */
class CustomRecyclerViewItemDivider(
    drawable: Drawable,
    positions: List<Int> = emptyList(),
    removeTopDivider: Boolean = true,
    countOfLastItemsWithoutDivider: Int = 1
) : RecyclerViewItemDivider(
    countOfLastItemsWithoutDivider = countOfLastItemsWithoutDivider,
    divider = drawable,
    excludeIndexList = positions + if (removeTopDivider) {
        positions.map { it - 1 }
    } else {
        emptyList()
    }
)
