package com.etrade.mobilepro.trade.data.customorder

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class CustomOrderEditPlaceResponseDto(
    @Json(name = "customOrderChange")
    override val order: CustomOrderDto?
) : CustomOrderResponse()
