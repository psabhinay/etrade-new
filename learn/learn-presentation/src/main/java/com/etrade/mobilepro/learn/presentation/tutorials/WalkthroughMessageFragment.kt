package com.etrade.mobilepro.learn.presentation.tutorials

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.etrade.mobilepro.dialoglayout.databinding.DialogLayoutUiBinding
import com.etrade.mobilepro.learn.presentation.R
import com.etrade.mobilepro.util.android.fragment.OrientationLock
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import javax.inject.Inject

const val SUBTITLE_TEXT = "SubtitleText"
const val POSITIVE_ACTION_TEXT = "PositiveActionText"
const val ACTION = "Action"

/**
 * This class allows walkthrough messages to appear over the Quotes Bottom Sheet
 */
class WalkthroughMessageFragment @Inject constructor(
    private val walkthroughStatesViewModel: WalkthroughStatesViewModel,
    private val orientationLock: OrientationLock
) : DialogFragment() {

    var binding: DialogLayoutUiBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
        orientationLock.init(this, ActivityInfo.SCREEN_ORIENTATION_LOCKED, applyIfTablet = true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_layout_ui, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) { /* intentionally blank */ }
        binding = DialogLayoutUiBinding.bind(view)
        setupViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun setupViews() {
        val action = arguments?.getString(ACTION)?.let { ActionsMap.get(it) }

        binding?.run {
            backgroundView.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.white))
            title.visibility = View.GONE
            subtitle.text = arguments?.getString(SUBTITLE_TEXT)
            positiveAction.text = arguments?.getString(POSITIVE_ACTION_TEXT)
            positiveAction.setOnClickListener {
                action?.let { action -> walkthroughStatesViewModel.onAction(action) }
            }
        }
    }
}
