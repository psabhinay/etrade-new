package com.etrade.mobilepro.streaming

import androidx.appcompat.app.AppCompatActivity
import com.etrade.mobilepro.common.di.Level1Streamer
import com.etrade.mobilepro.common.di.Level2Streamer
import javax.inject.Inject
import javax.inject.Singleton

interface StreamingSessionDelegateHelper {
    fun init(activity: AppCompatActivity)
    fun startStreamingSession()
    fun stopStreamingSession()
    fun startLevel2StreamingSession()
    fun stopLevel2StreamingSession()
}

@Singleton
class DefaultStreamingSessionDelegateHelper @Inject constructor(
    @Level1Streamer private val level1StreamingSessionDelegate: StreamingSessionDelegate,
    @Level2Streamer private val level2StreamingSessionDelegate: StreamingSessionDelegate
) : StreamingSessionDelegateHelper {
    override fun init(activity: AppCompatActivity) {
        level1StreamingSessionDelegate.init(activity)
        level2StreamingSessionDelegate.init(activity)
    }

    override fun startStreamingSession() {
        level1StreamingSessionDelegate.startStreamingSession()
    }

    override fun stopStreamingSession() {
        level1StreamingSessionDelegate.stopStreamingSession()
    }

    override fun startLevel2StreamingSession() {
        level2StreamingSessionDelegate.startStreamingSession()
    }

    override fun stopLevel2StreamingSession() {
        level2StreamingSessionDelegate.stopStreamingSession()
    }
}
