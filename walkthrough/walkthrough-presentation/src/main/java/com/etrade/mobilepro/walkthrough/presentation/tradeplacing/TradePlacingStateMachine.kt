package com.etrade.mobilepro.walkthrough.presentation.tradeplacing

import com.etrade.mobilepro.statemachine.EtStateMachine
import com.etrade.mobilepro.walkthrough.api.TradePlacingAction
import com.etrade.mobilepro.walkthrough.api.TradePlacingState
import com.tinder.StateMachine
import javax.inject.Inject

class TradePlacingStateMachine @Inject constructor() : EtStateMachine<TradePlacingState, TradePlacingAction> {

    private val stateMachine = StateMachine.create<TradePlacingState, TradePlacingAction, TradePlacingState> {
        initialState(TradePlacingState.Idle)
        state<TradePlacingState.Idle> {
            on<TradePlacingAction.StartWalkthrough> {
                transitionTo(TradePlacingState.DisplayInfoMessage, TradePlacingState.DisplayInfoMessage)
            }
            on<TradePlacingAction.Canceled> {
                transitionTo(TradePlacingState.Idle, TradePlacingState.Idle)
            }
        }
        state<TradePlacingState.DisplayInfoMessage> {
            on<TradePlacingAction.InfoMessageDisplayed> {
                transitionTo(TradePlacingState.DisplayTradePage, TradePlacingState.DisplayTradePage)
            }
            on<TradePlacingAction.Canceled> {
                transitionTo(TradePlacingState.Idle, TradePlacingState.Idle)
            }
        }
        state<TradePlacingState.DisplayTradePage> {
            on<TradePlacingAction.TradePageDisplayed> {
                transitionTo(TradePlacingState.HighlightSymbol, TradePlacingState.HighlightSymbol)
            }
            on<TradePlacingAction.Canceled> {
                transitionTo(TradePlacingState.Idle, TradePlacingState.Idle)
            }
        }
        state<TradePlacingState.HighlightSymbol> {
            on<TradePlacingAction.SymbolHighlighted> {
                transitionTo(TradePlacingState.HighlightOrderType, TradePlacingState.HighlightOrderType)
            }
            on<TradePlacingAction.Canceled> {
                transitionTo(TradePlacingState.Idle, TradePlacingState.Idle)
            }
        }
        state<TradePlacingState.HighlightOrderType> {
            on<TradePlacingAction.OrderTypeHighlighted> {
                transitionTo(TradePlacingState.HighlightQuantity, TradePlacingState.HighlightQuantity)
            }
            on<TradePlacingAction.Canceled> {
                transitionTo(TradePlacingState.Idle, TradePlacingState.Idle)
            }
        }
        state<TradePlacingState.HighlightQuantity> {
            on<TradePlacingAction.QuantityHighlighted> {
                transitionTo(TradePlacingState.HighlightPriceType, TradePlacingState.HighlightPriceType)
            }
            on<TradePlacingAction.Canceled> {
                transitionTo(TradePlacingState.Idle, TradePlacingState.Idle)
            }
        }
        state<TradePlacingState.HighlightPriceType> {
            on<TradePlacingAction.PriceTypeHighlighted> {
                transitionTo(TradePlacingState.DisplayPriceTypes, TradePlacingState.DisplayPriceTypes)
            }
            on<TradePlacingAction.Canceled> {
                transitionTo(TradePlacingState.Idle, TradePlacingState.Idle)
            }
        }
        state<TradePlacingState.DisplayPriceTypes> {
            on<TradePlacingAction.PriceTypesDisplayed> {
                transitionTo(TradePlacingState.EnterPriceType, TradePlacingState.EnterPriceType)
            }
            on<TradePlacingAction.Canceled> {
                transitionTo(TradePlacingState.Idle, TradePlacingState.Idle)
            }
        }
        state<TradePlacingState.EnterPriceType> {
            on<TradePlacingAction.PriceTypeEntered> {
                transitionTo(TradePlacingState.HighlightTerm, TradePlacingState.HighlightTerm)
            }
            on<TradePlacingAction.Canceled> {
                transitionTo(TradePlacingState.Idle, TradePlacingState.Idle)
            }
        }
        state<TradePlacingState.HighlightTerm> {
            on<TradePlacingAction.TermHighlighted> {
                transitionTo(TradePlacingState.HighlightPreview, TradePlacingState.HighlightPreview)
            }
            on<TradePlacingAction.Canceled> {
                transitionTo(TradePlacingState.Idle, TradePlacingState.Idle)
            }
        }
        state<TradePlacingState.HighlightPreview> {
            on<TradePlacingAction.PreviewHighlighted> {
                transitionTo(TradePlacingState.HighlightTrade, TradePlacingState.HighlightTrade)
            }
            on<TradePlacingAction.Canceled> {
                transitionTo(TradePlacingState.Idle, TradePlacingState.Idle)
            }
        }
        state<TradePlacingState.HighlightTrade> {
            on<TradePlacingAction.TradeHighlighted> {
                transitionTo(TradePlacingState.Idle, TradePlacingState.Idle)
            }
            on<TradePlacingAction.Canceled> {
                transitionTo(TradePlacingState.Idle, TradePlacingState.Idle)
            }
        }
        onTransition {
            val validTransition = it as? StateMachine.Transition.Valid ?: return@onTransition
            val newState = validTransition.sideEffect ?: return@onTransition
            onStateChange?.invoke(newState)
        }
    }

    override val currentState: TradePlacingState
        get() = stateMachine.state

    override var onStateChange: ((TradePlacingState) -> Unit)? = null

    override fun transition(action: TradePlacingAction) {
        stateMachine.transition(action)
    }
}
