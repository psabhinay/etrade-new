package com.etrade.mobilepro.news.presentation

import android.content.Context
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.news.presentation.filter.portfolio.AccountToPortfolioFilterMapper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

private const val ALL_PORTFOLIOS_STRING = "All"
private const val TEST_SHORT_NAME = "TEST_SHORT_NAME"
private const val ENC_ACC_ID = "abrakadabra"

class AccountToPortfolioFilterMapperTest {

    @Mock
    private lateinit var context: Context

    private lateinit var sut: AccountToPortfolioFilterMapper
    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        sut = AccountToPortfolioFilterMapper(context)
        whenever(context.getString(any())).thenReturn(ALL_PORTFOLIOS_STRING)
    }

    @Test
    fun `empty input filter list results in list with one ALL item`() {
        val input = emptyList<Account>()

        val output = sut.map(input)

        assertTrue(output.size == 1)
        assertTrue(output[0].title == ALL_PORTFOLIOS_STRING)
        assertTrue(output[0].isBaseAllPortfoliosItem)
    }

    @Test
    fun `non empty input filter list results in list of size + 1, with first ALL item`() {
        val input = listOf(Account(accountShortName = TEST_SHORT_NAME))

        val output = sut.map(input)

        assertTrue(output.size == 2)
        assertTrue(output[0].title == ALL_PORTFOLIOS_STRING)
        assertTrue(output[1].title == TEST_SHORT_NAME)
        assertFalse(output[1].isBaseAllPortfoliosItem)
    }

    @Test
    fun `mapped non ALL item has title == accountShortName and filterResourceId == encAccountId`() {
        val input = listOf(Account(accountShortName = TEST_SHORT_NAME, encAccountId = ENC_ACC_ID))

        val output = sut.map(input)

        assertTrue(output.size == 2)
        assertTrue(output[0].title == ALL_PORTFOLIOS_STRING)
        assertTrue(output[1].title == TEST_SHORT_NAME)
        assertTrue(output[1].filterResourceId == ENC_ACC_ID)
        assertFalse(output[1].isBaseAllPortfoliosItem)
    }
}
