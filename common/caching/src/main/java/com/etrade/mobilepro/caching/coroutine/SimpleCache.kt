package com.etrade.mobilepro.caching.coroutine

import androidx.annotation.VisibleForTesting
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.util.KeyValueStore
import com.etrade.mobilepro.util.Serializer
import javax.inject.Inject

const val NO_EXPIRATION = Long.MAX_VALUE

private val KEY_ITEMS = "${SimpleCache::class.java.canonicalName}.cached_item"
private val KEY_CACHED_AT = "${SimpleCache::class.java.canonicalName}.cached_at"

@VisibleForTesting
fun String.toDataKey() = "$this.$KEY_ITEMS"
@VisibleForTesting
fun String.toTimestampKey() = "$this.$KEY_CACHED_AT"

class SimpleCache<T> @Inject constructor(
    private val expirationTimeInMillis: Long,
    private val serializer: Serializer<T>,
    private val keyValueStore: KeyValueStore
) : Cache<T, String> {
    override suspend fun isCached(id: String): Boolean {
        return keyValueStore.getString(id.toTimestampKey())
            ?.toLongOrNull()
            ?.let { cachedAt ->
                val isNotExpired = System.currentTimeMillis() - cachedAt <= expirationTimeInMillis
                val data = keyValueStore.getString(id.toDataKey())

                isNotExpired && data?.isEmpty() == false
            } ?: false
    }

    override suspend fun get(id: String): ETResult<T> {
        return runCatchingET {
            keyValueStore.getString(id.toDataKey())
                ?.let { serializer.deserialize(it) }
                ?: throw NoSuchElementException()
        }
    }

    override suspend fun put(resource: T, id: String): ETResult<Unit> {
        return runCatchingET {
            keyValueStore.putString(id.toDataKey(), serializer.serialize(resource))

            keyValueStore.putString(id.toTimestampKey(), System.currentTimeMillis().toString())
        }
    }

    override suspend fun remove(id: String): ETResult<Unit> {
        return runCatchingET {
            keyValueStore.putString(id.toDataKey(), null)
            keyValueStore.putString(id.toTimestampKey(), null)
        }
    }
}
