package com.etrade.mobilepro.inboxmessages

import com.etrade.mobilepro.inboxmessages.InboxAttributes.SHOW_BANNER
import com.localytics.androidx.InboxCampaign

private const val VALUE_YES = "YES"

internal fun InboxCampaign.shouldBeShown(): Boolean =
    attributes.toCaseInsensitiveMap().getOrDefault(SHOW_BANNER, "true").lowercase().toBoolean()

internal fun String?.shouldBeShown(): Boolean = this?.equals(VALUE_YES, true) ?: false
