package com.etrade.mobilepro.screener.data.repo

import com.etrade.mobilepro.screener.api.EtfFundGroup
import com.etrade.mobilepro.screener.api.FundCategory
import com.etrade.mobilepro.screener.api.MutualFundGroup
import com.etrade.mobilepro.screener.api.datasource.EtfFundDataSource
import com.etrade.mobilepro.screener.api.datasource.MutualFundDataSource
import com.etrade.mobilepro.screener.api.repo.FundCategoriesRepo
import com.etrade.mobilepro.screener.data.rest.ScreenerApiClient
import com.etrade.mobilepro.util.Resource
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

private const val PARAM_MUTUAL_FUND = "MF"
private const val PARAM_ETF_FUND = "ETF"

class DefaultFundCategoriesRepo @Inject constructor(
    private val apiClient: ScreenerApiClient,
    private val etfFundDataSource: EtfFundDataSource,
    private val mfFundDataSource: MutualFundDataSource,
    private val toDomainMapper: (String) -> FundCategory
) : FundCategoriesRepo {
    private val memCacheEtf = mutableMapOf<EtfFundGroup.Type, List<FundCategory>>()
    private val memCacheMf = mutableMapOf<MutualFundGroup.Type, List<FundCategory>>()

    override fun loadEtfFundCategories(etfFundGroup: EtfFundGroup): Flowable<Resource<List<FundCategory>>> =
        Single
            .concat(
                startLoading(),
                loadEtfCategories(etfFundGroup.type)
            )
            .share()

    override fun loadMutualFundCategories(mutualFundGroup: MutualFundGroup): Flowable<Resource<List<FundCategory>>> =
        Single
            .concat(
                startLoading(),
                loadMfCategories(mutualFundGroup.type)
            )
            .share()

    override fun getLoadedMutualFundCategories(mutualFundGroup: MutualFundGroup): List<FundCategory> =
        memCacheMf.getOrPut(mutualFundGroup.type) { listOf(mfFundDataSource.getAllFundCategoriesItem()) }

    override fun getLoadedEtfFundCategories(etfFundGroup: EtfFundGroup): List<FundCategory> =
        memCacheEtf.getOrPut(etfFundGroup.type) { listOf(etfFundDataSource.getAllFundCategoriesItem()) }

    private fun loadEtfCategories(etfFundGroupType: EtfFundGroup.Type): Single<Resource<List<FundCategory>>> =
        apiClient
            .getFundCategory(
                screenType = PARAM_ETF_FUND,
                filterValue = etfFundGroupType.asRequestParam()
            )
            .toObservable()
            .map {
                it.categories.map(toDomainMapper)
            }
            .firstOrError()
            .doOnSuccess {
                memCacheEtf[etfFundGroupType] =
                    listOf(etfFundDataSource.getAllFundCategoriesItem()) + it
            }
            .map<Resource<List<FundCategory>>> {
                Resource.Success(it)
            }
            .onErrorReturn {
                Resource.Failed(error = it)
            }

    private fun loadMfCategories(mutualFundGroupType: MutualFundGroup.Type): Single<Resource<List<FundCategory>>> =
        apiClient
            .getFundCategory(
                screenType = PARAM_MUTUAL_FUND,
                filterValue = mutualFundGroupType.asRequestParam()
            )
            .toObservable()
            .map {
                it.categories.map(toDomainMapper)
            }
            .firstOrError()
            .doOnSuccess {
                memCacheMf[mutualFundGroupType] =
                    listOf(mfFundDataSource.getAllFundCategoriesItem()) + it
            }
            .map<Resource<List<FundCategory>>> {
                Resource.Success(it)
            }
            .onErrorReturn {
                Resource.Failed(error = it)
            }

    private fun startLoading(): Single<Resource<List<FundCategory>>> =
        Single.fromCallable {
            Resource.Loading()
        }
}
