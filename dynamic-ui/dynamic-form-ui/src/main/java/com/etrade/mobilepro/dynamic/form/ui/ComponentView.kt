package com.etrade.mobilepro.dynamic.form.ui

import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.dynamic.form.api.Component

interface ComponentView<in T : Component> {
    fun render(component: T, owner: LifecycleOwner)
}
