package com.etrade.mobilepro.bank.transactions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\bJ\u0012\u0010\u001c\u001a\u00020\u00052\b\u0010\u001d\u001a\u0004\u0018\u00010\u0005H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001d\u0010\n\u001a\u0004\u0018\u00010\u000b8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\f\u0010\rR\u0014\u0010\u0010\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u001a\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u0019X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001b\u00a8\u0006\u001e"}, d2 = {"Lcom/etrade/mobilepro/bank/transactions/BankTransactionGenericLayout;", "Lcom/etrade/mobilepro/dynamicui/api/GenericLayout;", "data", "Lcom/etrade/mobilepro/bank/transactions/BankTransactionDataDto;", "emptyValue", "", "emptyAmountDescription", "emptyDate", "(Lcom/etrade/mobilepro/bank/transactions/BankTransactionDataDto;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "inProcessLabel", "transactionDate", "Lorg/threeten/bp/LocalDate;", "getTransactionDate", "()Lorg/threeten/bp/LocalDate;", "transactionDate$delegate", "Lkotlin/Lazy;", "uniqueIdentifier", "getUniqueIdentifier", "()Ljava/lang/String;", "viewModelClass", "Ljava/lang/Class;", "Lcom/etrade/mobilepro/bank/transactions/BankTransactionViewModel;", "getViewModelClass", "()Ljava/lang/Class;", "viewModelFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "getViewModelFactory", "()Landroidx/lifecycle/ViewModelProvider$Factory;", "getFormattedAmount", "amount", "bank-dynamic_debug"})
public final class BankTransactionGenericLayout implements com.etrade.mobilepro.dynamicui.api.GenericLayout {
    private final com.etrade.mobilepro.bank.transactions.BankTransactionDataDto data = null;
    private final java.lang.String emptyValue = null;
    private final java.lang.String emptyAmountDescription = null;
    private final java.lang.String emptyDate = null;
    private java.lang.String inProcessLabel;
    @org.jetbrains.annotations.Nullable()
    private final kotlin.Lazy transactionDate$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.ViewModelProvider.Factory viewModelFactory = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.Class<com.etrade.mobilepro.bank.transactions.BankTransactionViewModel> viewModelClass = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String uniqueIdentifier = null;
    
    public BankTransactionGenericLayout(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.bank.transactions.BankTransactionDataDto data, @org.jetbrains.annotations.NotNull()
    java.lang.String emptyValue, @org.jetbrains.annotations.NotNull()
    java.lang.String emptyAmountDescription, @org.jetbrains.annotations.NotNull()
    java.lang.String emptyDate) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final org.threeten.bp.LocalDate getTransactionDate() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.lifecycle.ViewModelProvider.Factory getViewModelFactory() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.Class<com.etrade.mobilepro.bank.transactions.BankTransactionViewModel> getViewModelClass() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getUniqueIdentifier() {
        return null;
    }
    
    private final java.lang.String getFormattedAmount(java.lang.String amount) {
        return null;
    }
    
    public void refresh(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.dynamicui.api.DynamicViewModel viewModel) {
    }
}