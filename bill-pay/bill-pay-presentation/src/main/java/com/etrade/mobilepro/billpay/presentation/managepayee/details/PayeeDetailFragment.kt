package com.etrade.mobilepro.billpay.presentation.managepayee.details

import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.billpay.presentation.ADD_PAYMENT_TAB_INDEX
import com.etrade.mobilepro.billpay.presentation.BillPayViewModel
import com.etrade.mobilepro.billpay.presentation.R
import com.etrade.mobilepro.billpay.presentation.common.toListOfTableSectionItemLayout
import com.etrade.mobilepro.billpay.presentation.databinding.BillPayFragmentPayeeDetailBinding
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.setToolbarActionTextAndContentDescription
import com.etrade.mobilepro.common.toolbarActionEndView
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.util.android.binding.viewBinding
import javax.inject.Inject

@RequireLogin
class PayeeDetailFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory
) : OverlayBaseFragment() {

    private val binding by viewBinding(BillPayFragmentPayeeDetailBinding::bind)

    override val layoutRes: Int = R.layout.bill_pay_fragment_payee_detail
    private val args: PayeeDetailFragmentArgs by navArgs()
    private val sharedViewModel: BillPayViewModel by navGraphViewModels(R.id.bill_pay_nav_graph) {
        viewModelFactory
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        activity?.apply {
            toolbarActionEndView?.apply {
                setToolbarActionTextAndContentDescription(R.string.edit)
                setOnClickListener {
                    sharedViewModel.viewState.value?.let {
                        requireNotNull(it.billPaySessionId) { "No Bill Pay Session Id" }
                        findNavController().navigate(
                            PayeeDetailFragmentDirections.actionPayeeDetailToPayeeForm(
                                billPaySessionId = it.billPaySessionId,
                                payeeDetails = args.payeeDetails,
                                isEditMode = true
                            )
                        )
                    }
                }
                visibility = View.VISIBLE
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
    }

    private fun setupFragment() {
        binding.payeeDetailsSections.setContent(
            args.payeeDetails.toListOfTableSectionItemLayout(
                resources
            )
        )

        binding.buttonMakePayment.setOnClickListener {
            findNavController().navigate(
                PayeeDetailFragmentDirections.actionPayeeDetailToAddPayment(
                    toPayee = args.payeeDetails.payeeId,
                    tabIndex = ADD_PAYMENT_TAB_INDEX
                )
            )
        }
    }
}
