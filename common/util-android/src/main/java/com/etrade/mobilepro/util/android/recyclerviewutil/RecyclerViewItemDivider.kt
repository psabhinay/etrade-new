package com.etrade.mobilepro.util.android.recyclerviewutil

import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.roundToInt

/**
 * Draws the given line divider @param divider for a list item and
 * an empty line for the last item.
 */
open class RecyclerViewItemDivider(
    private val divider: Drawable,
    private val isVerticalOrientation: Boolean = true,
    private val countOfLastItemsWithoutDivider: Int = 1,
    private val excludeIndexList: List<Int>? = null
) : RecyclerView.ItemDecoration() {

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (isVerticalOrientation) {
            drawVertical(canvas, parent, state.itemCount)
        } else {
            drawHorizontal(canvas, parent, state.itemCount)
        }
    }

    /**
     *  This method adds an empty rect for the last list item of the Recycler Listview
     */
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val itemIndex = parent.getChildAdapterPosition(view)
        if (itemIndex == state.itemCount - countOfLastItemsWithoutDivider || excludeIndexList?.contains(itemIndex) == true) {
            outRect.setEmpty()
        } else {
            outRect.set(0, 0, 0, divider.intrinsicHeight)
        }
    }

    private fun drawVertical(canvas: Canvas, parent: RecyclerView, itemsCount: Int) {
        canvas.save()
        if (parent.clipToPadding) {
            canvas.clipRect(
                parent.paddingLeft, parent.paddingTop, parent.width - parent.paddingRight,
                parent.height - parent.paddingBottom
            )
        }
        val bounds = Rect()
        val childCount = parent.childCount
        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)
            parent.getDecoratedBoundsWithMargins(child, bounds)
            val bottom = bounds.bottom + child.translationY.roundToInt()
            val top = bottom - divider.intrinsicHeight
            divider.setBounds(child.left, top, child.right, bottom)

            if (parent.getChildAdapterPosition(child) < itemsCount - countOfLastItemsWithoutDivider) {
                if (drawInPosition(parent.getChildAdapterPosition(parent.getChildAt(i)))) {
                    divider.draw(canvas)
                }
            }
        }
        canvas.restore()
    }

    protected open fun drawInPosition(position: Int): Boolean {
        return !(excludeIndexList?.contains(position) ?: false)
    }

    private fun drawHorizontal(canvas: Canvas, parent: RecyclerView, itemsCount: Int) {
        canvas.save()
        val top: Int
        val bottom: Int
        val bounds = Rect()
        if (parent.clipToPadding) {
            top = parent.paddingTop
            bottom = parent.height - parent.paddingBottom
            canvas.clipRect(
                parent.paddingLeft, top,
                parent.width - parent.paddingRight, bottom
            )
        } else {
            top = 0
            bottom = parent.height
        }

        val childCount = parent.childCount
        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)
            parent.getDecoratedBoundsWithMargins(child, bounds)
            val right = bounds.right + Math.round(child.translationX)
            val left = right - divider.intrinsicWidth
            divider.setBounds(left, top, right, bottom)

            if (parent.getChildAdapterPosition(child) < itemsCount - countOfLastItemsWithoutDivider) {
                divider.draw(canvas)
            }
        }
        canvas.restore()
    }
}
