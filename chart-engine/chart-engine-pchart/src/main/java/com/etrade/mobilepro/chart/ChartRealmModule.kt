package com.etrade.mobilepro.chart

import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
class ChartRealmModule
