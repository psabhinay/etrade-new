package com.etrade.mobilepro.billpay.usecase

import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.billpay.api.Payment
import com.etrade.mobilepro.billpay.api.getPaymentDate
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.util.UseCase
import javax.inject.Inject

interface PaymentListUseCase : UseCase<PaymentListRequestParameters, ETResult<List<Payment>>>

class PaymentListUseCaseImpl @Inject constructor(
    private val billPayRepo: BillPayRepo
) : PaymentListUseCase {
    override suspend fun execute(parameter: PaymentListRequestParameters): ETResult<List<Payment>> =
        billPayRepo.getPaymentList(parameter.sessionId, parameter.status, parameter.numOfDays, parameter.startDate).map { unsortedList ->
            unsortedList.sortedWith(compareByDescending<Payment> { it.getPaymentDate() }.thenByDescending { it.paymentId })
        }
}
