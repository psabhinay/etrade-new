package com.etrade.mobilepro.bloombergtv.presentation.pip

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PixelFormat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.etrade.mobilepro.bloombergtv.presentation.R

private const val PIP_VIDEO_PLAYER_WIDTH = 600
private const val PIP_VIDEO_PLAYER_HEIGHT = 350
private const val PIP_VIDEO_PLAYER_INITIAL_X = 100
private const val PIP_VIDEO_PLAYER_INITIAL_Y = 100

class PipViewFactory(private val context: Context) {
    private val layoutInflater: LayoutInflater
        get() = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    @Suppress("InflateParams")
    fun createPipView(windowManager: WindowManager, onReady: (SurfaceHolder) -> Unit, onDestroyed: () -> Unit): ViewGroup {
        val holderCallbacks: SurfaceHolder.Callback = object : SurfaceHolder.Callback {
            override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) = Unit
            override fun surfaceDestroyed(holder: SurfaceHolder) = onDestroyed()
            override fun surfaceCreated(holder: SurfaceHolder) = onReady(holder)
        }
        val layout = layoutInflater.inflate(R.layout.activity_bloombergtv, null) as ViewGroup
        val videoView = layout.findViewById<SurfaceView>(R.id.videoSurface)
        videoView.holder?.addCallback(holderCallbacks)
        val initialPipLayoutParams = createPipLayoutParams()
        val touchListener = VideoSurfaceTouchListener(initialPipLayoutParams, onUpdate = { params -> windowManager.updateViewLayout(layout, params) })
        layout.setOnTouchListener(touchListener)
        windowManager.addView(layout, initialPipLayoutParams)
        return layout
    }

    private fun createPipLayoutParams(): WindowManager.LayoutParams = WindowManager.LayoutParams(
        PIP_VIDEO_PLAYER_WIDTH,
        PIP_VIDEO_PLAYER_HEIGHT,
        WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
        PixelFormat.TRANSLUCENT
    ).apply {
        gravity = Gravity.TOP or Gravity.START
        x = PIP_VIDEO_PLAYER_INITIAL_X
        y = PIP_VIDEO_PLAYER_INITIAL_Y
    }

    private class VideoSurfaceTouchListener(
        private val pipLayoutParams: WindowManager.LayoutParams,
        private val onUpdate: (WindowManager.LayoutParams) -> Unit
    ) : View.OnTouchListener {
        private var initialX: Int = 0
        private var initialY: Int = 0
        private var initialTouchX: Float = 0.toFloat()
        private var initialTouchY: Float = 0.toFloat()

        @SuppressLint("ClickableViewAccessibility")
        override fun onTouch(v: View, event: MotionEvent): Boolean {
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    initialX = pipLayoutParams.x
                    initialY = pipLayoutParams.y
                    initialTouchX = event.rawX
                    initialTouchY = event.rawY
                }
                MotionEvent.ACTION_UP -> {
                }
                MotionEvent.ACTION_MOVE -> {
                    pipLayoutParams.x = initialX + (event.rawX - initialTouchX).toInt()
                    pipLayoutParams.y = initialY + (event.rawY - initialTouchY).toInt()
                    onUpdate(pipLayoutParams)
                }
            }
            return false
        }
    }
}
