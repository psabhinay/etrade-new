package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.chartiq.DefaultChartIqRouter
import com.etrade.mobilepro.chartiq.api.repo.ChartIqDataRepo
import com.etrade.mobilepro.chartiq.data.repo.DefaultChartIqDataRepo
import com.etrade.mobilepro.chartiq.presentation.ChartIqClientFactory
import com.etrade.mobilepro.chartiq.presentation.ChartIqClientFactoryImpl
import com.etrade.mobilepro.chartiq.presentation.ChartIqRouter
import com.etrade.mobilepro.chartiq.presentation.ChartIqViewModelFactoryFactory
import com.etrade.mobilepro.chartiq.presentation.ChartIqViewModelFactoryFactoryImpl
import com.etrade.mobilepro.chartiq.usecase.CancelOrderUseCaseImpl
import com.etrade.mobilepro.chartiq.usecase.CancelOrderUserCase
import com.etrade.mobilepro.chartiq.usecase.EditOrderDataUseCase
import com.etrade.mobilepro.chartiq.usecase.EditOrderDataUseCaseImpl
import dagger.Binds
import dagger.Module

@Module
abstract class ChartIqModule {

    @Binds
    abstract fun bindChartIqViewModelFactoryFactory(factory: ChartIqViewModelFactoryFactoryImpl): ChartIqViewModelFactoryFactory

    @Binds
    abstract fun bindChartIqClientFactory(factory: ChartIqClientFactoryImpl): ChartIqClientFactory

    @Binds
    abstract fun bindChartIqDataRepo(repo: DefaultChartIqDataRepo): ChartIqDataRepo

    @Binds
    abstract fun bindCancelOrderUseCase(useCase: CancelOrderUseCaseImpl): CancelOrderUserCase

    @Binds
    abstract fun bindEditOrderDataUseCase(useCase: EditOrderDataUseCaseImpl): EditOrderDataUseCase

    @Binds
    abstract fun bindRouter(router: DefaultChartIqRouter): ChartIqRouter
}
