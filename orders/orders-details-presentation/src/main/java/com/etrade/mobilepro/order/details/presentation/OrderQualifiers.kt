package com.etrade.mobilepro.order.details.presentation

import javax.inject.Qualifier

@Qualifier
annotation class GeneralOrder

@Qualifier
annotation class MutualFundOrder

@Qualifier
annotation class PreviewMutualFundOrder

@Qualifier
annotation class PreviewOrder

@Qualifier
annotation class StockPlanOrder
