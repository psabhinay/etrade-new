package com.etrade.mobilepro.testutil

import org.simpleframework.xml.convert.AnnotationStrategy
import org.simpleframework.xml.core.Persister

class XmlDeserializer {
    companion object {
        fun <T> getObjectFromXml(resourcePath: String, clazz: Class<T>): T {
            val resource = clazz.getResource(resourcePath).readText()
            val persister = Persister(AnnotationStrategy())
            return persister.read(clazz, resource, false)
        }
    }
}
