package com.etrade.mobilepro.inboxmessages.binding

import android.view.View
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.menuitem.MenuItemViewWithArrowAndSubtitle

class DynamicNavigationBinding(
    private val onItemClickListener: ((InboxMessage) -> Unit)? = null
) : InboxMessageBinding {

    override val layoutRes: Int = com.etrade.mobilepro.inboxmessages.R.layout.inbox_message_dynamic_navigation_view

    override fun bind(item: InboxMessage, view: View) {
        if (view is MenuItemViewWithArrowAndSubtitle) {
            view.menuTitleText = item.title
            view.menuSubTitleText = item.subtitle
        }

        view.setOnClickListener {
            onItemClickListener?.invoke(item)
        }
    }
}
