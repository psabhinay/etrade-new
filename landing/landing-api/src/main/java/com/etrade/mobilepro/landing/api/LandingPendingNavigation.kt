package com.etrade.mobilepro.landing.api

import androidx.navigation.NavDirections
import androidx.navigation.NavOptions

data class LandingPendingNavigation(
    val actualFragmentId: Int?,
    val navDirections: NavDirections,
    val navOptions: NavOptions?,
    val navPreparation: (() -> Unit)?
)
