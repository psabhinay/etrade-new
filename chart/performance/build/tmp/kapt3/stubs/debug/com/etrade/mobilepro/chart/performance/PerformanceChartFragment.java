package com.etrade.mobilepro.chart.performance;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0007\u0018\u0000 -2\u00020\u0001:\u0001-B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0014\u0010\u0018\u001a\u0004\u0018\u00010\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0002J\u0014\u0010\u001c\u001a\u0004\u0018\u00010\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0002J\u0014\u0010\u001d\u001a\u0004\u0018\u00010\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0002J\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0002J\u0012\u0010\"\u001a\u00020\u001f2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u0018\u0010%\u001a\u00020\u001f2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)H\u0002J\u0018\u0010*\u001a\u00020\u001f2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)H\u0002J\u001a\u0010+\u001a\u00020\u001f2\u0006\u0010,\u001a\u00020\u00192\b\u0010#\u001a\u0004\u0018\u00010$H\u0016R\u001b\u0010\u0007\u001a\u00020\b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\nR\u001b\u0010\r\u001a\u00020\u000e8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0013\u001a\u00020\u00148BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0017\u0010\u0012\u001a\u0004\b\u0015\u0010\u0016\u00a8\u0006."}, d2 = {"Lcom/etrade/mobilepro/chart/performance/PerformanceChartFragment;", "Landroidx/fragment/app/Fragment;", "viewModelFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "snackbarUtilFactory", "Lcom/etrade/mobilepro/util/android/snackbar/SnackbarUtilFactory;", "(Landroidx/lifecycle/ViewModelProvider$Factory;Lcom/etrade/mobilepro/util/android/snackbar/SnackbarUtilFactory;)V", "binding", "Lcom/etrade/mobilepro/chart/performance/databinding/FragmentPerformanceChartBinding;", "getBinding", "()Lcom/etrade/mobilepro/chart/performance/databinding/FragmentPerformanceChartBinding;", "binding$delegate", "Lkotlin/properties/ReadOnlyProperty;", "snackBarUtil", "Lcom/etrade/mobilepro/util/android/snackbar/SnackBarUtil;", "getSnackBarUtil", "()Lcom/etrade/mobilepro/util/android/snackbar/SnackBarUtil;", "snackBarUtil$delegate", "Lkotlin/Lazy;", "viewModel", "Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel;", "getViewModel", "()Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel;", "viewModel$delegate", "displayChart", "Landroid/view/View;", "lineData", "Lcom/etrade/mobilepro/chart/model/ChartLineData;", "displayChartError", "displayChartLoading", "onChartState", "", "state", "Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel$ChartState;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onMarketSelectionChanged", "toggle", "Lcom/etrade/mobilepro/ui/multitoggle/Toggle;", "selected", "", "onTimeIntervalSelectionChanged", "onViewCreated", "view", "Companion", "performance_debug"})
@com.etrade.mobilepro.common.RequireLogin()
public final class PerformanceChartFragment extends androidx.fragment.app.Fragment {
    private final kotlin.properties.ReadOnlyProperty binding$delegate = null;
    private final kotlin.Lazy viewModel$delegate = null;
    private final kotlin.Lazy snackBarUtil$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.etrade.mobilepro.chart.performance.PerformanceChartFragment.Companion Companion = null;
    private static final int PADDING_IN_PD = 8;
    
    @javax.inject.Inject()
    public PerformanceChartFragment(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.ViewModelProvider.Factory viewModelFactory, @org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory snackbarUtilFactory) {
        super();
    }
    
    private final com.etrade.mobilepro.chart.performance.databinding.FragmentPerformanceChartBinding getBinding() {
        return null;
    }
    
    private final com.etrade.mobilepro.chart.performance.PerformanceChartViewModel getViewModel() {
        return null;
    }
    
    private final com.etrade.mobilepro.util.android.snackbar.SnackBarUtil getSnackBarUtil() {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @kotlin.Suppress(names = {"LongMethod"})
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void onChartState(com.etrade.mobilepro.chart.performance.PerformanceChartViewModel.ChartState state) {
    }
    
    private final void onMarketSelectionChanged(com.etrade.mobilepro.ui.multitoggle.Toggle toggle, boolean selected) {
    }
    
    private final void onTimeIntervalSelectionChanged(com.etrade.mobilepro.ui.multitoggle.Toggle toggle, @kotlin.Suppress(names = {"UNUSED_PARAMETER"})
    boolean selected) {
    }
    
    private final android.view.View displayChart(com.etrade.mobilepro.chart.model.ChartLineData lineData) {
        return null;
    }
    
    private final android.view.View displayChartLoading(com.etrade.mobilepro.chart.model.ChartLineData lineData) {
        return null;
    }
    
    private final android.view.View displayChartError(com.etrade.mobilepro.chart.model.ChartLineData lineData) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/etrade/mobilepro/chart/performance/PerformanceChartFragment$Companion;", "", "()V", "PADDING_IN_PD", "", "performance_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}