package com.etrade.mobilepro.chart.formatters

import com.etrade.mobilepro.chart.model.DisplayLabel
import com.github.mikephil.charting.formatter.ValueFormatter
import org.threeten.bp.Instant
import org.threeten.bp.format.DateTimeFormatter

class XAxisValueIndexToDataFormatter(
    private val dateFormatter: DateTimeFormatter,
    private val listOfLabels: MutableList<DisplayLabel>?
) : ValueFormatter() {

    /**
     * Called when a value from an axis is to be formatted
     * before being drawn. For performance reasons, avoid excessive calculations
     * and memory allocations inside this method.
     */
    override fun getFormattedValue(value: Float): String {
        listOfLabels?.let {
            for (label in listOfLabels) {
                if (value == label.index.toFloat() && value > 1f) {
                    return dateFormatter.format(Instant.ofEpochMilli(label.timeInMillis))
                }
            }
        }
        return ""
    }
}
