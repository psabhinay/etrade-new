package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.util.MutualConverter

object ConditionalOrderActionValueId : ActionValueId {
    override val buy: String = "1"
    override val buyToCover: String = "4"
    override val exchange: String = ""
    override val sell: String = "2"
    override val sellShort: String = "3"
    override val converter: MutualConverter<String, TradeAction> by lazy { createConverter() }
}

internal object ConditionalOrderOptionsActionValueId : ActionValueId {
    override val buy: String = "1"
    override val buyToCover: String = "3"
    override val exchange: String = ""
    override val sell: String = "4"
    override val sellShort: String = "2"
    override val converter: MutualConverter<String, TradeAction> by lazy { createConverter() }
}
