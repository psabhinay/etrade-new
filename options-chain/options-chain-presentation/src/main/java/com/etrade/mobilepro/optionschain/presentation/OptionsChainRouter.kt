package com.etrade.mobilepro.optionschain.presentation

import androidx.navigation.NavController
import com.etrade.mobilepro.optionschains.api.Option
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.trade.form.api.SecurityType

interface OptionsChainRouter {

    fun openDisclosuresCharacteristicsAndRisks(navController: NavController)

    fun navigateToQuoteDetails(navController: NavController, quoteTicker: SearchResultItem.Symbol)

    fun navigateToOptionLegSelect(navArgs: TradeNavArgs)

    fun navigateToLogin(navController: NavController)
}

data class TradeNavArgs(
    val navController: NavController,
    val symbol: SearchResultItem.Symbol,
    val transactionType: TransactionType,
    val securityType: SecurityType,
    val option: Option
)
