package com.etrade.mobilepro.bank

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.common.RequireLogin
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import javax.inject.Inject

@RequireLogin
class BankDateTransactionPickerFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory
) : DialogFragment() {

    private val bankHomeViewModel: BankHomeViewModel by navGraphViewModels(R.id.bank_nav_graph) { viewModelFactory }
    private val args: BankDateTransactionPickerFragmentArgs by navArgs()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val date = getDateForPicker()

        val dialog = DatePickerDialog(
            requireContext(),
            R.style.MaterialDatePicker,
            DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                LocalDate.of(year, month + 1, dayOfMonth).let {
                    if (args.isStart) {
                        bankHomeViewModel.updateStartDate(it)
                    } else {
                        bankHomeViewModel.updateEndDate(it)
                    }
                }
            },
            date.year,
            date.monthValue - 1,
            date.dayOfMonth
        )

        updateSelectableDateRange(dialog.datePicker)

        return dialog
    }

    private fun updateSelectableDateRange(datePicker: DatePicker) {
        if (args.isStart) {
            val endDate = args.endDate
            datePicker.maxDate = endDate.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()
            val minDate = LocalDate.of(endDate.year, endDate.monthValue, endDate.dayOfMonth).minusYears(2)
            datePicker.minDate = minDate.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()
        } else {
            val startDate = args.startDate
            datePicker.minDate = startDate.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()
            datePicker.maxDate = LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()
        }
    }

    private fun getDateForPicker(): LocalDate {
        return if (args.isStart) {
            args.startDate
        } else {
            args.endDate
        }
    }
}
