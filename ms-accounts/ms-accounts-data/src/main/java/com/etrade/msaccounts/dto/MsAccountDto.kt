package com.etrade.msaccounts.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MsAccountDto(
    @Json(name = "accountDisplayName")
    val accountDisplayName: String = "",
    @Json(name = "accountDisplayNumber")
    val accountDisplayNumber: String = "",
    @Json(name = "accountUuid")
    val accountUuid: String = "",
    @Json(name = "balance")
    val balance: String = "",
    @Json(name = "balanceType")
    val balanceType: String = "",
    @Json(name = "instType")
    val instType: String = "",
    @Json(name = "keyAccountId")
    val keyAccountId: String = "",
    @Json(name = "platform")
    val platform: String = ""
)
