package com.etrade.mobilepro.dynamicui.dto.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class StatementDocumentsRequestDto(
    @Json(name = "statements")
    val statements: StatementDocumentsDto
)

@JsonClass(generateAdapter = true)
class StatementDocumentsDto(
    @Json(name = "accountUuid")
    val accountUuid: String,
    @Json(name = "startDate")
    val startDate: String,
    @Json(name = "endDate")
    val endDate: String,
    @Json(name = "appVersion")
    val appVersion: String
)
