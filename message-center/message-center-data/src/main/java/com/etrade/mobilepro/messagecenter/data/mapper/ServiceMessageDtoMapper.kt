package com.etrade.mobilepro.messagecenter.data.mapper

import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.api.model.ServiceMessage
import com.etrade.mobilepro.messagecenter.data.dto.ServiceMessageDetailsResponseDto

private const val EMPTY_VALUE = "--"
private val regex = "  +|\t+".toRegex() // two and more spaces or one and more tabs
private const val ALTERNATIVE_ACCOUNT_KEY = "Account Number"

fun ServiceMessageDetailsResponseDto.toServiceMessage(message: Message): ServiceMessage =
    ServiceMessage(
        accountId = this.accountId ?: "",
        subject = this.subject?.removeGarbage(" ") ?: EMPTY_VALUE,
        payload = this.payload
            ?.filterNotNull()
            ?.associateBy(
                keySelector = { it.title?.removeGarbage()?.reduceAccountKeysToStandard() ?: EMPTY_VALUE },
                valueTransform = { it.value?.removeGarbage() ?: EMPTY_VALUE }
            )
            ?.toMutableMap()
            ?.apply {
                if (containsKey(ServiceMessage.ACCOUNT_KEY)) {
                    this[ServiceMessage.ACCOUNT_KEY] = message.accountShortName
                }
            } ?: emptyMap(),
        messages = this.textFields
            ?.filterNotNull()
            ?.map {
                it.removeGarbage(reduceExtraSpaceTo = " ")
            } ?: emptyList(),
        errorDescription = this.error?.desc
    )

private fun String.removeGarbage(reduceExtraSpaceTo: String = ""): String =
    this.trim()
        .replace("\n", "")
        .replace(regex, reduceExtraSpaceTo)
        .run {
            if (this.equals("null", ignoreCase = true)) {
                EMPTY_VALUE
            } else {
                this
            }
        }

private fun String.reduceAccountKeysToStandard(): String = if (this == ALTERNATIVE_ACCOUNT_KEY) { ServiceMessage.ACCOUNT_KEY } else { this }
