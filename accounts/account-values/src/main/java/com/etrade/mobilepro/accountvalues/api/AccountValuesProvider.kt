package com.etrade.mobilepro.accountvalues.api

import com.etrade.eo.accountvalues.api.AccountValuesVO
import io.reactivex.Observable

interface AccountValuesProvider {
    fun accountValues(accountId: String, isRealTimeQuotes: Boolean): Observable<AccountValuesVO>
}
