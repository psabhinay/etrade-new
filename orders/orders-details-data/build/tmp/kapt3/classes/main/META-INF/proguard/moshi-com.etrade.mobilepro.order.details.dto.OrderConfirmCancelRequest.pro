-if class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequest
-keepnames class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequest
-if class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequest
-keep class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequestJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequest
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequest
-keepclassmembers class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequest {
    public synthetic <init>(java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.util.List,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
