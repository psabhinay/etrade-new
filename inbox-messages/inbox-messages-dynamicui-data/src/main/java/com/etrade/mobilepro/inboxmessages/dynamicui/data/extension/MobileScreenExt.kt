package com.etrade.mobilepro.inboxmessages.dynamicui.data.extension

import com.etrade.mobile.accounts.dto.AccountDto
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.MobileScreen

internal fun MobileScreen?.extractViewsAndAccounts(): Pair<List<BaseScreenView>, List<AccountDto>> {
    return Pair(
        first = this?.views.orEmpty().filterNotNull(),
        second = this?.references.orEmpty()
            .flatMap { (it.data as? List<*>).orEmpty() }
            .mapNotNull {
                it as? AccountDto
            }
    )
}
