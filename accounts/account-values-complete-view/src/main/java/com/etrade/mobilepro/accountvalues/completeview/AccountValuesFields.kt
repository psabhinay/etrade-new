package com.etrade.mobilepro.accountvalues.completeview

enum class AccountValuesFields(val field: String) {
    DAYS_GAIN_DOLLAR("OPEN_DAY_NET_GAIN_USD"),
    TOTAL_GAIN_DOLLAR("OPEN_NET_GAIN_USD"),
    TOTAL_GAIN_PERCENT("OPEN_NET_GAIN_PCT"),
    NET_ACCOUNT_VALUE("NET_LIQUIDATION_VALUE_USD");

    companion object {
        @JvmStatic
        val FIELD_SET = values().map { it.field }.toSet()
    }
}
