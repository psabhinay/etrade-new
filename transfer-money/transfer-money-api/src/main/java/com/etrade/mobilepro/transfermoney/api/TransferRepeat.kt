package com.etrade.mobilepro.transfermoney.api

enum class TransferRepeat(val responseFrequency: String) {
    None("ONE_TIME"),
    Week("WEEKLY"),
    TwoWeeks("BIWEEKLY"),
    FourWeeks("EVERY4WEEKS"),
    Month("MONTHLY"),
    EndOfMonth("ENDOFMONTH"),
    TwoMonths("BIMONTHLY"),
    ThreeMonths("QUARTERLY"),
    FourMonths("TRIANNUAL"),
    SixMonths("SEMIANNUAL"),
    Year("YEARLY");

    companion object {
        /**
         * Creates a TransferRepeat from Transfer Activity responses
         */
        fun fromTransferFrequencyResponse(transferFrequency: String?): TransferRepeat =
            transferFrequency?.let { frequency ->
                values().firstOrNull { it.responseFrequency == frequency } ?: None
            } ?: None
    }
}
