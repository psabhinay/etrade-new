package com.etrade.mobilepro.caching

import com.etrade.mobilepro.caching.coroutine.Cache
import com.etrade.mobilepro.caching.coroutine.CacheController
import com.etrade.mobilepro.caching.coroutine.DataSource
import com.etrade.mobilepro.common.result.ETResult
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.stub
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

private const val ID = 1
private const val ITEM = "data"

@ExperimentalCoroutinesApi
class CacheControllerTest {

    @Mock
    private lateinit var cacheSource: Cache<String, Int>
    @Mock
    private lateinit var networkSource: DataSource<String, Int>

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun `non-cached resource is fetch from network and then put to cache`() {
        cacheSource.stub {
            onBlocking { isCached(ID) } doReturn false
            onBlocking { put(ITEM, ID) } doReturn ETResult.success(Unit)
        }
        networkSource.stub {
            onBlocking { fetch(ID) } doReturn ETResult.success(ITEM)
        }

        runBlockingTest {
            CacheController(ID, networkSource, cacheSource)
                .fetch()
                .run {
                    assert(isSuccess)
                    assert(getOrNull() == ITEM)
                }

            verify(cacheSource, times(1)).isCached(ID)
            verify(cacheSource, never()).get(ID)
            verify(networkSource, times(1)).fetch(ID)
            verify(cacheSource, times(1)).put(ITEM, ID)
        }
    }

    @Test
    fun `network source is not requested when there is a cached item`() {
        cacheSource.stub {
            onBlocking { isCached(ID) } doReturn true
            onBlocking { get(ID) } doReturn ETResult.success(ITEM)
        }
        networkSource.stub { }

        runBlockingTest {
            CacheController(ID, networkSource, cacheSource)
                .fetch()
                .run {
                    assert(isSuccess)
                    assert(getOrThrow() == ITEM)
                }

            verify(cacheSource, times(1)).isCached(ID)
            verify(cacheSource, times(1)).get(ID)
            verify(networkSource, never()).fetch(any())
        }
    }

    @Test
    fun `use network source as fallback when cache fails`() {
        cacheSource.stub {
            onBlocking { isCached(ID) } doReturn true
            onBlocking { get(ID) } doReturn ETResult.failure(IllegalAccessException("Cache corrupted"))
            onBlocking { put(ITEM, ID) } doReturn ETResult.success(Unit)
        }
        networkSource.stub {
            onBlocking { fetch(ID) } doReturn ETResult.success(ITEM)
        }

        runBlockingTest {
            CacheController(ID, networkSource, cacheSource)
                .fetch()
                .run {
                    assert(isSuccess)
                    assert(getOrThrow() == ITEM)
                }

            verify(cacheSource, times(1)).isCached(ID)
            verify(cacheSource, times(1)).get(ID)
            verify(networkSource, times(1)).fetch(ID)
            verify(cacheSource, times(1)).put(ITEM, ID)
        }
    }

    @Test
    fun `non-cached item and network failure must return failure`() {
        cacheSource.stub {
            onBlocking { isCached(ID) } doReturn false
        }
        networkSource.stub {
            onBlocking { fetch(ID) } doReturn ETResult.failure(IllegalAccessException("Network failure"))
        }

        runBlockingTest {
            CacheController(ID, networkSource, cacheSource)
                .fetch()
                .run {
                    assert(isFailure)
                }

            verify(cacheSource, times(1)).isCached(ID)
            verify(networkSource, times(1)).fetch(ID)
            verify(cacheSource, never()).get(ID)
            verify(cacheSource, never()).put(ITEM, ID)
        }
    }

    @Test
    fun `exception occurring on caching must return failure`() {
        cacheSource.stub {
            onBlocking { isCached(ID) } doReturn false
            onBlocking { put(ITEM, ID) } doReturn ETResult.failure(IllegalAccessException("Disk error"))
        }
        networkSource.stub {
            onBlocking { fetch(ID) } doReturn ETResult.success(ITEM)
        }

        runBlockingTest {
            CacheController(ID, networkSource, cacheSource)
                .fetch()
                .run {
                    assert(isFailure)
                }

            verify(cacheSource, times(1)).isCached(ID)
            verify(networkSource, times(1)).fetch(ID)
            verify(cacheSource, never()).get(ID)
            verify(cacheSource, times(1)).put(ITEM, ID)
        }
    }
}
