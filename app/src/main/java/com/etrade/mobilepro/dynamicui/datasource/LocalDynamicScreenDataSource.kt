package com.etrade.mobilepro.dynamicui.datasource

import android.content.res.Resources
import com.etrade.mobilepro.R
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.backends.mgs.BaseMobileResponse
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.etrade.mobilepro.backends.mgs.MobileScreen
import com.etrade.mobilepro.balance.section.dto.BalanceDetailsViewDto
import com.etrade.mobilepro.biometric.api.BiometricsProvider
import com.etrade.mobilepro.bloombergtv.widget.dto.BloombergWidgetViewDto
import com.etrade.mobilepro.chart.linechart.dto.MarketChartViewDto
import com.etrade.mobilepro.common.EULA_TITLE
import com.etrade.mobilepro.common.EULA_URL
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dynamic.screen.data.source.ScreenDataSource
import com.etrade.mobilepro.dynamicui.DynamicDividerViewDto
import com.etrade.mobilepro.dynamicui.DynamicViewType
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.home.overview.dto.OverviewFooterViewDto
import com.etrade.mobilepro.indices.dto.Action
import com.etrade.mobilepro.indices.dto.BehaviourOptions
import com.etrade.mobilepro.indices.dto.MarketIndicesViewDto
import com.etrade.mobilepro.login.data.dto.LoginViewDto
import com.etrade.mobilepro.markets.broader.dto.BroaderMarketIndicesViewDto
import com.etrade.mobilepro.markets.earnings.TodayEarningsViewDto
import com.etrade.mobilepro.markets.footer.MarketsOverviewFooterViewDto
import com.etrade.mobilepro.markets.futures.widget.MarketsFuturesViewDto
import com.etrade.mobilepro.movers.markets.dto.MarketMoverViewDto
import com.etrade.mobilepro.movers.portfolio.dto.PortfolioMoverViewDto
import com.etrade.mobilepro.news.api.dto.NewsWidgetBriefingDto
import com.etrade.mobilepro.news.api.dto.NewsWidgetPortfolioDto
import com.etrade.mobilepro.news.api.dto.NewsWidgetTodayDto
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.settings.dto.SettingDto
import com.etrade.mobilepro.settings.dto.SettingsViewDto
import com.etrade.mobilepro.settings.dto.SettingsViewType
import com.etrade.mobilepro.themeselection.NightModePreferences
import com.etrade.mobilepro.util.Resource
import com.etrade.msaccounts.dto.AggregateValuesViewDto
import com.etrade.msaccounts.dto.MsAccountsSummaryDto
import com.etrade.msaccounts.dto.MsAccountsSummaryViewDto
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.Date
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Constructs Dynamic Screen views Locally
 *
 */
@SuppressWarnings("LongMethod", "ComplexMethod")
class LocalDynamicScreenDataSource @Inject constructor(
    @Web private val webBaseUrl: String,
    private val res: Resources,
    private val biometricsProvider: BiometricsProvider,
    private val nightModePreferences: NightModePreferences,
    private val user: User
) : ScreenDataSource {

    override fun getScreen(request: ScreenRequest, showCachedData: Boolean, cacheExpiration: Long?): Observable<Resource<BaseMobileResponse>> {
        val thickDivider = DynamicDividerViewDto(null, null)
        val screen = when (request) {
            is ServicePath.OverView -> {
                val views = mutableListOf<BaseScreenView>()

                views.add(LoginViewDto(null, null))
                views.add(MarketIndicesViewDto("", null))
                views.add(MarketMoverViewDto("", null))
                views.add(NewsWidgetTodayDto("", null))
                views.add(NewsWidgetBriefingDto("", null))
                views.add(OverviewFooterViewDto(Date().time, null))

                MobileScreen(views.toList(), emptyList())
            }
            is ServicePath.Balances -> {
                val views = mutableListOf<BaseScreenView>()
                views.add(BalanceDetailsViewDto("", request.accountId, request.userId, null))
                MobileScreen(views.toList(), emptyList())
            }
            is ServicePath.Settings -> {
                val views = mutableListOf<BaseScreenView>()
                val thinDivider = DynamicDividerViewDto(true, null)
                views.add(
                    SettingsViewDto(
                        SettingDto(title = res.getString(R.string.theme_selection_theme), value = nightModePreferences.uiModeName),
                        listOf(
                            CallToActionDto(
                                res.getString(R.string.theme_selection_theme),
                                ClickActionDto(res.getString(R.string.theme_selection_deep_link))
                            )
                        ),
                        SettingsViewType.SETTINGS_ACTION_THEME_SELECTION.value
                    )
                )
                views.add(thinDivider)
                views.add(
                    SettingsViewDto(
                        SettingDto("End-User License Agreement"),
                        listOf(CallToActionDto(EULA_TITLE, ClickActionDto(EULA_URL))),
                        SettingsViewType.SETTINGS_ACTION_END_USER_LICENSE_AGREEMENT.value
                    )
                )
                views.add(thinDivider)
                views.add(
                    SettingsViewDto(
                        SettingDto(res.getString(R.string.privacy_policy_title)),
                        listOf(
                            CallToActionDto(
                                label = res.getString(R.string.privacy_policy_title),
                                clickActionDto = ClickActionDto(
                                    webViewUrl = webBaseUrl + res.getString(R.string.privacy_policy_url),
                                    webViewTitle = res.getString(R.string.privacy_policy_title)
                                )
                            )
                        ),
                        SettingsViewType.SETTINGS_ACTION_PRIVACY_POLICY.value
                    )
                )
                views.add(thinDivider)
                views.add(SettingsViewDto(SettingDto("Version"), null, SettingsViewType.SETTINGS_TEXT_VERSION.value))
                views.add(thinDivider)

                MobileScreen(views.toList(), emptyList())
            }
            is ServicePath.AuthenticatedSettings -> {
                val views = mutableListOf<BaseScreenView>()
                val thinDivider = DynamicDividerViewDto(true, null)
                views.add(
                    SettingsViewDto(
                        SettingDto(title = "Default Homepage"), null, SettingsViewType.SETTINGS_PICKER_DEFAULT_HOME_PAGE.value
                    )
                )
                views.add(thinDivider)
                views.add(SettingsViewDto(SettingDto("Streaming and Extended Hours"), null, SettingsViewType.SETTINGS_TOGGLE_STREAMING.value))
                views.add(thinDivider)
                views.add(
                    SettingsViewDto(
                        SettingDto(title = res.getString(R.string.theme_selection_theme), value = nightModePreferences.uiModeName),
                        listOf(
                            CallToActionDto(
                                res.getString(R.string.theme_selection_theme),
                                ClickActionDto(res.getString(R.string.theme_selection_deep_link))
                            )
                        ),
                        SettingsViewType.SETTINGS_ACTION_THEME_SELECTION.value
                    )
                )
                views.add(thickDivider)
                if (biometricsProvider.canAuthenticate().isBiometricSupported) {
                    views.add(thinDivider)
                    views.add(SettingsViewDto(SettingDto("Biometric Authentication"), null, SettingsViewType.SETTINGS_TOGGLE_BIOMETRIC_AUTH.value))
                }
                views.add(thinDivider)
                views.add(SettingsViewDto(SettingDto("Remember User ID"), null, SettingsViewType.SETTINGS_TOGGLE_REMEMBER_USER_ID.value))
                views.add(thinDivider)
                views.add(SettingsViewDto(SettingDto("Session Time Out"), null, SettingsViewType.SETTINGS_PICKER_SESSION_TIME_OUT.value))

                views.add(thickDivider)

                views.add(
                    SettingsViewDto(
                        SettingDto("Trading Defaults"),
                        listOf(CallToActionDto("Trading Defaults", ClickActionDto("etrade://trading-defaults"))),
                        SettingsViewType.SETTINGS_ACTION_TRADING_DEFAULTS.value
                    )
                )
                views.add(thinDivider)
                views.add(SettingsViewDto(SettingDto("Stock Alerts", false.toString()), null, SettingsViewType.SETTINGS_TOGGLE_STOCK_ALERTS.value))
                views.add(thinDivider)
                views.add(SettingsViewDto(SettingDto("Account Alerts", false.toString()), null, SettingsViewType.SETTINGS_TOGGLE_ACCOUNT_ALERTS.value))

                views.add(thickDivider)

                views.add(
                    SettingsViewDto(
                        SettingDto("End-User License Agreement"),
                        listOf(CallToActionDto("View EULA", ClickActionDto("etrade://overlay/eula"))),
                        SettingsViewType.SETTINGS_ACTION_END_USER_LICENSE_AGREEMENT.value
                    )
                )
                views.add(thinDivider)
                views.add(
                    SettingsViewDto(
                        SettingDto(res.getString(R.string.privacy_policy_title)),
                        listOf(
                            CallToActionDto(
                                label = res.getString(R.string.privacy_policy_title),
                                clickActionDto = ClickActionDto(
                                    webViewUrl = webBaseUrl + res.getString(R.string.privacy_policy_url),
                                    webViewTitle = res.getString(R.string.privacy_policy_title)
                                )
                            )
                        ),
                        SettingsViewType.SETTINGS_ACTION_PRIVACY_POLICY.value
                    )
                )
                views.add(thickDivider)
                views.add(SettingsViewDto(SettingDto("Version"), null, SettingsViewType.SETTINGS_TEXT_VERSION.value))
                views.add(thinDivider)

                MobileScreen(views.toList(), emptyList())
            }
            is ServicePath.MarketOverview -> {
                val views = mutableListOf<BaseScreenView>()
                views.add(
                    MarketIndicesViewDto(
                        "",
                        null,
                        useMarketChangeColor = true,
                        showFutures = false,
                        behaviourOptions = BehaviourOptions(clickAction = Action.OPEN_INDEX_CHART)
                    )
                )
                views.add(MarketMoverViewDto("", null))
                views.add(MarketsFuturesViewDto("", null))
                views.add(NewsWidgetTodayDto("", null))
                views.add(BroaderMarketIndicesViewDto("", null))
                views.add(MarketsOverviewFooterViewDto(Date().time, null))

                MobileScreen(views.toList(), emptyList())
            }
            is ServicePath.AuthenticatedMarketOverview -> {
                val views = mutableListOf<BaseScreenView>()
                views.add(
                    MarketIndicesViewDto(
                        "",
                        null,
                        useMarketChangeColor = true,
                        showFutures = false
                    )
                )
                views.add(MarketChartViewDto("", null))
                views.addAll(
                    request.widgetList
                        .map(::mapWidgetDtoNameToDto)
                )
                views.add(MarketsOverviewFooterViewDto(Date().time, null))

                MobileScreen(views.toList(), emptyList())
            }
            else -> throw IllegalAccessException("Local dynamic Screen not configured for type: ${request.url}")
        }
        val response = BaseMobileResponse(screen)
        val responseObservable: Observable<Resource<BaseMobileResponse>> =
            Observable.interval(2, TimeUnit.MILLISECONDS)
                .take(2)
                .map {
                    if (it == 0L) {
                        Resource.Loading(response)
                    } else {
                        Resource.Success(response)
                    }
                }
        return responseObservable.subscribeOn(Schedulers.io())
    }

    private fun getSettingsViewDtoForDisclosures(): BaseScreenView = SettingsViewDto(
        SettingDto(res.getString(R.string.disclosures)),
        listOf(CallToActionDto("View Disclosures", ClickActionDto("etrade://overlay/disclosures"))),
        SettingsViewType.SETTINGS_ACTION_DISCLOSURES.value
    )
}

@Suppress("ComplexMethod")
internal fun mapWidgetDtoNameToDto(widgetDtoName: String): BaseScreenView = when (widgetDtoName) {
    DynamicViewType.MARKET_CHART.dtoName -> MarketChartViewDto("", null)
    DynamicViewType.BLOOMBERG_TV.dtoName -> BloombergWidgetViewDto("", null)
    DynamicViewType.MARKET_MOVERS.dtoName -> MarketMoverViewDto("", null)
    DynamicViewType.MARKET_FUTURES.dtoName -> MarketsFuturesViewDto("", null)
    DynamicViewType.NEWS_TODAY.dtoName -> NewsWidgetTodayDto("", null)
    DynamicViewType.BROADER_INDICES.dtoName -> BroaderMarketIndicesViewDto("", null)
    DynamicViewType.EARNINGS_TODAY.dtoName -> TodayEarningsViewDto("", null)
    DynamicViewType.PORTFOLIO_MOVERS.dtoName -> PortfolioMoverViewDto("", null)
    DynamicViewType.NEWS_PORTFOLIO.dtoName -> NewsWidgetPortfolioDto("", null)
    DynamicViewType.NEWS_BRIEFING.dtoName -> NewsWidgetBriefingDto("", null)
    DynamicViewType.MS_ACCOUNTS_SUMMARY.dtoName -> MsAccountsSummaryViewDto(
        createEmptyMsAccountsSummaryDto(), null
    )
    DynamicViewType.MS_ACCOUNTS_AGGREGATE_VALUES.dtoName -> AggregateValuesViewDto()
    else -> throw IllegalArgumentException("Unknown widget name $widgetDtoName")
}

private fun createEmptyMsAccountsSummaryDto() = MsAccountsSummaryDto(
    emptyList(), emptyList(), "", emptyList()
)
