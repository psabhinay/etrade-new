package com.etrade.mobilepro.documents

data class StatementDocument(
    override val documentId: String,
    override val institutionType: String,
    override val title: String,
    override val postEdgeFolder: String,
    override val accountUuid: String,
    override val headerTitle: String? = null,
    override val filename: String? = null
) : Document {
    override fun updateFilename(filename: String?): Document = this.copy(filename = filename)
}
