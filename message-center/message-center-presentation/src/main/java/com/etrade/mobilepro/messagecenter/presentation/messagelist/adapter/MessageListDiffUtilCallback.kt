package com.etrade.mobilepro.messagecenter.presentation.messagelist.adapter

import androidx.recyclerview.widget.DiffUtil
import com.etrade.mobilepro.messagecenter.api.model.Message

internal class MessageListDiffUtilCallback(
    private val oldList: List<Message>,
    private val newList: List<Message>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    // message.id is an unreliable field, sometimes it may come as a backend error description with random characters inside which differ every time
    // caseNo seems to be enough, createdAtFormatted was added here for counter-insurance's sake
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition].caseNo == newList[newItemPosition].caseNo &&
            oldList[oldItemPosition].createdAtFormatted == newList[newItemPosition].createdAtFormatted

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition].status == newList[newItemPosition].status &&
            oldList[oldItemPosition].subjectId == newList[newItemPosition].subjectId &&
            oldList[oldItemPosition].accountId == newList[newItemPosition].accountId &&
            oldList[oldItemPosition].createdAtFormatted == newList[newItemPosition].createdAtFormatted
}
