package com.etrade.mobilepro.messagecenter.api.model

import org.threeten.bp.Instant

data class SupportMessage(
    val commonDetails: CommonDetails = CommonDetails(),
    val history: List<Communication> = emptyList()
) {
    data class CommonDetails(
        val userId: String = "",
        val topicId: String = "",
        val logId: String = "",
        val replyEnabled: Boolean = false,
        val subject: String = "",
        val etFirstname: String = "",
        val etLastname: String = "",
        val status: String = "",
        val tier: String = "",
        val accountName: String = "",
        val createdAt: Instant? = null,
        val createdAtFormatted: String = ""
    )
    data class Communication(
        val caseNo: String = "",
        val message: String = "",
        val createdAt: Instant? = null,
        val createdAtFormatted: String = ""
    )
}
