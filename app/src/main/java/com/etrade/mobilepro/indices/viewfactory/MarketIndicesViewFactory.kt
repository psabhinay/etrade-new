package com.etrade.mobilepro.indices.viewfactory

import android.content.res.Resources
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.di.DowJonesColor
import com.etrade.mobilepro.common.di.NasdaqColor
import com.etrade.mobilepro.common.di.SP500Color
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.DynamicWidget
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.indices.MarketIndices
import com.etrade.mobilepro.indices.dto.MarketIndicesViewDto
import com.etrade.mobilepro.indices.usecase.GetTopIndicesUseCase
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.util.ClickAction
import com.etrade.mobilepro.util.android.CallToAction
import com.etrade.mobilepro.util.android.ChangeColorPicker
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class MarketIndicesViewFactory @Inject constructor(
    private val getTopIndicesUseCase: GetTopIndicesUseCase,
    private val changeColorPicker: ChangeColorPicker,
    private val marketTradeStatusDelegate: MarketTradeStatusDelegate,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    private val streamingStatusController: StreamingStatusController,
    resources: Resources,
    @DowJonesColor private val dowJonesColor: Int,
    @NasdaqColor private val nasdaqColor: Int,
    @SP500Color private val sP500Color: Int,
    private val marketsService: MoverService
) : DynamicUiViewFactory {

    private val logger: Logger = LoggerFactory.getLogger(MarketIndicesViewFactory::class.java)
    private val defaultErrorMessage = resources.getString(R.string.error_message_general)

    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is MarketIndicesViewDto) {
            return object : GenericLayout, DynamicWidget {

                override val viewModelClass = MarketIndices::class.java

                override fun refresh(viewModel: DynamicViewModel) {
                    if (viewModel is MarketIndices) {
                        viewModel.getMarketIndices()
                    } else {
                        logger.error("view model is not of type MarketIndices")
                    }
                }

                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    toMarketIndicesView(dto)
                }
                override val type = DynamicWidget.Type.MARKET_INDICES

                override val uniqueIdentifier: String = javaClass.name
            }
        } else {
            throw DynamicUiViewFactoryException("MarketIndicesViewFactory registered to unknown type")
        }
    }

    private fun toMarketIndicesView(viewDto: MarketIndicesViewDto): MarketIndices {
        return MarketIndices(
            getTopIndicesUseCase = getTopIndicesUseCase,
            changeColorPicker = changeColorPicker,
            marketTradeStatusDelegate = marketTradeStatusDelegate,
            streamingQuoteProvider = streamingQuoteProvider,
            streamingStatusController = streamingStatusController,
            callToAction = viewDto.ctaList?.let {
                CallToAction(it[0].label, ClickAction(null, null, it[0].clickActionDto.appUrl))
            },
            defaultErrorMessage = defaultErrorMessage,
            dowJonesColor = dowJonesColor,
            nasdaqColor = nasdaqColor,
            sP500Color = sP500Color,
            marketsService = marketsService,
            useMarketChangeColor = viewDto.useMarketChangeColor,
            showFutures = viewDto.showFutures,
            behaviourOptions = viewDto.behaviourOptions
        )
    }
}
