package com.etrade.mobilepro.trade.presentation.usecase

import androidx.lifecycle.LiveData
import com.etrade.mobile.accounts.defaultaccount.DefaultAccountRepo
import com.etrade.mobilepro.balance.viewmodel.AccountBalanceViewModel
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.getOrElse
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.viewmodel.CreateFormUseCase
import com.etrade.mobilepro.dynamic.form.viewmodel.CreateFormUseCaseParameter
import com.etrade.mobilepro.orders.api.TradeOrderType
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quote.search.history.QuoteSearchHistoryViewModel
import com.etrade.mobilepro.trade.api.TradeAccountRepo
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.SelectedAccountDelegate
import com.etrade.mobilepro.trade.mutualfund.presentation.MutualFundTradeViewModel
import com.etrade.mobilepro.trade.optionlegselect.TradeLegBidAskViewModel
import com.etrade.mobilepro.trade.presentation.TradeConditionalOrderDisclosureViewModel
import com.etrade.mobilepro.trade.presentation.form.TradeFormFactory
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface CreateTradeFormUseCase : CreateFormUseCase

class CreateTradeFormUseCaseImpl @Inject constructor(
    private val tradeAccountRepo: TradeAccountRepo,
    private val defaultAccountRepo: DefaultAccountRepo,
    private val tradeFormFactory: TradeFormFactory,
    private val accountDelegateFactory: SelectedAccountDelegate.Factory
) : CreateTradeFormUseCase {

    override suspend fun execute(parameter: CreateFormUseCaseParameter): ETResult<LiveData<Form>> {
        if (parameter !is CreateTradeFormParameter) {
            return ETResult.failure(IllegalArgumentException("Invalid parameter instance."))
        }

        val (accounts, defaultAccountId) = withContext(Dispatchers.IO) {
            val tradeAccountDeferred = async { tradeAccountRepo.getTradeAccounts() }
            val defaultAccountIdDeferred = async { defaultAccountRepo.getAccountId() }
            tradeAccountDeferred.await() to defaultAccountIdDeferred.await()
        }

        return accounts.fold(
            onSuccess = {
                tradeFormFactory.createTradeForm(
                    accounts = it,
                    accountDelegate = accountDelegateFactory.create(
                        accounts.getOrElse { emptyList() },
                        defaultAccountId.getOrNull()
                    ),
                    parameter = parameter
                )
            },
            onFailure = {
                ETResult.failure(it)
            }
        )
    }
}

class CreateTradeFormParameter(
    override val conditionalOrderDisclosureViewModel: TradeConditionalOrderDisclosureViewModel,
    override val onActionClickListener: OnActionClickListener,
    override val quoteWidgetViewModel: QuotesWidgetViewModel,
    override val defaultValues: InputValuesSnapshot,
    override val tradeOrderType: TradeOrderType,
    override val walkthroughViewModel: WalkthroughStatesViewModel,
    val advancedOrderBidAskQuoteWidgetViewModel: QuotesWidgetViewModel,
    val advancedOrderQuoteWidgetViewModel: QuotesWidgetViewModel,
    val balanceViewModel: AccountBalanceViewModel,
    val quoteSearchHistoryViewModel: QuoteSearchHistoryViewModel,
    val tradeLegBidAskViewModel: TradeLegBidAskViewModel,
    val mutualFundTradeViewModel: MutualFundTradeViewModel,
    val disableIfNoChange: Boolean,
    val securityType: SecurityType?
) : TradeFormParameter {

    @Suppress("LongParameterList", "LongMethod")
    fun copy(
        conditionalOrderDisclosureViewModel: TradeConditionalOrderDisclosureViewModel = this.conditionalOrderDisclosureViewModel,
        onActionClickListener: OnActionClickListener = this.onActionClickListener,
        quoteWidgetViewModel: QuotesWidgetViewModel = this.quoteWidgetViewModel,
        defaultValues: InputValuesSnapshot = this.defaultValues,
        tradeOrderType: TradeOrderType = this.tradeOrderType,
        advancedOrderBidAskQuoteWidgetViewModel: QuotesWidgetViewModel = this.advancedOrderBidAskQuoteWidgetViewModel,
        advancedOrderQuoteWidgetViewModel: QuotesWidgetViewModel = this.advancedOrderQuoteWidgetViewModel,
        balanceViewModel: AccountBalanceViewModel = this.balanceViewModel,
        quoteSearchHistoryViewModel: QuoteSearchHistoryViewModel = this.quoteSearchHistoryViewModel,
        tradeLegBidAskViewModel: TradeLegBidAskViewModel = this.tradeLegBidAskViewModel,
        mutualFundTradeViewModel: MutualFundTradeViewModel = this.mutualFundTradeViewModel,
        disableIfNoChange: Boolean = this.disableIfNoChange,
        securityType: SecurityType? = this.securityType,
        walkthroughViewModel: WalkthroughStatesViewModel = this.walkthroughViewModel
    ): CreateTradeFormParameter {
        return CreateTradeFormParameter(
            conditionalOrderDisclosureViewModel,
            onActionClickListener,
            quoteWidgetViewModel,
            defaultValues,
            tradeOrderType,
            walkthroughViewModel,
            advancedOrderBidAskQuoteWidgetViewModel,
            advancedOrderQuoteWidgetViewModel,
            balanceViewModel,
            quoteSearchHistoryViewModel,
            tradeLegBidAskViewModel,
            mutualFundTradeViewModel,
            disableIfNoChange,
            securityType
        )
    }
}
