package com.etrade.mobilepro.dynamic.form.api

import com.etrade.mobilepro.util.ValueCoder
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class SwitchInputBuilderTest {

    @Test
    fun `check SwitchInputBuilder default value coder`() {
        SwitchInputBuilder("id", mock()).valueCoder.let { coder ->
            coder.check(false)
            coder.check(true)
        }
    }

    private fun ValueCoder<Boolean>.check(expected: Boolean) = assertEquals(expected, decode(encode(expected)))
}
