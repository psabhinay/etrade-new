package com.etrade.mobilepro.dropdown.viewmodel

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dropdown.DropDownManager
import com.etrade.mobilepro.viewdelegate.ViewDelegate

class DropDownViewDelegate<T>(
    private val viewModel: DropDownViewModel<T>,
    private val dropDownManager: DropDownManager<T>
) : ViewDelegate {

    override fun observe(owner: LifecycleOwner) {
        viewModel.dropDown.observe(
            owner,
            Observer {
                onDropDown(it)
            }
        )
    }

    private fun onDropDown(model: DropDownModel<T>) {
        dropDownManager.apply {
            init(
                tag = model.tag,
                title = model.title,
                items = model.items,
                initialSelectedPosition = model.initialSelectedPosition,
                dividerPosition = model.dividerPosition
            )

            setListener { _, item ->
                viewModel.selectDropDownItem(item)
            }
        }
    }
}
