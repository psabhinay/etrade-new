package com.etrade.mobilepro.chart.formatters

import com.github.mikephil.charting.formatter.ValueFormatter
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import org.threeten.bp.format.DateTimeFormatter

class XAxisValueMinuteToDateFormatter(
    private val dateFormatter: DateTimeFormatter
) : ValueFormatter() {

    /**
     * Called when a value from an axis is to be formatted
     * before being drawn. For performance reasons, avoid excessive calculations
     * and memory allocations inside this method.
     */

    override fun getFormattedValue(value: Float): String {
        val timeInMillis = Duration.ofMinutes(value.toLong()).toMillis()
        return dateFormatter.format(Instant.ofEpochMilli(timeInMillis))
    }
}
