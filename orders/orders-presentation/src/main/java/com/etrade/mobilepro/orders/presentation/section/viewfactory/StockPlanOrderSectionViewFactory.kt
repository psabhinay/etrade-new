package com.etrade.mobilepro.orders.presentation.section.viewfactory

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderHistoryItem
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.orders.presentation.section.OrderDetailLabelResources
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.safeParseBigDecimal
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.Locale

const val NET_EXCERCISE_SELL = "Net Exercise and Sell"
const val NET_EXCERCISE_HOLD = "Net Exercise and Hold"
const val ORDER_EXECUTED = "Order Executed"
const val ORDER_PLACED = "Order Placed"
const val ORDER_CANCEL_REQUESTED = "Cancel Order requested"
const val ORDER_CANCELLED = "Order Cancelled"
const val CUSTOM = "Custom"
const val GOOD_THROUGH = "Good Through "

class StockPlanOrderSectionViewFactory : StockPlanOrderDetailSectionViewFactory {
    override fun createSectionView(
        detailsDto: BaseOrderDetailsItem.StockPlanOrderDetailsItem,
        resources: OrderDetailLabelResources
    ): List<TableSectionItemLayout> {
        val sections = mutableListOf<TableSectionItemLayout>()

        addHistoryItems(detailsDto.orderHistoryList, sections, resources)

        sections.add(createSummarySection(resources.stock.orderStatus, resources.defaultResources.resource.getString(detailsDto.orderStatus.getLabel())))
        sections.add(createSummarySection(resources.stock.orderNumber, detailsDto.orderNumber))
        sections.add(createSummarySection(resources.stock.orderType, detailsDto.orderType))
        sections.add(createSummarySection(resources.stock.description, detailsDto.description))
        sections.add(createSummarySection(resources.stock.exerciseValue, detailsDto.exerciseValue))
        sections.add(createSummarySection(resources.stock.sharesToExcercise, detailsDto.sharesToExercise))
        sections.add(createSummarySection(resources.stock.sharesSold, detailsDto.sharesSold))
        if (detailsDto.term == CUSTOM) {
            sections.add(createSummarySection(resources.stock.term, GOOD_THROUGH + detailsDto.gtdExpiration))
        } else {
            sections.add(createSummarySection(resources.stock.term, getTermDisplayString(detailsDto.term, resources, detailsDto)))
        }
        sections.add(createSummarySection(resources.stock.sharesToSell, detailsDto.sharesToSell))
        sections.add(createSummarySection(resources.stock.priceType, detailsDto.priceType))
        sections.add(createSummarySection(resources.stock.limitPrice, detailsDto.limitPrice))
        sections.add(createSummarySection(resources.stock.estGrossProceeds, detailsDto.estGrossProceeds))
        sections.add(createSummarySection(resources.stock.totalPrice, detailsDto.totalPrice))
        sections.add(createSummarySection(resources.stock.estimatedCommission, detailsDto.commission))
        sections.add(createSummarySection(resources.stock.sharesToIssue, detailsDto.sharesToIssue))
        sections.add(createSummarySection(resources.stock.secFee, detailsDto.secFee))
        sections.add(createSummarySection(resources.stock.brokerAssistFee, detailsDto.brokerAssistFee))
        sections.add(createSummarySection(resources.stock.disbursementFee, detailsDto.disbursementFee))
        sections.add(createSummarySection(resources.stock.estTaxesWithheld, detailsDto.taxesWithheld))
        getEstSharesWithheldValueText(detailsDto)?.let { withheld ->
            when {
                detailsDto.orderStatus == OrderStatus.SETTLED -> sections.add(createSummarySection(resources.stock.sharesWithheldValue, withheld))
                detailsDto.orderStatus != OrderStatus.CANCELLED -> sections.add(createSummarySection(resources.stock.estSharesWithheldValue, withheld))
                else -> { }
            }
        }
        sections.add(createSummarySection(resources.stock.estNetProceeds, detailsDto.estNetProceeds))
        return sections
    }

    private fun getEstSharesWithheldValueText(dto: BaseOrderDetailsItem.StockPlanOrderDetailsItem): String? {
        val exercised = dto.sharesExercised.safeParseBigDecimal() ?: return null
        val exercisedValue = dto.exerciseValue.safeParseBigDecimal() ?: return null
        val decimalFormat = DecimalFormat("#.##")
        val sharesExercised = dto.sharesExercised.safeParseBigDecimal() ?: BigDecimal.ZERO
        val sharesSold = dto.sharesSold.safeParseBigDecimal() ?: BigDecimal.ZERO
        val sharesWithheld = sharesExercised - sharesSold
        decimalFormat.roundingMode = RoundingMode.UP
        decimalFormat.minimumFractionDigits = 2
        val withheldValue = when (dto.orderType) {
            NET_EXCERCISE_SELL -> {
                val sold = dto.sharesSold.safeParseBigDecimal() ?: return null
                val withheldShares = exercised - sold
                decimalFormat.format(withheldShares * exercisedValue)
            }
            NET_EXCERCISE_HOLD -> {
                val toKeep = dto.sharesToKeep.safeParseBigDecimal() ?: return null
                val withheldShares = exercised - toKeep
                decimalFormat.format(withheldShares * exercisedValue)
            }
            else -> return null
        }
        return "$sharesWithheld/$$withheldValue"
    }

    private fun addHistoryItems(histories: List<OrderHistoryItem>, sections: MutableList<TableSectionItemLayout>, resources: OrderDetailLabelResources) {
        val executedDate = histories.filter { it.transaction == ORDER_EXECUTED }.map { it.date }.firstOrNull()
        val placedDate = histories.filter { it.transaction == ORDER_PLACED }.map { it.date }.firstOrNull()
        val cancelledDate = histories.filter { it.transaction == ORDER_CANCELLED }.map { it.date }.firstOrNull()
        val cancelRequestedDate = histories.filter { it.transaction == ORDER_CANCEL_REQUESTED }.map { it.date }.firstOrNull()
        executedDate?.let {
            sections.add(createSummarySection(resources.stock.orderExecuted, it))
        }
        placedDate?.let {
            sections.add(createSummarySection(resources.stock.orderPlaced, it))
        }
        cancelRequestedDate?.let {
            sections.add(createSummarySection(resources.stock.cancelRequested, it))
        }
        cancelledDate?.let {
            sections.add(createSummarySection(resources.stock.cancelledDate, it))
        }
    }

    private fun getTermDisplayString(text: String, resources: OrderDetailLabelResources, detailsDto: BaseOrderDetailsItem.StockPlanOrderDetailsItem): String {
        return when (text.lowercase(Locale.ROOT)) {
            "custom" -> GOOD_THROUGH + detailsDto.gtdExpiration
            "day" -> resources.defaultResources.resource.getString(OrderTerm.GOOD_FOR_DAY.getLabel())
            "gtc", "guc" -> resources.defaultResources.resource.getString(OrderTerm.GOOD_UNTIL_CANCEL.getLabel())
            "fok" -> resources.defaultResources.resource.getString(OrderTerm.FILL_OR_KILL.getLabel())
            "ioc" -> resources.defaultResources.resource.getString(OrderTerm.IMMEDIATE_OR_CANCEL.getLabel())
            "ehday" -> resources.defaultResources.resource.getString(OrderTerm.EXTENDED_HOUR_DAY.getLabel())
            "ehioc" -> resources.defaultResources.resource.getString(OrderTerm.EXTENDED_HOUR_IOC.getLabel())
            "gtcdni" -> resources.defaultResources.resource.getString(OrderTerm.GOOD_UNTIL_CANCEL_DNI.getLabel())
            "gtcdnr" -> resources.defaultResources.resource.getString(OrderTerm.GOOD_UNTIL_CANCEL_DNR.getLabel())
            "gtddni" -> resources.defaultResources.resource.getString(OrderTerm.GOOD_TILL_DATE_DNI.getLabel())
            "gtddnr" -> resources.defaultResources.resource.getString(OrderTerm.GOOD_TILL_DATE_DNR.getLabel())
            "n/a" -> text
            else -> MarketDataFormatter.EMPTY_FORMATTED_VALUE
        }
    }
}
