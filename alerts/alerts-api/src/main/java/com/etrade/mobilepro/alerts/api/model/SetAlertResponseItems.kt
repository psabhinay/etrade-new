package com.etrade.mobilepro.alerts.api.model

interface AlertResponse {
    val hasError: Boolean
    val errorMessage: String
    val hasAlerts: Boolean
}

data class SetAlertResponse(
    override val hasError: Boolean,
    override val errorMessage: String,
    override val hasAlerts: Boolean,
    val alerts: List<UserAlertItem.Alert>
) : AlertResponse

data class SetNewsAlertResponse(
    override val hasError: Boolean,
    override val errorMessage: String,
    override val hasAlerts: Boolean,
    val alerts: List<UserAlertItem.NewsAlert>
) : AlertResponse
