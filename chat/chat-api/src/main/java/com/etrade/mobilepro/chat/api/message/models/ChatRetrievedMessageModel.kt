package com.etrade.mobilepro.chat.api.message.models

import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType
import java.io.Serializable

data class ChatRetrievedMessageModel(val data: ChatRetrievedMessageDataModel, val success: Boolean, val sequence: Int?) : Serializable

data class ChatRetrievedMessageDataModel(val items: List<ChatRetrieveMessageItemModel>?) : Serializable

data class ChatRetrieveMessageItemModel(
    val type: ChatRetrieveMessageType,
    val message: MessageModel
) : Serializable

data class MessageModel(
    val agentId: String? = null,
    val connectionTimeout: Long? = null,
    val name: String? = null,
    val queuePosition: Int? = null,
    val sneakPeekEnabled: Boolean? = null,
    val text: String? = null,
    val transcriptSaveEnabled: Boolean? = null,
    val url: String? = null,
    val userId: String? = null,
    val visitorId: String? = null,
    // Chat queue update
    val estimatedWaitTime: Int? = null,
    val position: Int? = null,
    // ChatRequest Fail
    val reason: String? = null
) : Serializable
