package com.etrade.mobilepro.quote

import androidx.annotation.DrawableRes
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.util.domain.data.deriveDayChange
import com.etrade.mobilepro.util.domain.data.deriveDayChangePercent
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom

@Suppress("MagicNumber")
@DrawableRes
fun getFundRatingsDrawable(rating: Int?) = when (rating) {
    1 -> R.drawable.fund_ratings_1
    2 -> R.drawable.fund_ratings_2
    3 -> R.drawable.fund_ratings_3
    4 -> R.drawable.fund_ratings_4
    5 -> R.drawable.fund_ratings_5
    else -> R.drawable.fund_ratings_0
}

fun MobileQuote.getFormattedLastPrice(): String {
    val lastPrice = extendedQuoteHrDetail?.lastPrice ?: quotePrice.lastPrice
    return MarketDataFormatter.formatMarketDataCustom(lastPrice.toBigDecimalOrNull())
}

fun MobileQuote.deriveDayChange() = deriveDayChange(extendedQuoteHrDetail?.change ?: "0", quotePrice.change)

fun MobileQuote.deriveDayChangePercent() = deriveDayChangePercent(deriveDayChange(), quotePrice.lastPrice, quotePrice.change)
