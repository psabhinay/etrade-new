package com.etrade.mobilepro.movers.portfolio.caching

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.common.di.Default
import com.etrade.mobilepro.dynamicui.CachedResponseModel
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.dynamicui.delegate.WithRealmDataSourceDelegate
import com.etrade.mobilepro.portfoliosnapshot.NeoPortfolioSnapshotResponse
import com.etrade.mobilepro.util.Resource
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.Moshi
import io.reactivex.Observable
import java.io.IOException
import javax.inject.Inject

class PortfolioSnapshotRepoImpl @Inject constructor(
    moshi: Moshi,
    @Default
    private val portfolioSnapshotDataSourceDelegate: WithRealmDataSourceDelegate
) : PortfolioSnapshotRepo {

    private val moshiAdapter = moshi.adapter(NeoPortfolioSnapshotResponse::class.java)

    override fun getPortfolioMovers(request: ScreenRequest, cacheExpiration: Long?): Observable<Resource<NeoPortfolioSnapshotResponse>> =
        SimpleCachedResource(
            id = request,
            dataSourceDelegate = portfolioSnapshotDataSourceDelegate,
            controller = object : SimpleCachedResource.Controller<CachedResponseModel> {
                override fun shouldRefresh(itemFromCache: CachedResponseModel): Boolean {
                    return cacheExpiration?.let {
                        System.currentTimeMillis() - itemFromCache.retrievedTimestamp >= it
                    } ?: true
                }
            },
            showCachedData = false
        ).asObservable().map { cachedResponse ->
            try {
                val deserializedResponse = cachedResponse.data?.response?.let {
                    moshiAdapter.fromJson(it)
                }

                when (cachedResponse) {
                    is Resource.Loading -> Resource.Loading(deserializedResponse)
                    is Resource.Success -> Resource.Success(deserializedResponse)
                    is Resource.Failed -> {
                        Resource.Failed(deserializedResponse, cachedResponse.error)
                    }
                }
            } catch (e: IOException) {
                Resource.Failed<NeoPortfolioSnapshotResponse>(error = e)
            } catch (e: JsonDataException) {
                Resource.Failed<NeoPortfolioSnapshotResponse>(error = e)
            }
        }
}
