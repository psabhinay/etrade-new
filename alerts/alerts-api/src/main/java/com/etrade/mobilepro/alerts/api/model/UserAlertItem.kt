package com.etrade.mobilepro.alerts.api.model

sealed class UserAlertItem {
    abstract val symbol: String
    abstract val description: String
    abstract val content: String
    abstract val delete: String
    abstract val deleteInfo: String
    abstract val isDeleteBtnVisible: Boolean
    abstract val edit: String
    abstract val lastEdited: String

    // this is common feature for all UserAlertItem entities
    abstract fun copyAndToggleDeleteBtn(isDeleteBtnVisible: Boolean): UserAlertItem

    data class Alert(
        override val symbol: String,
        override val content: String,
        override val delete: String,
        override val deleteInfo: String,
        override val description: String,
        override val isDeleteBtnVisible: Boolean,
        override val edit: String,
        override val lastEdited: String,
        val targetPriceHigh: StatusValue,
        val targetPriceLow: StatusValue,
        val dailyPercentHigh: StatusValue,
        val dailyPercentLow: StatusValue,
        val peTargetHigh: StatusValue,
        val peTargetLow: StatusValue,
        val dailyVolume: StatusValue
    ) : UserAlertItem() {
        override fun copyAndToggleDeleteBtn(isDeleteBtnVisible: Boolean): UserAlertItem = copy(isDeleteBtnVisible = isDeleteBtnVisible)
    }

    data class NewsAlert(
        override val symbol: String,
        override val content: String,
        override val delete: String,
        override val deleteInfo: String,
        override val description: String,
        override val isDeleteBtnVisible: Boolean,
        override val edit: String,
        override val lastEdited: String,
        val inPlayStatus: Boolean,
        val upDownStatus: Boolean,
        val earningsStatus: Boolean,
        val splitNoticeStatus: Boolean
    ) : UserAlertItem() {
        override fun copyAndToggleDeleteBtn(isDeleteBtnVisible: Boolean): UserAlertItem = copy(isDeleteBtnVisible = isDeleteBtnVisible)
    }

    companion object {
        fun isSameDeleteInfo(left: UserAlertItem, right: UserAlertItem): Boolean {
            return left.symbol == right.symbol &&
                left.description == right.description && left.deleteInfo == right.deleteInfo
        }
    }
}

data class StatusValue(
    private val value: String,
    private val alertStatus: AlertStatus
) {
    enum class AlertStatus { NONE, DISABLED, ENABLED }
    val status: Boolean = alertStatus != AlertStatus.NONE
    fun activeValue(): String = if (status) { value.removePrefix("0") } else { "" }
}
