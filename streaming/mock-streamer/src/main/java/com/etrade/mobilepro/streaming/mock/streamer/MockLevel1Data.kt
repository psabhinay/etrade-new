package com.etrade.mobilepro.streaming.mock.streamer

import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.OptionData
import com.etrade.mobilepro.streaming.api.StockData
import org.threeten.bp.LocalDate
import kotlin.random.Random

private const val MIN_RANDOM_VALUE = 0.0
private const val MAX_RANDOM_VALUE = 1000.0

private fun generateRandomValue(): String = Random.nextDouble(MIN_RANDOM_VALUE, MAX_RANDOM_VALUE).toString()

open class MockLevel1Data : Level1Data {
    override val ask: String? = generateRandomValue()
    override val askExchange: String? = generateRandomValue()
    override val askSize: String? = generateRandomValue()
    override val bid: String? = generateRandomValue()
    override val bidExchange: String? = generateRandomValue()
    override val bidSize: String? = generateRandomValue()
    override val change: String? = generateRandomValue()
    override val changePercent: String? = generateRandomValue()
    override val closingMark: String? = generateRandomValue()
    override val dayHigh: String? = generateRandomValue()
    override val dayLow: String? = generateRandomValue()
    override val earningsPerShare: String? = generateRandomValue()
    override val lastPrice: String? = generateRandomValue()
    override val lastPriceExchange: String? = generateRandomValue()
    override val lastVolume: String? = generateRandomValue()
    override val mark: String? = generateRandomValue()
    override val openPrice: String? = generateRandomValue()
    override val previousClosePrice: String? = generateRandomValue()
    override val priceToEarnings: String? = generateRandomValue()
    override val timestampSinceEpoch: Long? = System.currentTimeMillis()
    override val volume: String? = generateRandomValue()
    override val tickIndicator: Short? = Random.nextInt(-1, 2).toShort()
    override val yearHigh: String? = generateRandomValue()
    override val yearHighDate: LocalDate? = LocalDate.now()
    override val yearLow: String? = generateRandomValue()
    override val yearLowDate: LocalDate? = LocalDate.now()
}

class MockOptionData : MockLevel1Data(), OptionData {
    override val openInterest: String? = generateRandomValue()
    override val intrinsicValue: String? = generateRandomValue()
    override val delta: String? = generateRandomValue()
    override val gamma: String? = generateRandomValue()
    override val theta: String? = generateRandomValue()
    override val vega: String? = generateRandomValue()
    override val rho: String? = generateRandomValue()
    override val impliedVolatility: String? = generateRandomValue()
}

class MockStockData : MockLevel1Data(), StockData {
    override val beta: String? = generateRandomValue()
    override val dayChange: String? = generateRandomValue()
    override val dayChangePercent: String? = generateRandomValue()
    override val dividend: String? = generateRandomValue()
    override val dividendYield: String? = generateRandomValue()
    override val marketCap: String? = generateRandomValue()
    override val tradeStatus: String? = "N"
    override val tradeHaltReason: String? = null
    override val volumeTenDayAverage: String? = generateRandomValue()
}
