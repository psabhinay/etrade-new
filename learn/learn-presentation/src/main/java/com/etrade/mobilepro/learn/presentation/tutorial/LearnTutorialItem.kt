package com.etrade.mobilepro.learn.presentation.tutorial

import com.etrade.mobilepro.walkthrough.api.WalkthroughScenario

data class LearnTutorialItem(
    val tutorialIconId: Int,
    val tutorialText: String,
    val walkthroughScenario: WalkthroughScenario
)
