package com.etrade.mobilepro.trade.data.mutualfunds

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.OrigSysCode.Companion.origSysCodeKey
import com.etrade.mobilepro.orders.api.OrigSysCode.ORIG_SYS_CODE_ANDR
import com.etrade.mobilepro.trade.data.DAY_TRADING_FLAG
import com.etrade.mobilepro.trade.data.form.input.DefaultMutualFundTradeFormInputId.action
import com.etrade.mobilepro.trade.data.form.input.DefaultMutualFundTradeFormInputId.entireCashPosition
import com.etrade.mobilepro.trade.data.form.input.DefaultMutualFundTradeFormInputId.entireMarginPosition
import com.etrade.mobilepro.trade.data.form.input.DefaultMutualFundTradeFormInputId.fundToBuy
import com.etrade.mobilepro.trade.data.form.input.DefaultMutualFundTradeFormInputId.positionTypeToTrade
import com.etrade.mobilepro.trade.data.form.input.DefaultMutualFundTradeFormInputId.quantity
import com.etrade.mobilepro.trade.data.form.input.DefaultMutualFundTradeFormInputId.securityType
import com.etrade.mobilepro.trade.data.form.input.DefaultMutualFundTradeFormInputId.symbol
import com.etrade.mobilepro.trade.data.form.input.DefaultMutualFundTradeFormInputId.symbolCoder
import com.etrade.mobilepro.trade.data.mutualfunds.dto.LegDetails
import com.etrade.mobilepro.trade.data.mutualfunds.dto.MutualFundQuantity
import com.etrade.mobilepro.trade.data.mutualfunds.dto.MutualFundsTradePlaceRequestDto
import com.etrade.mobilepro.trade.data.mutualfunds.dto.MutualFundsTradeRequestValueDto
import com.etrade.mobilepro.util.filterNotNullValues
import com.etrade.mobilepro.util.json.toJsonString
import com.squareup.moshi.Moshi

fun toMutualFundBuyPlaceRequest(previewIds: List<String>, overnightIndicatorFlag: String): MutualFundsTradeRequestValueDto = MutualFundsTradeRequestValueDto(
    tradePlaceOrderRequestDto = MutualFundsTradePlaceRequestDto(
        previewIds = previewIds,
        overnightIndicatorFlag = overnightIndicatorFlag
    )
)

private fun String.toLegDetails(quantity: List<MutualFundQuantity>? = null, transaction: String = BUY_FUNDS_ORDER): LegDetails {
    val symbol = symbolCoder.decode(this)
    return LegDetails(symbol?.symbol, transaction, quantityRaw = quantity?.firstOrNull())
}

internal fun InputValuesSnapshot.createRequestBody(moshi: Moshi, isTablet: Boolean): String {
    val body: MutableMap<String, Any?> = toMutableMap()
    body[PLATFORM] = "android"
    body[DAY_TRADING_FLAG] = "true"
    body[origSysCodeKey] = ORIG_SYS_CODE_ANDR.numberCode
    body["orderSource"] = if (isTablet) "tablet" else "android"

    body.updatePositionQuantity()

    // Transaction can only be a Buy or Sell,
    // exchange is a sell transaction (primary leg) with a buy transaction
    val transaction: String = when (body[action]) {
        EXCHANGE_FUNDS_ORDER -> SELL_FUNDS_ORDER
        else -> body[action] as String
    }

    val primaryLeg = (this[symbol] as? String)?.toLegDetails(body.toQuantity(), transaction)

    // Leg to Exchange Owned Funds
    val exchangeToLeg = body[fundToBuy]?.toString()?.toLegDetails()

    body[symbol] = listOfNotNull(primaryLeg, exchangeToLeg)
    body.removeUnusedElements()
    return moshi.toJsonString(mapOf("value" to body.filterNotNullValues()))
}

private fun MutableMap<String, Any?>.toQuantity(): List<MutualFundQuantity> = listOfNotNull(
    toMutualFundQuantity(this[quantity.dollars.cash] as String?, this[quantity.dollars.margin] as String?),
    toMutualFundQuantity(this[quantity.shares.cash] as String?, this[quantity.shares.margin] as String?),
    toMutualFundQuantity(this[entireCashPosition] as String?, this[entireMarginPosition] as String?)
)

private fun MutableMap<String, Any?>.updatePositionQuantity() {
    when (this[positionTypeToTrade]) {
        QUANTITY_CASH_POSITION -> {
            this[POSITION_QUANTITY_SUBTYPE] = CASH_POSITION
            this[positionTypeToTrade] = QUANTITY_ENTIRE_POSITION
        }
        QUANTITY_ENTIRE_POSITION -> {
            this[POSITION_QUANTITY_SUBTYPE] = CASH_MARGIN_POSITION
            this[positionTypeToTrade] = QUANTITY_ENTIRE_POSITION
        }
        QUANTITY_MARGIN_POSITION -> {
            this[POSITION_QUANTITY_SUBTYPE] = MARGIN_POSITION
            this[positionTypeToTrade] = QUANTITY_ENTIRE_POSITION
        }
        // default value added when buy funds, for all other cases values set by the Form
        null -> this[positionTypeToTrade] = QUANTITY_TYPE_BUY
    }
}

private fun toMutualFundQuantity(cash: String?, margin: String?): MutualFundQuantity? {
    return if (cash != null || margin != null) {
        MutualFundQuantity(cash = cash, margin = margin)
    } else {
        null
    }
}

private fun MutableMap<String, Any?>.removeUnusedElements() {
    remove(quantity.dollars.cash)
    remove(quantity.dollars.margin)
    remove(quantity.shares.cash)
    remove(quantity.shares.margin)
    remove(entireCashPosition)
    remove(entireMarginPosition)
    remove(securityType)
    remove(fundToBuy)
}
