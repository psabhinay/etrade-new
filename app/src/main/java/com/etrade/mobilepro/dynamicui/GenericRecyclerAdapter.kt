package com.etrade.mobilepro.dynamicui

import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.dynamicui.api.DynamicLayout

class GenericRecyclerAdapter(
    lifecycleOwner: LifecycleOwner,
    values: List<DynamicLayout>
) : DynamicUiLayoutRecyclerAdapter(lifecycleOwner, values)
