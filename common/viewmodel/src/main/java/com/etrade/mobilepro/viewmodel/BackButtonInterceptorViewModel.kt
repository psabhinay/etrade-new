package com.etrade.mobilepro.viewmodel

import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import javax.inject.Inject

class BackButtonInterceptorViewModel @Inject constructor() : ViewModel(), ActivitySignalViewModel, LifecycleObserver {

    val backPressedSignal: LiveData<ConsumableLiveEvent<Unit>>
        get() = _backPressedSignal

    override val activitySignal: LiveData<ConsumableLiveEvent<ActivitySignal>>
        get() = _activitySignal

    private val _backPressedSignal: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()

    private val _activitySignal: MutableLiveData<ConsumableLiveEvent<ActivitySignal>> = MutableLiveData()

    private val onBackPressedCallback = object : OnBackPressedCallback(false) {
        override fun handleOnBackPressed() {
            _backPressedSignal.value = Unit.consumable()
        }
    }

    init {
        withActivity {
            (it as? FragmentActivity)?.onBackPressedDispatcher?.addCallback(onBackPressedCallback)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun connectListener() {
        onBackPressedCallback.isEnabled = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun disconnectListener() {
        onBackPressedCallback.isEnabled = false
    }

    private fun withActivity(signal: ActivitySignal) {
        _activitySignal.value = signal.consumable()
    }
}
