package com.etrade.mobilepro.education.adapter.holder

import android.view.View
import android.view.View.IMPORTANT_FOR_ACCESSIBILITY_YES
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.education.EducationItem
import com.etrade.mobilepro.education.databinding.AdapterEducationSectionBinding

class EducationSectionViewHolder(private val binding: AdapterEducationSectionBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(
        item: EducationItem,
        levelTemplate: String,
        clickListener: View.OnClickListener
    ) {
        val headerText = String.format(levelTemplate, item.level)
        with(binding) {
            header.text = headerText
            header.contentDescription = headerText
            header.importantForAccessibility = IMPORTANT_FOR_ACCESSIBILITY_YES
            header.visibility = View.VISIBLE
            with(body) {
                root.tag = item
                root.setOnClickListener(clickListener)
                title.text = item.title
                author.text = item.author
                length.text = item.length
                description.text = item.description
            }
        }
    }
}
