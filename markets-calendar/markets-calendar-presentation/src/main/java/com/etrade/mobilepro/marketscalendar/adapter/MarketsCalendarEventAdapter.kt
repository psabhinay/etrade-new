package com.etrade.mobilepro.marketscalendar.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.marketscalendar.R
import com.etrade.mobilepro.marketscalendar.adapter.holder.CalendarEventViewHolder
import com.etrade.mobilepro.marketscalendar.databinding.AdapterEventBinding
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails
import com.etrade.mobilepro.util.android.extension.viewBinding

class MarketsCalendarEventAdapter(
    private val onItemClickListener: (EventDetails) -> Unit
) : RecyclerView.Adapter<CalendarEventViewHolder>() {

    private val items = mutableListOf<EventDetails>()

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarEventViewHolder {
        val context = parent.context
        return CalendarEventViewHolder(
            parent.viewBinding(AdapterEventBinding::inflate),
            context.getString(R.string.empty_default),
            View.OnClickListener { onItemClickListener(it.tag as EventDetails) }
        )
    }

    override fun onBindViewHolder(holder: CalendarEventViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun updateEvents(events: List<EventDetails>?) {
        items.clear()
        events?.also { items.addAll(it) }
        notifyDataSetChanged()
    }
}
