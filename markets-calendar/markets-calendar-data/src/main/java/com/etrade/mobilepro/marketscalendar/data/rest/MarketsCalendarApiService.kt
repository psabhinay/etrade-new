package com.etrade.mobilepro.marketscalendar.data.rest

import com.etrade.mobilepro.marketscalendar.data.dto.EventsEarningsData
import com.etrade.mobilepro.marketscalendar.data.dto.EventsEconomicData
import com.etrade.mobilepro.marketscalendar.data.dto.EventsRequestBody
import com.etrade.mobilepro.marketscalendar.data.dto.EventsResponseDto
import com.etrade.mobilepro.marketscalendar.data.dto.EventsSummaryResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface MarketsCalendarApiService {

    @JsonMoshi
    @POST("/marketmobile/calendar/geteventssummary.json")
    fun getEventsSummary(@Body @JsonMoshi body: EventsRequestBody): Single<EventsSummaryResponseDto>

    @JsonMoshi
    @POST("/marketmobile/calendar/getevents.json")
    fun getEventsEarningsRx(@Body @JsonMoshi body: EventsRequestBody): Single<EventsResponseDto<EventsEarningsData>>

    @JsonMoshi
    @POST("/marketmobile/calendar/getevents.json")
    suspend fun getEventsEarnings(@Body @JsonMoshi body: EventsRequestBody): EventsResponseDto<EventsEarningsData>

    @JsonMoshi
    @POST("/marketmobile/calendar/getevents.json")
    fun getEventsEconomic(@Body @JsonMoshi body: EventsRequestBody): Single<EventsResponseDto<EventsEconomicData>>
}
