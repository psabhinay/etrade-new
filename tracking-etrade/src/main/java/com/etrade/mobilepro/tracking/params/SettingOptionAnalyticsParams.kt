package com.etrade.mobilepro.tracking.params

import com.etrade.mobilepro.tracking.trackerValue

private const val EVENT_NAME_BIOMETRIC_AUTHENTICATION = "Biometric Authentication"
private const val EVENT_NAME_STOCK_ALERTS = "Stock Alerts"
private const val EVENT_NAME_ACCOUNT_ALERTS = "Account Alerts"

private const val ATTRIBUTE_NAME_BIOMETRIC_AUTHENTICATION_ENABLED = "Biometric Authentication Enabled"
private const val ATTRIBUTE_NAME_STOCK_ALERTS_ENABLED = "Stock Alerts Enabled"
private const val ATTRIBUTE_NAME_ACCOUNT_ALERTS_ENABLED = "Account Alerts Enabled"

sealed class SettingOptionAnalyticsParams(
    override val name: String,
    private val attribute: String
) : AnalyticsParams {

    abstract val value: Boolean

    override val params: Map<String, String>
        get() = mapOf(
            attribute to value.trackerValue
        )
}

data class BiometricAuthenticationAnalyticsParams(override val value: Boolean) :
    SettingOptionAnalyticsParams(EVENT_NAME_BIOMETRIC_AUTHENTICATION, ATTRIBUTE_NAME_BIOMETRIC_AUTHENTICATION_ENABLED)

data class StockAlertsAnalyticsParams(override val value: Boolean) :
    SettingOptionAnalyticsParams(EVENT_NAME_STOCK_ALERTS, ATTRIBUTE_NAME_STOCK_ALERTS_ENABLED)

data class AccountAlertsAnalyticsParams(override val value: Boolean) :
    SettingOptionAnalyticsParams(EVENT_NAME_ACCOUNT_ALERTS, ATTRIBUTE_NAME_ACCOUNT_ALERTS_ENABLED)
