package com.etrade.mobilepro.common.navigation

import android.net.Uri
import androidx.annotation.IdRes
import androidx.navigation.NavDirections

interface NavRouter {
    fun hasDestinationInBackStack(@IdRes destinationId: Int): Boolean
    fun navigate(directions: NavDirections)
    fun navigate(uri: Uri)
    fun navigate(action: () -> Unit)
    fun navigateToLandingPage()
    fun onLoggedIn()
    fun navigateFromAppAction(uri: Uri)
}
