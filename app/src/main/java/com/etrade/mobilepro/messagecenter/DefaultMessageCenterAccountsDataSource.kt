package com.etrade.mobilepro.messagecenter

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterAccountsDataSource
import javax.inject.Inject

class DefaultMessageCenterAccountsDataSource @Inject constructor(
    private val daoProvider: AccountDaoProvider
) : MessageCenterAccountsDataSource {
    override suspend fun getAccountIdToShortDescriptionMap(): ETResult<Map<String, String>> {
        return runCatchingET {
            daoProvider.getAccountDao()
                .getAllAccountsNonAsync()
                .associateBy(keySelector = { it.accountId }, valueTransform = { it.accountShortName })
        }
    }
}
