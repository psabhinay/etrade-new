package com.etrade.mobilepro.masked.input.value.view

import android.content.Context
import android.util.AttributeSet

class LongMaskedInputEditText : MaskedInputEditText<Long?> {

    constructor(ctx: Context) : super(ctx)

    constructor(ctx: Context, attributeSet: AttributeSet?) : super(ctx, attributeSet)

    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    override fun convertTextToValue(text: CharSequence): Long? {
        return try {
            text.toString().toLong()
        } catch (e: NumberFormatException) {
            logger.error("Cannot parse $text into Long value", e)
            null
        }
    }
}
