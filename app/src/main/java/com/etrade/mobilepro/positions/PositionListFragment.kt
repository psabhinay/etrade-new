package com.etrade.mobilepro.positions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.alerts.presentation.AlertsSharedViewModel
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.common.observeWithTextView
import com.etrade.mobilepro.common.removeObservers
import com.etrade.mobilepro.databinding.FragmentPositionListBinding
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.dynamicui.DynamicUiLayoutRecyclerAdapter
import com.etrade.mobilepro.dynamicui.GenericRecyclerAdapter
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.toDynamicViewModels
import com.etrade.mobilepro.home.AccountsTabHostFragment
import com.etrade.mobilepro.home.OnAccountsTabSelectionListener
import com.etrade.mobilepro.navdestinations.openDisclosures
import com.etrade.mobilepro.orders.api.OrderAction
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.portfolio.PortfolioPositionsDisplayMode
import com.etrade.mobilepro.portfolio.PortfolioRecordsViewModel
import com.etrade.mobilepro.portfolio.model.ExtendedAccountSummaryViewModel
import com.etrade.mobilepro.portfolio.model.PositionTypeSelectorViewModel
import com.etrade.mobilepro.portfolio.tableview.PortfolioTableViewAdapter
import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.searchingapi.items.toSearchResultItem
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.tableviewutils.presentation.RestrictedTableView
import com.etrade.mobilepro.tableviewutils.presentation.ViewPagerRestrictedTableView
import com.etrade.mobilepro.tableviewutils.presentation.util.ColumnRowSizeCache
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionItem
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetClicksViewModel
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetDialogFragment
import com.etrade.mobilepro.util.InvocationCooldown
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.android.snackbar.showIfNotConsumed
import com.etrade.mobilepro.util.safeLet
import com.etrade.mobilepro.viewdelegate.AppMessageViewDelegate
import com.evrencoskun.tableview.SyncedTableOnChildAttachStateChangeListener
import com.evrencoskun.tableview.TableView
import kotlin.reflect.KClass

private const val KEY_LAST_POSITION = "lastPosition"
private const val KEY_VIEW_MODEL_POSITION = "viewModelPosition"
private const val ROLL_TWO_OPTIONS_TAG = "roll two options tag"
private const val SET_ALERT_ARGUMENT = "symbol"
private const val PORTFOLIO_VIEW_TYPE_TAG = "PORTFOLIO_VIEW_TYPE_TAG"

@Suppress("LargeClass", "TooManyFunctions")
abstract class PositionListFragment<T : PositionListViewModel>(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    private val mainNavigation: MainNavigation,
    private val marketTradeStatusDelegate: MarketTradeStatusDelegate,
    private val darkModeChecker: DarkModeChecker
) : Fragment(R.layout.fragment_position_list) {

    protected val binding by viewBinding(FragmentPositionListBinding::bind) { onDestroyBinding() }

    private fun onDestroyBinding() {
        removeTableviewChildAttachStateChangeListener()
        binding.contentView.adapter = null
    }

    protected abstract val viewModelClass: KClass<T>

    protected val positionListViewModel: T by lazy {
        ViewModelProvider(this, viewModelFactory).get(
            viewModelClass.java
        )
    }

    protected val sharedViewModel: AlertsSharedViewModel by activityViewModels { viewModelFactory }
    private val actionSheetClicksViewModel: ActionSheetClicksViewModel by viewModels { viewModelFactory }

    private val snackbarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    protected val viewAdapter: DynamicUiLayoutRecyclerAdapter
        get() = binding.contentView.adapter as DynamicUiLayoutRecyclerAdapter

    protected val portfolioRecordsViewModels: MutableList<PortfolioRecordsViewModel> =
        mutableListOf()
    protected val hasPositions: Boolean
        get() = portfolioRecordsViewModels.any { it.hasPositions }

    private val actionSheetCooldown = InvocationCooldown()

    private var extendedAccountSummaryViewModel: ExtendedAccountSummaryViewModel? = null
    private var positionTypeSelectorViewModel: PositionTypeSelectorViewModel? = null

    private var lastPosition: Int? = null
    private var viewModelPosition: Int? = null

    private lateinit var rollOptionsDropDown: DropDownMultiSelector<PortfolioRecord>

    private val staticHeaderView get() = getHeaderView(R.id.viewHeaderSelector)
    private val dynamicHeaderView get() = getHeaderView(R.id.selector_container)

    protected val View?.positionsView: RestrictedTableView?
        get() = this?.findViewById(R.id.positionsView)

    protected val View?.positionsHeaderView: RelativeLayout?
        get() = this?.findViewById(R.id.positionsHeaderView)

    private var previousHeaderHeight: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onCreate(positionListViewModel)

        if (this !is OnAccountsTabSelectionListener) {
            positionListViewModel.fetchData()
        }

        savedInstanceState?.apply {
            lastPosition = getSerializable(KEY_LAST_POSITION) as Int?
            viewModelPosition = getSerializable(KEY_VIEW_MODEL_POSITION) as Int?
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_position_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setAdapter()

        binding.swipeRefreshView.apply {
            applyColorScheme()
            setOnRefreshListener {
                positionListViewModel.refresh()
                onSwipeToRefresh()
            }
        }

        initViewModelObservers()
        initDropDowns()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.apply {
            putSerializable(KEY_LAST_POSITION, lastPosition)
            putSerializable(KEY_VIEW_MODEL_POSITION, viewModelPosition)
        }
    }

    override fun onResume() {
        super.onResume()
        portfolioRecordsViewModels.forEach {
            it.resumeStreaming()
        }

        // Type cast of marketHaltBanner as Text View is intentional, do not remove.
        marketTradeStatusDelegate.observeWithTextView(textView = binding.marketHaltBanner as TextView, lifecycleOwner = viewLifecycleOwner)
    }

    override fun onPause() {
        super.onPause()
        portfolioRecordsViewModels.forEach {
            it.pauseStreaming()
        }

        saveScrollState()

        marketTradeStatusDelegate.removeObservers(owner = viewLifecycleOwner)
    }

    protected abstract fun saveScrollState()

    protected open fun onCreate(viewModel: T) {
        // does nothing by default
    }

    protected open fun onSwipeToRefresh() {
        // does nothing by default
    }

    fun scrollToTop() {
        with(binding.contentView) {
            if (computeVerticalScrollOffset() > 0) {
                stopScroll()
                scrollBy(0, Int.MIN_VALUE)
                safeLet(staticHeaderView, dynamicHeaderView) { staticHeader, dynamicHeader ->
                    dynamicHeader.isVisible = true
                    staticHeader.isVisible = false
                }
            }
        }
    }

    private fun setHeaderVisibility() {
        staticHeaderView?.let { staticHeader ->
            val dynamicView = dynamicHeaderView
            if (dynamicView == null || binding.swipeRefreshView.isRefreshing) {
                return
            }
            if (dynamicView.y < 0F) {
                staticHeader.isVisible = true
                dynamicView.isVisible = false
                previousHeaderHeight = dynamicView.layoutParams.height.takeIf { it != 0 }
                dynamicView.layoutParams.height = 0
            } else {
                dynamicView.isVisible = true
                staticHeader.isVisible = false
                previousHeaderHeight?.let { dynamicView.layoutParams.height = it }
            }
        }
    }

    private fun setAdapter() {
        binding.contentView.apply {
            adapter = GenericRecyclerAdapter(
                this@PositionListFragment,
                emptyList()
            )
            setupRecyclerViewDivider(R.drawable.thick_divider, excludeIndexList = listOf(0, 1))
            setOnScrollChangeListener { _, _, _, _, _ ->
                setHeaderVisibility()
            }
            addTableviewChildAttachStateChangeListener()
            addOnChildAttachStateChangeListener(SyncedTableOnChildAttachStateChangeListener { takeIf { isPortfolioRecordsView }?.positionsView })
        }
    }

    protected fun removeTableviewChildAttachStateChangeListener() {
        binding.contentView.removeOnChildAttachStateChangeListener(onChildAttachStateChangeListener)
    }

    protected fun addTableviewChildAttachStateChangeListener() {
        binding.contentView.addOnChildAttachStateChangeListener(onChildAttachStateChangeListener)
    }

    private fun initViewModelObservers() {
        positionListViewModel.viewState.observe(
            viewLifecycleOwner,
            Observer { onViewStateChanged(it) }
        )
        sharedViewModel.alertWasSet.observe(
            viewLifecycleOwner,
            Observer { signal ->
                snackbarUtil.showIfNotConsumed(signal)
            }
        )
        observeActionSheetClicks()
    }

    private lateinit var tableTypeDropDownManager: BottomSheetSelector<String>

    private fun initTableTypeSelectionDropDown() {
        tableTypeDropDownManager = BottomSheetSelector(parentFragmentManager, resources)
        tableTypeDropDownManager.init(
            PORTFOLIO_VIEW_TYPE_TAG,
            getString(R.string.portfolio_view_title),
            resources.getStringArray(R.array.portfolio_table_view).toList(),
            positionListViewModel.tableViewIndex
        )

        tableTypeDropDownManager.setListener { position, _ ->
            positionTypeSelectorViewModel?.dropdownSelectedIndex = position
            portfolioRecordsViewModels.forEach { it.sortDelegate.tableViewIndex = position }
        }
    }

    private fun initDropDowns() {
        initTableTypeSelectionDropDown()

        rollOptionsDropDown = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = ROLL_TWO_OPTIONS_TAG,
                multiSelectEnabled = true,
                title = getString(R.string.portfolio_record_action_roll_two_options),
                itemsData = positionListViewModel.optionsToRoll,
                eventListener = { event, dropDownDialog ->
                    when (event) {
                        DropDownMultiSelector.Settings.Event.CLEAR_ALL -> positionListViewModel.clearSelectedOptionsToRoll()
                        DropDownMultiSelector.Settings.Event.SAVE -> {
                            dropDownDialog.get()?.dismiss()
                            positionListViewModel.getRollTwoOptionsDirections()?.let {
                                findNavController().navigate(it)
                            }
                        }
                    }
                },
                selectListener = { _, selected ->
                    positionListViewModel.toggleSelectionOptionToRoll(selected.value)
                },
                ghostText = getString(R.string.portfolio_roll_options_hint),
                saveButtonText = getString(R.string.done)
            )
        )
    }

    protected open fun onViewStateChanged(viewState: PositionListViewModel.ViewState) {
        clearPortfolioRecordsViewModels()
        extendedAccountSummaryViewModel = null

        when (viewState) {
            is PositionListViewModel.ViewState.Idle -> {
                if (darkModeChecker.uiMode != viewState.uiMode) {
                    viewAdapter.values = emptyList()
                    positionListViewModel.refresh()
                    return
                }
                hideLoading()
                viewAdapter.values = viewState.layouts.toDynamicViewModels(this) {
                    subscribeViewModel(it)
                    registerViewModel(it)
                }
                updateViewModelsWithPositions()
            }
            PositionListViewModel.ViewState.Loading -> {
                showLoading()
            }
            PositionListViewModel.ViewState.Error -> {
                hideLoading()
                snackbarUtil.errorSnackbar()
            }
        }
    }

    private fun updateViewModelsWithPositions() {
        portfolioRecordsViewModels
            .takeIf { it.size >= 1 }
            ?.lastOrNull()
            ?.forceFooter(true)
    }

    private fun clearPortfolioRecordsViewModels() {
        portfolioRecordsViewModels.apply {
            forEach { positionListViewModel.detach(it) }
            clear()
        }
    }

    private fun showLoading() = updateLoading(true)

    private fun hideLoading() = updateLoading(false)

    private fun updateLoading(loading: Boolean) {
        with(binding) {
            if (loading) {
                if (viewAdapter.values.isEmpty()) {
                    swipeRefreshView.isRefreshing = false
                    swipeRefreshView.isEnabled = false
                    loadingIndicator.show()
                } else {
                    swipeRefreshView.isRefreshing = true
                    loadingIndicator.hide()
                }
            } else {
                swipeRefreshView.isRefreshing = false
                swipeRefreshView.isEnabled = true
                loadingIndicator.hide()
            }
        }
    }

    @Suppress("ComplexMethod")
    private fun DynamicViewModel.handleCtaEvent(event: CtaEvent) {
        when (event) {
            is CtaEvent.UriEvent -> findNavController().navigate(event.uri)
            is CtaEvent.MessageEvent -> onMessageEvent(this, event)
            is CtaEvent.Disclosures -> startActivity(activity?.openDisclosures())
            is CtaEvent.LaunchPortfolioBottomSheet -> extendedAccountSummaryViewModel?.portfolioSummaryModel?.let { portfolioModel ->
                mainNavigation.launchExtendedPortfolioBottomSheet(activity, portfolioModel)
            }
            is CtaEvent.LaunchOrdersBottomSheet -> {
                mainNavigation.launchBottomSheetOrders(
                    activity,
                    event.accountId,
                    OrderStatus.valueOf(event.orderStatus)
                )
            }
            is CtaEvent.ShowDropdownSelector -> tableTypeDropDownManager.openDropDown()
            is CtaEvent.LaunchCustomizeColumns -> context?.let {
                positionListViewModel.onCustomizeViewClicked(it)
            }
            is CtaEvent.PositionModeTypeUpdated -> updatePositionModeType(event.index)
        }
    }

    private fun updatePositionModeType(index: Int) {
        val mode = PortfolioPositionsDisplayMode.from(index)
        portfolioRecordsViewModels.forEach {
            it.updatePositionsDisplayMode(mode)
            it.sortDelegate.positionsViewIndex = index
        }
    }

    private fun subscribeViewModel(viewModel: DynamicViewModel) {
        viewModel.clickEvents.observe(
            viewLifecycleOwner,
            Observer { event -> viewModel.handleCtaEvent(event) }
        )

        AppMessageViewDelegate(viewModel, snackbarUtil).observe(viewLifecycleOwner)
    }

    @Suppress("LongMethod")
    private fun registerViewModel(viewModel: DynamicViewModel) {
        when (viewModel) {
            is ExtendedAccountSummaryViewModel -> extendedAccountSummaryViewModel = viewModel
            is PositionTypeSelectorViewModel -> {
                binding.viewHeaderSelector.viewModel = viewModel
                positionTypeSelectorViewModel = viewModel
                binding.viewHeaderSelector.lifecycleOwner = viewLifecycleOwner
            }
            is PortfolioRecordsViewModel -> addPortfolioRecordsViewModel(
                viewModel.apply {
                    forceFooter(false) // reset footer
                    sortDelegate.sortInfo.observe(
                        viewLifecycleOwner,
                        Observer {
                            positionListViewModel.refresh(it)
                        }
                    )
                    sortDelegate.localSortInfo.observe(
                        viewLifecycleOwner,
                        Observer { sortInfo ->
                            portfolioRecordsViewModels.forEach { vm ->
                                vm.sortDelegate.conditionalColumnSort(sortInfo)
                            }
                        }
                    )
                }
            )
        }
    }

    private fun addPortfolioRecordsViewModel(viewModel: PortfolioRecordsViewModel) {
        positionListViewModel.attach(viewModel)
        portfolioRecordsViewModels.add(viewModel)
    }

    private fun onMessageEvent(viewModel: DynamicViewModel, event: CtaEvent.MessageEvent) {
        val index = portfolioRecordsViewModels.indexOf(viewModel)
        event.message.apply {
            (obj as List<*>)
                .filterIsInstance<ActionItem>()
                .also { showActionSheet(it, index, arg1) }
        }
    }

    private fun showActionSheet(items: List<ActionItem>, viewModelIndex: Int, position: Int) {
        actionSheetCooldown.invokeIfReady {
            viewModelPosition = viewModelIndex
            lastPosition = position

            ActionSheetDialogFragment.show(childFragmentManager, items)
        }
    }

    private fun observeActionSheetClicks() {
        actionSheetClicksViewModel.actionSheetClicks.observe(
            viewLifecycleOwner,
            Observer { event ->
                event.consume(this::handleActionSheetClick)
            }
        )
    }

    @Suppress("ComplexMethod")
    private fun handleActionSheetClick(item: ActionItem) {
        val viewModelIndex = viewModelPosition ?: return
        val position = lastPosition ?: return
        val viewModel = portfolioRecordsViewModels.getOrNull(viewModelIndex) ?: return
        actionSheetCooldown.startCooldown()
        when (item.id) {
            R.id.action_buy -> openTradePage(viewModel, position, orderAction = OrderAction.BUY_ACTION)
            R.id.action_exchange -> openTradePage(viewModel, position, orderAction = OrderAction.EXCHANGE_ACTION)
            R.id.action_see_details -> openQuoteDetails(viewModel, position)
            R.id.action_roll_option -> rollOneOption(viewModel, position)
            R.id.action_roll_two_options -> rollTwoOptions(viewModel, position)
            R.id.action_sell -> openTradePage(viewModel, position, orderAction = OrderAction.SELL_ACTION)
            R.id.action_set_alert -> openSetAlertPage(viewModel, position)
            R.id.action_tax_lot_show -> openTaxLots(viewModel, position)
        }
    }

    private fun openSetAlertPage(portfolioRecordsViewModel: PortfolioRecordsViewModel, position: Int) {
        val instrument = portfolioRecordsViewModel.peekRecord(position)?.instrument ?: return
        val symbol = SearchResultItem.Symbol(instrument.symbol, instrument.instrumentType)
        findNavController().navigate(R.id.setAlertFragment, bundleOf(SET_ALERT_ARGUMENT to symbol))
    }

    private fun openQuoteDetails(portfolioRecordsViewModel: PortfolioRecordsViewModel, position: Int) {
        val args = portfolioRecordsViewModel.peekRecord(position)?.instrument?.toSearchResultItem()?.toBundle() ?: return
        mainNavigation.navigateToQuoteDetails(activity, args)
    }

    private fun getHeaderView(layoutResId: Int) =
        view?.findViewById<RelativeLayout>(layoutResId)

    private fun openTaxLots(portfolioRecordsViewModel: PortfolioRecordsViewModel, position: Int) {
        val (positionId, symbol) = portfolioRecordsViewModel.peekRecord(position)?.position?.let {
            Pair(it.positionId, it.symbol)
        } ?: return
        mainNavigation.launchBottomSheetPortfolioTaxLot(
            activity,
            positionId,
            portfolioRecordsViewModel.accountUuid,
            symbol,
            portfolioRecordsViewModel.sortDelegate.tableViewType,
            portfolioRecordsViewModel.sortDelegate.positionTableMode
        )
    }

    private fun openTradePage(portfolioRecordsViewModel: PortfolioRecordsViewModel, position: Int, orderAction: OrderAction) {
        val record = portfolioRecordsViewModel.peekRecord(position) ?: return

        portfolioRecordsViewModel.persistAccountId()

        findNavController().navigate(positionListViewModel.getTradeDirections(record, portfolioRecordsViewModel.accountId, orderAction))
    }

    private fun rollOneOption(portfolioRecordsViewModel: PortfolioRecordsViewModel, position: Int) {
        val record = portfolioRecordsViewModel.peekRecord(position) ?: return
        positionListViewModel.getRollOptionsDirections(portfolioRecordsViewModel.accountId, listOf(record))?.let {
            findNavController().navigate(it)
        }
    }

    private fun rollTwoOptions(portfolioRecordsViewModel: PortfolioRecordsViewModel, position: Int) {
        if (positionListViewModel.rollTwoOptions(portfolioRecordsViewModel, position)) {
            rollOptionsDropDown.open()
        }
    }

    private val View.isPortfolioRecordsView: Boolean
        get() {
            val position = getPosition() ?: return false
            return viewAdapter.values.getOrNull(position) is PortfolioRecordsViewModel
        }

    private fun View.getPosition(): Int? = binding.contentView.layoutManager?.getPosition(this)?.takeIf { it >= 0 }

    protected open fun TableView.onDynamicViewAttached() { }

    private val onChildAttachStateChangeListener = OnChildAttachStateChangeListener()

    private inner class OnChildAttachStateChangeListener : RecyclerView.OnChildAttachStateChangeListener {

        private var rowSizeCache: ColumnRowSizeCache? = null

        override fun onChildViewAttachedToWindow(view: View) {
            applyForTableView(view) {
                // Note: this CANNOT be in post block, otherwise rows won't sync
                onDynamicViewAttached()
                if (parentFragment is AccountsTabHostFragment) {
                    (positionsView as? ViewPagerRestrictedTableView)?.position =
                        ViewPagerRestrictedTableView.PositionInViewPager.FIRST
                }

                positionListViewModel.scrollState.value?.let { consumable ->
                    consumable.consume { scrollState ->
                        positionsView?.restoreScrollState(scrollState)
                    }
                }
                topHeaderGroup += positionsHeaderView

                (adapter as? PortfolioTableViewAdapter)?.apply {
                    val cache = rowSizeCache
                    if (cache == null) {
                        if (cellRecyclerViewAdapter?.itemCount ?: 0 > 0) {
                            rowSizeCache = columnRowSizeCache
                        }
                    } else {
                        updateColumnWidthCache(cache)
                    }
                }
                applyPostConfiguration(view)
            }
        }

        @Suppress("ClickableViewAccessibility")
        private fun TableView.applyPostConfiguration(view: View) {
            post {
                if (this@PositionListFragment.view != null) {
                    val headerViewHeight = positionsHeaderView?.measuredHeight ?: 0
                    cornerContainerView.applyTopMargin(headerViewHeight)
                    columnHeaderRecyclerView.applyTopMargin(headerViewHeight)
                    rowHeaderRecyclerView.applyTopMargin(columnHeaderHeight + headerViewHeight)
                    cellRecyclerView.applyTopMargin(columnHeaderHeight + headerViewHeight)

                    view.findViewById<TextView>(R.id.noPositionsText).apply {
                        applyTopMargin(headerViewHeight)
                        setOnTouchListener { _, _ ->
                            // block all touch events
                            true
                        }
                    }
                }
                // seems redundant, but needed to correct horizontal scroll position
                scrollHandler.scrollToColumnPosition(scrollHandler.columnPosition, scrollHandler.columnPositionOffset)
            }
        }

        override fun onChildViewDetachedFromWindow(view: View) {
            applyForTableView(view) {
                adapter = null
            }
        }

        private fun applyForTableView(view: View, block: TableView.() -> Unit) {
            if (view.isPortfolioRecordsView) {
                view.positionsView?.apply(block)
            }
        }

        private fun View.applyTopMargin(margin: Int) {
            layoutParams = (layoutParams as FrameLayout.LayoutParams).apply {
                topMargin = margin
            }
        }
    }
}
