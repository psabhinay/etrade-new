package com.etrade.mobilepro.barcode.viewmodel

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.barcode.BarcodeReaderHandler
import com.etrade.mobilepro.barcode.R
import com.etrade.mobilepro.barcode.api.BarcodeHistoryRepo
import com.etrade.mobilepro.barcode.api.BarcodeItem
import com.etrade.mobilepro.barcode.data.BarcodeApiService
import com.etrade.mobilepro.barcode.data.BarcodeLookupResponse
import com.etrade.mobilepro.util.Resource
import com.google.mlkit.vision.barcode.Barcode
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import javax.inject.Inject

class BarcodeReaderViewModel @Inject constructor(
    private val resources: Resources,
    private val barcodeApiService: BarcodeApiService,
    private val barcodeHistoryRepo: BarcodeHistoryRepo
) : ViewModel() {

    private val _result = MutableLiveData<Resource<String>>()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    lateinit var barcodeReaderHandler: BarcodeReaderHandler
    private var shouldLookup = true
    private val cameraExecutor: ExecutorService = Executors.newSingleThreadExecutor()

    val result: LiveData<Resource<String>> = _result

    fun startCamera() {
        shouldLookup = true
        barcodeReaderHandler.start(cameraExecutor)
    }

    private fun handleSymbol(symbol: String?) {
        symbol
            ?.also { _result.postValue(Resource.Success(it)) }
            ?: _result.postValue(Resource.Success())
    }

    private fun handleProduct(barcodeItem: BarcodeItem?) {
        if (barcodeItem != null) {
            barcodeHistoryRepo.saveBarcodeToHistory(barcodeItem)
        }
    }

    private fun getBarcodeFormat(barcodeFormat: Int): String? {
        return when (barcodeFormat) {
            Barcode.FORMAT_UPC_A -> "UPCA"
            Barcode.FORMAT_UPC_E -> "UPCE"

            else -> null
        }
    }

    private data class BarcodeLookupHolder(
        val symbol: String?,
        val barcodeItem: BarcodeItem?
    )

    private fun BarcodeLookupResponse.convert(upc: String): BarcodeLookupHolder? {
        val response = data?.response ?: return null
        val security = response.securities?.match?.firstOrNull()
        val symbol = security?.symbol
        val companyName = response.product?.companyName ?: security?.description
        val productName = response.product?.name ?: resources.getString(R.string.barcode_no_product)

        val barcodeItem = if (symbol != null && companyName != null) {
            BarcodeItem(
                company = companyName,
                product = productName,
                upc = upc,
                symbol = symbol
            )
        } else {
            null
        }

        return BarcodeLookupHolder(symbol, barcodeItem)
    }

    private fun handleSuccess(barcodeLookupResponse: BarcodeLookupResponse, upc: String) {
        barcodeLookupResponse.convert(upc)
            ?.also { (symbol, barcodeItem) ->
                handleProduct(barcodeItem)
                handleSymbol(symbol)
            }
            ?: _result.postValue(Resource.Success())
    }

    fun lookupBarcode(upc: String?, barcodeFormat: Int) {
        if (shouldLookup) {
            shouldLookup = false
            _result.value = Resource.Loading()

            val format: String? = getBarcodeFormat(barcodeFormat)

            if (upc != null && format != null) {
                compositeDisposable.add(
                    barcodeApiService.barcodeLookup(upc, format)
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                            onSuccess = { handleSuccess(it, upc) },
                            onError = { _result.postValue(Resource.Failed()) }
                        )
                )
            } else {
                _result.value = Resource.Failed()
            }
        }
    }

    override fun onCleared() {
        compositeDisposable.clear()
        cameraExecutor.shutdown()
        barcodeReaderHandler.stop()
        super.onCleared()
    }
}
