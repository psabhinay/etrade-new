package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.holdings.dto.HoldingsResponseDto
import com.etrade.mobilepro.holdings.mapper.PositionHoldingJsonMapper
import com.etrade.mobilepro.holdings.repo.DefaultPositionsHoldingsRepo
import com.etrade.mobilepro.holdingsapi.PositionHoldingsItem
import com.etrade.mobilepro.holdingsapi.PositionsHoldingsRepo
import com.etrade.mobilepro.positionsholdingsapi.restapi.HoldingsApiClient
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class PositionHoldingsModule {
    @Provides
    @Singleton
    internal fun providePositionHoldingsApiClient(@MobileTrade retrofit: Retrofit): HoldingsApiClient = retrofit.create(HoldingsApiClient::class.java)

    @Provides
    internal fun provideHoldingsMapper(): (HoldingsResponseDto) -> PositionHoldingsItem = PositionHoldingJsonMapper()::map

    @Provides
    @Singleton
    internal fun provideQuotesRepo(
        apiClient: HoldingsApiClient,
        mapper: (@JvmSuppressWildcards HoldingsResponseDto) -> PositionHoldingsItem
    ): PositionsHoldingsRepo = DefaultPositionsHoldingsRepo(apiClient, mapper)
}
