package com.etrade.mobilepro.order.details.presentation.helper

import com.etrade.mobilepro.order.details.presentation.R

fun conditionalOtoSectionHeaderResource(index: Int): Int? {
    return when (index) {
        0 -> null
        1 -> R.string.orders_divider_conditional_oto_1
        2 -> R.string.orders_divider_conditional_oto_2
        else -> R.string.orders_divider_conditional_oto_n
    }
}
