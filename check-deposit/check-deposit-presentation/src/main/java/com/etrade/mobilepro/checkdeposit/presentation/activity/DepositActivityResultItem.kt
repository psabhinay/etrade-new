package com.etrade.mobilepro.checkdeposit.presentation.activity

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.checkdeposit.activity.service.DepositActivityItemDto
import com.etrade.mobilepro.fundsflow.api.FundsFlowAccount
import com.etrade.mobilepro.util.safeParseBigDecimal

private const val ACCOUNT_PREFIX_LENGTH = 4
private const val DEPOSIT_DATE_MULTIPLIER = 1000

data class DepositActivityResultItem(
    val depositActivityItem: DepositActivityItemDto,
    val associatedAccount: FundsFlowAccount?
) {
    val uiDepositValue: String by lazy {
        depositActivityItem.amount?.safeParseBigDecimal()?.let {
            MarketDataFormatter.formatCurrencyValue(it)
        } ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE
    }

    val uiDepositAccount: String by lazy {
        associatedAccount?.let { account ->
            account.nickname ?: getFallbackAccountNickname(account)
        } ?: depositActivityItem.accountId ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE
    }

    val uiDepositDate: String by lazy {
        depositActivityItem.depositDate?.toLongOrNull()?.let {
            DateFormattingUtils.formatToShortDate(it * DEPOSIT_DATE_MULTIPLIER)
        } ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE
    }

    private fun getFallbackAccountNickname(account: FundsFlowAccount): String {
        return "${account.accountId.takeLast(ACCOUNT_PREFIX_LENGTH)} ${account.accountType}"
    }
}

fun List<DepositActivityItemDto>.addAccounts(accounts: List<FundsFlowAccount>): List<DepositActivityResultItem> {
    val accountsMap = accounts.associateBy { it.accountId }
    return this.map { depositActivityItemDto ->
        DepositActivityResultItem(
            depositActivityItemDto,
            accountsMap[depositActivityItemDto.accountId]
        )
    }
}
