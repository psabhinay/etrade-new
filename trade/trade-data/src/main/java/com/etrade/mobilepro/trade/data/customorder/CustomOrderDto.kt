package com.etrade.mobilepro.trade.data.customorder

import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.api.OrderLeg
import com.etrade.mobilepro.trade.api.option.OptionOrder
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.data.form.value.CommonPriceTypeValueId
import com.etrade.mobilepro.trade.data.util.maskAccountId
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
internal class CustomOrderDto(
    @Json(name = "orderId")
    val orderId: String?,
    @Json(name = "accountId")
    val accountId: String,
    @Json(name = "allOrNone")
    val allOrNoneFromDto: String,
    @Json(name = "estimatedCommission")
    val estimatedCommission: String,
    @Json(name = "estimatedTotalCost")
    val estimatedTotalCost: String,
    @Json(name = "legList")
    val legDetails: List<LegDetailsResponseDto> = emptyList(),
    @Json(name = "limitPrice")
    val limitPriceString: String,
    @Json(name = "orderTerm")
    val orderTermTypeCode: String,
    @Json(name = "orderType")
    val orderType: String,
    @Json(name = "placedMs")
    val placedMs: Long?,
    @Json(name = "previewId")
    val previewId: String,
    @Json(name = "priceType")
    val priceTypeCode: String,
    @Json(name = "shortDescription")
    val shortDescription: String,
    @Json(name = "strategyType")
    val strategyType: String,
    @Json(name = "stopPrice")
    val stopPriceString: String
) : OptionOrder {

    override val account: String
        get() = shortDescription.takeIf { it.isNotBlank() } ?: accountId.maskAccountId()
    override val legs: List<OrderLeg>
        get() = legDetails.map { it.toOrderLeg() }
    override val priceType: PriceType
        get() = CommonPriceTypeValueId.converter.toValue(priceTypeCode) ?: PriceType.UNKNOWN
    override val term: OrderTerm
        get() = OrderTerm.mapOrderTerm(orderTermTypeCode)
    override val allOrNone: Boolean
        get() = allOrNoneFromDto != "0"
    override val limitPrice: BigDecimal?
        get() = limitPriceString.safeParseBigDecimal()
    override val stopPrice: BigDecimal?
        get() = stopPriceString.safeParseBigDecimal()
    override val fee: BigDecimal?
        get() = estimatedCommission.safeParseBigDecimal()
    override val cost: BigDecimal?
        get() = estimatedTotalCost.safeParseBigDecimal()
    override val strategy: StrategyType
        get() = StrategyType.fromTypeCode(strategyType)

    /**
     * your price - Calculated price for Trailing stop orders,
     * but from server response stop price is same as initial stop price.
     */
    override val yourPrice: String
        get() = stopPriceString

    private fun TradeLeg.toOrderLeg(): OrderLeg {
        return object : OrderLeg {
            override val symbol: String = this@toOrderLeg.symbol
            override val action: TransactionType = this@toOrderLeg.transactionType
            override val quantity: BigDecimal? = this@toOrderLeg.quantity.toBigDecimal()
        }
    }
}
