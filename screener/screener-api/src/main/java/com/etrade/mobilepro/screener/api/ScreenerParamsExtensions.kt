package com.etrade.mobilepro.screener.api

/**
 * Function mergeFollowingClauses should be used when we have collection of clauses.
 * This clauses will contain group of clauses with following intervals.
 * Like 0-25, 25-50. They are the same like one interval 0-50.
 * Merging following clauses reduce size and simplify final model.
 */

fun MutableList<Clause>.mergeFollowingClauses(): MutableList<Clause> {
    val valuesMarkedForMerge = mutableListOf<Clause>()

    // start with 1 because first element can't be merged, this element have no following second element before it
    for (i in 1 until this.size - 1 step 2) {
        if (this[i].values.first() == this[i + 1].values.first()) {
            valuesMarkedForMerge.add(this[i])
            valuesMarkedForMerge.add(this[i + 1])
        }
    }

    removeAll(valuesMarkedForMerge)

    return this
}
