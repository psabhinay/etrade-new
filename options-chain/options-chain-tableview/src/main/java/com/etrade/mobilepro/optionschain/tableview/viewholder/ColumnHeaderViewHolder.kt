package com.etrade.mobilepro.optionschain.tableview.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.optionschain.tableview.OptionsChainColumn
import com.etrade.mobilepro.optionschain.tableview.databinding.OptionsChainColumnHeaderBinding

class ColumnHeaderViewHolder(
    private val binding: OptionsChainColumnHeaderBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bindTo(column: OptionsChainColumn) {
        binding.columnHeaderTv.setText(column.textRes)
    }
}
