package com.etrade.mobilepro.util.updater

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.BuildConfig
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.preferences.KEY_DISPLAY_EULA
import com.etrade.mobilepro.tradingdefaults.preferences.DEFAULT_OPTIONS_CONTRACTS
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.util.DatabaseLocator
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import javax.inject.Inject

/**
 * Default implementation of [AppUpdater].
 *
 * @param applicationPreferences current application preferences
 * @param databaseLocator locates database file
 * @param keyValueStorage storage of raw values
 * @param migrations database migration procedures to run
 * @param tradingDefaultsPreferences trading settings defaults preferences
 */
class DefaultAppUpdater @Inject constructor(
    private val applicationPreferences: ApplicationPreferences,
    private val databaseLocator: DatabaseLocator,
    private val keyValueStorage: KeyValueStorage,
    private val migrations: Set<@JvmSuppressWildcards Migration>,
    private val tradingDefaultsPreferences: TradingDefaultsPreferences
) : AppUpdater {

    private val logger by lazy { LoggerFactory.getLogger(DefaultAppUpdater::class.java) }

    override fun update() {
        updateDatabase()
        updatePreferences()
    }

    @Suppress("TooGenericExceptionCaught")
    private fun updateDatabase() {
        val databaseFile = databaseLocator.getDatabasePath("EtmpDatabase.db")

        // If database does not exist we want to return from this method as soon as possible.
        try {
            if (!databaseFile.exists()) return
        } catch (e: Throwable) {
            logger.error("Unable to check if database exists", e)
            return
        }

        // We make only one attempt to run each migration procedure. If it fails - it fails. We don't want to have inconsistency between two launches of the app
        // as a result of two migration attempts. Because between two launches a user may create new data. There is inconsistency if the first migration attempt
        // fails and the second one succeeds.
        migrations.forEach {
            try {
                runBlocking { it.migrate(databaseFile) }
            } catch (e: Throwable) {
                logger.error("Watchlist migration have failed.", e)
            }
        }

        // Clean up for good.
        try {
            databaseFile.delete()
        } catch (e: Throwable) {
            logger.error("Database cannot be removed", e)
        }
    }

    @SuppressWarnings("MagicNumber")
    private fun updatePreferences() {
        if (applicationPreferences.appVersion == BuildConfig.VERSION_CODE) {
            return
        }

        if (applicationPreferences.appVersion < 90000) {
            keyValueStorage.run {
                runCatching { getStringValue(KEY_DISPLAY_EULA, null) }.getOrNull()?.let { value ->
                    applicationPreferences.shouldDisplayEULA = value.toBoolean()
                }

                runCatching { getStringValue("optionContractsKey", null) }.getOrNull()?.let {
                    tradingDefaultsPreferences.optionsContracts = it.toIntOrNull() ?: DEFAULT_OPTIONS_CONTRACTS
                }
            }
        }

        applicationPreferences.appVersion = BuildConfig.VERSION_CODE
    }
}
