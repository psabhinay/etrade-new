package com.etrade.mobilepro.portfolio

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.preferences.getEnumValue
import com.etrade.mobilepro.preferences.preference
import com.etrade.mobilepro.preferences.putEnumValue
import com.etrade.mobilepro.util.SortOrder
import javax.inject.Inject

private const val KEY_PORTFOLIO_TABLE_VIEW_INDEX = "portfolioTableViewIndex"
private const val KEY_PORTFOLIO_POSITIONS_DISPLAY_MODE_INDEX = "portfolioPositionsDisplayModeIndex"
private const val KEY_PORTFOLIO_SORT_COLUMN = "portfolioSortColumn"
private const val KEY_PORTFOLIO_SORT_DIRECTION = "portfolioSortDirection"
private const val KEY_PORTFOLIO_EXPAND_STATUS = "portfolioExpandStatus"

class PortfolioSettingsRepositoryImpl @Inject constructor(
    private val keyValueStorage: KeyValueStorage
) : PortfolioSettingsRepository {

    override var tableMode: PortfolioTableMode
        get() = PortfolioTableMode.from(
            keyValueStorage.getIntValue(
                KEY_PORTFOLIO_TABLE_VIEW_INDEX,
                0
            )
        )
        set(value) {
            keyValueStorage.putIntValue(KEY_PORTFOLIO_TABLE_VIEW_INDEX, value.index)
        }

    override var positionsDisplayMode: PortfolioPositionsDisplayMode
        get() = PortfolioPositionsDisplayMode.from(
            keyValueStorage.getIntValue(
                KEY_PORTFOLIO_POSITIONS_DISPLAY_MODE_INDEX,
                0
            )
        )
        set(value) {
            keyValueStorage.putIntValue(KEY_PORTFOLIO_POSITIONS_DISPLAY_MODE_INDEX, value.index)
        }

    private var portfolioSortColumn: String by keyValueStorage.preference(
        key = KEY_PORTFOLIO_SORT_COLUMN,
        default = PortfolioRecordColumn.SYMBOL.toString()
    )

    private var lastSelectedSortColumn: PortfolioRecordColumn
        set(value) {
            portfolioSortColumn = value.toString()
        }
        get() = PortfolioRecordColumn.valueOf(portfolioSortColumn)

    private var portfolioSortDirection: String by keyValueStorage.preference(
        key = KEY_PORTFOLIO_SORT_DIRECTION,
        default = SortOrder.UNDEFINED.toString()
    )

    private var lastSelectedSortDirection: SortOrder
        set(value) {
            portfolioSortDirection = value.toString()
        }
        get() = SortOrder.valueOf(portfolioSortDirection)

    override var sortInfo: SortInfo
        get() = SortInfo(lastSelectedSortColumn, lastSelectedSortDirection)

        set(value) {
            lastSelectedSortColumn = value.column
            lastSelectedSortDirection = value.order
        }

    override fun setExpandStatus(accountId: String, value: PositionsState) =
        keyValueStorage.putEnumValue(
            "$KEY_PORTFOLIO_EXPAND_STATUS-$accountId",
            value
        )

    override fun getExpandStatus(accountId: String): PositionsState =
        keyValueStorage.getEnumValue(
            key = "$KEY_PORTFOLIO_EXPAND_STATUS-$accountId",
            default = PositionsState.EXPANDED
        )
}
