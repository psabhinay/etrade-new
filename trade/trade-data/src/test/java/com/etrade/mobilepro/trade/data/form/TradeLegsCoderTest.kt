package com.etrade.mobilepro.trade.data.form

import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto
import com.etrade.mobilepro.util.ValueCoder
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TradeLegsCoderTest {

    private val coder: ValueCoder<List<TradeLeg>> = TradeLegsCoder(Moshi.Builder().build())

    @Test
    fun checkCoder() {
        val expected = TradeLegsCoder::class.getObjectFromJson<List<TradeLeg>>(
            resourcePath = "trade-legs.json",
            type = Types.newParameterizedType(List::class.java, LegDetailsCommonDto::class.java)
        ).toComparable()

        assertEquals(2, expected.size)
        assertEquals(expected, coder.decode(coder.encode(expected))?.toComparable())
    }

    private fun Iterable<TradeLeg>.toComparable(): List<TradeLeg> {
        return map { ComparableTradeLeg(it.symbol, it.transactionType, it.quantity, false) }
    }

    private data class ComparableTradeLeg(
        override val symbol: String,
        override val transactionType: TransactionType,
        override val quantity: Int,
        override val isAMOption: Boolean
    ) : TradeLeg {
        override val displaySymbol: String = symbol
    }
}
