package com.etrade.mobilepro.optionschain.presentation

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.compose.runtime.Composable
import androidx.compose.ui.viewinterop.AndroidViewBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.horizontallistpicker.HorizontalListPicker
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.optionschain.presentation.databinding.OptionsChainFragmentBinding
import com.etrade.mobilepro.optionschain.tableview.OptionChainTableViewModel
import com.etrade.mobilepro.optionschain.tableview.OptionItemClickListener
import com.etrade.mobilepro.optionschain.tableview.SelectedOptionsPair
import com.etrade.mobilepro.optionschain.tableview.adapter.OptionsListAdapter
import com.etrade.mobilepro.optionschain.tableview.view.OptionsChainTableView
import com.etrade.mobilepro.optionschains.api.Option
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quote.screen.api.TabVisibilityListener
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.trade.conditionalOrderDisclosure.presentation.ConditionalOrderDisclosureViewDelegate
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetClicksViewModel
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetViewDelegate
import com.etrade.mobilepro.uiwidgets.groupscrollrecyclerview.VerticalGroupScrollRecyclerView
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.clicklisteners.OnViewDebounceClickListener
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.viewdelegate.BlockingLoadingIndicatorViewDelegate
import com.etrade.mobilepro.viewdelegate.ViewDelegate
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

class OptionsChainViewDelegate @AssistedInject constructor(
    @Assisted private val fragment: Fragment,
    @Assisted private val symbol: SearchResultItem.Symbol,
    private val optionsChainRouter: OptionsChainRouter,
    private val isAuthenticated: UserAuthenticationState,
    snackbarUtilFactory: SnackbarUtilFactory,
    viewModelFactory: ViewModelProvider.Factory
) : TabVisibilityListener {

    private val viewModel: OptionsChainViewModel by fragment.viewModels { viewModelFactory }
    private val optionChainTableViewModel: OptionChainTableViewModel by fragment.viewModels { viewModelFactory }
    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ fragment.viewLifecycleOwner }) { fragment.view } }
    private val quoteWidgetViewModel: QuotesWidgetViewModel by fragment.viewModels { viewModelFactory }
    private val disclosureViewModel: OptionsChainConditionalOrderDisclosureViewModel by fragment.viewModels { viewModelFactory }
    private val actionSheetClicksViewModel: ActionSheetClicksViewModel by fragment.viewModels { viewModelFactory }

    private val symbolTicker: String get() = symbol.underlyerSymbol
    private val type: InstrumentType get() = symbol.type

    private val errorObserver: Observer<ConsumableLiveEvent<String>> = Observer {
        it.getNonConsumedContent()?.let { message -> snackBarUtil.errorSnackbar(message) }
    }

    private val handler = Handler(Looper.getMainLooper())

    private val actionSheetViewDelegate: ActionSheetViewDelegate by lazy {
        ActionSheetViewDelegate(
            viewModel,
            actionSheetClicksViewModel,
            fragment.childFragmentManager
        )
    }

    private val conditionalOrderDisclosureViewDelegate by lazy {
        ConditionalOrderDisclosureViewDelegate(
            fragment.requireContext(),
            snackBarUtil,
            disclosureViewModel
        )
    }

    private val viewDelegates: Iterable<ViewDelegate> by lazy {
        listOf(
            actionSheetViewDelegate,
            BlockingLoadingIndicatorViewDelegate(disclosureViewModel, fragment.childFragmentManager),
            conditionalOrderDisclosureViewDelegate
        )
    }

    private val List<SelectedOptionsPair>.totalInTheMoneyCount
        get() = count { pair -> pair.call.option.isInTheMoney }

    private val optionClickListener = object : OptionItemClickListener {
        override fun onOptionClicked(option: Option) {
            viewModel.selectOption(option, type)
        }
    }

    private var optionTableView: OptionsChainTableView? = null
    private var datePicker: HorizontalListPicker? = null
    private var optionsChainSwipeRefreshView: SwipeRefreshLayout? = null

    private var binding: OptionsChainFragmentBinding? = null

    private var doAnimate = false

    private var swipeRefreshListener: (() -> Unit)? = null

    @AssistedFactory
    interface Factory {
        fun create(
            fragment: Fragment,
            symbol: SearchResultItem.Symbol
        ): OptionsChainViewDelegate
    }

    private fun setUpViewModels() {
        viewDelegates.forEach {
            it.observe(fragment.viewLifecycleOwner)
        }
        viewModel.run {
            navEvent.observe(fragment.viewLifecycleOwner, { it.consume(::navigateToLegSelectOption) })
            navToQuoteDetailsSignal.observe(fragment.viewLifecycleOwner, { it.consume(::navigateToQuote) })
        }
        viewModel.disclosureViewModel = disclosureViewModel
        quoteWidgetViewModel.errorMessage.observe(fragment.viewLifecycleOwner, errorObserver)

        optionChainTableViewModel.apply {
            expirationDates.observe(
                fragment.viewLifecycleOwner,
                {
                    datePicker?.items = it
                }
            )
            selectedExpirationDatePosition.observe(
                fragment.viewLifecycleOwner,
                {
                    datePicker?.selectedItemPosition = it
                    val expirationDateContentDescription = fragment.getString(
                        R.string.label_dropdown_description,
                        fragment.getString(R.string.options_chain_expiration_date)
                    )
                    datePicker?.setSelectorContentDescription(
                        "$expirationDateContentDescription, ${datePicker?.items?.get(it)}"
                    )
                }
            )
            loadingIndicator.observe(fragment.viewLifecycleOwner, { optionsChainSwipeRefreshView?.isRefreshing = it })
            errorMessage.observe(fragment.viewLifecycleOwner, errorObserver)

            optionPairs.observe(fragment.viewLifecycleOwner, { handleOptionPairs(it) })
        }
    }

    private fun loadPage() {
        optionChainTableViewModel.loadOptionsChain(symbolTicker)
    }

    private fun refresh() {
        optionChainTableViewModel.refresh()
    }

    private fun handleOptionPairs(list: List<SelectedOptionsPair>) {
        optionTableView?.apply {
            setOptions(list)
            shiftTableForInTheMoneyVisibility(list.totalInTheMoneyCount)
            animateAndUpdateVisiblePositions()
            if (!this.isAttachedToWindow) {
                doAnimate = true
            }
        }
    }

    fun animateAndUpdateVisiblePositions() {
        optionTableView?.apply {
            this@OptionsChainViewDelegate.handler.post {
                animateFanIn()
                optionChainTableViewModel.updateVisiblePositions(getCurrentVisibleRange(), true)
                setOnScrollStoppedListener(object : VerticalGroupScrollRecyclerView.OnScrollStoppedListener {
                    override fun onScrollStopped(recyclerView: RecyclerView) {
                        optionChainTableViewModel.updateVisiblePositions(getCurrentVisibleRange())
                    }
                })
            }
        }
    }

    private fun navigateToLegSelectOption(navInfo: OptionsChainViewModel.NavInfo) {
        if (isAuthenticated()) {
            navigateToOptionLegSelect(navInfo)
        } else {
            navigateToLogin(navInfo)
        }
    }

    private fun navigateToOptionLegSelect(navInfo: OptionsChainViewModel.NavInfo) {
        if (navInfo.transactionType == null) {
            snackBarUtil.errorSnackbar(fragment.getString(R.string.error_message_general))
            return
        }

        navInfo.optionChainType?.let {
            optionsChainRouter.navigateToOptionLegSelect(
                navArgs = TradeNavArgs(
                    navController = fragment.findNavController(),
                    symbol = symbol,
                    transactionType = navInfo.transactionType,
                    securityType = it.toSecurityType(),
                    option = navInfo.option
                )
            )
        }
    }

    private fun navigateToLogin(navInfo: OptionsChainViewModel.NavInfo) {
        viewModel.savePendingNavInfo(navInfo)
        optionsChainRouter.navigateToLogin(fragment.findNavController())
    }

    private fun navigateToQuote(selectedSymbol: SearchResultItem.Symbol) {
        optionsChainRouter.navigateToQuoteDetails(
            fragment.findNavController(),
            selectedSymbol.copy(
                underlyerSymbol = symbolTicker,
                underlyingTypeCode = type
            )
        )
    }

    fun onDestroyView() {
        handler.removeCallbacksAndMessages(null)
    }

    fun stopStreaming() {
        optionChainTableViewModel.stopStreaming()
    }

    fun resumeStreaming() {
        optionChainTableViewModel.resumeStreaming()
    }

    private fun setUpViewAdapters() {
        val callAdapter = OptionsListAdapter(fragment.viewLifecycleOwner, R.layout.options_chain_item_row_call, fragment.requireContext())
        callAdapter.optionClickListener = optionClickListener
        val putAdapter = OptionsListAdapter(fragment.viewLifecycleOwner, R.layout.options_chain_item_row_put, fragment.requireContext())
        putAdapter.optionClickListener = optionClickListener
        optionTableView?.setAdapters(callAdapter, putAdapter)
    }

    private fun setupViewProperties() {
        optionTableView = binding?.optionsChainTable
        datePicker = binding?.optionsChainDatePicker
        optionsChainSwipeRefreshView = binding?.optionsChainSwipeRefreshView
    }

    private fun setupSwipeRefresh() {
        optionsChainSwipeRefreshView?.apply {
            applyColorScheme()
            setOnRefreshListener {
                swipeRefreshListener?.invoke()
                refresh()
            }
        }
    }

    private fun setupDatePicker() {
        datePicker?.apply {
            selector = BottomSheetSelector(fragment.childFragmentManager, resources)
            selectorTitle = fragment.getString(R.string.options_chain_expiration_date_selector)
            onSelectedItemPositionChangeListener = object : HorizontalListPicker.OnSelectedItemPositionChangeListener {
                override fun onNewSelectedItemPosition(position: Int) {
                    optionChainTableViewModel.updateSelectedExpirationDatePosition(position)
                    refresh()
                }
            }
        }
    }

    fun onCreate(savedInstanceState: Bundle?, refreshListener: (() -> Unit)?) {
        actionSheetViewDelegate.onCreate(savedInstanceState)
        swipeRefreshListener = refreshListener
    }

    @Composable
    fun OptionsChainCompose() {
        AndroidViewBinding(OptionsChainFragmentBinding::inflate) {
            if (binding != this) {
                binding = this
                lifecycleOwner = fragment.viewLifecycleOwner
                obj = quoteWidgetViewModel
                fragment.lifecycle.addObserver(quoteWidgetViewModel)

                disclosureButton.setOnClickListener(
                    OnViewDebounceClickListener(
                        {
                            fragment.findNavController().navigate(OptionsChainFragmentDirections.actionToOptionsChainDialog())
                        }
                    )
                )

                setupViewProperties()
                setUpViewAdapters()
                setUpViewModels()
                setupSwipeRefresh()
                setupDatePicker()
            }
        }
    }

    override fun onVisibilityChange(isVisible: Boolean) {
        if (isVisible) {
            loadPage()
            resumeStreaming()
            animateAndUpdateVisiblePositions()
        } else {
            conditionalOrderDisclosureViewDelegate.currentSnackBar?.dismiss()
            stopStreaming()
        }
    }
}
