package com.etrade.mobilepro.trade.api.mutualfunds

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.trade.api.OrderLeg

interface MutualFundsTradeLeg : OrderLeg {
    val legPositionType: String?
    val instrumentType: InstrumentType
}
