package com.etrade.mobilepro.chat.presentation.common

object ChatConstants {

    const val CHAT_EXTRA_OPEN_MESSAGES = "CHAT_EXTRA_OPEN_MESSAGES"
    const val IS_CHAT_LAUNCHED_FROM_MESSAGES_CENTER = "isChatLaunchedFromMessagesCenter"
    const val CHAT_START_POINT_MESSAGE = "message"

    internal const val CHAT_DEFAULT_DIALOG_TAG = "CHAT_DEFAULT_DIALOG_TAG"
    internal const val CHAT_REQUEST_CODE_DISCONNECT_DIALOG = 16
    internal const val CHAT_REQUEST_CODE_TERMINATE_DIALOG = 17
    internal const val CHAT_WINDOW_DRAW_REQUEST_CODE = 1234
    internal const val CHAT_SESSION_DATA = "chatSessionData"
    internal const val CHAT_PARAMS_REQUEST_BODY = "CHAT_PARAMS_REQUEST_BODY"
    internal const val CHAT_TERMINATE_TAG = "CHAT_TERMINATE_TAG"
    internal const val CHAT_NOTIFICATION_ID = 1023
    internal const val CHAT_SESSION_ID = "CHAT_SESSION_ID"
    internal const val CHAT_ACTIVITY_CLASS = "CHAT_ACTIVITY_CLASS"
    internal const val ACTION_START_MESSAGES_SERVICE = "ACTION_START_MESSAGES_SERVICE"
    internal const val ACTION_STOP_MESSAGES_SERVICE = "ACTION_STOP_MESSAGES_SERVICE"
    internal const val ACTION_CHAT_TERMINATE_BY_USER = "ACTION_CHAT_TERMINATE_BY_USER"
    internal const val FLOATING_BUTTON_WINDOW_MARGIN_X = 50
    internal const val FLOATING_BUTTON_WINDOW_MARGIN_Y = 150
    internal const val FLOATING_BUTTON_MAX_MESSAGES_NUMBER = 9
}
