package com.etrade.mobilepro.util.json

import com.squareup.moshi.Moshi
import java.util.Locale

private val booleanTrues = arrayOf("true", "y", "1", "enabled", "yes")

fun Moshi.toJsonString(map: Map<String, Any?>): String = adapter(Map::class.java).toJson(map)

fun coerceToBool(input: CharSequence?, defaultValue: Boolean = false): Boolean {
    val candidate = input?.toString()?.trim()?.lowercase(Locale.ROOT) ?: return defaultValue
    return booleanTrues.contains(candidate)
}

fun boolToInt(b: Boolean?): Int = if (b == true) {
    1
} else {
    0
}

fun boolToIntOrNull(b: Boolean?): Int? = if (b == true) {
    1
} else {
    null
}
