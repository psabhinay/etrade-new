package com.etrade.mobilepro.compose.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Snackbar
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp

@Composable
fun EtRetryActionButton(action: () -> Unit) {
    TextButton(onClick = action) { Text(text = stringResource(R.string.retry), color = colorResource(R.color.snack_bar_primary)) }
}

@Composable
fun EtRetrySnackbar(
    text: String = stringResource(R.string.error_message_general),
    action: (() -> Unit)? = null
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Bottom
    ) {
        Snackbar(
            modifier = Modifier.padding(8.dp),
            action = { action?.let { EtRetryActionButton(it) } },
            backgroundColor = colorResource(R.color.snack_bar_background),
            contentColor = colorResource(R.color.snack_bar_primary_inverse)
        ) {
            Text(text)
        }
    }
}
