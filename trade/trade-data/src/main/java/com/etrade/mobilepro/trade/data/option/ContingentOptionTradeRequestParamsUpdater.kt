package com.etrade.mobilepro.trade.data.option

import com.etrade.mobilepro.trade.data.TradeRequestParamsUpdater
import com.etrade.mobilepro.trade.data.form.input.DefaultContingentOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultContingentOrderInputId
import com.etrade.mobilepro.trade.data.util.removeParams

class ContingentOptionTradeRequestParamsUpdater : TradeRequestParamsUpdater {
    private val redundantPreviewParams: Set<String> = setOf(
        DefaultContingentOptionTradeFormInputId.securityType,
        DefaultContingentOptionTradeFormInputId.tradeLegs,
        DefaultContingentOptionTradeFormInputId.lookupSymbol,
        DefaultContingentOptionTradeFormInputId.strategy
    )
    private val redundantPlaceParams: Set<String> = redundantPreviewParams + setOf(
        DefaultContingentOptionTradeFormInputId.priceType,
        DefaultContingentOptionTradeFormInputId.action,
        DefaultContingentOrderInputId.symbol
    )

    override fun updatePreviewOrderParams(map: MutableMap<String, Any?>) {
        map.removeParams(redundantPreviewParams)
    }

    override fun updatePlaceOrderParams(map: MutableMap<String, Any?>) {
        map.removeParams(redundantPlaceParams)
    }
}
