package com.etrade.mobilepro.billpay.presentation.paymentpreview

import androidx.fragment.app.Fragment
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.presentation.R
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.formatDash
import com.etrade.mobilepro.util.formatters.createAmericanSimpleDateFormatter
import com.etrade.mobilepro.util.formatters.formatAccountName
import com.etrade.mobilepro.util.formatters.formatPayeeName
import org.threeten.bp.LocalDate
import java.math.BigDecimal

internal fun Fragment.getConfirmationNumberSection(confirmationNumber: String) = createSummarySection(
    label = getString(R.string.bill_pay_confirmation_number),
    value = confirmationNumber,
    valueDescription = confirmationNumber.characterByCharacter().formatDash()
)

internal fun Fragment.getDeliveryDateSection(deliveryDate: String) = createSummarySection(
    getString(R.string.bill_pay_payment_details_delivery_date), deliveryDate
)

internal fun Fragment.getScheduleDateSection(date: LocalDate) = createSummarySection(
    getString(R.string.schedule_date),
    createAmericanSimpleDateFormatter().format(date)
)

internal fun Fragment.getDestinationAccountSection(destinationAccount: Payee): TableSectionItemLayout {
    val key = getString(R.string.bill_pay_payment_details_pay_to)
    val value = requireNotNull(
        formatPayeeName(
            payeeName = destinationAccount.name,
            accountNumber = destinationAccount.accountNumber,
            payeeNickname = destinationAccount.nickName
        )
    )

    return createSummarySection(
        label = key,
        value = value,
        valueDescription = resources.accountNameContentDescription(value) ?: ""
    )
}

internal fun Fragment.getOriginAccountSection(originAccount: AccountDetails): TableSectionItemLayout {
    val key = getString(R.string.bill_pay_payment_details_pay_from)
    val value = formatAccountName(originAccount.accountName, originAccount.accountId)

    return createSummarySection(
        label = key,
        value = value,
        valueDescription = resources.accountNameContentDescription(value) ?: ""
    )
}

internal fun Fragment.getAvailableCashSection(originAccount: AccountDetails) = createSummarySection(
    getString(R.string.bill_pay_available_cash),
    formatCurrency(originAccount.brokerage?.cashAvailableForWithdrawal)
)

internal fun Fragment.getAmountSection(amount: BigDecimal) = createSummarySection(
    getString(R.string.amount),
    formatCurrency(amount)
)

internal fun Fragment.getMemoSection(memo: String?) = createSummarySection(
    getString(R.string.bill_pay_payment_details_memo),
    memo ?: getString(R.string.empty_default)
)

internal fun formatCurrency(value: BigDecimal?): String {
    return value?.let {
        MarketDataFormatter.formatCurrencyValue(it)
    } ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE
}
