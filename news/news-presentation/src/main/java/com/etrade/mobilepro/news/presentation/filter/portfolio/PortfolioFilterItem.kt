package com.etrade.mobilepro.news.presentation.filter.portfolio

data class PortfolioFilterItem(
    val title: String = "",
    val filterResourceId: String = "",
    val isBaseAllPortfoliosItem: Boolean = false,
    val isStockPlan: Boolean = false,
    val accountId: String
) {
    override fun toString(): String = title
}
