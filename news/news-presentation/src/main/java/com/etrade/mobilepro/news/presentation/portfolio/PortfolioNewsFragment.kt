package com.etrade.mobilepro.news.presentation.portfolio

import android.os.Bundle
import android.view.View
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.news.api.filter.FilterDataSource
import com.etrade.mobilepro.news.presentation.NewsSharedViewModel
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.news.presentation.adapter.NewsAdapter
import com.etrade.mobilepro.news.presentation.base.BaseFilterableNewsFragment
import com.etrade.mobilepro.news.presentation.filter.portfolio.ALL_PORTFOLIOS_RESOURCE_ID
import com.etrade.mobilepro.news.presentation.filter.portfolio.PortfolioFilterDataSource
import com.etrade.mobilepro.news.presentation.filter.portfolio.PortfolioFilterItem
import kotlinx.coroutines.ExperimentalCoroutinesApi

private const val HIDE_FILTER = "hide_filter_view"

@RequireLogin
class PortfolioNewsFragment : BaseFilterableNewsFragment<PortfolioFilterItem, PortfolioFilterDataSource, PortfolioNewsViewModel>() {

    override val viewModelClass: Class<PortfolioNewsViewModel> = PortfolioNewsViewModel::class.java

    override val filterDropDownTitle: Int = R.string.filter

    override val dividerPosition: Int = 0

    private var initialAccountId: String? = null

    private var hideFilter: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val (accountId, hideFilter) = router.portfolioNewsArgs(it)
            initialAccountId = accountId ?: ALL_PORTFOLIOS_RESOURCE_ID
            this.hideFilter = hideFilter
        } ?: run {
            hideFilter = savedInstanceState?.getBoolean(HIDE_FILTER) ?: false
        }
    }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (hideFilter) {
            hideFilterView()
        }
    }

    override fun bindViewModels(viewModel: PortfolioNewsViewModel, sharedViewModel: NewsSharedViewModel, adapter: NewsAdapter) {
        super.bindViewModels(viewModel, sharedViewModel, adapter)

        with(sharedViewModel.portfolioFilterDataSource) {
            initialAccountId?.also { findAndSelectItemByAccountId(it) }
        }
    }

    @ExperimentalCoroutinesApi
    override fun applyFilter(selectedItem: PortfolioFilterItem?) {
        if (selectedItem != null) {
            viewModel.applyFilter(selectedItem)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(HIDE_FILTER, hideFilter)
    }

    override fun getFilterDataSource(sharedViewModel: NewsSharedViewModel): FilterDataSource<PortfolioFilterItem> = sharedViewModel.portfolioFilterDataSource

    override fun getSelectedItemTitle(selectedItem: PortfolioFilterItem?): CharSequence = selectedItem?.title.orEmpty()
}
