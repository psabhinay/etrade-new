package com.etrade.mobilepro.inputvalidator.amount

import android.text.InputFilter
import com.etrade.mobilepro.inputvalueview.InputFieldManager

private const val PERCENT_FIELD_LENGTH_FOCUSED = 10
private const val PERCENT_FIELD_LENGTH_NOT_FOCUSED = 11

class AmountCustomizer : InputFieldManager.Customizer {
    override fun updateInputFilters(event: InputFieldManager.Event): List<InputFilter> =
        when (event) {
            InputFieldManager.Event.ON_INIT -> emptyList()
            InputFieldManager.Event.ON_FOCUS_GAINED -> listOf(InputFilter.LengthFilter(PERCENT_FIELD_LENGTH_FOCUSED))
            InputFieldManager.Event.ON_FOCUS_LOST -> listOf(InputFilter.LengthFilter(PERCENT_FIELD_LENGTH_NOT_FOCUSED))
        }
}
