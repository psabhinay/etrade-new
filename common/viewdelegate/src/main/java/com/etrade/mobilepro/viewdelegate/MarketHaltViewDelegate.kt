package com.etrade.mobilepro.viewdelegate

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner

interface MarketHaltViewDelegate {
    fun observe(lifecycleOwner: LifecycleOwner, activity: FragmentActivity)
}
